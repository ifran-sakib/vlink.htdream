﻿namespace PMS_HT.ViewModels
{
    public class CommonRoomDataViewModel
    {
        public decimal VatPercent { get; set; }
        public decimal ScPercent { get; set; }
        public decimal Vat { get; set; }
        public decimal SC{ get; set; }
        public decimal PerDayCharge { get; set; }        
    }
}