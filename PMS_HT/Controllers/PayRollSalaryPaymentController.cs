﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class PayRollSalaryPaymentController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            Session["EmployeeId"] = id;
            var tbl_salary_transactions = db.tbl_salary_transactions.Where(a=> a.Tran_Ref_Name.Equals ("SalaryPayment") && a.Emp_ID == id).Include(t => t.tbl_earning_head).Include(t => t.tbl_emp_info);
            ViewBag.EmpId = id;
            return View(tbl_salary_transactions.ToList());
        }

        public PartialViewResult PayableByMonthPartial(string month, string year)
        {
            int id = (int)Session["EmployeeId"];
            var Filter = db.tbl_salary_transactions.Where(t => t.Emp_ID == id && t.Month_Name.Equals(month) && t.Year_Name.Equals(year));
            ViewBag.payable = ((from z in Filter select z.Debit).Sum() ?? 0) - ((from z in Filter select z.Credit).Sum() ?? 0);
            return PartialView();
        }

        public ActionResult Add(int? id)
        {
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Emp_ID = id;
            return PartialView();
        }
        [HttpPost]
        public ActionResult Add(tbl_salary_transactions salaryPayment)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            if (ModelState.IsValid)
            {
                salaryPayment.Emp_ID = parameter;
                salaryPayment.Tran_Ref_Name = "SalaryPayment";
                db.tbl_salary_transactions.Add(salaryPayment);
                db.SaveChanges();
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salaryPayment.Emp_ID);
            return RedirectToAction("Index", new { id = parameter });
        }

        public ActionResult Edit(int id)
        {
            tbl_salary_transactions salaryPayment = db.tbl_salary_transactions.Find(id);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salaryPayment.Emp_ID);
            return PartialView(salaryPayment);
        }

        [HttpPost]
        public ActionResult Edit(tbl_salary_transactions salaryPayment, int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            if (ModelState.IsValid)
            {
                salaryPayment.Emp_ID = parameter;
                salaryPayment.Tran_Ref_Name = "SalaryPayment";
                db.Entry(salaryPayment).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salaryPayment.Emp_ID);
            return RedirectToAction("Index", new { id = parameter });
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            db.tbl_salary_transactions.Remove(db.tbl_salary_transactions.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
