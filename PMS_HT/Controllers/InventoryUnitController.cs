﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventoryUnitController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_unit.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_unit unit)
        {
            if (ModelState.IsValid)
            {
                db.tbl_unit.Add(unit);
                db.SaveChanges();
            }
            return RedirectToAction("Units","InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_unit.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_unit unit, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unit).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Units", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_unit.Remove(db.tbl_unit.Find(id));
            db.SaveChanges();
            return RedirectToAction("Units", "InventorySettings");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
