﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class BankBranchInfoController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_branch_info.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_branch_info tbl_branch_info = db.tbl_branch_info.Find(id);
            if (tbl_branch_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_branch_info);
        }
        
        public ActionResult Add()
        {
           
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "Branch_ID,Branch_Name,Address")] tbl_branch_info tbl_branch_info)
        {
            if (ModelState.IsValid)
            {
                db.tbl_branch_info.Add(tbl_branch_info);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_branch_info);
        }

        public ActionResult Edit(int? id)
        {
            tbl_branch_info branchInfo = db.tbl_branch_info.Find(id);
  
            return PartialView(branchInfo);
        }

        [HttpPost]
        public ActionResult Edit(tbl_branch_info branchInfo, int? id)
        {
            if (ModelState.IsValid)
            {
            
                db.Entry(branchInfo).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_branch_info.Remove(db.tbl_branch_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
