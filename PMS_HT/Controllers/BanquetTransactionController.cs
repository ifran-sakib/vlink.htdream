﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetTransactionController : CustomControllerBase
    {
        public ActionResult Index(int? id, int? id1)
        {
            var tbl_banquet_transactions = db.tbl_banquet_transactions.Where(a => a.Program_ID == id && a.Tran_Ref_Name.Equals("payment") && a.Banq_Booking_ID == id1).Include(t => t.tbl_banquet_prog);
            var banquetReceivable = (from tran in db.tbl_banquet_transactions where tran.Banq_Booking_ID == id1
                                     group tran by new { tran.Banq_Booking_ID } into g
                                     select (g.Sum(x => x.Debit) ?? 0) - (g.Sum(x => x.Credit) ?? 0)).FirstOrDefault();
            ViewBag.r = banquetReceivable;
            ViewBag.id = id1;
            ViewBag.Program_ID = id;
            Session["Program_ID"] = id;
            Session["Banq_Booking_ID"] = id1;

            return View(tbl_banquet_transactions.ToList());
        }

        public ActionResult Add(int? id, int? id2)
        {
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name");
            ViewBag.Program_ID = id;
            ViewBag.id2 = id2;
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_banquet_transactions transaction)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);

            int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
            if (ModelState.IsValid)
            {
                transaction.Program_ID = parameter;
                db.tbl_banquet_transactions.Add(transaction);
                transaction.Tran_Ref_Name = "payment";
                db.SaveChanges();
            }
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name", transaction.Program_ID);

            return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
        }

        public ActionResult Edit(int? id)
        {
            tbl_banquet_transactions transaction = db.tbl_banquet_transactions.Find(id);
            return PartialView(transaction);
        }

        [HttpPost]
        public ActionResult Edit(tbl_banquet_transactions transaction)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);

            int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);

            int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
            db.tbl_banquet_transactions.Remove(db.tbl_banquet_transactions.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
