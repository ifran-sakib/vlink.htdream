﻿using System;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace PMS_HT.Controllers
{
    public class AccountsDailyVacantRoomController : CustomControllerBase
    {
        public ActionResult Index(string dt)
        {
            var query = "";
            if (string.IsNullOrEmpty(dt))
            {
                query = "SELECT tbl_room_info.Room_No ,round(tbl_room_info.Rate*.15+tbl_room_info.Rate,-1) as Rate, tbl_vacant_room.Date FROM tbl_vacant_room INNER JOIN  tbl_room_info ON tbl_vacant_room.Room_ID = tbl_room_info.Room_ID where tbl_vacant_room.Date in (select max(tbl_vacant_room.Date) from tbl_vacant_room)";
            }
            else
            {
                query = "SELECT tbl_room_info.Room_No ,round(tbl_room_info.Rate*.15+tbl_room_info.Rate,-1) as Rate, tbl_vacant_room.Date FROM tbl_vacant_room INNER JOIN  tbl_room_info ON tbl_vacant_room.Room_ID = tbl_room_info.Room_ID where tbl_vacant_room.Date = '" + dt + "'";
            }
            var data = db.Database.SqlQuery<VacantRoomViewModel>(query);
            ViewBag.vacantroom = data;
            return View();
        }

        [HttpGet]
        public ActionResult DailyVacantRoom()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DailyVacantRoom(DateTime date)
        {
            bool hasData = (from vac in db.tbl_vacant_room where vac.Date == date select vac.Date).Any();
            if (hasData)
            {
                ViewBag.Message = "Vacant Room already calculated for selected date";
                var dt = date.ToString("yyyy-MM-dd");
                return RedirectToAction("Index", "AccountsDailyVacantRoom", new { dt = dt });
            }
            ViewBag.Message = "No data for this date";
            return View();
        }

        public JsonResult ExportReport(string dob)
        {
            DateTime dob1 = DateTime.ParseExact(dob, "dd/MM/yyyy", null);
            decimal vatVal = (decimal)1.15;
            var tbl_vacant_room = (from exp in db.tbl_vacant_room
                                   where exp.Date == dob1
                                   select new
                                   {
                                       Room_No = exp.tbl_room_info.Room_No,
                                       Rate = (Math.Round((exp.Rate * vatVal)/10))*10,
                                       Date = exp.Date
                                   }).ToList();


            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "VacantRoomReport.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }

            ReportDataSource DataSetVacantRoom = new ReportDataSource("DataSetVacantRoom", tbl_vacant_room);            
            lc.DataSources.Add(DataSetVacantRoom);

            int j = 0;
            ReportParameter[] param = new ReportParameter[5];
            param[j++] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[j++] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[j++] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[j++] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);
            param[j++] = new ReportParameter("Date", dob1.ToString("dd MMM yyyy"), false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }
    }
}