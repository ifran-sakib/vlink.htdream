﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetAmenitiesCostController : CustomControllerBase
    {
        public ActionResult Index( int? id , int? id1)
        {
            var tbl_banquet_transactions = db.tbl_banquet_transactions.Where(a=> a.Program_ID == id && a.Tran_Ref_Name.Equals("amenites") && a.Banq_Booking_ID == id1).Include(t => t.tbl_banquet_amenities).Include(t => t.tbl_banquet_booking).Include(t => t.tbl_banquet_prog);
            ViewBag.id = id1;
            ViewBag.Program_ID = id;
            Session["Program_ID"] = id;
            Session["Banq_Booking_ID"] = id1;
            return View(tbl_banquet_transactions.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_banquet_transactions tbl_banquet_transactions = db.tbl_banquet_transactions.Find(id);
            if (tbl_banquet_transactions == null)
            {
                return HttpNotFound();
            }
            return View(tbl_banquet_transactions);
        }

        public ActionResult Add(int? id , int? id2)
        {
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_banquet_amenities, "Banquet_Amenities_ID", "Amenities_Heading");
            ViewBag.Banq_Booking_ID = new SelectList(db.tbl_banquet_booking, "Banq_Booking_ID", "Banq_Booking_ID");
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name");
            ViewBag.Program_ID = id;
            ViewBag.id2 = id2;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "B_Tran_ID,Program_ID,Tran_Ref_ID,Banq_Booking_ID,Tran_Ref_Name,Particulars,Debit,Credit,Tran_Date")] tbl_banquet_transactions tbl_banquet_transactions)
        {
            if (ModelState.IsValid)
            {
                int parameter = Convert.ToInt32(Session["Program_ID"]);

                int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
                tbl_banquet_transactions.Tran_Ref_Name = "amenites";
                db.tbl_banquet_transactions.Add(tbl_banquet_transactions);

                db.SaveChanges();
                return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
            }

            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_banquet_amenities, "Banquet_Amenities_ID", "Amenities_Heading", tbl_banquet_transactions.Tran_Ref_ID);
            ViewBag.Banq_Booking_ID = new SelectList(db.tbl_banquet_booking, "Banq_Booking_ID", "Banq_Booking_ID", tbl_banquet_transactions.Banq_Booking_ID);
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name", tbl_banquet_transactions.Program_ID);
            return View(tbl_banquet_transactions);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_banquet_transactions tbl_banquet_transactions = db.tbl_banquet_transactions.Find(id);
            if (tbl_banquet_transactions == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_banquet_amenities, "Banquet_Amenities_ID", "Amenities_Heading", tbl_banquet_transactions.Tran_Ref_ID);
            ViewBag.Banq_Booking_ID = new SelectList(db.tbl_banquet_booking, "Banq_Booking_ID", "Banq_Booking_ID", tbl_banquet_transactions.Banq_Booking_ID);
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name", tbl_banquet_transactions.Program_ID);
            return PartialView(tbl_banquet_transactions);
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "B_Tran_ID,Program_ID,Tran_Ref_ID,Banq_Booking_ID,Tran_Ref_Name,Particulars,Debit,Credit,Tran_Date")] tbl_banquet_transactions tbl_banquet_transactions)
        {
            if (ModelState.IsValid)
            {
                int parameter = Convert.ToInt32(Session["Program_ID"]);

                int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
                db.Entry(tbl_banquet_transactions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_banquet_amenities, "Banquet_Amenities_ID", "Amenities_Heading", tbl_banquet_transactions.Tran_Ref_ID);
            ViewBag.Banq_Booking_ID = new SelectList(db.tbl_banquet_booking, "Banq_Booking_ID", "Banq_Booking_ID", tbl_banquet_transactions.Banq_Booking_ID);
            ViewBag.Program_ID = new SelectList(db.tbl_banquet_prog, "Program_ID", "Person_Name", tbl_banquet_transactions.Program_ID);
            return View(tbl_banquet_transactions);
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);

            int parameter2 = Convert.ToInt32(Session["Banq_Booking_ID"]);
            db.tbl_banquet_transactions.Remove(db.tbl_banquet_transactions.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter, id1 = parameter2 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
