﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    public class RestaurantDashboardController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();
        // GET: RestaurantDashboard
        public ActionResult Index()
        {
            ViewBag.AllOrdersCount = db.tbl_item_order_master.Where(a => DbFunctions.TruncateTime(a.Order_Date) == DateTime.Today.Date && (a.Status == "Ordered" || a.Status == "Served")).Count();
            ViewBag.CancelOrdersCount = db.tbl_item_order_master.Where(a => DbFunctions.TruncateTime(a.Order_Date) == DateTime.Today.Date && a.Status == "Cancelled").Count();
            ViewBag.PaidCount = db.tbl_item_order_master.Where(a => DbFunctions.TruncateTime(a.Order_Date) == DateTime.Today.Date && a.Status == "Paid").Count();


            ViewBag.Tables = db.tbl_dining_set_up.ToList();
            var current_orders = db.tbl_item_order_master.Where(a => (a.Status != "clear" && a.Status != "Paid" && a.Status != "Cancelled")).ToList();
            return View(current_orders);
        }

        public ActionResult Report()
        {
            ViewBag.AllOrders = db.tbl_item_order_master.Where(a => DbFunctions.TruncateTime(a.Order_Date) == DateTime.Today.Date && a.Status != "Cancelled").ToList();
            ViewBag.CancelOrders = db.tbl_item_order_master.Where(a => DbFunctions.TruncateTime(a.Order_Date) == DateTime.Today.Date && a.Status == "Cancelled").ToList();
            return View();
        }
    }
}