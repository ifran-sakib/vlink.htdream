﻿using PMS_HT.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskReserveBookingController : CustomControllerBase
    {
        FrontDeskGuestStayController GStay = new FrontDeskGuestStayController();
        public ActionResult Index()
        {
            var reserveBookingList = db.tbl_guest_stay_info.Where(a => a.Arrival_Date >= DateTime.Today.Date && (a.Status == "Reserved" || a.Status == "Booked")).OrderBy(b => b.Arrival_Date).ToList();
            return View(reserveBookingList);
        }

        [HttpGet]
        public ActionResult CancelReservation(int id)
        {
            var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id && a.Status == "Reserved");
            if (guest1 != null)
            {
                var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == guest1.Room_ID);
                if (rid != null)
                {
                    ViewBag.RoomNo = rid.Room_No;
                    ViewBag.id = id;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDeskReserveBooking");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelReservation(int stayId, int? pm)
        {
            if (ModelState.IsValid)
            {
                var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayId && a.Status == "Reserved");
                if (guest1 != null)
                {
                    var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                    if (tblEmpInfo != null)
                    {
                        int userId = tblEmpInfo.Emp_ID;
                        tbl_guest_attendee attendee = new tbl_guest_attendee();
                        attendee.Emp_ID = userId;
                        attendee.Purpose = "reserve_cancel";
                        attendee.Changed_Date = DateTime.Now;
                        attendee.Stay_Info_ID = guest1.Stay_Info_ID;
                        db.tbl_guest_attendee.Add(attendee);

                        guest1.Status = "Clear";
                        guest1.Check_Out_Note = "Reserved_Cancel";
                        db.tbl_guest_stay_info.Attach(guest1);
                        var updatedGuestInfo = db.Entry(guest1);
                        updatedGuestInfo.Property(a => a.Status).IsModified = true;
                        updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction("Index", "FrontDeskReserveBooking");
        }

        [HttpGet]
        public ActionResult CancelBooking(int id)
        {
            var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id && a.Status == "Booked");
            decimal roomCharge = 0, vat = 0, previousPaid = 0, discount = 0;

            if (guest1 != null)
            {
                var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == guest1.Room_ID);
                if (rid != null)
                {
                    ViewBag.RoomNo = rid.Room_No;
                    ViewBag.rate = rid.Rate;

                    ViewBag.BookedGuest = guest1;
                    Session["BookedGuestCancel"] = guest1;

                    DateTime bookingDateTime = guest1.Booking_Date.Value;
                    ViewBag.Booking_Date = bookingDateTime;

                    int noOfNight = GStay.StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.noOfNight = noOfNight;

                    var roomCharges = db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16)).ToList();
                    if (roomCharges != null && roomCharges.Count > 0)
                    {
                        foreach (var item in roomCharges)
                        {
                            roomCharge += (decimal)item.Debit;
                        }
                        ViewBag.RoomChargePerDay = roomCharges.FirstOrDefault().Debit;
                        ViewBag.RoomCharge = roomCharge;
                    }

                    var vats = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 2))).ToList();
                    if (vats != null && vats.Count > 0)
                    {
                        foreach (var item in vats)
                        {
                            vat += (decimal)item.Debit;
                        }
                        ViewBag.Vat = vat;
                        ViewBag.VatPercent = vats.FirstOrDefault().Debit;
                    }
                    //ViewBag.Vat = 0;
                    //ViewBag.VatPercent = 0;

                    var paidTransactions = db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 14)).ToList();
                    if (paidTransactions != null && paidTransactions.Count > 0)
                    {
                        foreach (var item in paidTransactions)
                        {
                            previousPaid += (decimal)item.Credit;
                        }
                        ViewBag.Paid = previousPaid;
                    }

                    var discountTransactions = db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1)).ToList();
                    if (discountTransactions != null && discountTransactions.Count > 0)
                    {
                        foreach (var item in discountTransactions)
                        {
                            discount += (decimal)item.Credit;
                        }
                        ViewBag.Discount = discount;
                    }
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDeskReserveBooking");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelBooking(decimal? refund, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    tbl_room_transactions refundTransaction = new tbl_room_transactions();
                    tbl_guest_attendee attendee = new tbl_guest_attendee();
                    attendee.Emp_ID = userId;
                    attendee.Changed_Date = DateTime.Now;
                    attendee.Purpose = "book_cancel";

                    var guest1 = (tbl_guest_stay_info)Session["BookedGuestCancel"];

                    if (refund != null && refund != 0)
                    {
                        refundTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                        refundTransaction.Room_ID = guest1.Room_ID;
                        refundTransaction.Guest_ID = guest1.Guest_ID;
                        refundTransaction.Debit = 0;
                        refundTransaction.Credit = refund;
                        refundTransaction.Tran_Date = DateTime.Now;
                        refundTransaction.Tran_Ref_ID = 13;
                        refundTransaction.Tran_Ref_Name = "cancellation_refund";
                        refundTransaction.Particulars = particulars;
                        refundTransaction.Emp_ID = userId;
                        db.tbl_room_transactions.Add(refundTransaction);
                    }
                    attendee.Stay_Info_ID = guest1.Stay_Info_ID;
                    guest1.Status = "Clear";
                    guest1.Check_Out_Note = "Booking_Cancel";
                    db.tbl_guest_stay_info.Attach(guest1);

                    var updatedGuestInfo = db.Entry(guest1);
                    updatedGuestInfo.Property(a => a.Status).IsModified = true;
                    updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;

                    db.tbl_guest_attendee.Add(attendee);
                }
                db.SaveChanges();
            }
            Session.Remove("BookedGuestCancel");
            return RedirectToAction("Index", "FrontDeskReserveBooking");
        }
    }
}