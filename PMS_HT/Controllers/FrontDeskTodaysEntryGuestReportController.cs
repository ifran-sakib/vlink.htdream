﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.IO;

namespace PMS_HT.Controllers
{
    public class FrontDeskTodaysEntryGuestReportController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var today = DateTime.Today.Date;
            var todayEntryCustomerInfo = db.tbl_guest_stay_info.Where(( a => ((a.Status != "Reserved" && a.Status != "Booked") || a.Check_Out_Note == "Temp_Stay") && (DbFunctions.TruncateTime(a.Arrival_Date) == today))).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.TodayEntryCustomerInfo = todayEntryCustomerInfo.ToList();
            return View();
        }

        //public ActionResult ExportReport()
        //{
        //    var today = DateTime.Today.Date;
        //    var todayEntryCustomerInfo = from stay in db.tbl_guest_stay_info
        //                                     //join guest in db.tbl_guest_info on stay.Guest_ID equals guest.Guest_ID
        //                                     //join room in db.tbl_room_info on stay.Room_ID equals room.Room_ID
        //                                 where ((stay.Status != "Reserved" && stay.Status != "Booked") || (stay.Check_Out_Note == "Temp_Stay")) && (DbFunctions.TruncateTime(stay.Arrival_Date) == today)
        //                                 select new
        //                                 {
        //                                     Room_No = stay.tbl_room_info.Room_No,
        //                                     Name = stay.tbl_guest_info.Name,
        //                                     Phone = stay.tbl_guest_info.Phone,
        //                                     Address = stay.tbl_guest_info.Address,
        //                                     Arrival_Date = (stay.Arrival_Date),
        //                                     Check_Out_Time = (stay.Check_Out_Time).ToString()
        //                                 };
        //    ReportDocument rd = new ReportDocument();

        //    rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), "TodaysGuestInfo.rpt"));
        //    rd.SetDataSource(todayEntryCustomerInfo);

        //    Response.Buffer = false;
        //    Response.ClearContent();
        //    Response.ClearHeaders();

        //    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    return File(stream, "application/pdf", "TodaysGuestInfo.pdf");
        //}
    }
}
