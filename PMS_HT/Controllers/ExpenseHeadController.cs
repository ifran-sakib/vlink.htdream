﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class ExpenseHeadController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_expense_head.ToList());
        }

        public ActionResult Add()
        {            
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_expense_head expense)
        {
            if (ModelState.IsValid)
            {
                
                db.tbl_expense_head.Add(expense);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {           
            return PartialView(db.tbl_expense_head.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_expense_head expense, int id)
        {
            if (ModelState.IsValid)
            {
         
                db.Entry(expense).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_expense_head.Remove(db.tbl_expense_head.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
