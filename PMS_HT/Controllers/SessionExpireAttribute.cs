﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PMS_HT.Controllers
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        private PMS_HT_DB db = new PMS_HT_DB();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            // check  sessions here
            if (ctx.Session["UserID"] == null || ctx.Session["UserName"] == null)
            {
                filterContext.Result = new RedirectResult("~/HrEmployee/Login");
                return;
            }

            //vacant room calculation
            if (DateTime.Now.Hour <= 12)
            {
                DateTime dt = DateTime.Today.Date;
                tbl_guest_attendee todayVacantRow = db.tbl_guest_attendee.FirstOrDefault(a => a.Purpose == "Vacant_Calculation" && DbFunctions.TruncateTime(a.Changed_Date) == dt);
                if (todayVacantRow == null)
                {
                    var query = "INSERT INTO [dbo].[tbl_vacant_room] ([Room_ID],[Rate], Date) (SELECT  tbl_room_info.Room_ID , tbl_room_info.Rate , '" + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + "' FROM tbl_room_info where tbl_room_info.Room_ID not in (select tbl_guest_stay_info.Room_ID from tbl_guest_stay_info where tbl_guest_stay_info.Status = 'CheckIn'))";
                    db.Database.ExecuteSqlCommand(query);

                    var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == ctx.User.Identity.Name);
                    if (tblEmpInfo != null)
                    {
                        tbl_guest_attendee attendee = new tbl_guest_attendee();
                        attendee.Emp_ID = tblEmpInfo.Emp_ID;
                        attendee.Purpose = "Vacant_Calculation";
                        attendee.Changed_Date = DateTime.Now;
                        db.tbl_guest_attendee.Add(attendee);
                        db.SaveChanges();
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

    }
}