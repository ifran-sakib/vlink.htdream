﻿using Microsoft.Reporting.WebForms;
using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Restaurant")]
    public class RestaurantOrderController : CustomControllerBase
    {
        public ActionResult Orders()
        {
            ViewBag.Tables = db.tbl_dining_set_up.Where(a => a.Status == "Vacant").ToList();
            ViewBag.Waiters = db.tbl_dining_waiter.ToList();
            ViewBag.Order_Method_ID = new SelectList(db.tbl_order_method, "Order_Method_ID", "Order_Method_Name");
            ViewBag.Food_Side_ID = new SelectList(db.tbl_food_side_extra, "Food_Side_ID", "Food_Side_Name");
            ViewBag.Foods = db.tbl_food.Where(a => a.Availability == true).OrderBy(a => a.tbl_food_category.Serial_No).ToList();
            ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No");
            return View();
        }

        [HttpPost]
        public JsonResult MakeOrder(tbl_item_order_master master, int? Room_ID)
        {
            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                int user_id = tblEmpInfo.Emp_ID;
                var isValidModel = TryUpdateModel(master);
                if (ModelState.IsValid)
                {
                    RestaurantDetail restaurantDetail = new RestaurantDetail();
                    tbl_item_order_ledger ld = new tbl_item_order_ledger();
                    tbl_item_order_master newrow = master;

                    //newrow.tbl_dining_set_up = null;
                    // newrow.tbl_dining_waiter = null;

                    tbl_dining_waiter[] waiter_list = new tbl_dining_waiter[master.tbl_dining_waiter.Count];
                    tbl_dining_set_up[] table_list = new tbl_dining_set_up[master.tbl_dining_set_up.Count];
                    tbl_item_order_details[] item_list = new tbl_item_order_details[master.tbl_item_order_details.Count];
                    tbl_item_order_ledger[] ledger = new tbl_item_order_ledger[1];

                    int i = 0;
                    foreach (var item in master.tbl_dining_waiter)
                    {
                        tbl_dining_waiter temp = db.tbl_dining_waiter.Find(item.Dining_Waiter_ID);
                        waiter_list[i++] = temp;
                    }
                    i = 0;
                    foreach (var item in master.tbl_dining_set_up)
                    {
                        tbl_dining_set_up dine = db.tbl_dining_set_up.Find(item.Dining_ID);
                        tbl_dining_set_up temp = dine;
                        dine.Status = "Ordered";
                        db.Entry(dine).State = EntityState.Modified;
                        table_list[i++] = temp;
                    }
                    i = 0;
                    decimal total = 0, side_total = 0, vat = 0, sc = 0, TotalFood = 0;
                    foreach (var item in master.tbl_item_order_details)
                    {
                        tbl_item_order_details temp = new tbl_item_order_details();
                        temp.Food_ID = item.Food_ID;
                        temp.Quantity = item.Quantity;
                        temp.Price = item.Price;
                        temp.Food_Side_ID = item.Food_Side_ID;
                        temp.Side_Quantity = item.Food_Side_ID == null ? null : item.Side_Quantity;
                        temp.Side_Price = null;
                        if (item.Food_Side_ID != null)
                        {
                            temp.Side_Price = Convert.ToDecimal(db.tbl_food_side_extra.Find(item.Food_Side_ID).Price);
                            side_total = (decimal)(item.Side_Quantity * temp.Side_Price);
                        }
                        total = (decimal)(item.Quantity * item.Price) + side_total;
                        vat = (total * restaurantDetail.vat) / 100;
                        sc = (total * restaurantDetail.sc) / 100;
                        temp.VAT = vat;
                        temp.SC = sc;
                        temp.Total = total + vat + sc;
                        temp.Note = item.Note;
                        temp.Order_Date = DateTime.Now;
                        item_list[i++] = temp;
                        TotalFood += (decimal)temp.Total;
                    }
                    if (master.Order_Method_ID == 3 && Room_ID == null) //roomservice
                    {
                        return Json("Select Room", JsonRequestBehavior.AllowGet);
                    }
                    if (master.Order_Method_ID == 3 && Room_ID != null) //roomservice
                    {
                        tbl_guest_stay_info guest = db.tbl_guest_stay_info.Where(a => a.Room_ID == Room_ID && a.Status == "checkin").FirstOrDefault();
                        if (guest == null)
                        {
                            return Json("No Guest Found!!", JsonRequestBehavior.AllowGet);
                        }
                        master.Stay_Info_ID = guest.Stay_Info_ID;
                    }
                    ld.Tran_Ref_ID = 1;
                    ld.Tran_Ref_Name = "FoodPrice";
                    ld.Particular = "Food Price";
                    ld.Debit = Math.Round(TotalFood);
                    ld.Credit = 0;
                    ld.Tran_Date = DateTime.Now;
                    ld.Note = null;
                    ledger[0] = ld;

                    newrow.tbl_dining_set_up = table_list;
                    newrow.tbl_dining_waiter = waiter_list;
                    newrow.tbl_item_order_details = item_list;
                    newrow.tbl_item_order_ledger = ledger;
                    newrow.Order_Date = DateTime.Now;
                    newrow.Received_By_ID = user_id;
                    db.tbl_item_order_master.Add(newrow);
                    db.SaveChanges();

                    var gStay = db.tbl_item_order_master.ToList().LastOrDefault();
                    var dbitems = db.tbl_item_order_details.Where(a => a.Order_Master_ID == gStay.Order_Master_ID).Include(b => b.tbl_food).ToList();
                    List<OrderedItem> items = new List<OrderedItem>();
                    foreach (var item in dbitems)
                    {
                        OrderedItem ro = new OrderedItem();
                        string desc = "";
                        ro.Food_No = item.tbl_food.Food_No ?? "";
                        desc += item.tbl_food.Food_Name;

                        if (item.tbl_food_side_extra != null)
                        {
                            desc += " - ";
                            desc += item.tbl_food_side_extra.Food_Side_Name;
                        }
                        if (item.Side_Quantity != null)
                        {
                            desc += "-" + (Convert.ToInt32(item.Side_Quantity)).ToString();
                        }
                        if (item.Note != null)
                        {
                            desc += " (" + item.Note + ")";
                        }
                        ro.Food_Desc = desc;
                        ro.Qty = Convert.ToInt32(item.Quantity);

                        items.Add(ro);
                    }

                    string waiterlist = "";
                    string ordertype = "";
                    if (gStay.tbl_dining_waiter.Count() > 0)
                    {
                        foreach (var item in gStay.tbl_dining_waiter)
                        {
                            waiterlist += item.tbl_emp_info.Emp_Name + ", ";
                        }
                        waiterlist = waiterlist.Substring(0, waiterlist.Length - 2);
                    }
                    if (gStay.Order_Method_ID == 3)
                    {
                        ordertype = "Room Service - " + gStay.tbl_guest_stay_info.tbl_room_info.Room_No;
                    }
                    else if (gStay.Order_Method_ID == 2)
                    {
                        ordertype = "Take Away";
                        if (gStay.tbl_dining_set_up.Count() > 0)
                        {
                            ordertype = "Take Away - ";
                            foreach (var item in gStay.tbl_dining_set_up)
                            {
                                ordertype += item.Dining_No + ", ";
                            }
                            ordertype = ordertype.Substring(0, ordertype.Length - 2);
                        }

                    }
                    else if (gStay.tbl_dining_set_up.Count() > 0)
                    {
                        foreach (var item in gStay.tbl_dining_set_up)
                        {
                            ordertype += item.Dining_No + ", ";
                        }
                        ordertype = ordertype.Substring(0, ordertype.Length - 2);
                    }
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    LocalReport lc = new LocalReport();
                    string reportPath = Path.Combine(Server.MapPath("~/Reports"), "KOT.rdlc");
                    if (System.IO.File.Exists(reportPath))
                    {
                        lc.ReportPath = reportPath;
                    }
                    else
                    {
                        return Json("");
                    }

                    ReportDataSource DataSetKot = new ReportDataSource("DataSetKot", items);

                    lc.DataSources.Add(DataSetKot);

                    ReportParameter[] param = new ReportParameter[6];
                    param[0] = new ReportParameter("propertyName", rpd.PropertyName, false);
                    param[1] = new ReportParameter("kotNo", gStay.Order_Master_ID.ToString(), false);
                    param[2] = new ReportParameter("steward", waiterlist, false);
                    param[3] = new ReportParameter("orderType", ordertype, false);
                    param[4] = new ReportParameter("note", gStay.Note ?? "", false);
                    param[5] = new ReportParameter("orderDate", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), false);

                    lc.SetParameters(param);

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    renderedBytes = lc.Render(
                        rpd.ReportType,
                        rpd.DeviceInfo,
                        out rpd.MimeType,
                        out rpd.Encoding,
                        out rpd.FileNameExtension,
                        out streams,
                        out warnings);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    String base64 = Convert.ToBase64String(renderedBytes);
                    return Json(base64, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int? id, int? Table_id)
        {
            if (id == null && Table_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_item_order_master master = new tbl_item_order_master();
            if (id != null)
            {
                master = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id && (a.Status == "Ordered" || a.Status == "Served"));
            }
            if (Table_id != null)
            {
                int tbid = Convert.ToInt32(Table_id);
                master = db.tbl_item_order_master.FirstOrDefault(a => a.tbl_dining_set_up.Select(b => b.Dining_ID).ToList().Contains(tbid) && (a.Status == "Ordered" || a.Status == "Served"));
            }
            if (master == null)
            {
                return HttpNotFound();
            }
            RestaurantDetail restaurantDetail = new RestaurantDetail();
            var vacant_tables = db.tbl_dining_set_up.Where(r => r.Status == "Vacant").ToList();
            var this_tables = master.tbl_dining_set_up.ToList();
            foreach (var item in this_tables)
            {
                vacant_tables.Add(item);
            }
            ViewBag.Tables = vacant_tables.OrderBy(r => r.Dining_ID);
            ViewBag.Waiters = db.tbl_dining_waiter.ToList();
            ViewBag.Order_Method_ID = new SelectList(db.tbl_order_method, "Order_Method_ID", "Order_Method_Name", master.Order_Method_ID);
            ViewBag.Food_Side_ID = new SelectList(db.tbl_food_side_extra, "Food_Side_ID", "Food_Side_Name");
            ViewBag.FSD = db.tbl_food_side_extra.ToList();
            ViewBag.Foods = db.tbl_food.Where(a => a.Availability == true).OrderBy(a => a.tbl_food_category.Serial_No).ToList();
            if (master.Stay_Info_ID == null)
            {
                ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No");
            }
            else
            {
                ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No", master.tbl_guest_stay_info.tbl_room_info.Room_ID);
            }
            ViewBag.Status = new SelectList(new List<SelectListItem>
                                    {
                                        new SelectListItem {Text = "Ordered", Value = "Ordered"},
                                        new SelectListItem {Text = "Served", Value = "Served"},
                                    }, "Value", "Text", master.Status);

            int elapsedTime = (int)(DateTime.Now - master.Order_Date.Value).TotalMinutes;
            if (elapsedTime >= restaurantDetail.Cancelationtime)
            {
                ViewBag.Cancellable = false;
            }
            else
            {
                ViewBag.Cancellable = true;
            }
            return View(master);
        }

        public JsonResult EditOrder(tbl_item_order_master master, int? Room_ID)
        {
            tbl_item_order_master master_db = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == master.Order_Master_ID);
            RestaurantDetail restaurantDetail = new RestaurantDetail();


            var nTables = master.tbl_dining_set_up.ToList().Select(a => a.Dining_ID);
            var nWaiter = master.tbl_dining_waiter.ToList().Select(a => a.Dining_Waiter_ID);
            var nItems = master.tbl_item_order_details.ToList().Select(a => a.Food_ID);
            List<tbl_item_order_details> new_items = master.tbl_item_order_details.ToList();
            if (master_db == null)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            master_db.Total_Person = master.Total_Person;
            master_db.Order_Method_ID = master.Order_Method_ID;
            master_db.Status = master.Status;
            master_db.Note = master.Note;

            decimal total = 0, side_total = 0, vat = 0, sc = 0, TotalFood = 0;
            //items edit begin
            if (master.tbl_item_order_details != null)
            {
                var old_items = master_db.tbl_item_order_details.ToList();
                var dff = master_db.tbl_item_order_details.ToList();


                foreach (var item in dff)
                {
                    total = 0; side_total = 0; vat = 0; sc = 0;
                    if (nItems.Contains(item.Food_ID))
                    {
                        tbl_item_order_details temp = new tbl_item_order_details();
                        temp = master.tbl_item_order_details.FirstOrDefault(a => a.Food_ID == item.Food_ID);

                        item.Food_ID = temp.Food_ID;
                        item.Quantity = temp.Quantity;
                        item.Price = temp.Price;
                        item.Food_Side_ID = temp.Food_Side_ID;
                        item.Side_Quantity = temp.Food_Side_ID == null ? null : temp.Side_Quantity;
                        item.Side_Price = null;
                        if (temp.Food_Side_ID != null)
                        {
                            item.Side_Price = Convert.ToDecimal(db.tbl_food_side_extra.Find(temp.Food_Side_ID).Price);
                            side_total = (decimal)(item.Side_Quantity * item.Side_Price);
                        }
                        total = (decimal)(item.Quantity * item.Price) + side_total;
                        vat = (total * restaurantDetail.vat) / 100;
                        sc = (total * restaurantDetail.sc) / 100;
                        item.VAT = vat;
                        item.SC = sc;
                        item.Total = total + vat + sc;
                        item.Note = temp.Note;
                        //item.Order_Date = DateTime.Now;
                        //item_list[i++] = temp;
                        TotalFood += (decimal)item.Total;
                        new_items.Remove(temp);

                        db.Entry(item).State = EntityState.Modified;
                    }
                    else
                    {
                        //TotalFood -= Convert.ToDecimal(item.Total);
                        db.tbl_item_order_details.Remove(item);
                    }
                }

                foreach (var item in new_items)
                {
                    tbl_item_order_details temp = new tbl_item_order_details();
                    temp.Food_ID = item.Food_ID;
                    temp.Quantity = item.Quantity;
                    temp.Price = item.Price;
                    temp.Food_Side_ID = item.Food_Side_ID;
                    temp.Side_Quantity = item.Food_Side_ID == null ? null : item.Side_Quantity;
                    temp.Side_Price = null;
                    if (item.Food_Side_ID != null)
                    {
                        temp.Side_Price = Convert.ToDecimal(db.tbl_food_side_extra.Find(item.Food_Side_ID).Price);
                        side_total = (decimal)(item.Side_Quantity * temp.Side_Price);
                    }
                    total = (decimal)(item.Quantity * item.Price) + side_total;
                    vat = (total * restaurantDetail.vat) / 100;
                    sc = (total * restaurantDetail.sc) / 100;
                    temp.VAT = vat;
                    temp.SC = sc;
                    temp.Total = total + vat + sc;
                    temp.Note = item.Note;
                    item.Order_Date = DateTime.Now;
                    master_db.tbl_item_order_details.Add(item);
                    //item_list[i++] = temp;
                    master_db.Status = "Ordered";
                    TotalFood += (decimal)temp.Total;
                }
            }
            //item edit end


            //cahnging alloted tables begin
            if (master.tbl_dining_set_up != null)
            {
                var new_tables = db.tbl_dining_set_up.ToList();
                List<tbl_dining_set_up> asd = db.tbl_dining_set_up.ToList();
                foreach (var item in asd)
                {
                    if (!nTables.Contains(item.Dining_ID))
                    {
                        new_tables.Remove(item);
                    }
                }
                foreach (var item in new_tables)
                {
                    if (!master_db.tbl_dining_set_up.Contains(item))
                    {
                        master_db.tbl_dining_set_up.Add(item);
                        tbl_dining_set_up this_table = db.tbl_dining_set_up.Find(item.Dining_ID);
                        this_table.Status = master_db.Status;
                        db.Entry(this_table).State = EntityState.Modified;
                    }
                }


                foreach (var item in master_db.tbl_dining_set_up.ToList())
                {
                    if (!new_tables.Contains(item))
                    {
                        master_db.tbl_dining_set_up.Remove(item);
                        tbl_dining_set_up this_table = db.tbl_dining_set_up.Find(item.Dining_ID);
                        this_table.Status = "Vacant";
                        db.Entry(this_table).State = EntityState.Modified;
                    }
                    else
                    {
                        tbl_dining_set_up this_table = db.tbl_dining_set_up.Find(item.Dining_ID);
                        this_table.Status = master_db.Status;
                        db.Entry(this_table).State = EntityState.Modified;
                    }
                }
            }
            //cahnging alloted tables end


            //cahnging waiters begin
            if (master.tbl_dining_waiter != null)
            {
                var new_waiters = db.tbl_dining_waiter.ToList();
                List<tbl_dining_waiter> asdf = db.tbl_dining_waiter.ToList();
                foreach (var item in asdf)
                {
                    if (!nWaiter.Contains(item.Dining_Waiter_ID))
                    {
                        new_waiters.Remove(item);
                    }
                }
                foreach (var item in new_waiters)
                {
                    if (!master_db.tbl_dining_waiter.Contains(item))
                    {
                        master_db.tbl_dining_waiter.Add(item);
                    }
                }

                foreach (var item in master_db.tbl_dining_waiter.ToList())
                {
                    if (!new_waiters.Contains(item))
                    {
                        master_db.tbl_dining_waiter.Remove(item);
                    }
                }
            }
            //cahnging Waiters end

            //if room service portion begin
            if (master.Order_Method_ID == 3 && Room_ID == null) //roomservice
            {
                return Json("Select Room", JsonRequestBehavior.AllowGet);
            }
            if (master.Order_Method_ID == 3 && Room_ID != null) //roomservice
            {
                tbl_guest_stay_info guest = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == Room_ID && a.Status == "checkin");
                if (guest == null)
                {
                    return Json("No Guest Found!!", JsonRequestBehavior.AllowGet);
                }
                master_db.Stay_Info_ID = guest.Stay_Info_ID;
            }
            //if room service portion end

            //item_order_ledger edit begin
            //var old_ledger = master_db.tbl_item_order_ledger.Where(a => a.Tran_Ref_ID == 1).FirstOrDefault();
            var old_ledger = db.tbl_item_order_ledger.FirstOrDefault(a => a.Order_Master_ID == master.Order_Master_ID && a.Tran_Ref_ID == 1);
            if (old_ledger != null)
            {
                old_ledger.Debit = TotalFood;
                db.Entry(old_ledger).State = EntityState.Modified;
            }
            //item_order_ledger edit begin
            db.Entry(master_db).State = EntityState.Modified;
            db.SaveChanges();

            var gStay = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == master.Order_Master_ID);
            var dbitems = db.tbl_item_order_details.Where(a => a.Order_Master_ID == gStay.Order_Master_ID).Include(b => b.tbl_food).ToList();
            List<OrderedItem> items = new List<OrderedItem>();
            foreach (var item in dbitems)
            {
                OrderedItem ro = new OrderedItem();
                string desc = "";
                ro.Food_No = item.tbl_food.Food_No ?? "";
                desc += item.tbl_food.Food_Name;

                if (item.tbl_food_side_extra != null)
                {
                    desc += " - ";
                    desc += item.tbl_food_side_extra.Food_Side_Name;
                }
                if (item.Side_Quantity != null)
                {
                    desc += "-" + (Convert.ToInt32(item.Side_Quantity)).ToString();
                }
                if (item.Note != null)
                {
                    desc += " (" + item.Note + ")";
                }
                ro.Food_Desc = desc;
                ro.Qty = Convert.ToInt32(item.Quantity);

                items.Add(ro);
            }

            string waiterlist = "";
            string ordertype = "";
            if (gStay.tbl_dining_waiter.Count() > 0)
            {
                foreach (var item in gStay.tbl_dining_waiter)
                {
                    waiterlist += item.tbl_emp_info.Emp_Name + ", ";
                }
                waiterlist = waiterlist.Substring(0, waiterlist.Length - 2);
            }
            if (gStay.Order_Method_ID == 3)
            {
                ordertype = "Room Service - " + gStay.tbl_guest_stay_info.tbl_room_info.Room_No;
            }
            else if (gStay.Order_Method_ID == 2)
            {
                ordertype = "Take Away";
                if (gStay.tbl_dining_set_up.Count() > 0)
                {
                    ordertype = "Take Away - ";
                    foreach (var item in gStay.tbl_dining_set_up)
                    {
                        ordertype += item.Dining_No + ", ";
                    }
                    ordertype = ordertype.Substring(0, ordertype.Length - 2);
                }

            }
            else if (gStay.tbl_dining_set_up.Count() > 0)
            {
                foreach (var item in gStay.tbl_dining_set_up)
                {
                    ordertype += item.Dining_No + ", ";
                }
                ordertype = ordertype.Substring(0, ordertype.Length - 2);
            }
            CommonRepoprtData rpd = new CommonRepoprtData();
            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "KOT.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("");
            }

            ReportDataSource DataSetKot = new ReportDataSource("DataSetKot", items);

            lc.DataSources.Add(DataSetKot);

            ReportParameter[] param = new ReportParameter[6];
            param[0] = new ReportParameter("propertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("kotNo", gStay.Order_Master_ID.ToString(), false);
            param[2] = new ReportParameter("steward", waiterlist, false);
            param[3] = new ReportParameter("orderType", ordertype, false);
            param[4] = new ReportParameter("note", gStay.Note ?? "", false);
            param[5] = new ReportParameter("orderDate", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Pay(int? id, int? Table_id)
        {
            if (id == null && Table_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_item_order_master master = new tbl_item_order_master();
            if (id != null)
            {
                master = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id && (a.Status == "Ordered" || a.Status == "Served"));
            }
            if (Table_id != null)
            {
                int tbid = Convert.ToInt32(Table_id);
                master = db.tbl_item_order_master.FirstOrDefault(a => a.tbl_dining_set_up.Select(b => b.Dining_ID).ToList().Contains(tbid) && (a.Status == "Ordered" || a.Status == "Served"));
            }
            if (master == null)
            {
                return HttpNotFound();
            }
            if (master.Stay_Info_ID == null)
            {
                ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No");
            }
            else
            {
                ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No", master.tbl_guest_stay_info.tbl_room_info.Room_ID);
            }
            ViewBag.Pay_Method_ID = new SelectList(db.tbl_payment_method, "Pay_Method_ID", "Pay_Method_Name");
            return View(master);
        }

        [HttpPost]
        public ActionResult Pay(int? id, int? Table_id, int Pay_Method_ID, int? Room_ID, decimal? discount, decimal? cashPay, string particular, string GuestOption, int? FreqID, string Name, string Phone, string Address, string Email)
        {
            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                int userId = tblEmpInfo.Emp_ID;
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tbl_item_order_master master = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id);
                if (master == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Pay_Method_ID = new SelectList(db.tbl_payment_method, "Pay_Method_ID", "Pay_Method_Name");
                if (master.Stay_Info_ID == null)
                {
                    ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No");
                }
                else
                {
                    ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "checkin").OrderBy(c => c.tbl_room_info.Room_No).Select(b => b.tbl_room_info), "Room_ID", "Room_No", master.tbl_guest_stay_info.tbl_room_info.Room_ID);
                }
                if ((Pay_Method_ID == 3 || Pay_Method_ID == 5) && Room_ID == null)
                {
                    ViewBag.ErrorMsg = "Select room!!";
                    return View(master);
                }
                decimal totalPayable = Convert.ToDecimal(master.tbl_item_order_ledger.Where(a => a.Tran_Ref_ID == 1).Sum(b => b.Debit));
                decimal due = totalPayable;
                tbl_item_order_ledger discountRow = new tbl_item_order_ledger();
                tbl_item_order_ledger cashRow = new tbl_item_order_ledger();
                tbl_item_order_ledger cardRow = new tbl_item_order_ledger();
                tbl_item_order_ledger roomRow = new tbl_item_order_ledger();
                discountRow.Order_Master_ID = id;
                discountRow.Tran_Date = DateTime.Now;
                discountRow.Particular = particular;
                discountRow.Debit = 0;

                cashRow.Order_Master_ID = id;
                cashRow.Tran_Date = DateTime.Now;
                cashRow.Particular = particular;
                cashRow.Debit = 0;

                cardRow.Order_Master_ID = id;
                cardRow.Tran_Date = DateTime.Now;
                cardRow.Particular = particular;
                cardRow.Debit = 0;

                roomRow.Order_Master_ID = id;
                roomRow.Tran_Date = DateTime.Now;
                roomRow.Particular = particular;
                roomRow.Debit = 0;

                if (discount != null)
                {
                    due -= Convert.ToDecimal(discount);
                    discountRow.Tran_Ref_ID = 2;
                    discountRow.Tran_Ref_Name = "Discount";
                    discountRow.Credit = discount;
                    master.tbl_item_order_ledger.Add(discountRow);
                }
                if ((Pay_Method_ID == 1 || Pay_Method_ID == 4 || Pay_Method_ID == 5) && cashPay != null) //cash
                {
                    due -= Convert.ToDecimal(cashPay);
                    cashRow.Tran_Ref_ID = 3;
                    cashRow.Tran_Ref_Name = "CashPayment";
                    cashRow.Credit = cashPay;
                    master.tbl_item_order_ledger.Add(cashRow);
                }
                if (Pay_Method_ID == 2 || Pay_Method_ID == 4) //card
                {
                    cardRow.Tran_Ref_ID = 4;
                    cardRow.Tran_Ref_Name = "CardPayment";
                    cardRow.Credit = due;
                    master.tbl_item_order_ledger.Add(cardRow);
                    due = 0;
                }
                if (Pay_Method_ID == 3 || Pay_Method_ID == 5) //roomservice
                {
                    roomRow.Tran_Ref_ID = 5;
                    roomRow.Tran_Ref_Name = "RoomPayment";
                    roomRow.Credit = due;
                    master.tbl_item_order_ledger.Add(roomRow);

                    tbl_guest_stay_info guest = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == Room_ID && a.Status == "checkin");
                    if (guest == null)
                    {
                        ViewBag.ErrorMsg = "No guest found in the selected room!!";
                        return View(master);
                    }
                    master.Stay_Info_ID = guest.Stay_Info_ID;

                    tbl_room_transactions tran = new tbl_room_transactions();
                    tran.Room_ID = guest.Room_ID;
                    tran.Guest_ID = guest.Guest_ID;
                    tran.Tran_Ref_ID = 5;
                    tran.Tran_Ref_Name = "otherBill";
                    tran.Particulars = "Restaurant";
                    tran.Debit = due;
                    tran.Credit = 0;
                    tran.Tran_Date = DateTime.Now;
                    tran.Stay_Info_ID = guest.Stay_Info_ID;
                    tran.Emp_ID = userId;

                    db.tbl_room_transactions.Add(tran);
                    due = 0;
                }
                if (due > 5)
                {
                    ViewBag.ErrorMsg = "Due Cann't be more then 5tk";
                    return View(master);
                }
                if (GuestOption == "person")
                {
                    tbl_restaurant_guest_info guest = new tbl_restaurant_guest_info();
                    guest.Name = Name;
                    guest.Address = Address;
                    guest.Phone = Phone;
                    guest.Email = Email;
                    if (FreqID != null)
                    {
                        master.Rest_Guest_ID = FreqID;
                    }
                    else
                    {
                        master.tbl_restaurant_guest_info = guest;
                    }
                }

                if (GuestOption == "company")
                {
                    tbl_restaurant_company_info company = new tbl_restaurant_company_info();
                    company.Name = Name;
                    company.Address = Address;
                    company.Phone = Phone;
                    company.Email = Email;
                    if (FreqID != null)
                    {
                        master.Rest_Company_ID = FreqID;
                    }
                    else
                    {
                        master.tbl_restaurant_company_info = company;
                    }
                }

                var this_tables = master.tbl_dining_set_up.ToList();
                foreach (var item in this_tables)
                {
                    item.Status = "Vacant";
                    db.Entry(item).State = EntityState.Modified;
                }

                master.Payment_Date = DateTime.Now;
                master.Pay_Method_ID = Pay_Method_ID;
                master.Status = "Paid";
                db.Entry(master).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Index", "RestaurantDashboard");
        }

        [HttpGet]
        public ActionResult Cancel(int? id)
        {
            //Camncel policy conditions
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_item_order_master master = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id);
            if (master == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = master.Order_Master_ID;
            return PartialView();
        }

        [HttpPost]
        public ActionResult Cancel(int id)
        {
            //Camncel policy conditions
            tbl_item_order_master master = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id);
            if (master == null)
            {
                return HttpNotFound();
            }

            var this_tables = master.tbl_dining_set_up.ToList();
            foreach (var item in this_tables)
            {
                item.Status = "Vacant";
                db.Entry(item).State = EntityState.Modified;
            }
            master.Status = "Cancelled";
            db.Entry(master).State = EntityState.Modified;
            db.SaveChanges();


            return RedirectToAction("Index", "RestaurantDashboard");
        }

        public JsonResult Bill(int id, decimal? discount)
        {
            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                int userId = tblEmpInfo.Emp_ID;
            }

            var gStay = db.tbl_item_order_master.FirstOrDefault(a => a.Order_Master_ID == id);
            if (gStay == null)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (discount == null)
            {
                discount = 0;
            }
            decimal discountPercent = 0;

            RestaurantDetail restaurantDetail = new RestaurantDetail();
            decimal totalPayable = Math.Floor(Convert.ToDecimal(gStay.tbl_item_order_ledger.Where(a => a.Tran_Ref_ID == 1).Sum(b => b.Debit)));
            discountPercent = Convert.ToDecimal((discount * 100) / totalPayable);
            var dbitems = db.tbl_item_order_details.Where(a => a.Order_Master_ID == gStay.Order_Master_ID).Include(b => b.tbl_food).ToList();
            List<BillItem> items = new List<BillItem>();
            foreach (var item in dbitems)
            {
                BillItem ro = new BillItem();
                decimal price = Convert.ToDecimal(item.Price);
                decimal qty = Convert.ToDecimal(item.Quantity);

                //itemwise vat  + sc calculation begin
                decimal vat = (price * restaurantDetail.vat) / 100;
                decimal sc = (price * restaurantDetail.sc) / 100;
                price = price + vat + sc;
                //itemwise vat  + sc calculation end

                ro.Food_No = item.tbl_food.Food_No ?? "";
                ro.Food_Name = item.tbl_food.Food_Name ?? "";
                ro.Food_Price = price;
                ro.Qty = Convert.ToInt32(qty);
                ro.Total = price * qty;

                items.Add(ro);
            }
            var side_items = db.tbl_item_order_details.Where(a => a.Order_Master_ID == id && a.tbl_food_side_extra != null).GroupBy(b => b.Food_Side_ID).Select(lg =>
                                new {
                                    Order_Master_ID = lg.FirstOrDefault().Order_Master_ID.Value,
                                    Food_Side_ID = lg.Key,
                                    Total_Side_Quantity = lg.Sum(w => w.Side_Quantity),
                                    Side_Price = lg.FirstOrDefault().Side_Price.Value,
                                    Food_Side_Name = lg.FirstOrDefault().tbl_food_side_extra.Food_Side_Name
                                });
            foreach (var item in side_items)
            {
                BillItem ro = new BillItem();
                decimal price = Convert.ToDecimal(item.Side_Price);
                decimal qty = Convert.ToDecimal(item.Total_Side_Quantity);

                //itemwise vat  + sc calculation begin
                decimal vat = (price * restaurantDetail.vat) / 100;
                decimal sc = (price * restaurantDetail.sc) / 100;
                price = price + vat + sc;
                //itemwise vat  + sc calculation end

                ro.Food_No = "S";
                ro.Food_Name = item.Food_Side_Name;
                ro.Food_Price = price;
                ro.Qty = Convert.ToInt32(qty);
                ro.Total = price * qty;

                items.Add(ro);
            }
            string waiterlist = "";
            string ordertype = "";
            if (gStay.tbl_dining_waiter.Count() > 0)
            {
                foreach (var item in gStay.tbl_dining_waiter)
                {
                    waiterlist += item.tbl_emp_info.Emp_Name + ", ";
                }
                waiterlist = waiterlist.Substring(0, waiterlist.Length - 2);
            }
            if (gStay.Order_Method_ID == 3)
            {
                ordertype = "Room Service - " + gStay.tbl_guest_stay_info.tbl_room_info.Room_No;
            }
            else if (gStay.Order_Method_ID == 2)
            {
                ordertype = "Take Away";
                if (gStay.tbl_dining_set_up.Count() > 0)
                {
                    ordertype = "Take Away - ";
                    foreach (var item in gStay.tbl_dining_set_up)
                    {
                        ordertype += item.Dining_No + ", ";
                    }
                    ordertype = ordertype.Substring(0, ordertype.Length - 2);
                }

            }
            else if (gStay.tbl_dining_set_up.Count() > 0)
            {
                foreach (var item in gStay.tbl_dining_set_up)
                {
                    ordertype += item.Dining_No + ", ";
                }
                ordertype = ordertype.Substring(0, ordertype.Length - 2);
            }
            CommonRepoprtData rpd = new CommonRepoprtData();
            tbl_property_information prop = db.tbl_property_information.FirstOrDefault();
            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "Bill.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("");
            }

            ReportDataSource DataSetRestaurantBill = new ReportDataSource("DataSetRestaurantBill", items);

            lc.DataSources.Add(DataSetRestaurantBill);

            ReportParameter[] param = new ReportParameter[11];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", prop.Address, false);
            param[2] = new ReportParameter("PropertyPhone", "PH : " + rpd.PropertyPhone, false);
            param[3] = new ReportParameter("PropertyCity", prop.City + "-" + prop.Postal_Code, false);
            param[4] = new ReportParameter("VATRegistrationNumber", "VAT Reg. : " + rpd.VATRegistrationNumber, false);
            param[5] = new ReportParameter("KotNo", "Invoice # : " + gStay.Order_Master_ID.ToString(), false);
            param[6] = new ReportParameter("Steward", waiterlist, false);
            param[7] = new ReportParameter("OrderType", ordertype, false);
            param[8] = new ReportParameter("Discount", discount.ToString(), false);
            param[9] = new ReportParameter("TotalPayable", (totalPayable - discount).ToString(), false);
            param[10] = new ReportParameter("DiscountLabel", "Discount(" + discountPercent.ToString("F1", CultureInfo.InvariantCulture) + "%)", false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RoomDetail(int id)
        {
            var data = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == id && a.Status == "checkin");
            if (data == null)
            {
                return Json("No Data Found", JsonRequestBehavior.AllowGet);
            }

            return PartialView(data);
        }

        public JsonResult GetFrequentInfo(string type, string phone)
        {
            if (type == "person")
            {
                var data = db.tbl_restaurant_guest_info.FirstOrDefault(a => a.Phone == phone);
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            if (type == "company")
            {
                var data = db.tbl_restaurant_company_info.FirstOrDefault(a => a.Phone == phone);
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("none", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class RestaurantDetail
    {
        public decimal vat;
        public decimal sc;
        public int Cancelationtime;
        public RestaurantDetail()
        {
            vat = 15;
            sc = 10;
            Cancelationtime = 10;
        }
    }

    public class OrderedItem
    {
        public string Food_No { get; set; }
        public string Food_Desc { get; set; }
        public int Qty { get; set; }
    }
    public class BillItem
    {
        public string Food_No { get; set; }
        public string Food_Name { get; set; }
        public decimal Food_Price { get; set; }
        public int Qty { get; set; }
        public decimal Total { get; set; }
    }
}