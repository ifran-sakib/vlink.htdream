﻿using System;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetAccountReceivableController : CustomControllerBase
    {
        public ActionResult AccountReceivable()
        {
            return View();
        }

        public PartialViewResult ViewDateWiseBanquetReceivable(DateTime? dob, DateTime? date)
        {
            var query = from tbl_banquet_prog in db.tbl_banquet_prog
                        join tbl_banquet_info in db.tbl_banquet_info on tbl_banquet_prog.Banquet_ID equals tbl_banquet_info.Banquet_ID
                        join tbl_banquet_booking in db.tbl_banquet_booking on tbl_banquet_prog.Program_ID equals tbl_banquet_booking.Program_ID
                        join Prorgram_Purpose in db.Prorgram_Purpose on tbl_banquet_booking.P_Purpose_ID equals Prorgram_Purpose.P_Purpose_ID
                        join tbl_banquet_transactions t in db.tbl_banquet_transactions on tbl_banquet_booking.Banq_Booking_ID equals t.Banq_Booking_ID
                        where tbl_banquet_booking.Start_Date >= dob && tbl_banquet_booking.End_Date <= date
                        group t by new
                        {
                            tbl_banquet_info.Banquet_Name,
                            tbl_banquet_prog.Person_Name,
                            tbl_banquet_prog.Person_Adress,
                            tbl_banquet_prog.Company_Name,
                            tbl_banquet_prog.Company_Adress,
                            tbl_banquet_booking.Start_Date,
                            tbl_banquet_booking.End_Date,
                            Prorgram_Purpose.Purpose_Name
                        } into g
                        select new BanquetCreditListViewModel
                        {
                            Banquet_Name = g.Key.Banquet_Name,
                            Person_Name = g.Key.Person_Name,
                            Person_Adress = g.Key.Person_Adress,
                            Company_Name = g.Key.Company_Name,
                            Company_Adress = g.Key.Company_Adress,
                            Start_Date = g.Key.Start_Date,
                            End_Date = g.Key.End_Date,
                            Purpose_Name = g.Key.Purpose_Name,
                            Debit = g.Sum(x => x.Debit),
                            Credit = g.Sum(x => x.Credit),
                            Due = g.Sum(x => x.Debit) - g.Sum(x => x.Credit)
                        };

            ViewBag.payable = query.ToList();
            return PartialView();
        }
    }
}