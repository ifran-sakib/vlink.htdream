﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager")]
    public class FrontDeskRoomController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_room_info = db.tbl_room_info.Include(t => t.tbl_floor_setup).Include(t => t.tbl_room_view_setup).Include(t => t.tbl_roomtype_setup);
            return View(tbl_room_info.ToList());
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_info tbl_room_info = db.tbl_room_info.Find(id);
            if (tbl_room_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_room_info);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Create()
        {
            ViewBag.Floor_ID = new SelectList(db.tbl_floor_setup, "Floor_ID", "Floor_No");
            ViewBag.Room_view_ID = new SelectList(db.tbl_room_view_setup, "Room_view_ID", "View_type");
            ViewBag.Roomtype_ID = new SelectList(db.tbl_roomtype_setup, "Roomtype_ID", "Roomtype_Name");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult Create([Bind(Include = "Room_ID,Room_No,Roomtype_ID,Floor_ID,Room_view_ID,Rate,Status,YsnActive,Created_Date")] tbl_room_info tbl_room_info)
        {
            if (ModelState.IsValid)
            {
                tbl_room_info.Created_Date = DateTime.Now;
                db.tbl_room_info.Add(tbl_room_info);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Floor_ID = new SelectList(db.tbl_floor_setup, "Floor_ID", "Floor_No", tbl_room_info.Floor_ID);
            ViewBag.Room_view_ID = new SelectList(db.tbl_room_view_setup, "Room_view_ID", "View_type", tbl_room_info.Room_view_ID);
            ViewBag.Roomtype_ID = new SelectList(db.tbl_roomtype_setup, "Roomtype_ID", "Roomtype_Name", tbl_room_info.Roomtype_ID);
            return PartialView(tbl_room_info);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_info tbl_room_info = db.tbl_room_info.Find(id);
            if (tbl_room_info == null)
            {
                return HttpNotFound();
            }
            ViewBag.Floor_ID = new SelectList(db.tbl_floor_setup, "Floor_ID", "Floor_No", tbl_room_info.Floor_ID);
            ViewBag.Room_view_ID = new SelectList(db.tbl_room_view_setup, "Room_view_ID", "View_type", tbl_room_info.Room_view_ID);
            ViewBag.Roomtype_ID = new SelectList(db.tbl_roomtype_setup, "Roomtype_ID", "Roomtype_Name", tbl_room_info.Roomtype_ID);
            return PartialView(tbl_room_info);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Room_ID,Room_No,Roomtype_ID,Floor_ID,Room_view_ID,Rate,Status,YsnActive,Created_Date")] tbl_room_info tbl_room_info)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_room_info).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Floor_ID = new SelectList(db.tbl_floor_setup, "Floor_ID", "Floor_No", tbl_room_info.Floor_ID);
            ViewBag.Room_view_ID = new SelectList(db.tbl_room_view_setup, "Room_view_ID", "View_type", tbl_room_info.Room_view_ID);
            ViewBag.Roomtype_ID = new SelectList(db.tbl_roomtype_setup, "Roomtype_ID", "Roomtype_Name", tbl_room_info.Roomtype_ID);
            return PartialView(tbl_room_info);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_info tbl_room_info = db.tbl_room_info.Find(id);
            if (tbl_room_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_room_info);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_room_info tbl_room_info = db.tbl_room_info.Find(id);
            db.tbl_room_info.Remove(tbl_room_info);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult UniqueRoomCheck(string str)
        {
            tbl_room_info room = db.tbl_room_info.FirstOrDefault(r => r.Room_No == str);
            if (room == null)
            {
                return Json("unique", JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
