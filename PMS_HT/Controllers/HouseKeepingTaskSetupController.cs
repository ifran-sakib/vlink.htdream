﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class HouseKeepingTaskSetupController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_task_setup.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_task_setup taskSetup)
        {
            if (ModelState.IsValid)
            {
                db.tbl_task_setup.Add(taskSetup);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_task_setup.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_task_setup taskSetup, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(taskSetup).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_task_setup.Remove(db.tbl_task_setup.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
