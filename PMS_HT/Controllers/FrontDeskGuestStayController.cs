﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using PMS_HT.CustomClass;
using PMS_HT.Areas.Services.Models;
using System.Globalization;
using PMS_HT.ViewModels;

namespace PMS_HT.Controllers
{
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [SessionExpire]
    public class FrontDeskGuestStayController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        public ActionResult Index()
        {
            var tbl_guest_stay_info = db.tbl_guest_stay_info.Include(t => t.tbl_guest_info)
                .Include(t => t.tbl_room_info);
            return View(tbl_guest_stay_info.ToList());
        }

        private tbl_service VatServiceChargeData()
        {
            return db.tbl_service.Find(1);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);

            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_guest_stay_info);
        }

        [HttpGet]
        public ActionResult Add(string id)
        {
            decimal rate = 0;

            ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && DbFunctions.AddHours(a.Arrival_Date, 3) >= DateTime.Now).OrderBy(b => b.tbl_room_info.Room_No).Select(a => a.tbl_room_info), "Room_ID", "Room_No");
            if (User.IsInRole("Super Admin"))
            {
                ViewBag.Room_ID = new SelectList(db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn").OrderBy(b => b.tbl_room_info.Room_No).Select(a => a.tbl_room_info), "Room_ID", "Room_No");
            }
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name");

            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));

            if (rid != null)
            {
                rate = Convert.ToDecimal(rid.Rate);
                ViewBag.RoomNo = id;
            }
            Session["valImage"] = "";
            Session["rno"] = id;

            ViewBag.ArrivalDate = DateTime.Now.ToString("dd MMMM yyyy hh:mm tt");
            ViewBag.DepartureDate = NewDepartureDate(DateTime.Now).Data;
            ViewBagDiscountLevel();
            CommonRepoprtData rpd = new CommonRepoprtData();
            ViewBag.PropertyName = rpd.PropertyName;
            ViewBag.PropertyAddress = rpd.PropertyAddress;
            ViewBag.PropertyPhone = rpd.PropertyPhone;
            ViewBag.PropertyLogo = rpd.PropertyLogo;

            ViewBagCommonRoomData(rate);

            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, string gID, string feet, string inch, string RoomOption, decimal? discount, decimal? paid, string particulars)
        {
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_guest_stay_info.Room_ID);
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name");

            CommonRepoprtData rpd = new CommonRepoprtData();
            ViewBag.PropertyName = rpd.PropertyName;
            ViewBag.PropertyAddress = rpd.PropertyAddress;
            ViewBag.PropertyPhone = rpd.PropertyPhone;
            ViewBag.PropertyLogo = rpd.PropertyLogo;

            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    string rno = (string)Session["rno"];
                    var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(rno));
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rid.Rate.Value);

                        int count = (int)Session["dayCount"];
                        if (count == 0)
                        {
                            count = 1;
                        }
                        int temp = count;
                        if (commonRoomData.VatPercent > 0)
                        {
                            temp += count;
                        }
                        /* IF 12 NOON START
                        double dayCountFromSession = (double)Session["dayCount"];
                        int count = Convert.ToInt32(Math.Floor(dayCountFromSession));
                        bool halfDay = true;

                        if (((dayCountFromSession * 2) % 2) == 0)       //Checking for half days
                        {
                            halfDay = false;
                        }
                        int temp = 2 * count;
                        if (halfDay)
                        {
                            temp += 2;
                        }
                        IF 12 NOON END */


                        if (commonRoomData.ScPercent > 0)
                        {
                            temp += count;
                        }

                        tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                        tbl_guest_attendee attendee = new tbl_guest_attendee();
                        attendee.Emp_ID = userId;
                        attendee.Changed_Date = DateTime.Now;
                        tbl_guest_stay_history shiftRow = new tbl_guest_stay_history();
                        shiftRow.Shift_Date = DateTime.Now;
                        shiftRow.Emp_ID = userId;
                        shiftRow.Reason = particulars;

                        string st = tbl_guest_stay_info.tbl_guest_info.Signature;

                        if (!string.IsNullOrEmpty(st))
                        {
                            tbl_guest_stay_info.tbl_guest_info.Signature = st;
                        }
                        if (tbl_guest_stay_info.Arrival_Date.Date != DateTime.Today.Date &&
                            tbl_guest_stay_info.Status == "CheckIn" && RoomOption != "Shift")
                        {
                            ViewBag.FutureDateError = "Future Date Cann't Be Selected As Arrival Date In CheckIn";
                            return View(tbl_guest_stay_info);
                        }
                        if (RoomOption == "Shift")
                        {
                            gID = "";
                        }
                        tbl_guest_stay_info.tbl_guest_info.National_ID = guestA.National_ID;
                        tbl_guest_stay_info.tbl_guest_info.Country_ID = guestA.Country_ID;
                        tbl_guest_stay_info.tbl_guest_info.Driving_License = guestA.Driving_License;
                        tbl_guest_stay_info.tbl_guest_info.Tin_Certificate = guestA.Tin_Certificate;
                        tbl_guest_stay_info.tbl_guest_info.VIP = guestA.VIP;
                        tbl_guest_stay_info.tbl_guest_info.Height = feet + " feet " + inch + " inch";
                        if (gID != "") //frequent guest
                        {
                            tbl_guest_stay_info.Guest_ID = Convert.ToInt32(gID);

                            tbl_guest_info guest_info =
                                db.tbl_guest_info.FirstOrDefault(g => g.Guest_ID == tbl_guest_stay_info.Guest_ID);

                            //Guest Info Edit begin
                            {
                                guest_info.Name = tbl_guest_stay_info.tbl_guest_info.Name;
                                guest_info.Father_Name = tbl_guest_stay_info.tbl_guest_info.Father_Name;
                                guest_info.Mother_Name = tbl_guest_stay_info.tbl_guest_info.Mother_Name;
                                guest_info.Maritial_Status = tbl_guest_stay_info.tbl_guest_info.Maritial_Status;
                                guest_info.Spouse_Name = tbl_guest_stay_info.tbl_guest_info.Spouse_Name;
                                guest_info.Phone = tbl_guest_stay_info.tbl_guest_info.Phone;
                                guest_info.AdditionalPhone = tbl_guest_stay_info.tbl_guest_info.AdditionalPhone;
                                guest_info.Address = tbl_guest_stay_info.tbl_guest_info.Address;
                                guest_info.Permanent_Address = tbl_guest_stay_info.tbl_guest_info.Permanent_Address;
                                guest_info.Email = tbl_guest_stay_info.tbl_guest_info.Email;
                                guest_info.National_ID = tbl_guest_stay_info.tbl_guest_info.National_ID;
                                guest_info.Driving_License = tbl_guest_stay_info.tbl_guest_info.Driving_License;
                                guest_info.Tin_Certificate = tbl_guest_stay_info.tbl_guest_info.Tin_Certificate;
                                guest_info.Nationality = tbl_guest_stay_info.tbl_guest_info.Nationality;
                                guest_info.Purpose_of_visit = tbl_guest_stay_info.tbl_guest_info.Purpose_of_visit;
                                guest_info.Passport_No = tbl_guest_stay_info.tbl_guest_info.Passport_No;
                                guest_info.Country_ID = tbl_guest_stay_info.tbl_guest_info.Country_ID;
                                guest_info.Visa_No = tbl_guest_stay_info.tbl_guest_info.Visa_No;
                                guest_info.Visa_Date = tbl_guest_stay_info.tbl_guest_info.Visa_Date;
                                guest_info.Arrived_From = tbl_guest_stay_info.tbl_guest_info.Arrived_From;
                                guest_info.Going_To = tbl_guest_stay_info.tbl_guest_info.Going_To;
                                guest_info.Date_of_Arr_in_Country =
                                    tbl_guest_stay_info.tbl_guest_info.Date_of_Arr_in_Country;
                                guest_info.Nearest_Relative_Contact_Info = tbl_guest_stay_info.tbl_guest_info
                                    .Nearest_Relative_Contact_Info;
                                guest_info.Age = tbl_guest_stay_info.tbl_guest_info.Age;
                                guest_info.Occupation = tbl_guest_stay_info.tbl_guest_info.Occupation;
                                guest_info.Signature = tbl_guest_stay_info.tbl_guest_info.Signature;
                                guest_info.Identification_Mark = tbl_guest_stay_info.tbl_guest_info.Identification_Mark;
                                guest_info.Height = feet + " feet " + inch + " inch";
                                guest_info.Skin_Color = tbl_guest_stay_info.tbl_guest_info.Skin_Color;
                                guest_info.VIP = guestA.VIP;
                            }
                            tbl_guest_stay_info.tbl_guest_info = null;
                            db.Entry(guest_info).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        DateTime arrDate = tbl_guest_stay_info.Arrival_Date;
                        DateTime depDate = tbl_guest_stay_info.Departure_Date;

                        for (int i = 0; i < temp; i++)
                        {
                            transactions[i] = new tbl_room_transactions();
                        }

                        tbl_room_transactions paidTransaction = new tbl_room_transactions();
                        tbl_room_transactions discountTransaction = new tbl_room_transactions();


                        if (tbl_guest_stay_info.Status == "Reserved")
                        {
                            tbl_guest_stay_info.Reserved_Date = DateTime.Now;
                            attendee.Purpose = "reserve";
                        }
                        else if (tbl_guest_stay_info.Status == "Booked")
                        {
                            tbl_guest_stay_info.Booking_Date = DateTime.Now;
                            attendee.Purpose = "book";
                        }
                        else if (tbl_guest_stay_info.Status == "CheckIn" && RoomOption != "Shift")
                        {
                            tbl_guest_stay_info.Airport_Pickup = null;
                            tbl_guest_stay_info.Transportation = null;
                            tbl_guest_stay_info.Special_Request = null;
                        }



                        decimal unroundedTotalPayable = commonRoomData.PerDayCharge * count;
                        decimal totalPayable = Rounder.Round(unroundedTotalPayable);

                        tbl_guest_stay_info.Room_ID = rid.Room_ID;
                        foreach (var item in transactions)
                        {
                            item.Room_ID = rid.Room_ID;
                        }
                        paidTransaction.Room_ID = rid.Room_ID;
                        discountTransaction.Room_ID = rid.Room_ID;

                        tbl_guest_info guest;
                        var guest_stay = new tbl_guest_stay_info();

                        //frequent guest
                        if (tbl_guest_stay_info.Guest_ID != null)
                        {
                            guest = new tbl_guest_info();
                            guest.Guest_ID = (int)tbl_guest_stay_info.Guest_ID;
                            db.tbl_guest_stay_info.Add(tbl_guest_stay_info);
                            db.SaveChanges();
                        }


                        // Room Shift Start
                        else if (Session["multi-Guest"] != null && RoomOption == "Shift")
                        {
                            guest_stay = (tbl_guest_stay_info)Session["multi-Guest"];
                            guest_stay.Departure_Date = DateTime.Now;
                            guest_stay.Status = "Clear";
                            guest_stay.Check_Out_Date = DateTime.Now.Date;
                            guest_stay.Check_Out_Time = DateTime.Now.TimeOfDay;
                            guest_stay.Check_Out_Note = "Temp_Stay";

                            db.tbl_guest_stay_info.Attach(guest_stay);
                            var updatedGuestStayInfo = db.Entry(guest_stay);
                            updatedGuestStayInfo.Property(a => a.Check_Out_Note).IsModified = true;
                            updatedGuestStayInfo.Property(a => a.Departure_Date).IsModified = true;
                            updatedGuestStayInfo.Property(a => a.Status).IsModified = true;
                            updatedGuestStayInfo.Property(a => a.Check_Out_Date).IsModified = true;
                            updatedGuestStayInfo.Property(a => a.Check_Out_Time).IsModified = true;

                            shiftRow.Shift_From_ID = guest_stay.Stay_Info_ID;
                            //make previous room dirty begin
                            tbl_room_info previous_room =
                                db.tbl_room_info.FirstOrDefault(r => r.Room_ID == guest_stay.Room_ID);
                            previous_room.Status = "Dirty";
                            db.tbl_room_info.Attach(previous_room);
                            var updatedRoomInfo = db.Entry(previous_room);
                            updatedRoomInfo.State = EntityState.Modified;
                            //make previous room dirty end

                            guest = new tbl_guest_info();
                            guest.Guest_ID = (int)guest_stay.Guest_ID;

                            tbl_guest_stay_info.tbl_guest_info = guest_stay.tbl_guest_info;
                            tbl_guest_stay_info.Guest_ID = guest.Guest_ID;

                            db.tbl_guest_stay_info.Add(tbl_guest_stay_info);
                            db.SaveChanges();

                            attendee.Purpose = "shift";
                        }
                        // Room Shift End

                        else
                        {
                            db.tbl_guest_stay_info.Add(tbl_guest_stay_info);
                            db.SaveChanges();
                            var query =
                                "WITH x AS (SELECT *, r = RANK() OVER (ORDER BY Guest_ID DESC) from tbl_guest_info) " +
                                "SELECT * FROM x WHERE r = 1";
                            var data = db.Database.SqlQuery<tbl_guest_info>(query);
                            guest = data.FirstOrDefault();
                        }

                        var query2 =
                            "WITH x AS (SELECT *, r = RANK() OVER (ORDER BY Stay_Info_ID DESC) from tbl_guest_stay_info) " +
                            "SELECT * FROM x WHERE r = 1";
                        var data2 = db.Database.SqlQuery<tbl_guest_stay_info>(query2);
                        var gStay = data2.FirstOrDefault();

                        attendee.Stay_Info_ID = gStay.Stay_Info_ID;
                        shiftRow.Shift_To_ID = gStay.Stay_Info_ID;

                        if (tbl_guest_stay_info.Status != "Reserved" && RoomOption != "Shift")
                        {
                            foreach (var item in transactions)
                            {
                                if (!string.IsNullOrEmpty(gID))
                                {
                                    item.Guest_ID = Convert.ToInt32(gID);
                                }
                                else
                                {
                                    item.Guest_ID = guest.Guest_ID;
                                }
                                item.Stay_Info_ID = gStay.Stay_Info_ID;
                                item.Emp_ID = userId;
                            }
                            if (!string.IsNullOrEmpty(gID))
                            {
                                paidTransaction.Guest_ID = Convert.ToInt32(gID);
                                discountTransaction.Guest_ID = Convert.ToInt32(gID);
                            }
                            else
                            {
                                paidTransaction.Guest_ID = guest.Guest_ID;
                                discountTransaction.Guest_ID = guest.Guest_ID;
                            }
                            paidTransaction.Stay_Info_ID = gStay.Stay_Info_ID;
                            paidTransaction.Emp_ID = userId;
                            discountTransaction.Stay_Info_ID = gStay.Stay_Info_ID;
                            discountTransaction.Emp_ID = userId;

                            DateTime initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);

                            //for (int i = 0; i < 2 * count;)
                            for (int i = 0; i < temp;)
                            {
                                transactions[i].Tran_Ref_ID = 4;
                                transactions[i].Tran_Ref_Name = "Regular Charge";
                                transactions[i].Debit = rate;
                                transactions[i].Credit = 0;
                                transactions[i].Particulars = particulars;
                                transactions[i].Tran_Date = initialdate;
                                db.tbl_room_transactions.Add(transactions[i]);
                                i++;
                                if (commonRoomData.VatPercent > 0)
                                {
                                    transactions[i].Tran_Ref_ID = 2;
                                    transactions[i].Tran_Ref_Name = "Vat";
                                    transactions[i].Debit = commonRoomData.Vat;
                                    transactions[i].Credit = 0;
                                    transactions[i].Particulars = particulars;
                                    transactions[i].Tran_Date = initialdate;
                                    db.tbl_room_transactions.Add(transactions[i]);
                                    i++;
                                }
                                if (commonRoomData.ScPercent > 0)
                                {
                                    transactions[i].Tran_Ref_ID = 17;
                                    transactions[i].Tran_Ref_Name = "SC";
                                    transactions[i].Debit = commonRoomData.SC;
                                    transactions[i].Credit = 0;
                                    transactions[i].Particulars = particulars;
                                    transactions[i].Tran_Date = initialdate;
                                    db.tbl_room_transactions.Add(transactions[i]);
                                    i++;
                                }

                                initialdate = NextBillingDateTime(initialdate);
                            }

                            /*12 NOON START
                             if (halfDay)
                            {
                                int i = 2 * count;

                                transactions[i].Tran_Ref_ID = 16;
                                transactions[i].Tran_Ref_Name = "Half Regular Charge";
                                transactions[i].Debit = rate / 2;
                                transactions[i].Credit = 0;
                                transactions[i].Particulars = particulars;
                                transactions[i].Tran_Date = initialdate;
                                db.tbl_room_transactions.Add(transactions[i]);

                                i++;
                                if (vat > 0)
                                {
                                    transactions[i].Tran_Ref_ID = 15;
                                    transactions[i].Tran_Ref_Name = "Half Vat";
                                    transactions[i].Debit = vat / 2;
                                    transactions[i].Credit = 0;
                                    transactions[i].Particulars = particulars;
                                    transactions[i].Tran_Date = initialdate;
                                    db.tbl_room_transactions.Add(transactions[i])
                                    i++;
                                }
                                if (sc > 0)
                                {
                                    transactions[i].Tran_Ref_ID = 17;
                                    transactions[i].Tran_Ref_Name = "Half SC";
                                    transactions[i].Debit = sc / 2;
                                    transactions[i].Credit = 0;
                                    transactions[i].Particulars = particulars;
                                    transactions[i].Tran_Date = initialdate;
                                    db.tbl_room_transactions.Add(transactions[i]);
                                    i++;
                                }     
                            }                                                          
                            12 NOON END*/

                            initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                            if (discount != null && discount != 0)
                            {
                                decimal discountLevel = ViewBagDiscountLevel();
                                decimal discountableAmount = (totalPayable * discountLevel) / 100;
                                if (discount > discountableAmount)
                                {
                                    discount = discountableAmount;
                                }
                                discountTransaction.Debit = 0;
                                discountTransaction.Credit = discount;
                                discountTransaction.Tran_Ref_ID = 1;
                                discountTransaction.Tran_Ref_Name = "Discount";
                                discountTransaction.Tran_Date = initialdate;
                                discountTransaction.Particulars = particulars;
                                if (tbl_guest_stay_info.Status == "Booked")
                                {
                                    discountTransaction.Particulars = "Booking Discount";
                                }
                                db.tbl_room_transactions.Add(discountTransaction);
                            }

                            if (paid != null && paid != 0)
                            {
                                paidTransaction.Debit = 0;
                                paidTransaction.Credit = paid;
                                paidTransaction.Tran_Date = initialdate;
                                paidTransaction.Tran_Ref_ID = 3;
                                paidTransaction.Tran_Ref_Name = "paid";
                                paidTransaction.Particulars = particulars;
                                if (tbl_guest_stay_info.Status == "Booked")
                                {
                                    paidTransaction.Tran_Date = DateTime.Now;
                                    paidTransaction.Tran_Ref_ID = 14;
                                    paidTransaction.Tran_Ref_Name = "booking_paid";
                                }
                                db.tbl_room_transactions.Add(paidTransaction);
                            }
                            db.SaveChanges();

                            attendee.Purpose = "checkin";
                            if (tbl_guest_stay_info.Status == "Booked")
                            {
                                attendee.Purpose = "book";
                            }
                        }


                        else if (tbl_guest_stay_info.Status != "Reserved" && RoomOption == "Shift")
                        {
                            var roomShift = db.tbl_room_transactions
                                .Where(a => a.Stay_Info_ID == guest_stay.Stay_Info_ID)
                                .ToList();
                            int looper = 0;

                            foreach (var item in roomShift)
                            {
                                if (item.Tran_Ref_ID == 4)
                                {
                                    roomShift[looper].Debit = rate;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else if (item.Tran_Ref_ID == 2)
                                {
                                    roomShift[looper].Debit = commonRoomData.Vat;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else if (item.Tran_Ref_ID == 16)
                                {
                                    roomShift[looper].Debit = rate / 2;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else if (item.Tran_Ref_ID == 15)
                                {
                                    roomShift[looper].Debit = commonRoomData.Vat / 2;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else if (item.Tran_Ref_ID == 17)
                                {
                                    roomShift[looper].Debit = commonRoomData.SC;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else if (item.Tran_Ref_ID == 18)
                                {
                                    roomShift[looper].Debit = commonRoomData.SC / 2;
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomInfo = db.Entry(roomShift[looper]);
                                    updateRoomInfo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomInfo.Property(a => a.Debit).IsModified = true;
                                    updateRoomInfo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                else
                                {
                                    roomShift[looper].Room_ID = tbl_guest_stay_info.Room_ID;
                                    roomShift[looper].Stay_Info_ID = gStay.Stay_Info_ID;

                                    db.tbl_room_transactions.Attach(roomShift[looper]);

                                    var updateRoomNo = db.Entry(roomShift[looper]);
                                    updateRoomNo.Property(a => a.Room_ID).IsModified = true;
                                    updateRoomNo.Property(a => a.Stay_Info_ID).IsModified = true;
                                }
                                looper++;
                            }
                            db.SaveChanges();
                            db.tbl_guest_stay_history.Add(shiftRow);
                        }

                        db.tbl_guest_attendee.Add(attendee);
                        db.SaveChanges();

                        if (gStay.Status != "CheckIn")
                        {
                            string message = "Dear ";
                            message += tbl_guest_stay_info.tbl_guest_info.Name.ToUpper();
                            message += ", Your ";
                            message += tbl_guest_stay_info.Status == "Booked" ? "booking" : "reservation";
                            message += " has been confirmed. Your confirmation no is ";
                            message += (gStay.Stay_Info_ID + 500).ToString();
                            message += ". We are expecting you on ";
                            message += gStay.Arrival_Date.ToString("dd-MM-yyyy");
                            message += ". Thank you - " + rpd.PropertyName;

                            SendSms sendSms = new SendSms(tbl_guest_stay_info.tbl_guest_info.Phone, message);
                            //if (!string.IsNullOrEmpty(tbl_guest_stay_info.tbl_guest_info.Email))
                            //{
                            //    SendEmail sendEmail = new SendEmail(tbl_guest_stay_info.tbl_guest_info.Email,
                            //        "Reservation Confirmation", rid.Room_ID,
                            //        tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date, count,
                            //        Convert.ToDecimal(paid), 1,
                            //        (Convert.ToInt32(tbl_guest_stay_info.No_of_Adult) +
                            //         Convert.ToInt32(tbl_guest_stay_info.No_of_Child)),
                            //        tbl_guest_stay_info.tbl_guest_info.Name, tbl_guest_stay_info.tbl_guest_info.Address,
                            //        tbl_guest_stay_info.tbl_guest_info.Phone, (gStay.Stay_Info_ID + 500));
                            //}
                        }
                    }
                }
                Session.Remove("rno");
                Session.Remove("valImage");
                Session.Remove("dayCount");
                return RedirectToAction("Index", "FrontDesk");
            }

            return View(tbl_guest_stay_info);
        }

        public JsonResult Rebind()
        {
            string path = Session["valImage"].ToString();

            return Json(path, JsonRequestBehavior.AllowGet);
        }

        public void Capture(DateTime? q)
        {
            var stream = Request.InputStream;
            using (var reader = new StreamReader(stream))
            {
                var dump = reader.ReadToEnd();
                string rno = (string)Session["rno"];
                DateTime nm = DateTime.Now;
                if (q != null)
                {
                    nm = q.Value;
                }
                string date = nm.ToString("yyyyMMdd_HHmmss");
                var path = Server.MapPath("~/WebImages/" + date + "_" + rno + ".jpg");
                System.IO.File.WriteAllBytes(path, String_To_Bytes2(dump));
                ViewData["path"] = date + "_" + rno + ".jpg";
                Session["valImage"] = date + "_" + rno + ".jpg";
            }
        }

        private byte[] String_To_Bytes2(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];

            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }
            return bytes;
        }

        public JsonResult GetRoomWiseGuestInfo(int rno)
        {
            var multiList = db.tbl_guest_stay_info.Where(a => a.Room_ID == rno && DbFunctions.AddHours(a.Arrival_Date, 3) >= DateTime.Now && a.Status == "CheckIn").ToList();
            if (User.IsInRole("Super Admin"))
            {
                multiList = db.tbl_guest_stay_info.Where(a => a.Room_ID == rno && a.Status == "CheckIn").ToList();
            }
            if (multiList.Count != 0)
            {
                Session["multi-Guest"] = multiList.Last();
            }

            string arr = "";
            string dep = "";
            var guest = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == rno && DbFunctions.AddHours(a.Arrival_Date, 3) >= DateTime.Now && a.Status == "CheckIn");
            if (User.IsInRole("Super Admin"))
            {
                guest = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == rno && a.Status == "CheckIn");
            }
            if (guest != null)
            {
                var previousShiftRoom =
                    db.tbl_guest_stay_history.FirstOrDefault(a => a.Shift_From_ID == guest.Stay_Info_ID ||
                                                                  a.Shift_To_ID == guest.Stay_Info_ID);
                if (previousShiftRoom != null)
                {
                    return Json("Performed Shift Once!!");
                }
                arr = guest.Arrival_Date.ToString("dd MMMM yyyy hh:mm tt");
                dep = guest.Departure_Date.ToString("dd MMMM yyyy hh:mm tt");

                var guestList = db.tbl_guest_stay_info.Where(a => a.Room_ID == rno && a.Status == "CheckIn").Select(
                    a => new
                    {
                        Arrival_Date = arr,
                        Departure_Date = dep,
                        Name = a.tbl_guest_info.Name,
                        Father_Name = a.tbl_guest_info.Father_Name,
                        Mother_Name = a.tbl_guest_info.Mother_Name,
                        Maritial_Status = a.tbl_guest_info.Maritial_Status,
                        Spouse_Name = a.tbl_guest_info.Spouse_Name,
                        Phone = a.tbl_guest_info.Phone,
                        AdditionalPhone = a.tbl_guest_info.AdditionalPhone,
                        Address = a.tbl_guest_info.Address,
                        Permanent_Address = a.tbl_guest_info.Permanent_Address,
                        Email = a.tbl_guest_info.Email,
                        Passport_No = a.tbl_guest_info.Passport_No,
                        Country_ID = a.tbl_guest_info.Country_ID,
                        National_ID = a.tbl_guest_info.National_ID,
                        Driving_License = a.tbl_guest_info.Driving_License,
                        Tin_Certificate = a.tbl_guest_info.Tin_Certificate,
                        Nationality = a.tbl_guest_info.Nationality,
                        Purpose_of_visit = a.tbl_guest_info.Purpose_of_visit,
                        Visa_No = a.tbl_guest_info.Visa_No,
                        Visa_Date = a.tbl_guest_info.Visa_Date.ToString(),
                        Arrived_From = a.tbl_guest_info.Arrived_From,
                        Going_To = a.tbl_guest_info.Going_To,
                        Date_of_Arr_in_Country = a.tbl_guest_info.Date_of_Arr_in_Country.ToString(),
                        No_of_Adult = a.No_of_Adult,
                        No_of_Child = a.No_of_Child,
                        Age = a.tbl_guest_info.Age.ToString(),
                        Identification_Mark = a.tbl_guest_info.Identification_Mark,
                        Skin_Color = a.tbl_guest_info.Skin_Color,
                        Height = a.tbl_guest_info.Height,
                        Image = a.Image,
                        Signature = a.tbl_guest_info.Signature,
                        Occupation = a.tbl_guest_info.Occupation,
                        Nearest_Relative_Contact_Info = a.tbl_guest_info.Nearest_Relative_Contact_Info

                    }).ToList();

                if (guestList.Count != 0)
                {
                    var guest1 = guestList.Last();
                    Session["ShiftGuest"] = guest;
                    return Json(guest1);
                }
            }

            return Json("This room is nor Shiftable!!!");
        }

        public JsonResult GetFrequentGuestInfo(string phone)
        {
            var guestList = db.tbl_guest_info.Where(a => a.Phone == phone).Select(a => new
            {
                Guest_ID = a.Guest_ID,
                Name = a.Name,
                Father_Name = a.Father_Name,
                Mother_Name = a.Mother_Name,
                Maritial_Status = a.Maritial_Status,
                Spouse_Name = a.Spouse_Name,
                Phone = a.Phone,
                AdditionalPhone = a.AdditionalPhone,
                Address = a.Address,
                Permanent_Address = a.Permanent_Address,
                Email = a.Email,
                National_ID = a.National_ID,
                Driving_License = a.Driving_License,
                Tin_Certificate = a.Tin_Certificate,
                Nationality = a.Nationality,
                Purpose_of_visit = a.Purpose_of_visit,
                Passport_No = a.Passport_No,
                Country_ID = a.Country_ID,
                Visa_No = a.Visa_No,
                Visa_Date = DbFunctions.TruncateTime(a.Visa_Date).ToString(),
                Arrived_From = a.Arrived_From,
                Going_To = a.Going_To,
                Date_of_Arr_in_Country = a.Date_of_Arr_in_Country.ToString(),
                Age = a.Age.ToString(),
                Identification_Mark = a.Identification_Mark,
                Skin_Color = a.Skin_Color,
                Height = a.Height,
                Signature = a.Signature,
                Occupation = a.Occupation,
                Nearest_Relative_Contact_Info = a.Nearest_Relative_Contact_Info,
                DNR = a.DNR

            }).ToList();

            if (guestList.Count != 0)
            {
                var guest1 = guestList.Last();
                return Json(guest1);
            }

            return Json("");
        }

        [NonAction]
        public int StayDayCalculator(DateTime arriveDate, DateTime departDate)
        {
            int gracePeriod = 3;
            int noOfNight = 0;
            int totalStayInSeconds = (int)((departDate - arriveDate).TotalSeconds);
            noOfNight = totalStayInSeconds / 86400;
            if ((totalStayInSeconds % 86400) > (gracePeriod * 3600))
            {
                noOfNight++;
            }
            return noOfNight == 0 ? 1 : noOfNight;
        }

        [NonAction]
        public DateTime NextBillingDateTime(DateTime initialdate)
        {
            return initialdate.AddDays(1);
        }

        public JsonResult GetRateInfo(DateTime arriveDate, DateTime departDate, string method)
        {
            if (arriveDate >= departDate)
            {
                return Json("Invalid");
            }

            int noOfNight = StayDayCalculator(arriveDate, departDate);

            Session["dayCount"] = noOfNight;
            if (Session["NoOfNight"] != null && (method == "E" || method == "EB"))
            {
                var totalNights = (int)Session["NoOfNight"];

                if (noOfNight < totalNights)
                {
                    return Json("Invalid");
                }
            }
            return Json(noOfNight);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            decimal discount = 0, previousPaid = 0, otherBill = 0;
            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                var rate = Convert.ToDecimal(rid.Rate);
                ViewBag.RoomNo = id;
                int roomId = rid.Room_ID;
                var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == roomId && a.Status == "CheckIn");
                if (guest1 != null)
                {
                    ViewBag.RoomHistory = id;
                    var previousStayHistory = db.tbl_guest_stay_history.FirstOrDefault(a => a.Shift_To_ID == guest1.Stay_Info_ID);
                    if (previousStayHistory != null)
                    {
                        ViewBag.PreviousStay = previousStayHistory;
                    }

                    ViewBag.GuestStayInfo = guest1;
                    Session["GuestStayInfo"] = guest1;
                    ViewBag.CheckedInBy = db.tbl_guest_attendee
                        .FirstOrDefault(a => a.Stay_Info_ID == guest1.Stay_Info_ID && (a.Purpose == "checkin" || a.Purpose == "shift" || a.Purpose == "booked_to_checkin" || a.Purpose == "reserved_to_checkin"))
                        .tbl_emp_info.user_name;

                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.National_ID))
                    {
                        ViewBag.NidFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Driving_License))
                    {
                        ViewBag.D_LicenseFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Tin_Certificate))
                    {
                        ViewBag.TinFlag = 1;
                    }
                    ViewBag.NoOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    Session["NoOfNight"] = ViewBag.NoOfNight;

                    //var roomCharges =
                    //    (db.tbl_room_transactions.Where(
                    //        a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                    //             (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16)))
                    //    .ToList();
                    //Session["RoomCharges"] = roomCharges;
                    //if (roomCharges != null)
                    //{
                    //    foreach (var item in roomCharges)
                    //    {
                    //        roomCharge += Convert.ToDecimal(item.Debit);
                    //    }
                    //    ViewBag.RoomCharge = roomCharge;
                    //    Session["RoomCharge"] = roomCharge;
                    //}

                    var paidTransactions = db.tbl_room_transactions
                       .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 14))
                       .ToList();
                    if (paidTransactions != null && paidTransactions.Count > 0)
                    {
                        foreach (var item in paidTransactions)
                        {
                            previousPaid += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Paid = previousPaid;

                    var discountTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1))
                        .ToList();
                    if (discountTransactions != null && discountTransactions.Count > 0)
                    {
                        foreach (var item in discountTransactions)
                        {
                            discount += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Discount = discount;

                    //var vats = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                    //                                                (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 15)))
                    //    .ToList();
                    //if (vats != null)
                    //{
                    //    foreach (var item in vats)
                    //    {
                    //        vat += Convert.ToDecimal(item.Debit);
                    //    }
                    //    ViewBag.Vat = vat;
                    //    ViewBag.VatPercent = vats.FirstOrDefault().Debit;
                    //    Session["Vat"] = vat;
                    //}

                    var otherBills = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 5))
                        .ToList();
                    if (otherBills != null)
                    {
                        foreach (var item in otherBills)
                        {
                            otherBill += Convert.ToDecimal(item.Debit);
                        }                        
                    }
					ViewBag.OtherBill = otherBill;
					
                    //var paidTransactions =
                    //    (db.tbl_room_transactions.Where(
                    //        a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                    //             (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 14)))
                    //    .ToList();
                    //if (paidTransactions != null)
                    //{
                    //    foreach (var item in paidTransactions)
                    //    {
                    //        paid += Convert.ToDecimal(item.Credit);
                    //    }
                    //    ViewBag.Paid = paid;
                    //}
                    ViewBagCommonRoomData(rate);

                    Session["rno"] = id;
                    ViewBagDiscountLevel();
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, HttpPostedFileBase file, bool? latePayment, string feet, string inch, string details, decimal? amount, decimal? discount, decimal? paid, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    string rno = (string)Session["rno"];
                    var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(rno));
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                        int roomId = rid.Room_ID;
                        var guest1 = db.tbl_guest_stay_info
                            .FirstOrDefault(a => a.Room_ID == roomId && a.Status == "CheckIn");
                        if (guest1?.Guest_ID != null)
                        {
                            var guestId = (int)guest1.Guest_ID;
                            var guest_info = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == guestId);
                            if (guest_info != null)
                            {
                                DateTime ltPaydate = DateTime.Today.Date;
                                ltPaydate = ltPaydate.AddMinutes(-5);
                                bool ltpay = latePayment != null && latePayment == true && ltPaydate > guest1.Arrival_Date;

                                tbl_room_transactions paidTransaction = new tbl_room_transactions();
                                tbl_room_transactions discountTransaction = new tbl_room_transactions();
                                tbl_room_transactions otherBillTransaction = new tbl_room_transactions();


                                //unwanted begin
                                //decimal lastPaidSlice = 0;
                                //var lastPaidTransaction = db.tbl_room_transactions
                                //    .Where(a => a.Stay_Info_ID == guest1.Stay_Info_ID && a.Tran_Ref_ID == 3).ToList()
                                //    .LastOrDefault();
                                //if (lastPaidTransaction != null)
                                //{
                                //    lastPaidSlice = (decimal)lastPaidTransaction.Credit;
                                //}
                                //decimal paidReminder = 0, paidQuotient = 0, additionalMoney = 0;
                                //int length = 0;
                                //decimal roomChargeWithVat = (decimal)(rate + (rate * 15 / 100));
                                //var ceilOfRoomCharge = Convert.ToDecimal(Math.Ceiling(roomChargeWithVat));
                                //if (paid != 0 && paid != null)
                                //{
                                //    if (lastPaidSlice < ceilOfRoomCharge)
                                //    {
                                //        additionalMoney = (decimal)(ceilOfRoomCharge - lastPaidSlice);
                                //        paid = paid - additionalMoney;
                                //    }

                                //    paidReminder = (decimal)(paid % ceilOfRoomCharge);
                                //    paidQuotient = (decimal)(paid / ceilOfRoomCharge);
                                //    length = (int)paidQuotient;
                                //}
                                //unwanted end


                                //guestA.Image = guest_info.Image;
                                if (file != null && file.ContentLength > 0)
                                {
                                    string date = guest1.Arrival_Date.ToString("yyyyMMdd_HHmmss");
                                    var path = Server.MapPath("~/WebImages/" + date + "_" + rno + ".jpg");
                                    var img = "../../WebImages/" + date + "_" + rno + ".jpg";
                                    guest1.Image = img;
                                    file.SaveAs(path);
                                }

                                guest1.Departure_Date = tbl_guest_stay_info.Departure_Date;
                                guest1.No_of_Adult = tbl_guest_stay_info.No_of_Adult;
                                guest1.No_of_Child = tbl_guest_stay_info.No_of_Child;
                                //Guest Info Edit begin
                                {
                                    guest_info.Name = guestA.Name;
                                    guest_info.Father_Name = guestA.Father_Name;
                                    guest_info.Mother_Name = guestA.Mother_Name;
                                    guest_info.Maritial_Status = guestA.Maritial_Status;
                                    guest_info.Spouse_Name = guestA.Spouse_Name;
                                    guest_info.Phone = guestA.Phone;
                                    guest_info.AdditionalPhone = guestA.AdditionalPhone;
                                    guest_info.Address = guestA.Address;
                                    guest_info.Permanent_Address = guestA.Permanent_Address;
                                    guest_info.Email = guestA.Email;
                                    guest_info.National_ID = guestA.National_ID;
                                    guest_info.Driving_License = guestA.Driving_License;
                                    guest_info.Tin_Certificate = guestA.Tin_Certificate;
                                    guest_info.Nationality = guestA.Nationality;
                                    guest_info.Purpose_of_visit = guestA.Purpose_of_visit;
                                    guest_info.Passport_No = guestA.Passport_No;
                                    guest_info.Country_ID = guestA.Country_ID;
                                    guest_info.Visa_No = guestA.Visa_No;
                                    guest_info.Visa_Date = guestA.Visa_Date;
                                    guest_info.Arrived_From = guestA.Arrived_From;
                                    guest_info.Going_To = guestA.Going_To;
                                    guest_info.Date_of_Arr_in_Country = guestA.Date_of_Arr_in_Country;
                                    guest_info.Age = guestA.Age;
                                    guest_info.Nearest_Relative_Contact_Info = guestA.Nearest_Relative_Contact_Info;
                                    guest_info.Occupation = guestA.Occupation;
                                    guest_info.Identification_Mark = guestA.Identification_Mark;
                                    guest_info.Height = feet + " feet " + inch + " inch";
                                    guest_info.Skin_Color = guestA.Skin_Color;
                                    guest_info.Signature = guestA.Signature;
                                    guest_info.Special_Preference = guestA.Special_Preference;
                                    guest_info.Birth_Date = guestA.Birth_Date;
                                    guest_info.VIP = guestA.VIP;
                                    guest_info.DNR = guestA.DNR;
                                }
                                db.Entry(guest1).State = EntityState.Modified;
                                db.Entry(guest_info).State = EntityState.Modified;

                                int noOfNights = (int)Session["NoOfNight"];
                                int newDayCount = StayDayCalculator(tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date);
                                int extraDays = newDayCount - noOfNights;


                                //regular charge and vat rows adding for extended days begin

                                if (extraDays > 0)
                                {
                                    var count = extraDays;
                                    var temp = count;
                                    if (commonRoomData.VatPercent > 0)
                                    {
                                        temp += count;
                                    }
                                    if (commonRoomData.ScPercent > 0)
                                    {
                                        temp += count;
                                    }
                                    DateTime initialdate = DateTime.Now;
                                    tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                                    for (int i = 0; i < temp; i++)
                                    {
                                        transactions[i] = new tbl_room_transactions();
                                        transactions[i].Stay_Info_ID = guest1.Stay_Info_ID;
                                        transactions[i].Room_ID = guest1.Room_ID;
                                        transactions[i].Guest_ID = guest1.Guest_ID;
                                        transactions[i].Emp_ID = userId;
                                    }
                                    initialdate = (DateTime)db.tbl_room_transactions
                                        .Where(a => a.Stay_Info_ID == guest1.Stay_Info_ID && a.Tran_Ref_ID == 4)
                                        .Select(b => b.Tran_Date)
                                        .ToList()
                                        .Last();
                                    initialdate = initialdate.AddDays(1);
                                    for (int i = 0; i < temp;)
                                    {
                                        transactions[i].Tran_Ref_ID = 4;
                                        transactions[i].Tran_Ref_Name = "Regular Charge";
                                        transactions[i].Debit = rate;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);
                                        i++;
                                        if (commonRoomData.VatPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 2;
                                            transactions[i].Tran_Ref_Name = "Vat";
                                            transactions[i].Debit = commonRoomData.Vat;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        if (commonRoomData.ScPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 17;
                                            transactions[i].Tran_Ref_Name = "SC";
                                            transactions[i].Debit = commonRoomData.SC;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }

                                        initialdate = NextBillingDateTime(initialdate);
                                    }
                                }

                                if (discount != null && discount != 0)
                                {
                                    decimal discountLevel = ViewBagDiscountLevel();
                                    decimal discountableAmount = (Convert.ToDecimal(commonRoomData.PerDayCharge * newDayCount) * discountLevel) / 100;

                                    if (discount > discountableAmount)
                                    {
                                        discount = discountableAmount;
                                    }

                                    discountTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                                    discountTransaction.Room_ID = guest1.Room_ID;
                                    discountTransaction.Guest_ID = guest1.Guest_ID;
                                    discountTransaction.Tran_Ref_ID = 1;
                                    discountTransaction.Tran_Ref_Name = "Discount";
                                    discountTransaction.Credit = discount;
                                    discountTransaction.Debit = 0;
                                    discountTransaction.Particulars = particulars;
                                    discountTransaction.Emp_ID = userId;
                                    discountTransaction.Tran_Date = DateTime.Now;
                                    if (ltpay == true)
                                    {
                                        discountTransaction.Tran_Date = ltPaydate;
                                    }
                                    db.tbl_room_transactions.Add(discountTransaction);
                                }

                                if (paid != null && paid != 0)
                                {
                                    paidTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                                    paidTransaction.Room_ID = guest1.Room_ID;
                                    paidTransaction.Guest_ID = guest1.Guest_ID;
                                    paidTransaction.Debit = 0;
                                    paidTransaction.Credit = paid;
                                    paidTransaction.Tran_Date = DateTime.Now;
                                    paidTransaction.Tran_Ref_ID = 3;
                                    paidTransaction.Tran_Ref_Name = "paid";
                                    paidTransaction.Particulars = particulars;
                                    paidTransaction.Emp_ID = userId;
                                    if (ltpay == true)
                                    {
                                        paidTransaction.Tran_Date = ltPaydate;
                                        paidTransaction.Particulars = particulars + " Late_Payment";
                                    }
                                    db.tbl_room_transactions.Add(paidTransaction);
                                }

                                //initialdate = DateTime.Now;
                                //if (additionalMoney > 0)
                                //{
                                //    paidTransaction.Room_ID = guest1.Room_ID;
                                //    paidTransaction.Guest_ID = guest1.Guest_ID;
                                //    paidTransaction.Debit = 0;
                                //    paidTransaction.Credit = additionalMoney;
                                //    paidTransaction.Tran_Date = initialdate;
                                //    paidTransaction.Tran_Ref_ID = 3;
                                //    paidTransaction.Tran_Ref_Name = "paid";
                                //    paidTransaction.Particulars = particulars;
                                //    db.tbl_room_transactions.Add(paidTransaction);
                                //    db.SaveChanges();
                                //}

                                //if (paid != null && paid != 0)
                                //{
                                //    if (paidQuotient != 0)
                                //    {
                                //        for (int i = 0; i < length; i++)
                                //        {
                                //            paidTransaction.Room_ID = guest1.Room_ID;
                                //            paidTransaction.Guest_ID = guest1.Guest_ID;
                                //            paidTransaction.Debit = 0;
                                //            paidTransaction.Credit = ceilOfRoomCharge;
                                //            paidTransaction.Tran_Date = initialdate;
                                //            initialdate = initialdate.AddDays(1);
                                //            paidTransaction.Tran_Ref_ID = 3;
                                //            paidTransaction.Tran_Ref_Name = "paid";
                                //            paidTransaction.Particulars = particulars;
                                //            db.tbl_room_transactions.Add(paidTransaction);
                                //            db.SaveChanges();
                                //        }
                                //    }
                                //    if (paidReminder != 0)
                                //    {
                                //        paidTransaction.Room_ID = guest1.Room_ID;
                                //        paidTransaction.Guest_ID = guest1.Guest_ID;
                                //        paidTransaction.Debit = 0;
                                //        paidTransaction.Credit = paidReminder;
                                //        paidTransaction.Tran_Date = initialdate;
                                //        paidTransaction.Tran_Ref_ID = 3;
                                //        paidTransaction.Tran_Ref_Name = "paid";
                                //        paidTransaction.Particulars = particulars;
                                //        db.tbl_room_transactions.Add(paidTransaction);
                                //        db.SaveChanges();
                                //    }
                                //}

                                if (amount != null && amount != 0)
                                {
                                    otherBillTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                                    otherBillTransaction.Room_ID = guest1.Room_ID;
                                    otherBillTransaction.Guest_ID = guest1.Guest_ID;
                                    otherBillTransaction.Debit = amount;
                                    otherBillTransaction.Credit = 0;
                                    otherBillTransaction.Tran_Date = DateTime.Now;
                                    otherBillTransaction.Tran_Ref_ID = 5;
                                    otherBillTransaction.Tran_Ref_Name = "otherBill";
                                    otherBillTransaction.Particulars = details;
                                    otherBillTransaction.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(otherBillTransaction);
                                }

                                db.SaveChanges();
                            }
                        }
                    }
                }
                //guest info edit end
                Session.Remove("RoomCharge");
                Session.Remove("NoOfNight");
                Session.Remove("Vat");
                Session.Remove("Discount");
                Session.Remove("RoomCharges");
                Session.Remove("rno");
                Session.Remove("GuestStayInfo");
                return RedirectToAction("Index", "FrontDesk");
            }
            ViewBag.Guest_ID = new SelectList(db.tbl_guest_info, "Guest_ID", "Name", tbl_guest_stay_info.Guest_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_guest_stay_info.Room_ID);

            CommonRepoprtData rpd = new CommonRepoprtData();
            ViewBag.PropertyName = rpd.PropertyName;
            ViewBag.PropertyAddress = rpd.PropertyAddress;
            ViewBag.PropertyPhone = rpd.PropertyPhone;
            ViewBag.PropertyLogo = rpd.PropertyLogo;
            return View(tbl_guest_stay_info);
        }

        [HttpGet]
        public ActionResult CheckOut(string id)
        {
            decimal roomCharge = 0, vat = 0, discount = 0, paid = 0, otherBill = 0, refund = 0, roomDue = 0, sc = 0;
            int gracePeriod = 3;

            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                var rate = Convert.ToDecimal(rid.Rate);
                ViewBagCommonRoomData(rate);
                ViewBag.RoomNo = id;
                int roomId = rid.Room_ID;
                var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == roomId && a.Status == "CheckIn");
                if (guest1 != null)
                {
                    var previousStayHistory = db.tbl_guest_stay_history.FirstOrDefault(a => a.Shift_To_ID == guest1.Stay_Info_ID);
                    if (previousStayHistory != null)
                    {
                        ViewBag.PreviousStay = previousStayHistory;
                    }

                    ViewBag.GuestName = guest1.tbl_guest_info.Name;
                    ViewBag.Phone = guest1.tbl_guest_info.Phone;
                    ViewBag.Address = guest1.tbl_guest_info.Address;
                    ViewBag.ArrivalDate = guest1.Arrival_Date;
                    ViewBag.DepartureDate = guest1.Departure_Date;
                    ViewBag.dnr = guest1.tbl_guest_info.DNR ?? false;

                    Session["Guest"] = guest1;
                    int noOfNight = StayDayCalculator(guest1.Arrival_Date, DateTime.Now);

                    ViewBag.NoOfNight = noOfNight;
                    if (noOfNight == 0)
                    {
                        ViewBag.NoOfNight = 1;
                    }

                    //Session["NoOfNight"] = ViewBag.NoOfNight;
                    var roomChargesS = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    (DbFunctions.AddHours(a.Tran_Date, gracePeriod) < DateTime.Now ||
                                     DbFunctions.TruncateTime(a.Tran_Date) == guest1.Arrival_Date.Date) &&
                                    (a.Tran_Ref_ID == 4)).ToList();
                    var roomChargesM = db.tbl_room_transactions
                        .Where(a => a.Guest_ID == (guest1.Guest_ID) && a.Tran_Date >= (guest1.Arrival_Date) &&
                                    (a.Tran_Ref_ID == 4)).ToList();

                    var vats = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    (DbFunctions.AddHours(a.Tran_Date, gracePeriod) < DateTime.Now ||
                                     DbFunctions.TruncateTime(a.Tran_Date) == guest1.Arrival_Date.Date) &&
                                    (a.Tran_Ref_ID == 2)).ToList();

                    var scs = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    (DbFunctions.AddHours(a.Tran_Date, gracePeriod) < DateTime.Now ||
                                     DbFunctions.TruncateTime(a.Tran_Date) == guest1.Arrival_Date.Date) &&
                                    (a.Tran_Ref_ID == 17)).ToList();

                    ViewBag.RoomChargeListS = roomChargesS;
                    ViewBag.RoomChargeListM = roomChargesM;

                    //Print Invoice Begin                    
                    var roomListS = db.tbl_guest_stay_info
                        .Where(a => a.Guest_ID == (guest1.Guest_ID) && a.Room_ID == rid.Room_ID && a.Status == "CheckIn")
                        .Include(a => a.tbl_room_info)
                        .OrderBy(x => x.tbl_room_info.Rate)
                        .ToList();
                    var roomListM = db.tbl_guest_stay_info
                        .Where(a => a.Guest_ID == (guest1.Guest_ID) && a.Status == "CheckIn")
                        .Include(a => a.tbl_room_info)
                        .OrderBy(x => x.tbl_room_info.Rate)
                        .ToList();
                    ViewBag.RoomListS = roomListS;
                    ViewBag.RoomListM = roomListM;
                    //Print Invoice End


                    if (roomChargesS != null)
                    {
                        foreach (var item in roomChargesS)
                        {
                            roomCharge += (decimal)item.Debit;
                        }
                    }
                    ViewBag.RoomChargeS = roomCharge.ToString("F", CultureInfo.InvariantCulture);

                    if (vats != null)
                    {
                        foreach (var item in vats)
                        {
                            vat += (decimal)item.Debit;
                        }
                    }
                    ViewBag.Vat = vat.ToString("F", CultureInfo.InvariantCulture);


                    if (scs != null)
                    {
                        foreach (var item in scs)
                        {
                            sc += (decimal)item.Debit;
                        }
                    }
                    ViewBag.SC = sc.ToString("F", CultureInfo.InvariantCulture);




                    var otherBillsS = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    DbFunctions.TruncateTime(a.Tran_Date) <= DateTime.Today.Date &&
                                    (a.Tran_Ref_ID == 5)).ToList();
                    ViewBag.OtherBillListS = otherBillsS;
                    if (otherBillsS != null)
                    {
                        foreach (var item in otherBillsS)
                        {
                            otherBill += (decimal)item.Debit;
                        }
                    }
                    ViewBag.OtherBillS = otherBill.ToString("F", CultureInfo.InvariantCulture);



                    var refunds = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    DbFunctions.TruncateTime(a.Tran_Date) <= DateTime.Today.Date &&
                                    (a.Tran_Ref_ID == 7)).ToList();
                    if (refunds != null)
                    {
                        foreach (var item in refunds)
                        {
                            refund += (decimal)item.Credit;
                        }
                    }
                    ViewBag.refund = refund.ToString("F", CultureInfo.InvariantCulture);

                    var discounts = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    DbFunctions.TruncateTime(a.Tran_Date) <= DateTime.Today.Date &&
                                    (a.Tran_Ref_ID == 1)).ToList();
                    ViewBag.DiscountList = discounts;
                    if (discounts != null && discounts.Count != 0)
                    {
                        foreach (var item in discounts)
                        {
                            discount += (decimal)item.Credit;
                        }
                    }
                    ViewBag.Discount = discount.ToString("F", CultureInfo.InvariantCulture);

                    var paidTransactionsS = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                                    (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 14)).ToList();
                    ViewBag.paidTransactionsS = paidTransactionsS;
                    if (paidTransactionsS != null)
                    {
                        foreach (var item in paidTransactionsS)
                        {
                            paid += (decimal)item.Credit;
                        }
                    }
                    ViewBag.PaidS = paid.ToString("F", CultureInfo.InvariantCulture);

                    decimal roomChargeWithVatSc = (roomCharge + vat + sc);
                    ViewBag.RoomChargeWithVatSc = roomChargeWithVatSc.ToString("F", CultureInfo.InvariantCulture);
                    decimal roomTotalCharge = (roomChargeWithVatSc + otherBill) - discount;
                    ViewBag.RoomTotalCharge = roomTotalCharge.ToString("F", CultureInfo.InvariantCulture);
                    decimal roomTotalChargeRounded = Rounder.Round(roomTotalCharge);
                    ViewBag.RoomTotalChargeRounded = roomTotalChargeRounded.ToString("F", CultureInfo.InvariantCulture);
                    roomDue = roomTotalChargeRounded - paid;
                    ViewBag.RoomDueS = roomDue.ToString("F", CultureInfo.InvariantCulture);
                    ViewBag.RoomDueRounded = roomDue.ToString("F", CultureInfo.InvariantCulture);

                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    ViewBag.VATRegistrationNumber = rpd.VATRegistrationNumber;
                    ViewBag.InvoiceNo = guest1.Stay_Info_ID;

                    ViewBagDiscountLevel();
                    ViewBag.Rno = id;
                    Session["rno"] = id;
                    Session["RoomDue"] = roomDue;
                    return PartialView();
                }
            }

            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckOut(string RoomOption, decimal? extraCharge, decimal? discountAdjustment, bool? latePayment, bool? outstandingPayment, bool? DNR, decimal? refund, decimal paid, string particulars, decimal netPayable)
        {
            if (ModelState.IsValid)
            {
                decimal roomDue = Convert.ToDecimal(Session["RoomDue"]);
                decimal netpayable = roomDue;
                if (discountAdjustment != null && discountAdjustment != 0)
                {
                    decimal discountLevel = ViewBagDiscountLevel();
                    decimal discountableAmount = (roomDue * discountLevel) / 100;
                    if (discountAdjustment > discountableAmount)
                    {
                        discountAdjustment = discountableAmount;
                    }
                }
                if (roomDue < -5 && Convert.ToDecimal(extraCharge) == 0)
                {
                    netpayable = Convert.ToDecimal(netpayable + Convert.ToDecimal(refund) -
                                                   Convert.ToDecimal(discountAdjustment));
                }
                else if (roomDue < 0 && Convert.ToDecimal(extraCharge) != 0)
                {
                    netpayable = Convert.ToDecimal(netpayable + Convert.ToDecimal(refund) +
                                                   Convert.ToDecimal(extraCharge) -
                                                   Convert.ToDecimal(discountAdjustment));
                }
                else
                {
                    netpayable = Convert.ToDecimal(Convert.ToDecimal(extraCharge) + netpayable -
                                                   Convert.ToDecimal(refund) - Convert.ToDecimal(discountAdjustment));
                }
                netPayable = netpayable;

                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    tbl_room_transactions paidTransaction1 = new tbl_room_transactions();
                    tbl_room_transactions discountTransaction = new tbl_room_transactions();
                    tbl_room_transactions extraChargeTransaction = new tbl_room_transactions();
                    tbl_room_transactions refundTransaction = new tbl_room_transactions();
                    tbl_room_transactions surplusTransaction = new tbl_room_transactions();
                    tbl_guest_attendee attendee = new tbl_guest_attendee();
                    attendee.Emp_ID = userId;
                    attendee.Purpose = "checkout";
                    attendee.Changed_Date = DateTime.Now;

                    var roomFromSession = (string)Session["rno"];
                    tbl_room_info rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(roomFromSession));
                    Session.Remove("rno");
                    if (rid != null)
                    {
                        int roomId = rid.Room_ID;
                        var guestStayInfo = db.tbl_guest_stay_info
                            .FirstOrDefault(a => a.Room_ID == roomId && a.Status == "CheckIn");
                        DateTime ltPaydate = DateTime.Today.Date;
                        ltPaydate = ltPaydate.AddMinutes(-5);
                        bool ltpay = false;
                        bool outPay = false;
                        bool hasOutPay = false;
                        string note = "";
                        if (guestStayInfo != null)
                        {
                            if (latePayment != null && latePayment == true && ltPaydate > guestStayInfo.Arrival_Date)
                            {
                                ltpay = true;
                            }
                            if (outstandingPayment != null && outstandingPayment == true)
                            {
                                outPay = true;
                            }

                            int guestId = guestStayInfo.tbl_guest_info.Guest_ID;
                            var guest = db.tbl_guest_info.Find(guestId);
                            if (guest != null)
                            {
                                decimal roomCount = db.tbl_guest_stay_info
                                    .Where(a => a.Guest_ID == guestId && a.Departure_Date >= DateTime.Today.Date &&
                                                a.Status == "CheckIn").ToList().Count;
                                var remainingRefund = refund;

                                if (RoomOption == "Multiple")
                                {
                                    var multiStay =
                                        db.tbl_guest_stay_info.Where(
                                            a => a.Room_ID != roomId && a.Guest_ID == guestId &&
                                                 a.Departure_Date >= DateTime.Today.Date && a.Status == "CheckIn");

                                    //if (multiStay != null)
                                    //{
                                    //    foreach (var item in multiStay)
                                    //    {
                                    //        item.Check_Out_Date = DateTime.Today.Date;
                                    //        item.Check_Out_Time = DateTime.Now.TimeOfDay;

                                    //        DateTime temp1 = item.Departure_Date;

                                    //        if (item.Check_Out_Date < temp1.Date)
                                    //        {
                                    //            item.Check_Out_Note = "Early Check out";
                                    //        }
                                    //        item.Status = "Clear";
                                    //        db.tbl_guest_stay_info.Attach(item);

                                    //        var updatedGuestInfo1 = db.Entry(item);
                                    //        updatedGuestInfo1.State = EntityState.Modified;

                                    //        //Transaction for multiple check out
                                    //        {
                                    //            decimal roomCharge = 0, vat = 0, prevPaid = 0, otherBill = 0;
                                    //            int mRoomId = (int)item.Room_ID;
                                    //            //var roomCharges = (db.tbl_room_transactions.Where(a => a.Guest_ID == guestId && a.Room_ID == mRoomId && (a.Tran_Ref_ID == 1 || a.Tran_Ref_ID == 4))).ToList();
                                    //            var roomCharges =
                                    //            (db.tbl_room_transactions.Where(
                                    //                a => a.Stay_Info_ID == guestStayInfo.Stay_Info_ID &&
                                    //                     (a.Tran_Ref_ID == 1 || a.Tran_Ref_ID == 4))).ToList();
                                    //            var dailyRoomCharge = item.tbl_guest_info.tbl_room_transactions
                                    //                .Where(a => a.Tran_Ref_ID == 1 || a.Tran_Ref_ID == 4)
                                    //                .FirstOrDefault();
                                    //            var dailyAmount = dailyRoomCharge.Debit;

                                    //            if (roomCharges != null)
                                    //            {
                                    //                foreach (var charge in roomCharges)
                                    //                {
                                    //                    roomCharge += (decimal)charge.Debit;
                                    //                }
                                    //            }
                                    //            //var vats = (db.tbl_room_transactions.Where(a => a.Guest_ID == guestId && a.Room_ID == mRoomId && (a.Tran_Ref_ID == 2))).ToList();
                                    //            var vats = (db.tbl_room_transactions.Where(
                                    //                    a => a.Stay_Info_ID == guestStayInfo.Stay_Info_ID &&
                                    //                         (a.Tran_Ref_ID == 2)))
                                    //                .ToList();

                                    //            if (vats != null)
                                    //            {
                                    //                foreach (var v in vats)
                                    //                {
                                    //                    vat += (decimal)v.Debit;
                                    //                }
                                    //            }
                                    //            //var otherBills = (db.tbl_room_transactions.Where(a => a.Guest_ID == guestId && a.Room_ID == mRoomId && (a.Tran_Ref_ID == 5))).ToList();
                                    //            var otherBills =
                                    //                (db.tbl_room_transactions.Where(
                                    //                    a => a.Stay_Info_ID == guestStayInfo.Stay_Info_ID &&
                                    //                         (a.Tran_Ref_ID == 5)))
                                    //                .ToList();

                                    //            if (otherBills != null)
                                    //            {
                                    //                foreach (var ob in otherBills)
                                    //                {
                                    //                    otherBill += (decimal)ob.Debit;
                                    //                }
                                    //            }

                                    //            var totalPayable = roomCharge + vat + otherBill;
                                    //            //var paidTransactions = (db.tbl_room_transactions.Where(a => a.Guest_ID == guestId && a.Room_ID == mRoomId && (a.Tran_Ref_ID == 3))).ToList();
                                    //            var paidTransactions =
                                    //                (db.tbl_room_transactions.Where(
                                    //                    a => a.Stay_Info_ID == guestStayInfo.Stay_Info_ID &&
                                    //                         (a.Tran_Ref_ID == 3)))
                                    //                .ToList();

                                    //            if (paidTransactions != null)
                                    //            {
                                    //                foreach (var p in paidTransactions)
                                    //                {
                                    //                    prevPaid += (decimal)p.Credit;
                                    //                }
                                    //            }

                                    //            decimal due = 0;

                                    //            if (discountAdjustment != 0)
                                    //            {
                                    //                due = Math.Round(totalPayable) - prevPaid -
                                    //                      (decimal)(discountAdjustment / roomCount);
                                    //            }
                                    //            else
                                    //            {
                                    //                due = Math.Round(totalPayable) - prevPaid;
                                    //            }

                                    //            tbl_room_transactions multiPay = new tbl_room_transactions();
                                    //            tbl_room_transactions discount = new tbl_room_transactions();

                                    //            if (paid != 0)
                                    //            {
                                    //                if (paid >= due)
                                    //                {
                                    //                    multiPay.Room_ID = item.Room_ID;
                                    //                    multiPay.Guest_ID = item.Guest_ID;
                                    //                    multiPay.Debit = 0;
                                    //                    multiPay.Credit = due;
                                    //                    multiPay.Tran_Date = DateTime.Now;
                                    //                    multiPay.Tran_Ref_ID = 3;
                                    //                    multiPay.Tran_Ref_Name = "paid";
                                    //                    multiPay.Particulars = particulars;
                                    //                    db.tbl_room_transactions.Add(multiPay);
                                    //                    paid = paid - due;
                                    //                }

                                    //                else
                                    //                {
                                    //                    multiPay.Room_ID = item.Room_ID;
                                    //                    multiPay.Guest_ID = item.Guest_ID;
                                    //                    multiPay.Debit = 0;
                                    //                    multiPay.Credit = paid;
                                    //                    multiPay.Tran_Date = DateTime.Now;
                                    //                    multiPay.Tran_Ref_ID = 3;
                                    //                    multiPay.Tran_Ref_Name = "paid";
                                    //                    multiPay.Particulars = particulars;
                                    //                    db.tbl_room_transactions.Add(multiPay);

                                    //                    tbl_room_transactions badDebt = new tbl_room_transactions();
                                    //                    badDebt.Room_ID = item.Room_ID;
                                    //                    badDebt.Stay_Info_ID = item.Stay_Info_ID;
                                    //                    badDebt.Guest_ID = item.Guest_ID;
                                    //                    badDebt.Debit = 0;
                                    //                    badDebt.Credit = due - paid;
                                    //                    badDebt.Tran_Date = DateTime.Now;
                                    //                    badDebt.Tran_Ref_ID = 8;
                                    //                    badDebt.Tran_Ref_Name = "badDebt";
                                    //                    badDebt.Particulars = particulars;
                                    //                    db.tbl_room_transactions.Add(badDebt);

                                    //                    paid = 0;
                                    //                }

                                    //                if (discountAdjustment != null && discountAdjustment != 0)
                                    //                {
                                    //                    discount.Room_ID = item.Room_ID;
                                    //                    discount.Guest_ID = item.Guest_ID;
                                    //                    discount.Credit = discountAdjustment / roomCount;
                                    //                    discount.Debit = 0;
                                    //                    discount.Tran_Date = DateTime.Now;
                                    //                    discount.Tran_Ref_ID = 1;
                                    //                    discount.Tran_Ref_Name = "Discount";
                                    //                    discount.Particulars = particulars;
                                    //                    db.tbl_room_transactions.Add(discount);
                                    //                }
                                    //            }

                                    //            else
                                    //            {
                                    //                multiPay.Room_ID = item.Room_ID;
                                    //                multiPay.Guest_ID = item.Stay_Info_ID;
                                    //                multiPay.Stay_Info_ID = item.Guest_ID;
                                    //                if (due <= 0 && (refund == null || refund == 0) &&
                                    //                    discountAdjustment != 0)
                                    //                {
                                    //                    multiPay.Debit = -due;
                                    //                    multiPay.Credit = 0;
                                    //                    multiPay.Tran_Ref_ID = 9;
                                    //                    multiPay.Tran_Ref_Name = "surPlus";
                                    //                }
                                    //                else if ((discountAdjustment == null || discountAdjustment == 0) &&
                                    //                         due > 0)
                                    //                {
                                    //                    multiPay.Debit = 0;
                                    //                    multiPay.Credit = due;
                                    //                    multiPay.Tran_Ref_ID = 8;
                                    //                    multiPay.Tran_Ref_Name = "badDebt";
                                    //                }
                                    //                if (discountAdjustment != null && discountAdjustment != 0)
                                    //                {
                                    //                    multiPay.Credit = discountAdjustment / roomCount;
                                    //                    multiPay.Debit = 0;
                                    //                    multiPay.Tran_Ref_ID = 1;
                                    //                    multiPay.Tran_Ref_Name = "Discount";
                                    //                }
                                    //                multiPay.Tran_Date = DateTime.Now;
                                    //                multiPay.Particulars = particulars;
                                    //                db.tbl_room_transactions.Add(multiPay);
                                    //            }

                                    //            if (refund != null && refund != 0)
                                    //            {
                                    //                var refundAmount = refund * (dailyAmount / roomCharge);

                                    //                tbl_room_transactions multiRefund = new tbl_room_transactions();
                                    //                multiRefund.Room_ID = item.Room_ID;
                                    //                multiRefund.Stay_Info_ID = item.Stay_Info_ID;
                                    //                multiRefund.Guest_ID = item.Guest_ID;
                                    //                multiRefund.Debit = 0;
                                    //                multiRefund.Credit = -refundAmount;
                                    //                multiRefund.Tran_Date = DateTime.Now;
                                    //                multiRefund.Tran_Ref_ID = 7;
                                    //                multiRefund.Tran_Ref_Name = "refund";
                                    //                multiRefund.Particulars = particulars;
                                    //                db.tbl_room_transactions.Add(multiRefund);

                                    //                remainingRefund = refund - refundAmount;
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                }



                                guestStayInfo.Check_Out_Date = DateTime.Today.Date;
                                guestStayInfo.Check_Out_Time = DateTime.Now.TimeOfDay;

                                guest.DNR = DNR;

                                int miniutes = (int)Math.Round((DateTime.Now - guestStayInfo.Arrival_Date).TotalMinutes);

                                if (guestStayInfo.Arrival_Date.Date == DateTime.Now.Date)
                                {
                                    note = "Entry Charge";
                                }
                                else if (miniutes / 1440 >= 1 && miniutes % 1440 > 180)
                                {
                                    note = "Running Charge";
                                }
                                guestStayInfo.Status = "Clear";
                                rid.Status = "Dirty";
                                if (paid != 0)
                                {
                                    if (paid < netPayable)
                                    {
                                        paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                        paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                        paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                        paidTransaction1.Debit = 0;
                                        paidTransaction1.Credit = paid;
                                        paidTransaction1.Tran_Date = DateTime.Now;
                                        paidTransaction1.Tran_Ref_ID = 3;
                                        paidTransaction1.Tran_Ref_Name = "paid";
                                        paidTransaction1.Particulars = particulars;
                                        paidTransaction1.Emp_ID = userId;
                                        if (ltpay)
                                        {
                                            paidTransaction1.Tran_Date = ltPaydate;
                                            paidTransaction1.Particulars = particulars + " Late_Payment";
                                        }
                                        if (!outPay)
                                        {
                                            paidTransaction1.Credit = netPayable - paid;
                                            paidTransaction1.Tran_Ref_ID = 8;
                                            paidTransaction1.Tran_Ref_Name = "badDebt";
                                        }
                                        else
                                        {
                                            paidTransaction1.Credit = netPayable - paid;
                                            paidTransaction1.Tran_Ref_ID = 11;
                                            paidTransaction1.Tran_Ref_Name = "OutStanding";
                                            hasOutPay = true;
                                        }
                                        db.tbl_room_transactions.Add(paidTransaction1);
                                    }
                                    else if (paid > netPayable)
                                    {
                                        paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                        paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                        paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                        paidTransaction1.Debit = 0;
                                        paidTransaction1.Credit = paid;
                                        paidTransaction1.Tran_Date = DateTime.Now;
                                        paidTransaction1.Tran_Ref_ID = 3;
                                        paidTransaction1.Tran_Ref_Name = "paid";
                                        paidTransaction1.Particulars = particulars;
                                        paidTransaction1.Emp_ID = userId;
                                        if (ltpay)
                                        {
                                            paidTransaction1.Tran_Date = ltPaydate;
                                            paidTransaction1.Particulars = particulars + " Late_Payment";
                                        }
                                        db.tbl_room_transactions.Add(paidTransaction1);


                                        surplusTransaction.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                        surplusTransaction.Room_ID = guestStayInfo.Room_ID;
                                        surplusTransaction.Guest_ID = guestStayInfo.Guest_ID;
                                        surplusTransaction.Debit = paid - netPayable;
                                        surplusTransaction.Credit = 0;
                                        surplusTransaction.Tran_Date = DateTime.Now;
                                        surplusTransaction.Tran_Ref_ID = 9;
                                        surplusTransaction.Tran_Ref_Name = "surPlus";
                                        surplusTransaction.Particulars = particulars;
                                        surplusTransaction.Emp_ID = userId;
                                        db.tbl_room_transactions.Add(surplusTransaction);
                                    }
                                    else
                                    {
                                        paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                        paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                        paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                        paidTransaction1.Debit = 0;
                                        paidTransaction1.Credit = paid;
                                        paidTransaction1.Tran_Date = DateTime.Now;
                                        paidTransaction1.Tran_Ref_ID = 3;
                                        paidTransaction1.Tran_Ref_Name = "paid";
                                        paidTransaction1.Particulars = particulars;
                                        paidTransaction1.Emp_ID = userId;
                                        if (ltpay)
                                        {
                                            paidTransaction1.Tran_Date = ltPaydate;
                                            paidTransaction1.Particulars = particulars + " Late_Payment";
                                        }
                                        db.tbl_room_transactions.Add(paidTransaction1);
                                    }
                                }
                                else if (paid == 0 && netPayable < -5 && refund != (-netPayable))
                                {
                                    paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                    paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                    paidTransaction1.Debit = -netPayable;
                                    paidTransaction1.Credit = 0;
                                    paidTransaction1.Tran_Date = DateTime.Now;
                                    paidTransaction1.Tran_Ref_ID = 9;
                                    paidTransaction1.Tran_Ref_Name = "surPlus";
                                    paidTransaction1.Particulars = particulars;
                                    paidTransaction1.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(paidTransaction1);
                                }
                                else if (paid == 0 && netPayable > 5 && outPay != true)
                                {
                                    paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                    paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                    paidTransaction1.Debit = 0;
                                    paidTransaction1.Credit = netPayable;
                                    paidTransaction1.Tran_Date = DateTime.Now;
                                    paidTransaction1.Tran_Ref_ID = 8;
                                    paidTransaction1.Tran_Ref_Name = "badDebt";
                                    paidTransaction1.Particulars = particulars;
                                    paidTransaction1.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(paidTransaction1);
                                }
                                else if (paid == 0 && netPayable > 5 && outPay == true)
                                {
                                    paidTransaction1.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    paidTransaction1.Room_ID = guestStayInfo.Room_ID;
                                    paidTransaction1.Guest_ID = guestStayInfo.Guest_ID;
                                    paidTransaction1.Debit = 0;
                                    paidTransaction1.Credit = netPayable;
                                    paidTransaction1.Tran_Date = DateTime.Now;
                                    paidTransaction1.Tran_Ref_ID = 11;
                                    paidTransaction1.Tran_Ref_Name = "OutStanding";
                                    paidTransaction1.Particulars = particulars;
                                    paidTransaction1.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(paidTransaction1);
                                    hasOutPay = true;
                                }

                                if (extraCharge != null && extraCharge != 0)
                                {
                                    extraChargeTransaction.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    extraChargeTransaction.Room_ID = guestStayInfo.Room_ID;
                                    extraChargeTransaction.Guest_ID = guestStayInfo.Guest_ID;
                                    extraChargeTransaction.Debit = extraCharge;
                                    extraChargeTransaction.Credit = 0;
                                    extraChargeTransaction.Tran_Date = DateTime.Now;
                                    extraChargeTransaction.Tran_Ref_ID = 6;
                                    extraChargeTransaction.Tran_Ref_Name = "extraCharge";
                                    extraChargeTransaction.Particulars = particulars;
                                    extraChargeTransaction.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(extraChargeTransaction);
                                }

                                if (refund != null && refund != 0)
                                {
                                    refundTransaction.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    refundTransaction.Room_ID = guestStayInfo.Room_ID;
                                    refundTransaction.Guest_ID = guestStayInfo.Guest_ID;
                                    refundTransaction.Debit = 0;
                                    refundTransaction.Credit = remainingRefund;
                                    refundTransaction.Tran_Date = DateTime.Now;
                                    refundTransaction.Tran_Ref_ID = 7;
                                    refundTransaction.Tran_Ref_Name = "refund";
                                    refundTransaction.Particulars = particulars;
                                    refundTransaction.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(refundTransaction);
                                }

                                if (discountAdjustment != null && discountAdjustment != 0)
                                {
                                    discountTransaction.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                    discountTransaction.Room_ID = guestStayInfo.Room_ID;
                                    discountTransaction.Guest_ID = guestStayInfo.Guest_ID;
                                    if (RoomOption == "Multiple")
                                    {
                                        discountTransaction.Credit = discountAdjustment / roomCount;
                                    }
                                    else
                                    {
                                        discountTransaction.Credit = discountAdjustment;
                                    }
                                    discountTransaction.Debit = 0;
                                    discountTransaction.Tran_Date = DateTime.Now;
                                    discountTransaction.Tran_Ref_ID = 1;
                                    discountTransaction.Tran_Ref_Name = "Discount";
                                    discountTransaction.Particulars = particulars;
                                    discountTransaction.Emp_ID = userId;
                                    db.tbl_room_transactions.Add(discountTransaction);
                                }
                                if (note.Length > 1 && hasOutPay == true)
                                {
                                    note += " Outstanding";
                                }
                                else if (note.Length <= 1 && hasOutPay == true)
                                {
                                    note = "Outstanding";
                                }
                                if (note.Length > 1)
                                {
                                    guestStayInfo.Check_Out_Note = note;
                                }
                                db.tbl_guest_stay_info.Attach(guestStayInfo);
                                var updatedGuestStayInfo = db.Entry(guestStayInfo);
                                updatedGuestStayInfo.State = EntityState.Modified;

								db.tbl_guest_info.Attach(guest);
                                var updatedGuestInfo = db.Entry(guest);
                                updatedGuestInfo.State = EntityState.Modified;
								
                                db.tbl_room_info.Attach(rid);
                                var updatedRoomInfo = db.Entry(rid);
                                updatedRoomInfo.Property(a => a.Status).IsModified = true;

                                attendee.Stay_Info_ID = guestStayInfo.Stay_Info_ID;
                                db.tbl_guest_attendee.Add(attendee);
                                db.SaveChanges();

                                if (true)
                                {
                                    CommonRepoprtData rpd = new CommonRepoprtData();
                                    string message = "আমাদের সাথে সাথে থাকার জন্য ধন্যবাদ । পরবর্তীতে আসার সময় অনুগ্রহ করে আপনার ভোটার আইডি কার্ডটি নিয়ে আসবেন। - " + rpd.PropertyName;

                                    SendSms sendSms = new SendSms(guestStayInfo.tbl_guest_info.Phone, message);
                                    //if (!string.IsNullOrEmpty(tbl_guest_stay_info.tbl_guest_info.Email))
                                    //{
                                    //    SendEmail sendEmail = new SendEmail(tbl_guest_stay_info.tbl_guest_info.Email,
                                    //        "Reservation Confirmation", rid.Room_ID,
                                    //        tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date, count,
                                    //        Convert.ToDecimal(paid), 1,
                                    //        (Convert.ToInt32(tbl_guest_stay_info.No_of_Adult) +
                                    //         Convert.ToInt32(tbl_guest_stay_info.No_of_Child)),
                                    //        tbl_guest_stay_info.tbl_guest_info.Name, tbl_guest_stay_info.tbl_guest_info.Address,
                                    //        tbl_guest_stay_info.tbl_guest_info.Phone, (gStay.Stay_Info_ID + 500));
                                    //}
                                }
                            }
                        }
                    }
                }
            }

            Session.Remove("Guest");
            Session.Remove("RoomDue");

            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpGet]
        public ActionResult ReservedCheckIn(string id)
        {
            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                var rate = Convert.ToDecimal(rid.Rate);
                ViewBag.RoomNo = id;
                int roomId = rid.Room_ID;
                DateTime selectedDate = DateTime.Today.Date;

                if (Session["date"] != null)
                {
                    selectedDate = Convert.ToDateTime(Session["date"]);
                }

                var guest1 = db.tbl_guest_stay_info
                    .FirstOrDefault(a => a.Room_ID == roomId && DbFunctions.TruncateTime(a.Arrival_Date) <= selectedDate &&
                             DbFunctions.TruncateTime(a.Departure_Date) >= selectedDate && a.Status == "Reserved");
                if (guest1 != null)
                {
                    if (guest1.Reserved_Date != null)
                    {
                        DateTime bookingDateTime = guest1.Reserved_Date.Value;
                        ViewBag.Booking_Date = bookingDateTime;
                    }

                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.National_ID))
                    {
                        ViewBag.NidFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Driving_License))
                    {
                        ViewBag.D_LicenseFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Tin_Certificate))
                    {
                        ViewBag.TinFlag = 1;
                    }
                    ViewBag.ReservedGuest = guest1;
                    int noOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.NoOfNight = noOfNight;

                    ViewBagCommonRoomData(rate);

                    Session["ReservedRoomID"] = rid.Room_ID;
                    Session["ReservedGuest"] = guest1;
                    Session["ReservedStayInfoId"] = guest1.Stay_Info_ID;

                    ViewBagDiscountLevel();
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReservedCheckIn(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, string feet, string inch, decimal? discount, decimal? paid, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    int roomId = (int)Session["ReservedRoomID"];
                    int stayInfoId = (int)Session["ReservedStayInfoId"];

                    var rid = db.tbl_room_info.Find(roomId);
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                        DateTime selectedDate = DateTime.Today.Date;
                        if (Session["date"] != null)
                        {
                            selectedDate = Convert.ToDateTime(Session["date"]);
                        }

                        var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayInfoId);
                        if (guest1?.Guest_ID != null)
                        {
                            var guestId = (int)guest1.Guest_ID;
                            var guest_info = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == guestId);
                            if (guest_info != null)
                            {
                                tbl_guest_attendee attendee = new tbl_guest_attendee();
                                attendee.Emp_ID = userId;
                                attendee.Changed_Date = DateTime.Now;
                                attendee.Purpose = "reserved_to_checkin";
                                attendee.Stay_Info_ID = stayInfoId;
                                db.tbl_guest_attendee.Add(attendee);

                                int count = StayDayCalculator(tbl_guest_stay_info.Arrival_Date,
                                    tbl_guest_stay_info.Departure_Date);
                                if (count == 0)
                                {
                                    count = 1;
                                }
                                int temp = count;
                                if (commonRoomData.VatPercent > 0)
                                {
                                    temp += count;
                                }
                                if (commonRoomData.ScPercent > 0)
                                {
                                    temp += count;
                                }

                                string st = tbl_guest_stay_info.tbl_guest_info.Signature;
                                if (!string.IsNullOrEmpty(st))
                                {
                                    tbl_guest_stay_info.tbl_guest_info.Signature = st;
                                }
                                if (tbl_guest_stay_info.Arrival_Date.Date != DateTime.Today.Date)
                                {
                                    ViewBag.FutureDateError = "Future Date Cann't Be Selected As Arrival Date In CheckIn";
                                    return View(tbl_guest_stay_info);
                                }
                                guest1.Status = "CheckIn";
                                guest1.Arrival_Date = tbl_guest_stay_info.Arrival_Date;
                                guest1.Departure_Date = tbl_guest_stay_info.Departure_Date;
                                guest1.No_of_Child = tbl_guest_stay_info.No_of_Child;
                                guest1.No_of_Adult = tbl_guest_stay_info.No_of_Adult;
                                guest1.Image = tbl_guest_stay_info.Image;
                                db.Entry(guest1).State = EntityState.Modified;


                                //Guest Info Edit
                                {
                                    guest_info.Name = guestA.Name;
                                    guest_info.Father_Name = guestA.Father_Name;
                                    guest_info.Mother_Name = guestA.Mother_Name;
                                    guest_info.Maritial_Status = guestA.Maritial_Status;
                                    guest_info.Spouse_Name = guestA.Spouse_Name;
                                    guest_info.Phone = guestA.Phone;
                                    guest_info.AdditionalPhone = guestA.AdditionalPhone;
                                    guest_info.Address = guestA.Address;
                                    guest_info.Permanent_Address = guestA.Permanent_Address;
                                    guest_info.Email = guestA.Email;
                                    guest_info.National_ID = guestA.National_ID;
                                    guest_info.Driving_License = guestA.Driving_License;
                                    guest_info.Tin_Certificate = guestA.Tin_Certificate;
                                    guest_info.Nationality = guestA.Nationality;
                                    guest_info.Purpose_of_visit = guestA.Purpose_of_visit;
                                    guest_info.Passport_No = guestA.Passport_No;
                                    guest_info.Country_ID = guestA.Country_ID;
                                    guest_info.Visa_No = guestA.Visa_No;
                                    guest_info.Visa_Date = guestA.Visa_Date;
                                    guest_info.Arrived_From = guestA.Arrived_From;
                                    guest_info.Going_To = guestA.Going_To;
                                    guest_info.Date_of_Arr_in_Country = guestA.Date_of_Arr_in_Country;
                                    guest_info.Age = guestA.Age;
                                    guest_info.Identification_Mark = guestA.Identification_Mark;
                                    guest_info.Signature = tbl_guest_stay_info.tbl_guest_info.Signature;
                                    guest_info.Height = feet + " feet " + inch + " inch";
                                    guest_info.Skin_Color = guestA.Skin_Color;
                                    guest_info.Occupation = guestA.Occupation;
                                    guest_info.Nearest_Relative_Contact_Info = guestA.Nearest_Relative_Contact_Info;
                                    guest_info.Special_Preference = guestA.Special_Preference;
                                    guest_info.Birth_Date = guestA.Birth_Date;
                                    guest_info.VIP = guestA.VIP;
                                    guest_info.DNR = guestA.DNR;
                                }
                                db.Entry(guest_info).State = EntityState.Modified;


                                //create rows from transaction begin

                                tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                                for (int i = 0; i < temp; i++)
                                {
                                    transactions[i] = new tbl_room_transactions();
                                }
                                tbl_room_transactions paidTransaction = new tbl_room_transactions();
                                tbl_room_transactions discountTransaction = new tbl_room_transactions();
                                foreach (var item in transactions)
                                {
                                    item.Room_ID = roomId;
                                    item.Guest_ID = guestId;
                                    item.Stay_Info_ID = stayInfoId;
                                    item.Emp_ID = userId;
                                }
                                paidTransaction.Room_ID = roomId;
                                paidTransaction.Guest_ID = guestId;
                                paidTransaction.Stay_Info_ID = stayInfoId;
                                paidTransaction.Emp_ID = userId;

                                discountTransaction.Room_ID = roomId;
                                discountTransaction.Guest_ID = guestId;
                                discountTransaction.Stay_Info_ID = stayInfoId;
                                discountTransaction.Emp_ID = userId;

                                //create rows from transaction end


                                DateTime initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);

                                //transactions rows insert begin

                                for (int i = 0; i < temp;)
                                {
                                    transactions[i].Tran_Ref_ID = 4;
                                    transactions[i].Tran_Ref_Name = "Regular Charge";
                                    transactions[i].Debit = rate;
                                    transactions[i].Credit = 0;
                                    transactions[i].Particulars = particulars;
                                    transactions[i].Tran_Date = initialdate;
                                    db.tbl_room_transactions.Add(transactions[i]);

                                    i++;
                                    if (commonRoomData.VatPercent > 0)
                                    {
                                        transactions[i].Tran_Ref_ID = 2;
                                        transactions[i].Tran_Ref_Name = "Vat";
                                        transactions[i].Debit = commonRoomData.Vat;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);
                                        i++;
                                    }
                                    if (commonRoomData.ScPercent > 0)
                                    {
                                        transactions[i].Tran_Ref_ID = 17;
                                        transactions[i].Tran_Ref_Name = "SC";
                                        transactions[i].Debit = commonRoomData.SC;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);
                                        i++;
                                    }

                                    initialdate = NextBillingDateTime(initialdate);
                                }

                                initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                if (discount != null && discount != 0)
                                {
                                    decimal discountLevel = ViewBagDiscountLevel();
                                    decimal discountableAmount = (Convert.ToDecimal(commonRoomData.PerDayCharge * count) * discountLevel) / 100;
                                    if (discount > discountableAmount)
                                    {
                                        discount = discountableAmount;
                                    }

                                    discountTransaction.Debit = 0;
                                    discountTransaction.Credit = discount;
                                    discountTransaction.Tran_Ref_ID = 1;
                                    discountTransaction.Tran_Ref_Name = "Discount";
                                    discountTransaction.Tran_Date = initialdate;
                                    discountTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(discountTransaction);
                                }

                                if (paid != null && paid != 0)
                                {
                                    paidTransaction.Debit = 0;
                                    paidTransaction.Credit = paid;
                                    paidTransaction.Tran_Date = initialdate;
                                    paidTransaction.Tran_Ref_ID = 3;
                                    paidTransaction.Tran_Ref_Name = "paid";
                                    paidTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(paidTransaction);
                                }
                                //transactions rows insert end

                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            Session.Remove("ReservedRoomID");
            Session.Remove("ReservedGuest");
            Session.Remove("ReservedStayInfoId");
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpGet]
        public ActionResult EditReservation(int id)
        {
            var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id && a.Status == "Reserved");
            decimal rate = 0;
            if (guest1 != null)
            {
                var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == guest1.Room_ID);
                if (rid != null)
                {
                    rate = Convert.ToDecimal(rid.Rate);
                    ViewBag.RoomNo = rid.Room_No;
                    int roomId = rid.Room_ID;
                    if (guest1.Reserved_Date != null)
                    {
                        DateTime bookingDateTime = guest1.Reserved_Date.Value;
                        ViewBag.Booking_Date = bookingDateTime;
                    }

                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.National_ID))
                    {
                        ViewBag.NidFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Driving_License))
                    {
                        ViewBag.D_LicenseFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Tin_Certificate))
                    {
                        ViewBag.TinFlag = 1;
                    }
                    ViewBag.ReservedGuest = guest1;
                    double noOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.NoOfNight = noOfNight;


                    ViewBagCommonRoomData(rate);
                    Session["ReservedRoomID"] = rid.Room_ID;
                    Session["ReservedGuest"] = guest1;
                    Session["ReservedStayInfoId"] = guest1.Stay_Info_ID;
                    ViewBagDiscountLevel();
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditReservation(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, string feet, string inch, decimal? discount, decimal? paid, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    int roomId = (int)Session["ReservedRoomID"];
                    int stayInfoId = (int)Session["ReservedStayInfoId"];

                    var rid = db.tbl_room_info.Find(roomId);
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                        DateTime selectedDate = DateTime.Today.Date;
                        if (Session["date"] != null)
                        {
                            selectedDate = Convert.ToDateTime(Session["date"]);
                        }

                        var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayInfoId);
                        if (guest1?.Guest_ID != null)
                        {
                            var guestId = (int)guest1.Guest_ID;
                            var guest_info = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == guestId);
                            if (guest_info != null)
                            {
                                tbl_guest_attendee attendee = new tbl_guest_attendee();
                                attendee.Emp_ID = userId;
                                attendee.Changed_Date = DateTime.Now;
                                attendee.Purpose = "edit_reservation";
                                attendee.Stay_Info_ID = stayInfoId;

                                int count = StayDayCalculator(tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date);
                                if (count == 0)
                                {
                                    count = 1;
                                }
                                int temp = count;
                                if (commonRoomData.VatPercent > 0)
                                {
                                    temp += count;
                                }
                                if (commonRoomData.ScPercent > 0)
                                {
                                    temp += count;
                                }

                                bool hasPaid = false;
                                if ((paid != null && paid != 0) || (discount != null && discount != 0))
                                {
                                    hasPaid = true;
                                }

                                string st = tbl_guest_stay_info.tbl_guest_info.Signature;
                                if (!string.IsNullOrEmpty(st))
                                {
                                    tbl_guest_stay_info.tbl_guest_info.Signature = st;
                                }

                                if (hasPaid)
                                {
                                    guest1.Status = "Booked";
                                    guest1.Booking_Date = DateTime.Now;
                                    attendee.Purpose = "reserved_to_booking";
                                }
                                db.tbl_guest_attendee.Add(attendee);

                                guest1.Arrival_Date = tbl_guest_stay_info.Arrival_Date;
                                guest1.Departure_Date = tbl_guest_stay_info.Departure_Date;
                                guest1.No_of_Child = tbl_guest_stay_info.No_of_Child;
                                guest1.No_of_Adult = tbl_guest_stay_info.No_of_Adult;
                                guest1.Image = tbl_guest_stay_info.Image;
                                guest1.Airport_Pickup = tbl_guest_stay_info.Airport_Pickup;
                                guest1.Special_Request = tbl_guest_stay_info.Special_Request;
                                guest1.Transportation = tbl_guest_stay_info.Transportation;
                                db.Entry(guest1).State = EntityState.Modified;


                                //Guest Info Edit
                                {
                                    guest_info.Name = guestA.Name;
                                    guest_info.Father_Name = guestA.Father_Name;
                                    guest_info.Mother_Name = guestA.Mother_Name;
                                    guest_info.Maritial_Status = guestA.Maritial_Status;
                                    guest_info.Spouse_Name = guestA.Spouse_Name;
                                    guest_info.Phone = guestA.Phone;
                                    guest_info.AdditionalPhone = guestA.AdditionalPhone;
                                    guest_info.Address = guestA.Address;
                                    guest_info.Permanent_Address = guestA.Permanent_Address;
                                    guest_info.Email = guestA.Email;
                                    guest_info.National_ID = guestA.National_ID;
                                    guest_info.Driving_License = guestA.Driving_License;
                                    guest_info.Tin_Certificate = guestA.Tin_Certificate;
                                    guest_info.Nationality = guestA.Nationality;
                                    guest_info.Purpose_of_visit = guestA.Purpose_of_visit;
                                    guest_info.Passport_No = guestA.Passport_No;
                                    guest_info.Country_ID = guestA.Country_ID;
                                    guest_info.Visa_No = guestA.Visa_No;
                                    guest_info.Visa_Date = guestA.Visa_Date;
                                    guest_info.Arrived_From = guestA.Arrived_From;
                                    guest_info.Going_To = guestA.Going_To;
                                    guest_info.Date_of_Arr_in_Country = guestA.Date_of_Arr_in_Country;
                                    guest_info.Age = guestA.Age;
                                    guest_info.Identification_Mark = guestA.Identification_Mark;
                                    guest_info.Signature = tbl_guest_stay_info.tbl_guest_info.Signature;
                                    guest_info.Height = feet + " feet " + inch + " inch";
                                    guest_info.Skin_Color = guestA.Skin_Color;
                                    guest_info.Occupation = guestA.Occupation;
                                    guest_info.Nearest_Relative_Contact_Info = guestA.Nearest_Relative_Contact_Info;
                                    guest_info.Special_Preference = guestA.Special_Preference;
                                    guest_info.Birth_Date = guestA.Birth_Date;
                                    guest_info.VIP = guestA.VIP;
                                    guest_info.DNR = guestA.DNR;
                                }
                                db.Entry(guest_info).State = EntityState.Modified;

                                if (hasPaid)
                                {
                                    //create rows from transaction begin

                                    tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                                    for (int i = 0; i < temp; i++)
                                    {
                                        transactions[i] = new tbl_room_transactions();
                                    }
                                    tbl_room_transactions paidTransaction = new tbl_room_transactions();
                                    tbl_room_transactions discountTransaction = new tbl_room_transactions();
                                    foreach (var item in transactions)
                                    {
                                        item.Room_ID = roomId;
                                        item.Guest_ID = guestId;
                                        item.Stay_Info_ID = stayInfoId;
                                        item.Emp_ID = userId;
                                    }
                                    paidTransaction.Room_ID = roomId;
                                    paidTransaction.Guest_ID = guestId;
                                    paidTransaction.Stay_Info_ID = stayInfoId;
                                    paidTransaction.Emp_ID = userId;

                                    discountTransaction.Room_ID = roomId;
                                    discountTransaction.Guest_ID = guestId;
                                    discountTransaction.Stay_Info_ID = stayInfoId;
                                    discountTransaction.Emp_ID = userId;

                                    //create rows from transaction end

                                    DateTime initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);

                                    //transactions rows insert begin

                                    for (int i = 0; i < temp;)
                                    {
                                        transactions[i].Tran_Ref_ID = 4;
                                        transactions[i].Tran_Ref_Name = "Regular Charge";
                                        transactions[i].Debit = rate;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);

                                        i++;
                                        if (commonRoomData.VatPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 2;
                                            transactions[i].Tran_Ref_Name = "Vat";
                                            transactions[i].Debit = commonRoomData.Vat;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        if (commonRoomData.ScPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 17;
                                            transactions[i].Tran_Ref_Name = "SC";
                                            transactions[i].Debit = commonRoomData.SC;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        initialdate = NextBillingDateTime(initialdate);
                                    }

                                    initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                    if (discount != null && discount != 0)
                                    {
                                        decimal discountLevel = ViewBagDiscountLevel();
                                        decimal discountableAmount = (Convert.ToDecimal(commonRoomData.PerDayCharge * count) * discountLevel) / 100;
                                        if (discount > discountableAmount)
                                        {
                                            discount = discountableAmount;
                                        }

                                        discountTransaction.Debit = 0;
                                        discountTransaction.Credit = discount;
                                        discountTransaction.Tran_Ref_ID = 1;
                                        discountTransaction.Tran_Ref_Name = "Discount";
                                        discountTransaction.Tran_Date = initialdate;
                                        discountTransaction.Particulars = particulars;
                                        db.tbl_room_transactions.Add(discountTransaction);
                                    }

                                    if (paid != null && paid != 0)
                                    {
                                        paidTransaction.Debit = 0;
                                        paidTransaction.Credit = paid;
                                        paidTransaction.Tran_Date = initialdate;
                                        paidTransaction.Tran_Ref_ID = 3;
                                        paidTransaction.Tran_Ref_Name = "paid";
                                        paidTransaction.Particulars = particulars;
                                        db.tbl_room_transactions.Add(paidTransaction);
                                    }
                                    //transactions rows insert end
                                }


                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            Session.Remove("ReservedRoomID");
            Session.Remove("ReservedGuest");
            Session.Remove("ReservedStayInfoId");
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpGet]
        public ActionResult CancelReservation(string id)
        {
            ViewBag.Guest_ID = new SelectList(db.tbl_guest_info, "Guest_ID", "Name");
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No");

            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                ViewBag.RoomNo = id;
                int roomId = rid.Room_ID;
                DateTime selectedDate = DateTime.Today.Date;

                if (Session["date"] != null)
                {
                    selectedDate = Convert.ToDateTime(Session["date"]);
                }
                var guest1 =
                    db.tbl_guest_stay_info.FirstOrDefault(
                        a => a.Room_ID == roomId && DbFunctions.TruncateTime(a.Arrival_Date) <= selectedDate &&
                             DbFunctions.TruncateTime(a.Departure_Date) >= selectedDate && a.Status == "Reserved");
                if (guest1 != null)
                {
                    Session["ReservedRoomCancel"] = rid;
                    Session["ReservedGuest"] = guest1;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelReservation()
        {
            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                int userId = tblEmpInfo.Emp_ID;
                tbl_guest_attendee attendee = new tbl_guest_attendee();
                attendee.Emp_ID = userId;
                attendee.Purpose = "reserve_cancel";
                attendee.Changed_Date = DateTime.Now;

                ViewBag.Guest_ID = new SelectList(db.tbl_guest_info, "Guest_ID", "Name");
                ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No");

                var rid = (tbl_room_info)Session["ReservedRoomCancel"];
                var guest1 = (tbl_guest_stay_info)Session["ReservedGuest"];
                if (guest1 != null)
                {
                    attendee.Stay_Info_ID = guest1.Stay_Info_ID;
                    guest1.Status = "Clear";
                    guest1.Check_Out_Note = "Reserved_Cancel";

                    db.tbl_guest_stay_info.Attach(guest1);
                    var updatedGuestInfo = db.Entry(guest1);
                    updatedGuestInfo.Property(a => a.Status).IsModified = true;
                    updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;

                    db.tbl_guest_attendee.Add(attendee);
                    db.SaveChanges();
                }
            }
            Session.Remove("ReservedRoomCancel");
            Session.Remove("ReservedGuest");
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpGet]
        public ActionResult BookedCheckIn(string id)
        {
            decimal rate = 0, previousPaid = 0, discount = 0;
            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                rate = Convert.ToDecimal(rid.Rate);
                ViewBag.RoomNo = id;
                int roomId = rid.Room_ID;
                DateTime selectedDate = DateTime.Today.Date;

                if (Session["date"] != null)
                {
                    selectedDate = Convert.ToDateTime(Session["date"]);
                }
                var guest1 = db.tbl_guest_stay_info
                    .FirstOrDefault(a => a.Room_ID == roomId
                    && DbFunctions.TruncateTime(a.Arrival_Date) <= selectedDate &&
                             DbFunctions.TruncateTime(a.Departure_Date) >= selectedDate && a.Status == "Booked");
                if (guest1 != null)
                {
                    DateTime bookingDateTime = guest1.Booking_Date.Value;
                    ViewBag.Booking_Date = bookingDateTime;

                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.National_ID))
                    {
                        ViewBag.NidFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Driving_License))
                    {
                        ViewBag.D_LicenseFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Tin_Certificate))
                    {
                        ViewBag.TinFlag = 1;
                    }
                    ViewBag.BookedGuest = guest1;
                    int noOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.NoOfNight = noOfNight;

                    //var roomCharges =
                    //    (db.tbl_room_transactions.Where(
                    //        a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                    //             (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16)))
                    //    .ToList();
                    //if (roomCharges != null && roomCharges.Count > 0)
                    //{
                    //    foreach (var item in roomCharges)
                    //    {
                    //        roomCharge += Convert.ToDecimal(item.Debit);
                    //    }
                    //    ViewBag.RoomChargePerDay = roomCharges.FirstOrDefault().Debit;
                    //    ViewBag.RoomCharge = roomCharge;
                    //}

                    //var vats = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) &&
                    //                                                (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 15)))
                    //    .ToList();
                    //if (vats != null && vats.Count > 0)
                    //{
                    //    foreach (var item in vats)
                    //    {
                    //        vat += Convert.ToDecimal(item.Debit);
                    //    }
                    //    ViewBag.Vat = vat;
                    //    ViewBag.VatPercent = vats.FirstOrDefault().Debit;
                    //}

                    var paidTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 14))
                        .ToList();
                    if (paidTransactions != null && paidTransactions.Count > 0)
                    {
                        foreach (var item in paidTransactions)
                        {
                            previousPaid += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Paid = previousPaid;

                    var discountTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1))
                        .ToList();
                    if (discountTransactions != null && discountTransactions.Count > 0)
                    {
                        foreach (var item in discountTransactions)
                        {
                            discount += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Discount = discount;
                    ViewBagCommonRoomData(rate);

                    Session["BookedGuest"] = guest1;
                    Session["BookedRoomID"] = rid.Room_ID;
                    Session["BookedStayInfoId"] = guest1.Stay_Info_ID;

                    ViewBagDiscountLevel();
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BookedCheckIn(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, string feet, string inch, decimal? discount, decimal? paid, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    int roomId = (int)Session["BookedRoomID"];
                    int stayInfoId = (int)Session["BookedStayInfoId"];

                    DateTime selectedDate = DateTime.Today.Date;
                    if (Session["date"] != null)
                    {
                        selectedDate = Convert.ToDateTime(Session["date"]);
                    }
                    var rid = db.tbl_room_info.Find(roomId);
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                        var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayInfoId);
                        if (guest1?.Guest_ID != null)
                        {
                            var guestId = (int)guest1.Guest_ID;

                            var guest_info = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == guestId);
                            if (guest_info != null)
                            {
                                tbl_guest_attendee attendee = new tbl_guest_attendee();
                                attendee.Emp_ID = userId;
                                attendee.Changed_Date = DateTime.Now;
                                attendee.Purpose = "booked_to_checkin";
                                attendee.Stay_Info_ID = stayInfoId;
                                db.tbl_guest_attendee.Add(attendee);

                                int count = StayDayCalculator(tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date);
                                if (count == 0)
                                {
                                    count = 1;
                                }
                                int temp = count;
                                if (commonRoomData.VatPercent > 0)
                                {
                                    temp += count;
                                }
                                if (commonRoomData.ScPercent > 0)
                                {
                                    temp += count;
                                }
                                bool isStayDateChanged = true;
                                if (guest1.Arrival_Date == tbl_guest_stay_info.Arrival_Date && guest1.Departure_Date == tbl_guest_stay_info.Departure_Date)
                                {
                                    isStayDateChanged = false;
                                }
                                else
                                {
                                    isStayDateChanged = true;
                                }

                                string st = tbl_guest_stay_info.tbl_guest_info.Signature;
                                if (!string.IsNullOrEmpty(st))
                                {
                                    tbl_guest_stay_info.tbl_guest_info.Signature = st;
                                }
                                if (tbl_guest_stay_info.Arrival_Date.Date != DateTime.Today.Date)
                                {
                                    ViewBag.FutureDateError = "Future Date Cann't Be Selected As Arrival Date In CheckIn";
                                    return View(tbl_guest_stay_info);
                                }
                                guest1.Status = "CheckIn";
                                guest1.Arrival_Date = tbl_guest_stay_info.Arrival_Date;
                                guest1.Departure_Date = tbl_guest_stay_info.Departure_Date;
                                guest1.No_of_Child = tbl_guest_stay_info.No_of_Child;
                                guest1.No_of_Adult = tbl_guest_stay_info.No_of_Adult;
                                guest1.Image = tbl_guest_stay_info.Image;
                                db.Entry(guest1).State = EntityState.Modified;

                                //Guest Info Edit
                                {
                                    guest_info.Name = guestA.Name;
                                    guest_info.Father_Name = guestA.Father_Name;
                                    guest_info.Mother_Name = guestA.Mother_Name;
                                    guest_info.Maritial_Status = guestA.Maritial_Status;
                                    guest_info.Spouse_Name = guestA.Spouse_Name;
                                    guest_info.Phone = guestA.Phone;
                                    guest_info.AdditionalPhone = guestA.AdditionalPhone;
                                    guest_info.Address = guestA.Address;
                                    guest_info.Permanent_Address = guestA.Permanent_Address;
                                    guest_info.Email = guestA.Email;
                                    guest_info.National_ID = guestA.National_ID;
                                    guest_info.Driving_License = guestA.Driving_License;
                                    guest_info.Tin_Certificate = guestA.Tin_Certificate;
                                    guest_info.Nationality = guestA.Nationality;
                                    guest_info.Purpose_of_visit = guestA.Purpose_of_visit;
                                    guest_info.Passport_No = guestA.Passport_No;
                                    guest_info.Country_ID = guestA.Country_ID;
                                    guest_info.Visa_No = guestA.Visa_No;
                                    guest_info.Visa_Date = guestA.Visa_Date;
                                    guest_info.Arrived_From = guestA.Arrived_From;
                                    guest_info.Going_To = guestA.Going_To;
                                    guest_info.Date_of_Arr_in_Country = guestA.Date_of_Arr_in_Country;
                                    guest_info.Age = guestA.Age;
                                    guest_info.Identification_Mark = guestA.Identification_Mark;
                                    guest_info.Signature = tbl_guest_stay_info.tbl_guest_info.Signature;
                                    guest_info.Height = feet + " feet " + inch + " inch";
                                    guest_info.Skin_Color = guestA.Skin_Color;
                                    guest_info.Occupation = guestA.Occupation;
                                    guest_info.Nearest_Relative_Contact_Info = guestA.Nearest_Relative_Contact_Info;
                                    guest_info.Special_Preference = guestA.Special_Preference;
                                    guest_info.Birth_Date = guestA.Birth_Date;
                                    guest_info.VIP = guestA.VIP;
                                    guest_info.DNR = guestA.DNR;
                                }
                                db.Entry(guest_info).State = EntityState.Modified;


                                //create rows from transaction begin

                                tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                                for (int i = 0; i < temp; i++)
                                {
                                    transactions[i] = new tbl_room_transactions();
                                }
                                tbl_room_transactions paidTransaction = new tbl_room_transactions();
                                tbl_room_transactions discountTransaction = new tbl_room_transactions();
                                foreach (var item in transactions)
                                {
                                    item.Room_ID = roomId;
                                    item.Guest_ID = guestId;
                                    item.Stay_Info_ID = stayInfoId;
                                    item.Emp_ID = userId;
                                }
                                paidTransaction.Room_ID = roomId;
                                paidTransaction.Guest_ID = guestId;
                                paidTransaction.Stay_Info_ID = stayInfoId;
                                paidTransaction.Emp_ID = userId;

                                discountTransaction.Room_ID = roomId;
                                discountTransaction.Guest_ID = guestId;
                                discountTransaction.Stay_Info_ID = stayInfoId;
                                discountTransaction.Emp_ID = userId;

                                //create rows from transaction end      

                                DateTime initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                if (isStayDateChanged)
                                {
                                    //remove previous regular charge and vat begin
                                    var previousRegularVat = db.tbl_room_transactions
                                        .Where(a => a.Stay_Info_ID == stayInfoId &&
                                                    (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 15 ||
                                                     a.Tran_Ref_ID == 16 || a.Tran_Ref_ID == 17 ||
                                                     a.Tran_Ref_ID == 18))
                                        .ToList();
                                    if (previousRegularVat != null)
                                    {
                                        foreach (var item in previousRegularVat)
                                        {
                                            db.tbl_room_transactions.Remove(item);
                                        }
                                    }
                                    //remove previous regular charge and vat end


                                    //add new regular charge and vat begin
                                    for (int i = 0; i < temp;)
                                    {
                                        transactions[i].Tran_Ref_ID = 4;
                                        transactions[i].Tran_Ref_Name = "Regular Charge";
                                        transactions[i].Debit = rate;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);

                                        i++;
                                        if (commonRoomData.VatPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 2;
                                            transactions[i].Tran_Ref_Name = "Vat";
                                            transactions[i].Debit = commonRoomData.Vat;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        if (commonRoomData.ScPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 17;
                                            transactions[i].Tran_Ref_Name = "SC";
                                            transactions[i].Debit = commonRoomData.SC;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        initialdate = NextBillingDateTime(initialdate);
                                    }
                                    //add new regular charge and vat end
                                }

                                initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                //change datetime of discount rows given in the booking time begin
                                var previousDiscount = db.tbl_room_transactions
                                    .Where(a => a.Stay_Info_ID == stayInfoId && a.Tran_Ref_ID == 1).ToList();
                                if (previousDiscount != null)
                                {
                                    foreach (var item in previousDiscount)
                                    {
                                        item.Tran_Date = initialdate;
                                        db.Entry(item).State = EntityState.Modified;
                                    }
                                }
                                //change datetime of discount rows given in the booking time end


                                initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                if (discount != null && discount != 0)
                                {
                                    decimal discountLevel = ViewBagDiscountLevel();
                                    decimal discountableAmount = (Convert.ToDecimal(commonRoomData.PerDayCharge * count) * discountLevel) / 100;
                                    if (discount > discountableAmount)
                                    {
                                        discount = discountableAmount;
                                    }

                                    discountTransaction.Debit = 0;
                                    discountTransaction.Credit = discount;
                                    discountTransaction.Tran_Ref_ID = 1;
                                    discountTransaction.Tran_Ref_Name = "Discount";
                                    discountTransaction.Tran_Date = initialdate;
                                    discountTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(discountTransaction);
                                }

                                if (paid != null && paid != 0)
                                {
                                    paidTransaction.Debit = 0;
                                    paidTransaction.Credit = paid;
                                    paidTransaction.Tran_Date = initialdate;
                                    paidTransaction.Tran_Ref_ID = 3;
                                    paidTransaction.Tran_Ref_Name = "paid";
                                    paidTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(paidTransaction);
                                }
                                //transactions rows insert end
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            Session.Remove("BookedGuest");
            Session.Remove("BookedRoomID");
            Session.Remove("BookedStayInfoId");
            return RedirectToAction("Index", "FrontDesk");
        }


        [HttpGet]
        public ActionResult EditBooking(int id)
        {
            var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id && a.Status == "Booked");
            if (guest1 != null)
            {
                decimal rate = 0, previousPaid = 0, discount = 0;
                var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == guest1.Room_ID);
                if (rid != null)
                {
                    rate = Convert.ToDecimal(rid.Rate);
                    ViewBag.RoomNo = rid.Room_No;
                    int roomId = rid.Room_ID;
                    DateTime selectedDate = DateTime.Today.Date;

                    if (Session["date"] != null)
                    {
                        selectedDate = Convert.ToDateTime(Session["date"]);
                    }
                    DateTime bookingDateTime = guest1.Booking_Date.Value;
                    ViewBag.Booking_Date = bookingDateTime;

                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.National_ID))
                    {
                        ViewBag.NidFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Driving_License))
                    {
                        ViewBag.D_LicenseFlag = 1;
                    }
                    if (!string.IsNullOrEmpty(guest1.tbl_guest_info.Tin_Certificate))
                    {
                        ViewBag.TinFlag = 1;
                    }
                    ViewBag.BookedGuest = guest1;
                    int noOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.NoOfNight = noOfNight;

                    //var roomChargeTransactions = db.tbl_room_transactions
                    //    .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16))
                    //    .ToList();
                    //if (roomChargeTransactions != null && roomChargeTransactions.Count > 0)
                    //{
                    //    foreach (var item in roomChargeTransactions)
                    //    {
                    //        roomCharge += Convert.ToDecimal(item.Debit);
                    //    }                        
                    //}
                    //ViewBag.RoomCharge = roomCharge;

                    //var vatTransactions = db.tbl_room_transactions
                    //    .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 15))
                    //    .ToList();
                    //if (vatTransactions != null && vatTransactions.Count > 0)
                    //{
                    //    foreach (var item in vatTransactions)
                    //    {
                    //        previousVat += Convert.ToDecimal(item.Debit);
                    //    }                        
                    //}


                    //var scTransactions = db.tbl_room_transactions
                    //    .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 17 || a.Tran_Ref_ID == 18))
                    //    .ToList();
                    //if (scTransactions != null && scTransactions.Count > 0)
                    //{
                    //    foreach (var item in scTransactions)
                    //    {
                    //        previousSc += Convert.ToDecimal(item.Debit);
                    //    }
                    //}

                    var paidTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 14))
                        .ToList();
                    if (paidTransactions != null && paidTransactions.Count > 0)
                    {
                        foreach (var item in paidTransactions)
                        {
                            previousPaid += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Paid = previousPaid;

                    var discountTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1))
                        .ToList();
                    if (discountTransactions != null && discountTransactions.Count > 0)
                    {
                        foreach (var item in discountTransactions)
                        {
                            discount += Convert.ToDecimal(item.Credit);
                        }
                    }
                    ViewBag.Discount = discount;
                    ViewBagCommonRoomData(rate);


                    Session["BookedGuest"] = guest1;
                    Session["BookedRoomID"] = rid.Room_ID;
                    Session["BookedStayInfoId"] = guest1.Stay_Info_ID;
                    ViewBagDiscountLevel();
                    CommonRepoprtData rpd = new CommonRepoprtData();
                    ViewBag.PropertyName = rpd.PropertyName;
                    ViewBag.PropertyAddress = rpd.PropertyAddress;
                    ViewBag.PropertyPhone = rpd.PropertyPhone;
                    ViewBag.PropertyLogo = rpd.PropertyLogo;
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBooking(tbl_guest_stay_info tbl_guest_stay_info, tbl_guest_info guestA, string feet, string inch, decimal? discount, decimal? paid, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    int roomId = (int)Session["BookedRoomID"];
                    int stayInfoId = (int)Session["BookedStayInfoId"];

                    var rid = db.tbl_room_info.Find(roomId);
                    if (rid != null)
                    {
                        var rate = Convert.ToDecimal(rid.Rate);
                        CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                        var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayInfoId);
                        if (guest1?.Guest_ID != null)
                        {
                            var guestId = (int)guest1.Guest_ID;
                            var guest_info = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == guestId);
                            if (guest_info != null)
                            {
                                tbl_guest_attendee attendee = new tbl_guest_attendee();
                                attendee.Emp_ID = userId;
                                attendee.Changed_Date = DateTime.Now;
                                attendee.Purpose = "edit_booking";
                                attendee.Stay_Info_ID = stayInfoId;
                                db.tbl_guest_attendee.Add(attendee);

                                int count = StayDayCalculator(tbl_guest_stay_info.Arrival_Date, tbl_guest_stay_info.Departure_Date);
                                if (count == 0)
                                {
                                    count = 1;
                                }
                                int temp = count;
                                if (commonRoomData.VatPercent > 0)
                                {
                                    temp += count;
                                }
                                if (commonRoomData.ScPercent > 0)
                                {
                                    temp += count;
                                }
                                bool isStayDateChanged = true;
                                if (guest1.Arrival_Date == tbl_guest_stay_info.Arrival_Date && guest1.Departure_Date == tbl_guest_stay_info.Departure_Date)
                                {
                                    isStayDateChanged = false;
                                }
                                else
                                {
                                    isStayDateChanged = true;
                                }

                                string st = tbl_guest_stay_info.tbl_guest_info.Signature;
                                if (!string.IsNullOrEmpty(st))
                                {
                                    tbl_guest_stay_info.tbl_guest_info.Signature = st;
                                }
                                
                                guest1.Arrival_Date = tbl_guest_stay_info.Arrival_Date;
                                guest1.Departure_Date = tbl_guest_stay_info.Departure_Date;
                                guest1.No_of_Child = tbl_guest_stay_info.No_of_Child;
                                guest1.No_of_Adult = tbl_guest_stay_info.No_of_Adult;
                                guest1.Image = tbl_guest_stay_info.Image;
                                guest1.Airport_Pickup = tbl_guest_stay_info.Airport_Pickup;
                                guest1.Special_Request = tbl_guest_stay_info.Special_Request;
                                guest1.Transportation = tbl_guest_stay_info.Transportation;
                                db.Entry(guest1).State = EntityState.Modified;

                                //Guest Info Edit
                                {
                                    guest_info.Name = guestA.Name;
                                    guest_info.Father_Name = guestA.Father_Name;
                                    guest_info.Mother_Name = guestA.Mother_Name;
                                    guest_info.Maritial_Status = guestA.Maritial_Status;
                                    guest_info.Spouse_Name = guestA.Spouse_Name;
                                    guest_info.Phone = guestA.Phone;
                                    guest_info.AdditionalPhone = guestA.AdditionalPhone;
                                    guest_info.Address = guestA.Address;
                                    guest_info.Permanent_Address = guestA.Permanent_Address;
                                    guest_info.Email = guestA.Email;
                                    guest_info.National_ID = guestA.National_ID;
                                    guest_info.Driving_License = guestA.Driving_License;
                                    guest_info.Tin_Certificate = guestA.Tin_Certificate;
                                    guest_info.Nationality = guestA.Nationality;
                                    guest_info.Purpose_of_visit = guestA.Purpose_of_visit;
                                    guest_info.Passport_No = guestA.Passport_No;
                                    guest_info.Country_ID = guestA.Country_ID;
                                    guest_info.Visa_No = guestA.Visa_No;
                                    guest_info.Visa_Date = guestA.Visa_Date;
                                    guest_info.Arrived_From = guestA.Arrived_From;
                                    guest_info.Going_To = guestA.Going_To;
                                    guest_info.Date_of_Arr_in_Country = guestA.Date_of_Arr_in_Country;
                                    guest_info.Age = guestA.Age;
                                    guest_info.Identification_Mark = guestA.Identification_Mark;
                                    guest_info.Signature = tbl_guest_stay_info.tbl_guest_info.Signature;
                                    guest_info.Height = feet + " feet " + inch + " inch";
                                    guest_info.Skin_Color = guestA.Skin_Color;
                                    guest_info.Occupation = guestA.Occupation;
                                    guest_info.Nearest_Relative_Contact_Info = guestA.Nearest_Relative_Contact_Info;
                                    guest_info.Special_Preference = guestA.Special_Preference;
                                    guest_info.Birth_Date = guestA.Birth_Date;
                                    guest_info.VIP = guestA.VIP;
                                    guest_info.DNR = guestA.DNR;
                                }
                                db.Entry(guest_info).State = EntityState.Modified;


                                //create rows from transaction begin

                                tbl_room_transactions[] transactions = new tbl_room_transactions[temp];
                                for (int i = 0; i < temp; i++)
                                {
                                    transactions[i] = new tbl_room_transactions();
                                }
                                tbl_room_transactions paidTransaction = new tbl_room_transactions();
                                tbl_room_transactions discountTransaction = new tbl_room_transactions();
                                foreach (var item in transactions)
                                {
                                    item.Room_ID = roomId;
                                    item.Guest_ID = guestId;
                                    item.Stay_Info_ID = stayInfoId;
                                    item.Emp_ID = userId;
                                }
                                paidTransaction.Room_ID = roomId;
                                paidTransaction.Guest_ID = guestId;
                                paidTransaction.Stay_Info_ID = stayInfoId;
                                paidTransaction.Emp_ID = userId;

                                discountTransaction.Room_ID = roomId;
                                discountTransaction.Guest_ID = guestId;
                                discountTransaction.Stay_Info_ID = stayInfoId;
                                discountTransaction.Emp_ID = userId;

                                //create rows from transaction end


                                DateTime initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);

                                if (isStayDateChanged)
                                {
                                    //change datetime of discount rows given in the booking time begin
                                    var previousDiscount = db.tbl_room_transactions
                                        .Where(a => a.Stay_Info_ID == stayInfoId && a.Tran_Ref_ID == 1).ToList();
                                    if (previousDiscount != null)
                                    {
                                        foreach (var item in previousDiscount)
                                        {
                                            item.Tran_Date = initialdate;
                                            db.Entry(item).State = EntityState.Modified;
                                        }
                                    }
                                    //change datetime of discount rows given in the booking time end


                                    //remove previous regular charge and vat begin
                                    var previousRegularVat = db.tbl_room_transactions
                                        .Where(a => a.Stay_Info_ID == stayInfoId &&
                                                    (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 15 ||
                                                     a.Tran_Ref_ID == 16 || a.Tran_Ref_ID == 17 ||
                                                     a.Tran_Ref_ID == 18))
                                        .ToList();
                                    if (previousRegularVat != null)
                                    {
                                        foreach (var item in previousRegularVat)
                                        {
                                            db.tbl_room_transactions.Remove(item);
                                        }
                                    }
                                    //remove previous regular charge and vat end


                                    //add new regular charge and vat begin
                                    for (int i = 0; i < temp;)
                                    {
                                        transactions[i].Tran_Ref_ID = 4;
                                        transactions[i].Tran_Ref_Name = "Regular Charge";
                                        transactions[i].Debit = rate;
                                        transactions[i].Credit = 0;
                                        transactions[i].Particulars = particulars;
                                        transactions[i].Tran_Date = initialdate;
                                        db.tbl_room_transactions.Add(transactions[i]);

                                        i++;
                                        if (commonRoomData.VatPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 2;
                                            transactions[i].Tran_Ref_Name = "Vat";
                                            transactions[i].Debit = commonRoomData.Vat;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        if (commonRoomData.ScPercent > 0)
                                        {
                                            transactions[i].Tran_Ref_ID = 17;
                                            transactions[i].Tran_Ref_Name = "SC";
                                            transactions[i].Debit = commonRoomData.SC;
                                            transactions[i].Credit = 0;
                                            transactions[i].Particulars = particulars;
                                            transactions[i].Tran_Date = initialdate;
                                            db.tbl_room_transactions.Add(transactions[i]);
                                            i++;
                                        }
                                        initialdate = NextBillingDateTime(initialdate);
                                    }
                                    //add new regular charge and vat end
                                }

                                initialdate = tbl_guest_stay_info.Arrival_Date.AddSeconds(5);
                                if (discount != null && discount != 0)
                                {
                                    decimal discountLevel = ViewBagDiscountLevel();
                                    decimal discountableAmount = (Convert.ToDecimal(commonRoomData.PerDayCharge * count) * discountLevel) / 100;
                                    if (discount > discountableAmount)
                                    {
                                        discount = discountableAmount;
                                    }

                                    discountTransaction.Debit = 0;
                                    discountTransaction.Credit = discount;
                                    discountTransaction.Tran_Ref_ID = 1;
                                    discountTransaction.Tran_Ref_Name = "Discount";
                                    discountTransaction.Tran_Date = initialdate;
                                    discountTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(discountTransaction);
                                }

                                if (paid != null && paid != 0)
                                {
                                    paidTransaction.Debit = 0;
                                    paidTransaction.Credit = paid;
                                    paidTransaction.Tran_Date = DateTime.Now;
                                    paidTransaction.Tran_Ref_ID = 14;
                                    paidTransaction.Tran_Ref_Name = "booking_paid";
                                    paidTransaction.Particulars = particulars;
                                    db.tbl_room_transactions.Add(paidTransaction);
                                }
                                //transactions rows insert end
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            Session.Remove("BookedStayInfoId");
            Session.Remove("BookedGuest");
            Session.Remove("BookedRoomID");
            return RedirectToAction("Index", "FrontDesk");
        }

        public ActionResult CancelBooking(string id)
        {
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No");

            decimal roomCharge = 0, vat = 0, previousPaid = 0, discount = 0;
            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id));
            if (rid != null)
            {
                decimal rate = Convert.ToDecimal(rid.Rate);
                Session["BookedRoomCancel"] = rid;
                ViewBag.RoomNo = id;
                CommonRoomDataViewModel commonRoomData = ViewBagCommonRoomData(rate);

                int roomId = rid.Room_ID;
                DateTime selectedDate = DateTime.Today.Date;

                if (Session["date"] != null)
                {
                    selectedDate = Convert.ToDateTime(Session["date"]);
                }

                var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Room_ID == roomId && DbFunctions.TruncateTime(a.Arrival_Date) <= selectedDate && DbFunctions.TruncateTime(a.Departure_Date) >= selectedDate && a.Status == "Booked");
                if (guest1 != null)
                {
                    if (guest1.Booking_Date != null)
                    {
                        DateTime bookingDateTime = guest1.Booking_Date.Value;
                        ViewBag.Booking_Date = bookingDateTime;
                    }
                    ViewBag.BookedGuest = guest1;
                    Session["BookedGuestCancel"] = guest1;
                    int noOfNight = StayDayCalculator(guest1.Arrival_Date, guest1.Departure_Date);
                    ViewBag.NoOfNight = noOfNight;

                    //var roomCharges = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16))).ToList();
                    //if (roomCharges != null && roomCharges.Count > 0)
                    //{
                    //    foreach (var item in roomCharges)
                    //    {
                    //        roomCharge += (decimal)item.Debit;
                    //    }
                    //    ViewBag.RoomChargePerDay = roomCharges.FirstOrDefault().Debit;
                    //    ViewBag.RoomCharge = roomCharge;
                    //}

                    //var vats = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 15))).ToList();
                    //if (vats != null && vats.Count > 0)
                    //{
                    //    foreach (var item in vats)
                    //    {
                    //        vat += (decimal)item.Debit;
                    //    }
                    //    ViewBag.Vat = vat;
                    //    ViewBag.VatPercent = vats.FirstOrDefault().Debit;
                    //}

                    var paidTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 14))
                        .ToList();
                    if (paidTransactions != null && paidTransactions.Count > 0)
                    {
                        foreach (var item in paidTransactions)
                        {
                            previousPaid += (decimal)item.Credit;
                        }
                    }
                    ViewBag.Paid = previousPaid;

                    var discountTransactions = db.tbl_room_transactions
                        .Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1))
                        .ToList();
                    if (discountTransactions != null && discountTransactions.Count > 0)
                    {
                        foreach (var item in discountTransactions)
                        {
                            discount += (decimal)item.Credit;
                        }
                    }
                    ViewBag.Discount = discount;

                    var totalCharge = noOfNight * commonRoomData.PerDayCharge;
                    ViewBag.TotalCharge = totalCharge.ToString("F", CultureInfo.InvariantCulture);
                    ViewBag.TotalChargeRounded = Rounder.Round(totalCharge).ToString("F", CultureInfo.InvariantCulture);
                    return PartialView();
                }
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelBooking(decimal? refund, string particulars)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    tbl_room_transactions refundTransaction = new tbl_room_transactions();
                    tbl_guest_attendee attendee = new tbl_guest_attendee();
                    attendee.Emp_ID = userId;
                    attendee.Changed_Date = DateTime.Now;
                    attendee.Purpose = "book_cancel";

                    var rid = (tbl_room_info)Session["BookedRoomCancel"];
                    var guest1 = (tbl_guest_stay_info)Session["BookedGuestCancel"];

                    if (refund != null && refund != 0)
                    {
                        refundTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                        refundTransaction.Room_ID = guest1.Room_ID;
                        refundTransaction.Guest_ID = guest1.Guest_ID;
                        refundTransaction.Debit = 0;
                        refundTransaction.Credit = refund;
                        refundTransaction.Tran_Date = DateTime.Now;
                        refundTransaction.Tran_Ref_ID = 13;
                        refundTransaction.Tran_Ref_Name = "cancellation_refund";
                        refundTransaction.Particulars = particulars;
                        refundTransaction.Emp_ID = userId;
                        db.tbl_room_transactions.Add(refundTransaction);
                    }
                    attendee.Stay_Info_ID = guest1.Stay_Info_ID;
                    guest1.Status = "Clear";
                    guest1.Check_Out_Note = "Booking_Cancel";
                    db.tbl_guest_stay_info.Attach(guest1);

                    var updatedGuestInfo = db.Entry(guest1);
                    updatedGuestInfo.Property(a => a.Status).IsModified = true;
                    updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;

                    db.tbl_guest_attendee.Add(attendee);
                    db.SaveChanges();
                }
            }
            Session.Remove("BookedRoomCancel");
            Session.Remove("BookedGuestCancel");
            return RedirectToAction("Index", "FrontDesk");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);

            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_guest_stay_info);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);
            db.tbl_guest_stay_info.Remove(tbl_guest_stay_info);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult NewDepartureDate(DateTime Start_Date)
        {
            DateTime End_Date = Start_Date.AddDays(1);
            return Json(End_Date.ToString("dd MMMM yyyy hh:mm tt"));
        }

        private decimal ViewBagDiscountLevel()
        {
            ViewBag.DiscountLevel = 0;
            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(u => u.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                decimal discountLevel = Convert.ToDecimal(tblEmpInfo.Discount_Lavel);
                ViewBag.DiscountLevel = discountLevel;
                return discountLevel;
            }
            return 0;
        }

        public JsonResult ErrorMessageAdd(DateTime Start_Date, DateTime End_Date, string method)
        {
            string rno = (string)Session["rno"];
            var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(rno));
            tbl_guest_stay_info guest1 = null;
            int? roomId = null;

            if (method == "E")
            {
                guest1 = (tbl_guest_stay_info)Session["GuestStayInfo"];
            }
            else if (method == "ER")
            {
                guest1 = (tbl_guest_stay_info)Session["ReservedGuest"];
                roomId = (int)Session["ReservedRoomID"];
            }
            else if (method == "S")
            {
                guest1 = (tbl_guest_stay_info)Session["ShiftGuest"];
            }
            else if (method == "EB")
            {
                guest1 = (tbl_guest_stay_info)Session["BookedGuest"];
                roomId = (int)Session["BookedRoomID"];
            }
            if (roomId != null)
            {
                rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == roomId);
            }

            var prevProgram = db.tbl_guest_stay_info.Where(a => a.Status != "Clear").ToList();
            var message = "";

            if (Start_Date >= End_Date)
            {
                message = "Invalid date range.";
                return Json(message.ToList(), JsonRequestBehavior.AllowGet);
            }
            if (Start_Date <= DateTime.Today.Date && (method == "A" || method == "ER" || method == "EB"))	//for checkin arrival cant be previous. 
            {
                message = "Previous Date Can't be Selected as Arrival Date";
                return Json(message.ToList(), JsonRequestBehavior.AllowGet);
            }
            if (rid != null)
            {
                if (guest1 != null)
                {
                    foreach (var item in prevProgram)
                    {
                        if (item.Arrival_Date < End_Date && item.Departure_Date > Start_Date && item.Room_ID == rid.Room_ID && item.Status != "Clear" && item.Guest_ID != guest1.Guest_ID)
                        {
                            message = "Room-" + rid.Room_No + " is " + item.Status + " in selected date range. Please choose another Date or Room.";
                            return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    foreach (var item in prevProgram)
                    {
                        if (item.Arrival_Date < End_Date && item.Departure_Date > Start_Date && item.Room_ID == rid.Room_ID && item.Status != "Clear")
                        {
                            message = "Room-" + rid.Room_No + " is " + item.Status + " in selected date range. Please choose another Date or Room.";
                            return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(message.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InOutChange(int id)
        {
            tbl_guest_stay_info guest_stay = db.tbl_guest_stay_info.Find(id);
            if (guest_stay != null)
            {
                guest_stay.InOrOut = guest_stay.InOrOut.HasValue ? !guest_stay.InOrOut : true;
                db.Entry(guest_stay).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        public ActionResult MakeClear(string id)
        {
            tbl_room_info tbl_room_info = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id) && a.Status == "Dirty");
            if (tbl_room_info != null)
            {
                tbl_room_info.Status = "Clear";
                db.Entry(tbl_room_info).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        public ActionResult MakeAvailable(string id)
        {
            tbl_room_info tbl_room_info = db.tbl_room_info.FirstOrDefault(a => a.Room_No.Equals(id) && a.Status == "OutOfOrder");
            if (tbl_room_info != null)
            {
                tbl_room_info.Status = "Clear";
                db.Entry(tbl_room_info).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "FrontDesk");
        }

        public CommonRoomDataViewModel ViewBagCommonRoomData(decimal rate)
        {
            CommonRoomDataViewModel commonRoomData = new CommonRoomDataViewModel();
            tbl_service roomData = db.tbl_service.Find(1);
            decimal vatPercent = Convert.ToDecimal(roomData.VAT);
            decimal scPercent = Convert.ToDecimal(roomData.SC);
            decimal vat = Convert.ToDecimal(rate * vatPercent / 100);
            decimal sc = Convert.ToDecimal(rate * scPercent / 100);
            decimal perDayCharge = rate + vat + sc;

            ViewBag.Rate = rate.ToString("F", CultureInfo.InvariantCulture);
            ViewBag.VatPercent = vatPercent.ToString("F1", CultureInfo.InvariantCulture);
            ViewBag.ScPercent = scPercent.ToString("F1", CultureInfo.InvariantCulture);
            ViewBag.Vat = vat.ToString("F", CultureInfo.InvariantCulture);
            ViewBag.SC = sc.ToString("F", CultureInfo.InvariantCulture);
            ViewBag.PerDayCharge = perDayCharge.ToString("F", CultureInfo.InvariantCulture);

            commonRoomData.VatPercent = vatPercent;
            commonRoomData.ScPercent = scPercent;
            commonRoomData.Vat = vat;
            commonRoomData.SC = sc;
            commonRoomData.PerDayCharge = perDayCharge;
            return commonRoomData;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}