﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskGuestOverviewController : CustomControllerBase
    {
        FrontDeskGuestStayController GStay = new FrontDeskGuestStayController();

        public ActionResult AllGuest()
        {
            var guests = db.tbl_guest_info.OrderByDescending(a => a.Guest_ID).ToList();
            ViewBag.Guests = guests;
            return View(guests);
        }

        public ActionResult DayWiseGuest()
        {
            return View();
        }

        public PartialViewResult SearchDayWise(DateTime? dt)
        {
            DateTime dt2 = DateTime.Now;
            if (dt != null)
            {
                dt2 = dt.Value;
            }
            var guests = db.tbl_guest_stay_info.Where(a => a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && DbFunctions.TruncateTime(a.Arrival_Date) == dt2.Date).OrderBy(a => a.Arrival_Date).ThenBy(a => a.Room_ID).ToList();
            ViewBag.date = dt2.ToString("dd/MM/yyyy");
            ViewBag.GuestList = guests;
            ViewBag.TotalGuest = guests.Count();
            CommonRepoprtData rpd = new CommonRepoprtData();
            ViewBag.PropertyName = rpd.PropertyName;
            ViewBag.PropertyAddress = rpd.PropertyAddress;
            ViewBag.PropertyPhone = rpd.PropertyPhone;
            ViewBag.PropertyLogo = rpd.PropertyLogo;
            return PartialView();
        }

        public ActionResult DetailInfo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var detGuest = db.tbl_guest_info.FirstOrDefault(a => a.Guest_ID == id);
            var stayInfo = db.tbl_guest_stay_info.Where(a => a.Guest_ID == id && a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel").ToList();
            ViewBag.StayInfo = stayInfo;
            if (detGuest == null)
            {
                return HttpNotFound();
            }
            return PartialView(detGuest);
        }

        public JsonResult SerialFinder(string types, DateTime dt, string roomNo)
        {
            var count = 0;
            var rid = db.tbl_room_info.Where(r => r.Room_No == roomNo).Select(r => r.Room_ID).FirstOrDefault();

            if (types == "A")
            {
                count = db.tbl_guest_stay_info.Count(a => DbFunctions.TruncateTime(a.Arrival_Date) == dt.Date && a.Arrival_Date < dt && a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel");
                var sameTimeList = db.tbl_guest_stay_info.Count(a => a.Arrival_Date == dt && a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && a.Room_ID < rid);
                count += sameTimeList + 1;
            }
            else if (types == "E")
            {
                count = db.tbl_guest_stay_info.Count(a => DbFunctions.TruncateTime(a.Arrival_Date) == dt.Date && a.Arrival_Date < dt && a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel");
                var sameTimeList = db.tbl_guest_stay_info.Count(a => a.Arrival_Date == dt && a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && a.Room_ID < rid);
                count += sameTimeList + 1;
            }
            else if (types == "C")
            {
                count = db.tbl_guest_stay_info.Count(a => DbFunctions.TruncateTime(a.Check_Out_Date) == dt.Date && a.Check_Out_Time < dt.TimeOfDay && a.Status == "Clear" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel");
                var sameTimeList = db.tbl_guest_stay_info.Count(a => DbFunctions.TruncateTime(a.Check_Out_Date) == dt.Date && a.Check_Out_Time == dt.TimeOfDay && a.Status == "Clear" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && a.Room_ID < rid);
                count += sameTimeList + 1;
            }
            else
            {
                count = 999;
            }
            return Json(count);
        }

        public JsonResult GuestStayData(int id)
        {
            var guest = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id);
            if (guest != null)
            {
                DateTime checkoutDateTime = guest.Check_Out_Date ?? guest.Departure_Date;
                if (guest.Check_Out_Time.HasValue)
                {
                    checkoutDateTime = checkoutDateTime.Add(guest.Check_Out_Time.Value);
                }
                var checkInEmployeeRow =
                    db.tbl_guest_attendee.FirstOrDefault(
                        a => a.Stay_Info_ID == id && (a.Purpose == "checkin" || a.Purpose == "shift" || a.Purpose == "booked_to_checkin" || a.Purpose == "reserved_to_checkin"));
                var checkOutEmployeeRow =
                    db.tbl_guest_attendee.FirstOrDefault(
                        a => a.Stay_Info_ID == id && a.Purpose == "checkout");
                string checkedInBy = "";
                string checkedOutBy = "";
                if (checkInEmployeeRow != null)
                {
                    checkedInBy = checkInEmployeeRow.tbl_emp_info.user_name;
                }
                if (checkOutEmployeeRow != null)
                {
                    checkedOutBy = checkOutEmployeeRow.tbl_emp_info.user_name;
                }
                decimal rate = Convert.ToDecimal(guest.tbl_room_info.Rate) * (decimal)1.15;
                rate = (Math.Round(rate / 10)) * 10;
                decimal paid = 0;
                var allPaidRow =
                    db.tbl_room_transactions.Where(a => a.Stay_Info_ID == id &&
                                                        (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 12 ||
                                                         a.Tran_Ref_ID == 14)).ToList();
                foreach (var item in allPaidRow)
                {
                    paid += (decimal)item.Credit;
                }
                double noOfNight = 0;
                if (!guest.Check_Out_Date.HasValue)
                {
                    noOfNight = GStay.StayDayCalculator(guest.Arrival_Date, DateTime.Now);
                }
                else
                {
                    noOfNight = GStay.StayDayCalculator(guest.Arrival_Date, checkoutDateTime);
                }
                decimal totalRent = (decimal)noOfNight * rate;

                var dt = new
                {
                    checkedInBy = checkedInBy,
                    checkedOutBy = checkedOutBy,
                    room_rent = rate,
                    room_advance = paid,
                    total_rent = totalRent,
                    total_days = noOfNight
                };
                return Json(dt, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Gallery()
        {
            return View();
        }

        public PartialViewResult SearchGallery(DateTime? dt)
        {
            DateTime dt2 = DateTime.Now;
            if (dt != null)
            {
                dt2 = dt.Value;
            }
            var guests = db.tbl_guest_stay_info.Where(a => a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && DbFunctions.TruncateTime(a.Arrival_Date) == dt2.Date).OrderBy(a => a.Arrival_Date).ThenBy(a => a.Room_ID).ToList();
            ViewBag.date = dt2.ToString("dd/MM/yyyy");
            ViewBag.GuestList = guests;
            ViewBag.TotalGuest = guests.Count();
            return PartialView();
        }

        public ActionResult TransactionDetail(int id)
        {
            tbl_guest_stay_info GuestInfo = db.tbl_guest_stay_info.Where(a => a.Stay_Info_ID == id).Include(r => r.tbl_room_info).Include(g => g.tbl_guest_info).FirstOrDefault();
            if (GuestInfo == null)
            {
                return HttpNotFound();
            }

            var all_transactions = db.tbl_room_transactions.Where(t => t.Stay_Info_ID == id).Include(r => r.tbl_room_info).Include(g => g.tbl_guest_info);
            DateTime checkoutDateTime = GuestInfo.Check_Out_Date ?? GuestInfo.Departure_Date;
            if (GuestInfo.Check_Out_Time.HasValue)
            {
                checkoutDateTime = checkoutDateTime.Add(GuestInfo.Check_Out_Time.Value);
            }
            ViewBag.GuestInfo = GuestInfo;
            ViewBag.checkoutDateTime = checkoutDateTime;
            var transactionResult = all_transactions.ToList();
            return View(transactionResult);
        }

        public JsonResult ExportReport(int id)
        {
            tbl_guest_stay_info GuestInfo = db.tbl_guest_stay_info.Where(a => a.Stay_Info_ID == id).Include(r => r.tbl_room_info).Include(g => g.tbl_guest_info).FirstOrDefault();
            var all_transactions = db.tbl_room_transactions.Where(t => t.Stay_Info_ID == id).Include(r => r.tbl_room_info).Include(g => g.tbl_guest_info);
            DateTime checkoutDateTime = GuestInfo.Check_Out_Date ?? GuestInfo.Departure_Date;
            if (GuestInfo.Check_Out_Time.HasValue)
            {
                checkoutDateTime = checkoutDateTime.Add(GuestInfo.Check_Out_Time.Value);
            }
            string CheckOutString = GuestInfo.Check_Out_Date.HasValue ? checkoutDateTime.ToString("dd/MM/yyyy HH:mm") : "";

            int noOfNight = 0;
            if (!GuestInfo.Check_Out_Date.HasValue)
            {
                noOfNight = GStay.StayDayCalculator(GuestInfo.Arrival_Date, DateTime.Now);
            }
            else
            {
                noOfNight = GStay.StayDayCalculator(GuestInfo.Arrival_Date, checkoutDateTime);
            }

            //totalRoomCharge
            decimal discount = 0, paid = 0, refund = 0, baddebt = 0, surplus = 0, otherbill = 0, vat = 0, sc = 0;
            decimal regular = Convert.ToDecimal(all_transactions.Where(a => (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16) && (a.Tran_Date <= checkoutDateTime || DbFunctions.TruncateTime(a.Tran_Date.Value) == DbFunctions.TruncateTime(GuestInfo.Arrival_Date))).ToList().Sum(b => b.Debit.Value));
            vat = Convert.ToDecimal(all_transactions.Where(a => (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 15) && (a.Tran_Date <= checkoutDateTime || DbFunctions.TruncateTime(a.Tran_Date.Value) == DbFunctions.TruncateTime(GuestInfo.Arrival_Date))).ToList().Sum(b => b.Debit.Value));
            sc = Convert.ToDecimal(all_transactions.Where(a => (a.Tran_Ref_ID == 17 || a.Tran_Ref_ID == 18) && (a.Tran_Date <= checkoutDateTime || DbFunctions.TruncateTime(a.Tran_Date.Value) == DbFunctions.TruncateTime(GuestInfo.Arrival_Date))).ToList().Sum(b => b.Debit.Value));

            List<RoomWiseGuestTran> roomWiseGuestTranList = new List<RoomWiseGuestTran>();
            RoomWiseGuestTran perRoom = new RoomWiseGuestTran();
            perRoom.RoomNo = GuestInfo.tbl_room_info.Room_No;
            perRoom.RoomCharge = GuestInfo.tbl_room_info.Rate ?? 0;
            perRoom.NoOfNight = noOfNight;
            perRoom.TotalRoomCharge = regular;
            roomWiseGuestTranList.Add(perRoom);

            RoomWiseGuestTran vatRow = new RoomWiseGuestTran();
            vatRow.RoomNo = "VAT";
            vatRow.RoomCharge = 0;
            vatRow.NoOfNight = 0;
            vatRow.TotalRoomCharge = vat;
            roomWiseGuestTranList.Add(vatRow);

            RoomWiseGuestTran scRow = new RoomWiseGuestTran();
            scRow.RoomNo = "SC";
            scRow.RoomCharge = 0;
            scRow.NoOfNight = 0;
            scRow.TotalRoomCharge = sc;
            roomWiseGuestTranList.Add(scRow);

            List<tbl_room_transactions> paidList = all_transactions.Where(a => a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 12 || a.Tran_Ref_ID == 14).ToList();
            List<tbl_room_transactions> discountList = all_transactions.Where(a => a.Tran_Ref_ID == 1).ToList();
            List<tbl_room_transactions> refundList = all_transactions.Where(a => a.Tran_Ref_ID == 7).ToList();
            List<tbl_room_transactions> baddebtList = all_transactions.Where(a => a.Tran_Ref_ID == 8).ToList();
            List<tbl_room_transactions> surplusList = all_transactions.Where(a => a.Tran_Ref_ID == 9).ToList();
            var otherBillList = (from tran in all_transactions
                where tran.Tran_Ref_ID == 5 || tran.Tran_Ref_ID == 6
                select new
                {
                    Debit = tran.Debit ?? 0,
                    Particular = tran.Particulars,
                    Tran_Date = tran.Tran_Date.Value
                }).ToList();
            //all_transactions.Where(a => a.Tran_Ref_ID == 5).ToList();
            if (otherBillList != null && otherBillList.Count != 0)
            {
                foreach (var item in otherBillList)
                {
                    otherbill += (decimal)item.Debit;
                }
            }
            if (discountList != null && discountList.Count != 0)
            {
                foreach (var item in discountList)
                {
                    discount += (decimal)item.Credit;
                }
            }
            if (paidList != null && paidList.Count != 0)
            {
                foreach (var item in paidList)
                {
                    paid += (decimal)item.Credit;
                }
            }
            if (refundList != null && refundList.Count != 0)
            {
                foreach (var item in refundList)
                {
                    refund += (decimal)item.Credit;
                }
            }
            if (baddebtList != null && baddebtList.Count != 0)
            {
                foreach (var item in baddebtList)
                {
                    baddebt += (decimal)item.Credit;
                }
            }
            if (surplusList != null && surplusList.Count != 0)
            {
                foreach (var item in surplusList)
                {
                    surplus += (decimal)item.Debit;
                }
            }

            decimal due = regular + vat  + sc + otherbill - discount + refund - baddebt - paid;
            if (due >= -5 && due <= 5)
            {
                due = 0;
            }
                       


            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "GuestTransactionsReport.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }
            
            ReportDataSource DataSetRoomWiseGuestTran = new ReportDataSource("DataSetRoomWiseGuestTran", roomWiseGuestTranList);
            ReportDataSource DataSetRoomTransactions = new ReportDataSource("DataSetRoomTransactions", otherBillList);
            lc.DataSources.Add(DataSetRoomWiseGuestTran);
            lc.DataSources.Add(DataSetRoomTransactions);

            int j = 0;
            ReportParameter[] param = new ReportParameter[14];
            param[j++] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[j++] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[j++] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[j++] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);

            param[j++] = new ReportParameter("GuestName", GuestInfo.tbl_guest_info.Name.ToUpper(), false);
            param[j++] = new ReportParameter("GuestPhone", GuestInfo.tbl_guest_info.Phone, false);
            param[j++] = new ReportParameter("GuestAddress", GuestInfo.tbl_guest_info.Address, false);
            param[j++] = new ReportParameter("ArrivalDate", GuestInfo.Arrival_Date.ToString("dd/MM/yyyy HH:mm"), false);
            param[j++] = new ReportParameter("CheckOutDate", CheckOutString, false);            

            param[j++] = new ReportParameter("Discount", discount.ToString(), false);
            param[j++] = new ReportParameter("Paid", paid.ToString(), false);
            param[j++] = new ReportParameter("Refund", refund.ToString(), false);
            param[j++] = new ReportParameter("Baddebt", baddebt.ToString(), false);
            param[j++] = new ReportParameter("Due", due.ToString(), false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

    }

    public class RoomWiseGuestTran
    {
        public string RoomNo { get; set; }
        public decimal RoomCharge { get; set; }
        public int NoOfNight { get; set; }
        public decimal TotalRoomCharge { get; set; }
    }
}