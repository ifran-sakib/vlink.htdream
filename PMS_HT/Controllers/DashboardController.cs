using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class DashboardController : CustomControllerBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var today = DateTime.Today.Date;
            AllViewBagMaker(today);
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(DateTime? dt)
        {
            var today = DateTime.Today.Date;
            if (dt != null)
            {
                today = dt.Value.Date;
            }
            AllViewBagMaker(today);
            
            return View();
        }

        [NonAction]
        public void AllViewBagMaker(DateTime today)
        {
            int totalRoom = db.tbl_room_info.Count();
            var checkinCount = (from a in db.tbl_guest_stay_info where (a.Status == "CheckIn" || (a.Status == "Clear" && a.Check_Out_Date > today)) && DbFunctions.TruncateTime(a.Arrival_Date) <= today select a.Room_ID).Count();
            var totalEntryToday = (from a in db.tbl_guest_stay_info
                                   where a.Status != "Reserved" && 
                                   a.Status != "Booked" && 
                                   a.Check_Out_Note != "Temp_Stay" && 
                                   a.Check_Out_Note != "Reserved_Cancel" && 
                                   a.Check_Out_Note != "Reserved_Auto_Cancel" && 
                                   a.Check_Out_Note != "Booked_Cancel" && 
                                   a.Check_Out_Note != "Booked_Auto_Cancel" && 
                                   DbFunctions.TruncateTime(a.Arrival_Date) == today && a.Departure_Date >= today
                                   select a.Room_ID).Count();
            var totalCheckOutToday = (from a in db.tbl_guest_stay_info where a.Check_Out_Date == today && a.Check_Out_Note != "Temp_Stay" select a.Room_ID);
            //var reservedCount = (from a in db.tbl_guest_stay_info
            //                     where (a.Status == "Reserved" || 
            //                        (a.Status == "Clear" && 
            //                        (a.Check_Out_Note == "Reserved_Cancel" || a.Check_Out_Note == "Reserved_Auto_Cancel"))) 
            //                     && DbFunctions.TruncateTime(a.Arrival_Date) == today
            //                     select a.Room_ID).Count();
            //var bookingCount = (from a in db.tbl_guest_stay_info
            //                    where (a.Status == "Booked" ||
            //                        (a.Status == "Clear" &&
            //                        (a.Check_Out_Note == "Booked_Cancel" || a.Check_Out_Note == "Booked_Auto_Cancel"))) 
            //                    && DbFunctions.TruncateTime(a.Arrival_Date) == today select a.Room_ID).Count();
            var reservedCount = (from a in db.tbl_guest_stay_info where a.Status == "Reserved" && DbFunctions.TruncateTime(a.Arrival_Date) == today select a.Room_ID).Count();
            var bookingCount = (from a in db.tbl_guest_stay_info where a.Status == "Booked" && DbFunctions.TruncateTime(a.Arrival_Date) == today select a.Room_ID).Count();
            var dirtyCount = db.tbl_room_info.Count(a => a.Status == "Dirty");
            if (today.Date != DateTime.Today.Date)
            {
                dirtyCount = 0;
            }
            var Count = (from gsi in db.tbl_guest_stay_info where gsi.Status != "Clear" && DbFunctions.TruncateTime(gsi.Arrival_Date) <= today select (gsi.Room_ID)).Distinct().Count();

            var vacantCount = totalRoom - checkinCount - reservedCount - bookingCount - dirtyCount;
            var previousVacant = totalRoom - (checkinCount - (totalEntryToday - totalCheckOutToday.Count()));

            ViewBag.PreviousVacant = previousVacant;
            ViewBag.TotalCheckIn = checkinCount;
            ViewBag.TotalEntryToday = totalEntryToday;
            ViewBag.TotalCheckOutToday = totalCheckOutToday.Count();
            ViewBag.TotalReserved = reservedCount;
            ViewBag.TotalBooking = bookingCount;
            ViewBag.TotalVacant = vacantCount;
            ViewBag.TotalDirty = dirtyCount;

            var todayEntryCustomerInfo = db.tbl_guest_stay_info.Where((a => a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && DbFunctions.TruncateTime(a.Arrival_Date) == today)).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.TodayEntryCustomerInfo = todayEntryCustomerInfo.ToList();

            var previousEntryCheckInCustomerInfo = db.tbl_guest_stay_info.Where(a => (a.Status == "CheckIn" || (a.Status == "Clear" && a.Check_Out_Date > today)) && DbFunctions.TruncateTime(a.Arrival_Date) < today).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.PreviousEntryCheckInCustomerInfo = previousEntryCheckInCustomerInfo.ToList();

            var checkInCustomerInfo = db.tbl_guest_stay_info.Where(a => (a.Status == "CheckIn" || (a.Status == "Clear" && a.Check_Out_Date > today)) && DbFunctions.TruncateTime(a.Arrival_Date) <= today).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.CheckInCustomerInfo = checkInCustomerInfo.ToList();

            var totalCheckOutTodayInfo = db.tbl_guest_stay_info.Where(a => a.Check_Out_Date == today && a.Check_Out_Note != "Temp_Stay").OrderBy(a => a.Check_Out_Time).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.TotalCheckOutCustomerInfo = totalCheckOutTodayInfo.ToList();

            var reservedCustomerInfo = db.tbl_guest_stay_info.Where(a => a.Status == "Reserved" && DbFunctions.TruncateTime(a.Arrival_Date) == today).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.ReservedCustomerInfo = reservedCustomerInfo.ToList();

            var bookedCustomerInfo = db.tbl_guest_stay_info.Where(a => a.Status == "Booked").OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            ViewBag.BookedCustomerInfo = bookedCustomerInfo.ToList();

            var roomIncome = (from rt in db.tbl_room_transactions where DbFunctions.TruncateTime(rt.Tran_Date) == today && (rt.Tran_Ref_ID == 3 || rt.Tran_Ref_ID == 7) select rt.Credit).Sum() ?? 0;
            var banquetIncome = (from bt in db.tbl_banquet_transactions where DbFunctions.TruncateTime(bt.Tran_Date) == today select bt.Credit).Sum() ?? 0;
            var othersIncome = (from otherIncome in db.tbl_others_income where otherIncome.Created_Date == today select otherIncome.Amount).Sum() ?? 0;
            var bankIncome = (from a in db.tbl_bank_transaction where (a.Tran_Type == "Withdrawn" && a.Created_Date == today) select a.Credit).Sum() ?? 0;

            var bankExpense = (from a in db.tbl_bank_transaction where (a.Tran_Type.Equals("Deposit") && a.Created_Date == today) select a.Debit).Sum() ?? 0;
            var expense = (from exp in db.tbl_expense where exp.Created_Date == today select exp.Amount).Sum() ?? 0;
            var salaryExpense = (from salExp in db.tbl_salary_transactions where (salExp.Tran_Ref_Name == "SalaryPayment" && salExp.Date == today) select salExp.Credit).Sum() ?? 0;
            //var inventoryExpense = (from pm in db.tbl_purchase_master where pm.Purchase_Date == today select pm.Advanced_Amount).Sum() ?? 0;

            var inventoryExpense = (from pm in db.tbl_supplier_ledger where DbFunctions.TruncateTime(pm.Transaction_Date) == today && pm.Tran_Ref_ID == 2 select pm.Credit).Sum() ?? 0;


            var profit = roomIncome + banquetIncome + bankIncome - expense - bankExpense - salaryExpense - inventoryExpense;
            ViewBag.Income = roomIncome + banquetIncome + bankIncome;
            ViewBag.Expense = expense + bankExpense + salaryExpense + inventoryExpense;
            ViewBag.Profit = profit;
            ViewBag.date = today.ToString("dd/MM/yyyy");

            FrontDeskRoomColor();
        }
        

        //public ActionResult PieChart(string dt)
        //{
        //    ArrayList xValue = new ArrayList();
        //    ArrayList yValue = new ArrayList();
            
        //    var today = DateTime.Today.Date;
        //    if (dt != "")
        //    {
        //        today = DateTime.ParseExact(dt, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //    }
        //    //var checkInCustomerInfo = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn").Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);


        //    //checkInCustomerInfo.ToList().ForEach(rs => xValue.Add(rs.tbl_room_info.Room_No));
        //    //checkInCustomerInfo.ToList().ForEach(rs => yValue.Add(rs.Status));

        //    //new Chart(width: 450, height: 280, theme: ChartTheme.Green)
        //    //.AddTitle("Chart Test[Pie Chart]")
        //    //.AddSeries("Default", chartType: "Column", xValue: xValue, yValues: yValue)
        //    //.Write("bmp");

        //    var checkinCount = (from a in db.tbl_guest_stay_info where DbFunctions.TruncateTime(a.Arrival_Date) <= today && ((DbFunctions.TruncateTime(a.Check_Out_Date.Value) > today && a.Check_Out_Note != "Temp_Stay") || a.Status == "CheckIn") select a.Room_ID).Count();
        //    var reservedCount = (from a in db.tbl_guest_stay_info where a.Status == "Reserved" && DbFunctions.TruncateTime(a.Arrival_Date) == today select a.Room_ID).Count();
        //    var bookedCount = (from a in db.tbl_guest_stay_info where a.Status == "Booked" && DbFunctions.TruncateTime(a.Arrival_Date) == today select a.Room_ID).Count();
        //    var totalRoom = (from ri in db.tbl_room_info select ri.Room_ID).Count();
        //    var Count = (from gsi in db.tbl_guest_stay_info where gsi.Status != "Clear" && DbFunctions.TruncateTime(gsi.Arrival_Date) <= today select (gsi.Room_ID)).Distinct().Count();

        //    var dirtyCount = db.tbl_room_info.Count(a => a.Status == "Dirty");

        //    if (today.Date != DateTime.Today.Date)
        //    {
        //        dirtyCount = 0;
        //    }
        //    var vacantCount = totalRoom - checkinCount - reservedCount - bookedCount - dirtyCount;
        //    ViewBag.TotalCheckIn = checkinCount;
        //    new Chart(width: 550, height: 380, theme: ChartTheme.Blue)
        //    .AddTitle("Room Status")
        //    .AddLegend("Color Label")
        //    .AddSeries("Default", chartType: "Pie", 
        //            xValue: new[] { "CheckIn", "Reserved", "Booked", "Dirty", "Vacant" },
        //            yValues: new[] { checkinCount, reservedCount, bookedCount, dirtyCount, vacantCount }
        //        )
        //    .Write("png");
        //    return null;
        //}
    }
}