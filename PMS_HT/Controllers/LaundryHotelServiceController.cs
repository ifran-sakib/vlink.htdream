﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class LaundryHotelServiceController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_hotel_service.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_hotel_service hotelService)
        {
            if (ModelState.IsValid)
            {
                db.tbl_hotel_service.Add(hotelService);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_hotel_service.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_hotel_service hotelService, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hotelService).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_hotel_service.Remove(db.tbl_hotel_service.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //// GET: LaundryHotelService/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_hotel_service tbl_hotel_service = db.tbl_hotel_service.Find(id);
        //    if (tbl_hotel_service == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_hotel_service);
        //}

        //// GET: LaundryHotelService/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: LaundryHotelService/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Service_ID,Service_Heading,Service_Description,Cost")] tbl_hotel_service tbl_hotel_service)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.tbl_hotel_service.Add(tbl_hotel_service);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(tbl_hotel_service);
        //}

        //// GET: LaundryHotelService/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_hotel_service tbl_hotel_service = db.tbl_hotel_service.Find(id);
        //    if (tbl_hotel_service == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_hotel_service);
        //}

        //// POST: LaundryHotelService/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Service_ID,Service_Heading,Service_Description,Cost")] tbl_hotel_service tbl_hotel_service)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(tbl_hotel_service).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(tbl_hotel_service);
        //}

        //// GET: LaundryHotelService/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_hotel_service tbl_hotel_service = db.tbl_hotel_service.Find(id);
        //    if (tbl_hotel_service == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_hotel_service);
        //}

        //// POST: LaundryHotelService/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    tbl_hotel_service tbl_hotel_service = db.tbl_hotel_service.Find(id);
        //    db.tbl_hotel_service.Remove(tbl_hotel_service);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
