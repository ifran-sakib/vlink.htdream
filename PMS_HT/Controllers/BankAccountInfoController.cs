﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class BankAccountInfoController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_account_info = db.tbl_account_info.Include(t => t.tbl_bank_info).Include(t => t.tbl_branch_info).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1);
            return View(tbl_account_info.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Bank_ID = new SelectList(db.tbl_bank_info, "Bank_ID", "Bank_Name");
            ViewBag.Branch_ID = new SelectList(db.tbl_branch_info, "Branch_ID", "Branch_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_account_info accountInfo)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Bank_ID = new SelectList(db.tbl_bank_info, "Bank_ID", "Bank_Name");
                ViewBag.Branch_ID = new SelectList(db.tbl_branch_info, "Branch_ID", "Branch_Name");
                db.tbl_account_info.Add(accountInfo);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_account_info tbl_account_info = db.tbl_account_info.Find(id);
            if (tbl_account_info == null)
            {
                return HttpNotFound();
            }
            if(!User.IsInRole("Super Admin") && tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }

            ViewBag.Bank_ID = new SelectList(db.tbl_bank_info, "Bank_ID", "Bank_Name", tbl_account_info.Bank_ID);
            ViewBag.Branch_ID = new SelectList(db.tbl_branch_info, "Branch_ID", "Branch_Name", tbl_account_info.Branch_ID);
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_account_info.Creted_By);
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_account_info.Modified_By);

            return PartialView(db.tbl_account_info.Find(id));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Acc_ID,Acc_No,Acc_Name,Note,Bank_ID,Branch_ID,Opening_Balance,Creted_By,Created_Date,Modified_By,Modified_Date")] tbl_account_info tbl_account_info)
        {
            if (!User.IsInRole("Super Admin") && tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(tbl_account_info).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Bank_ID = new SelectList(db.tbl_bank_info, "Bank_ID", "Bank_Name", tbl_account_info.Bank_ID);
            ViewBag.Branch_ID = new SelectList(db.tbl_branch_info, "Branch_ID", "Branch_Name", tbl_account_info.Branch_ID);
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_account_info.Creted_By);
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_account_info.Modified_By);

            return RedirectToAction("Index");
        }


        public ActionResult Delete(int id)
        {
            tbl_account_info tbl_account_info = db.tbl_account_info.Find(id);
            if (tbl_account_info == null)
            {
                return HttpNotFound();
            }
            if (!User.IsInRole("Super Admin") && tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }
            db.tbl_account_info.Remove(db.tbl_account_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult IntervalSelector(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_account_info tbl_account_info = db.tbl_account_info.Find(id);
            if (tbl_account_info == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = id;
            return PartialView(db.tbl_account_info.Find(id));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
