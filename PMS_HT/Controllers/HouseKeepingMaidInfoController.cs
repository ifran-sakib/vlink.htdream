﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class HouseKeepingMaidInfoController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            return View(db.tbl_maid_info.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_maid_info maid)
        {
            if (ModelState.IsValid)
            {
                //if(maid.File != null)
                //{
                //    maid.Image = "";
                //    if (maid.File.ContentLength > 0)
                //    {
                //        var filename = maid.Name + "_" +Path.GetFileName(maid.File.FileName);
                //        var path = Path.Combine(Server.MapPath("~/images/maid"), filename);
                //        maid.File.SaveAs(path);
                //        maid.Image = filename;
                //    }
                //}
                db.tbl_maid_info.Add(maid);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_maid_info.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_maid_info maid, int id)
        {
            if (ModelState.IsValid)
            {
                //if (maid.File != null)
                //{
                //    maid.Image = "";
                //    if (maid.File.ContentLength > 0)
                //    {
                //        var filename = maid.Name + "_" + Path.GetFileName(maid.File.FileName);
                //        var path = Path.Combine(Server.MapPath("~/images/maid"), filename);
                //        maid.File.SaveAs(path);
                //        maid.Image = filename;
                //    }
                //}
                //else
                //{
                //    var GetImageName = db.tbl_maid_info.Where(d => d.Maid_ID == id).Select(d => d.Image).FirstOrDefault();
                //    maid.Image = GetImageName;
                //}
                db.Entry(maid).State = EntityState.Modified;
                db.SaveChanges();
            }           
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_maid_info.Remove(db.tbl_maid_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            Session["Maid_ID"] = id;
            ViewBag.Maid_ID = id;
            //from customer in db.Customers
            //join order in db.Orders on customer.ID equals order.ID
            //join product in db.Products on order.ProductID equals product.ID
            //join info in db.Info on product.InfoID equals info.ID
            //select new { customer.Name, info.BriefDescription };
            //ViewBag.Image = db.tbl_maid_info.Where(x => x.Maid_ID == id).Select(x => x.Image).FirstOrDefault();
            ViewBag.status = "In Process";
            ViewBag.status2 = "Done";

            ViewBag.taskAssign = from maid in db.tbl_maid_info
                                 join assing in db.tbl_task_assignment on maid.Maid_ID equals assing.Maid_ID
                                 join room in db.tbl_room_info on assing.Room_ID equals room.Room_ID
                                 join task in db.tbl_task_setup on assing.Task_ID equals task.Task_ID
                                 where assing.Maid_ID == id
                                 select new TaskAssignViewModel
                                 {
                                     Task_Heading = task.Task_Heading,
                                     RoomNo = room.Room_No,
                                     Note = assing.Note,
                                     Task_Assignment_Date_Time = assing.Task_Assignment_Date_Time,
                                     Task_Status = assing.Task_Status,
                                    Task_Assignment_ID = assing.Task_Assignment_ID
                                 };

            ViewBag.schedule = from schedule in db.tbl_task_schdule
                               join task in db.tbl_task_setup on schedule.Task_ID equals task.Task_ID
                               join maid in db.tbl_maid_info on schedule.Maid_ID equals maid.Maid_ID
                               join room in db.tbl_room_info on schedule.Room_ID equals room.Room_ID
                               where schedule.Maid_ID == id
                               select new TaskScheduleViewModel
                               {
                                   TaskHeading = task.Task_Heading,
                                   RoomNo = room.Room_No,
                                   ScheduleDateTime = schedule.Schdule_Date_Time,
                                   TaskStatus = schedule.Task_Schdule_Status,
                                   Task_Schdule_ID = schedule.Task_Schdule_ID,
                                   Note = schedule.Note
                               };

            return View(db.tbl_maid_info.FirstOrDefault(d => d.Maid_ID == id));
        }

        [HttpPost]
        public ActionResult Done(IEnumerable<int> done, int? id)
        {
            try
            {
                var some = db.tbl_task_assignment.Where(x => done.Contains(x.Task_Assignment_ID)).ToList();
                some.ForEach(a => a.Task_Status = true);

                var some2 = db.tbl_task_schdule.Where(x => done.Contains(x.Task_Schdule_ID)).ToList();
                some2.ForEach(a => a.Task_Schdule_Status = "Done");
                db.SaveChanges();
            }
            catch { }
            // return RedirectToAction("Details");
            //  return RedirectToAction("Details", "HouseKeepingMaidInfo", new { id = 6});
            //return RedirectToAction("Details", new { id = 6 });
            // return View(db.tbl_maid_info.Where(d => d.Maid_ID == id).FirstOrDefault());
            return RedirectToAction("Details", "HouseKeepingMaidInfo", new { id = id });
               
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
