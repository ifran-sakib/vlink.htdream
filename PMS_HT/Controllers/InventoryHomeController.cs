﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Inventory")]
    public class InventoryHomeController : CustomControllerBase
    {
        public ActionResult Index()
        {
            string message = "";
            List<string> alerts = new List<string>();
            var tbl_item = db.tbl_item.ToList();
            foreach (var item in tbl_item)
            {
                decimal remain_stock = Convert.ToDecimal(item.tbl_stock.Select(a => a.Stock_In).Sum() - item.tbl_stock.Select(a => a.Stock_Out).Sum());
                if (remain_stock > 0 && remain_stock <= item.Reorder_Level)
                {
                    message = item.Item_Name;
                    alerts.Add(message);
                }
            }
            ViewBag.Alert = alerts;
            ViewBag.AlertCount = alerts.Count;
            ViewBag.Locations = db.tbl_stock_location.ToList();
            return View(db.tbl_item.ToList());
        }

        public ActionResult Location(int id)
        {
            var tbl_stock = from st in db.tbl_stock
                            join item in db.tbl_item
                            on st.Item_ID equals item.Item_ID
                            join loc in db.tbl_stock_location
                            on st.Stock_Location_ID equals loc.Stock_Location_ID
                            where st.Stock_Location_ID == id
                            group st by new { st.Item_ID, st.tbl_stock_location.Stock_Location_ID, st.tbl_item.Item_Name, st.tbl_stock_location.Stock_Location_Name,st.tbl_item.tbl_unit.Unit_Name } into a
                            select new StockViewModel
                            {
                                Item_ID = (int)a.Key.Item_ID,
                                Stock_Location_ID = (int)a.Key.Stock_Location_ID,
                                Item_Name = a.Key.Item_Name,
                                Stock_Location_Name = a.Key.Stock_Location_Name,
                                Unit_Name = a.Key.Unit_Name,
                                Quantity = (Decimal)(a.Sum(x => x.Stock_In) ?? 0) - (a.Sum(x => x.Stock_Out) ?? 0)
                            };
            return View(tbl_stock.ToList());
        }
    }
}