using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Reporting.WebForms;
using PMS_HT.ViewModels;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskController : CustomControllerBase
    {
        public ActionResult Index()
        {
            ViewBag.Date = Session["date"] ?? DateTime.Today.Date;

            //db.Database.ExecuteSqlCommand("DELETE FROM tbl_room_transactions where Room_ID > 80");
            DateTime baseDate = DateTime.Now.AddDays(-1);

            var status = db.tbl_guest_stay_info.Where(a => a.Arrival_Date > baseDate || (a.Status == "CheckIn" || a.Status == "Reserved" || a.Status == "Booked")).OrderBy(b => b.Room_ID).ToList();
            ViewBag.status = status;
            ViewBag.count = status.Count;

            ViewBag.floors = db.tbl_floor_setup.ToList();
            ViewBag.categories = db.tbl_roomtype_setup.ToList();
            ViewBag.rooms = db.tbl_room_info.ToList();
            ViewBag.roomview = db.tbl_room_view_setup.ToList();

            List<string> alerts = new List<string>();
            foreach (var item in status.Where(a => a.Status != "Clear"))
            {
                DateTime initialDate = item.Departure_Date;

                var rid = db.tbl_room_info.FirstOrDefault(a => a.Room_ID == item.Room_ID);
                if (rid != null)
                {
                    var rate = Convert.ToDecimal(rid.Rate);
                    FrontDeskGuestStayController gStay = new FrontDeskGuestStayController();
                    CommonRoomDataViewModel commonRoomData = gStay.ViewBagCommonRoomData(rid.Rate.Value);

                    string message;

                    //Reserve auto cancellation after midnight begin
                    if (item.Status == "Reserved" && item.Arrival_Date <= DateTime.Today.Date)
                    {
                        item.Status = "Clear";
                        item.Check_Out_Note = "Reserved_Auto_Cancel";
                        db.tbl_guest_stay_info.Attach(item);
                        var updatedGuestInfo = db.Entry(item);
                        updatedGuestInfo.Property(a => a.Status).IsModified = true;
                        updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;
                    }
                    //Reserve auto cancellation after midnight end

                    //Booking auto cancellation after Departure Time begin
                    else if (item.Status == "Booked" && item.Departure_Date < DateTime.Now)
                    {
                        item.Status = "Clear";
                        item.Check_Out_Note = "Booking_Auto_Cancel";
                        db.tbl_guest_stay_info.Attach(item);
                        var updatedGuestInfo = db.Entry(item);
                        updatedGuestInfo.Property(a => a.Status).IsModified = true;
                        updatedGuestInfo.Property(a => a.Check_Out_Note).IsModified = true;
                    }
                    //Booking auto cancellation after Departure Time end

                    else if (item.Status == "CheckIn" && item.tbl_room_transactions.Count == 0)
                    {
                        message = "Error in " + rid.Room_No + ". Please contact admin.";
                        alerts.Add(message);
                    }

                    else if (item.Status == "CheckIn" && item.Departure_Date <= DateTime.Now.AddMinutes(30) && item.Departure_Date.AddHours(3) > DateTime.Now) //showing notification of add stay
                    {
                        message = "Room-" + rid.Room_No + " is scheduled to be checked out at" + item.Departure_Date.TimeOfDay;
                        alerts.Add(message);
                    }

                    else if (item.Status == "CheckIn" && item.Departure_Date.AddHours(3) <= DateTime.Now) //Increment stay by one day if 3 hours grace period exceeded
                    {
                        int extendedStay = 0;
                        while (initialDate.AddHours(3) < DateTime.Now)
                        {
                            tbl_room_transactions newRegularEntry = new tbl_room_transactions();
                            tbl_room_transactions newVatEntry = new tbl_room_transactions();
                            tbl_room_transactions newScEntry = new tbl_room_transactions();

                            newRegularEntry.Stay_Info_ID = item.Stay_Info_ID;
                            newRegularEntry.Room_ID = item.Room_ID;
                            newRegularEntry.Guest_ID = item.Guest_ID;
                            newRegularEntry.Tran_Ref_ID = 4;
                            newRegularEntry.Tran_Ref_Name = "Regular Charge";
                            newRegularEntry.Credit = 0;
                            newRegularEntry.Debit = rate;
                            newRegularEntry.Particulars = "Auto_Add_Stay";
                            newRegularEntry.Tran_Date = initialDate;
                            db.tbl_room_transactions.Add(newRegularEntry);

                            if (commonRoomData.VatPercent > 0)
                            {
                                newVatEntry.Stay_Info_ID = item.Stay_Info_ID;
                                newVatEntry.Room_ID = item.Room_ID;
                                newVatEntry.Guest_ID = item.Guest_ID;
                                newVatEntry.Tran_Ref_ID = 2;
                                newVatEntry.Tran_Ref_Name = "Vat";
                                newVatEntry.Credit = 0;
                                newVatEntry.Debit = commonRoomData.Vat;
                                newVatEntry.Particulars = "Auto_Add_Stay";
                                newVatEntry.Tran_Date = initialDate;
                                db.tbl_room_transactions.Add(newVatEntry);
                            }

                            if (commonRoomData.ScPercent > 0)
                            {
                                newScEntry.Stay_Info_ID = item.Stay_Info_ID;
                                newScEntry.Room_ID = item.Room_ID;
                                newScEntry.Guest_ID = item.Guest_ID;
                                newScEntry.Tran_Ref_ID = 17;
                                newScEntry.Tran_Ref_Name = "SC";
                                newScEntry.Credit = 0;
                                newScEntry.Debit = commonRoomData.SC;
                                newScEntry.Particulars = "Auto_Add_Stay";
                                newScEntry.Tran_Date = initialDate;
                                db.tbl_room_transactions.Add(newScEntry);
                            }

                            initialDate = initialDate.AddDays(1);
                            extendedStay++;
                        }
                        item.Departure_Date = initialDate;
                        db.tbl_guest_stay_info.Attach(item);
                        var updatedGuestInfo = db.Entry(item);
                        updatedGuestInfo.Property(a => a.Departure_Date).IsModified = true;
                        message = "Room-" + rid.Room_No + " is extended by " + extendedStay;
                        if (extendedStay > 1)
                        {
                            message += " days.";
                        }
                        else
                        {
                            message += " day.";
                        }
                        alerts.Add(message);
                    }
                }
            }
            ViewBag.Alert = alerts;
            ViewBag.AlertCount = alerts.Count;
            db.SaveChanges();


            //auto multiple entry delete start
            var query_multiple = "select * from tbl_guest_stay_info where Status = 'CheckIn' AND Room_ID in (select Room_ID from tbl_guest_stay_info where Status = 'CheckIn' group by Room_ID having count(*) > 1)";
            var data_multiple = db.Database.SqlQuery<tbl_guest_stay_info>(query_multiple);
            List<tbl_guest_stay_info> tempList_multiple = data_multiple.ToList();
            if (tempList_multiple.Count > 1)
            {
                int last_stay_info_id = tempList_multiple.LastOrDefault().Stay_Info_ID;
                int row_delete = db.Database.ExecuteSqlCommand("DELETE FROM tbl_room_transactions where Stay_Info_ID = " + last_stay_info_id);
                row_delete = db.Database.ExecuteSqlCommand("DELETE FROM tbl_guest_stay_info where Stay_Info_ID = " + last_stay_info_id);
            }
            //auto multiple entry delete end

            DateTime lastBackupDate = FrontDeskRoomColor();
            int nowHour = DateTime.Now.Hour;
            int nowMint = DateTime.Now.Minute;
  
            //Backup portion begin here     
            if (nowHour == 15 && nowMint >= 0 && nowMint <= 59)
            {
                if (lastBackupDate.Date != DateTime.Today.Date)
                {
                    Task.Run(() => Backup());
                }
            }
            //Backup portion end  
            
            return View();
        }

        //add this backup ethod for backing up data
        private async Task Backup()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PMS_HT_DB"].ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            string databaseName = builder.InitialCatalog;

            string projectFolder = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/DB_Backup"));
            string backupDrive = rpd.BackUpDrive;
            string backupDirectory = backupDrive + ":\\DBBackUp";
            var fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".bak";
            string attachmentName = backupDirectory + "\\" + fileName;

            if (!Directory.Exists(backupDirectory))
            {
                try
                {
                    Directory.CreateDirectory(backupDirectory);

                    SqlConnection connection = new SqlConnection(connectionString);
                    connection.Open();
                    SqlCommand sqlcmd = new SqlCommand("backup database " + databaseName + " to disk='" + attachmentName + "'", connection);
                    await sqlcmd.ExecuteNonQueryAsync();
                    connection.Close();

                    tbl_preferences backupDateRow = db.tbl_preferences.Find(7);
                    backupDateRow.Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                    db.Entry(backupDateRow).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception exception)
                {
                    ViewBag.ExceptionMessage = exception.Message;
                }
            }
        }

        public ActionResult DateLoad()
        {
            ViewBag.status = db.tbl_guest_stay_info.ToList();
            ViewBag.Date = Session["date"];

            return View();
        }

        public PartialViewResult search(string id)
        {
            var status = db.tbl_guest_stay_info.OrderBy(a => a.Room_ID).ToList();
            ViewBag.status = status;
            ViewBag.floors = db.tbl_floor_setup.ToList();
            ViewBag.categories = db.tbl_roomtype_setup.ToList();
            ViewBag.rooms = db.tbl_room_info.ToList();
            ViewBag.roomview = db.tbl_room_view_setup.ToList();
            ViewBag.count = status.Count;
            DateTime today = DateTime.Today.Date;
            DateTime selectedDate = Convert.ToDateTime(id);
            if (selectedDate < today)
            {
                Session["Day"] = "past";
            }
            else if (selectedDate > today)
            {
                Session["Day"] = "future";
            }
            else
            {
                Session["Day"] = "today";
            }

            Session["date"] = selectedDate;
            ViewBag.Date = selectedDate;
            FrontDeskRoomColor();
            return PartialView();
        }

        public ActionResult PrintRoomReport(string catagory)
        {
            var rom = db.tbl_room_info.OrderBy(a => a.Floor_ID).ToList();
            if (catagory == "Catagory")
            {
                rom = db.tbl_room_info.OrderBy(a => a.Roomtype_ID).ToList();
            }
            for (int i = 0; i < rom.Count; i++)
            {
                int roomId = rom[i].Room_ID;
                var status = (from guest in db.tbl_guest_stay_info
                              where guest.Room_ID == roomId
                              && guest.Status == "CheckIn"
                              select guest.tbl_guest_info.Name).ToList().FirstOrDefault();
                if (status == null)
                {
                    rom[i].Status = "Clear";
                }
                else
                {
                    rom[i].Status = status;
                }
            }
            return View(rom);
        }

        [AllowAnonymous]
        public JsonResult ExportReport(string catagory)
        {
            int checkedRoom = 0;
            var roomList = (from r in db.tbl_room_info
                orderby r.Floor_ID
                select new RoomViewModel
                {
                    Room_ID = r.Room_ID,
                    Room_No = r.Room_No,
                    Floor_ID = r.Floor_ID ?? 0,
                    Status = "",
                    arrival_date = "",
                    Roomtype_Name = r.tbl_roomtype_setup.Roomtype_Name
                }).ToList();
            if (catagory == "Catagory")
            {
                roomList = (from r in db.tbl_room_info
                    orderby r.Roomtype_ID
                    select new RoomViewModel
                    {
                        Room_ID = r.Room_ID,
                        Room_No = r.Room_No,
                        Floor_ID = r.Floor_ID ?? 0,
                        Status = "",
                        arrival_date = "",
                        Roomtype_Name = r.tbl_roomtype_setup.Roomtype_Name
                    }).ToList();
            }
            else if (catagory == "RoomView")
            {
                roomList = (from r in db.tbl_room_info
                    orderby r.Room_view_ID
                    select new RoomViewModel
                    {
                        Room_ID = r.Room_ID,
                        Room_No = r.Room_No,
                        Floor_ID = r.Floor_ID ?? 0,
                        Status = "",
                        arrival_date = "",
                        Roomtype_Name = r.tbl_room_view_setup.View_type
                    }).ToList();
            }
            for (int i = 0; i < roomList.Count; i++)
            {
                int roomId = roomList[i].Room_ID;
                var stat = (from guest in db.tbl_guest_stay_info
                    where guest.Room_ID == roomId
                          && guest.Status == "CheckIn"
                    select new
                    {
                        name = guest.tbl_guest_info.Name,
                        arrival_date = guest.Arrival_Date
                    })
                    .ToList()
                    .FirstOrDefault();
                if (stat == null)
                {
                    roomList[i].Status = "Vacant";
                }
                else
                {
                    roomList[i].Status = stat.name;
                    roomList[i].arrival_date = stat.arrival_date.ToString("dd/MM/yyyy hh:mm tt");
                    checkedRoom++;
                }
            }

            string title = catagory + "wise Room Information";

            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "RoomReport.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }

            ReportDataSource DataSetRooms = new ReportDataSource("DataSetRooms", roomList);
            lc.DataSources.Add(DataSetRooms);

            ReportParameter[] param = new ReportParameter[7];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);
            param[4] = new ReportParameter("Title", title, false);
            param[5] = new ReportParameter("CheckedRoom", checkedRoom.ToString(), false);
            param[6] = new ReportParameter("VacantRoom", (roomList.Count - checkedRoom).ToString(), false);
            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);

        }
    }
}