﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class LaundryGermentController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_return_germent_in.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_return_germent_in germent)
        {
            if (ModelState.IsValid)
            {
                db.tbl_return_germent_in.Add(germent);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_return_germent_in.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_return_germent_in germent, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(germent).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_return_germent_in.Remove(db.tbl_return_germent_in.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
