﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantDineLocationController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantDineLocation
        public ActionResult Index()
        {
            return View(db.tbl_dining_location.ToList());
        }

        // GET: RestaurantDineLocation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_location tbl_dining_location = db.tbl_dining_location.Find(id);
            if (tbl_dining_location == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_location);
        }

        // GET: RestaurantDineLocation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RestaurantDineLocation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Dining_Location_ID,Location_Name,Location_Type,Point_A,Point_B,Point_C,Radius")] tbl_dining_location tbl_dining_location)
        {
            if (ModelState.IsValid)
            {
                db.tbl_dining_location.Add(tbl_dining_location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_dining_location);
        }

        // GET: RestaurantDineLocation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_location tbl_dining_location = db.tbl_dining_location.Find(id);
            if (tbl_dining_location == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_dining_location);
        }

        // POST: RestaurantDineLocation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Dining_Location_ID,Location_Name,Location_Type,Point_A,Point_B,Point_C,Radius")] tbl_dining_location tbl_dining_location)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_dining_location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_dining_location);
        }

        // GET: RestaurantDineLocation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_location tbl_dining_location = db.tbl_dining_location.Find(id);
            if (tbl_dining_location == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_location);
        }

        // POST: RestaurantDineLocation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_dining_location tbl_dining_location = db.tbl_dining_location.Find(id);
            db.tbl_dining_location.Remove(tbl_dining_location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
