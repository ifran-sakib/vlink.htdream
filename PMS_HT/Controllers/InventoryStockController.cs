﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace PMS_HT.Controllers
{
    public class InventoryStockController : CustomControllerBase
    {
        public ActionResult Index()
        {
            //var tbl_stock = db.tbl_stock.Include(t => t.tbl_item).Include(t => t.tbl_stock_location);
            var tbl_stock = from st in db.tbl_stock
                            join item in db.tbl_item
                            on st.Item_ID equals item.Item_ID
                            join loc in db.tbl_stock_location
                            on st.Stock_Location_ID equals loc.Stock_Location_ID
                            group st by new { st.Item_ID, st.tbl_stock_location.Stock_Location_ID, st.tbl_item.Item_Name, st.tbl_stock_location.Stock_Location_Name } into a
                            select new StockViewModel
                            {
                                Item_ID = (int)a.Key.Item_ID,
                                Stock_Location_ID = (int)a.Key.Stock_Location_ID,
                                Item_Name = a.Key.Item_Name,
                                Stock_Location_Name = a.Key.Stock_Location_Name,
                                Quantity = (Decimal)(a.Sum(x => x.Stock_In) ?? 0) - (a.Sum(x => x.Stock_Out) ?? 0)
                            };
            return View(tbl_stock.ToList());
        }

        public ActionResult CurrentStock()
        {
            //var tbl_stock = db.tbl_stock.Include(t => t.tbl_item).Include(t => t.tbl_stock_location);
            var tbl_stock = from st in db.tbl_stock
                            join item in db.tbl_item
                            on st.Item_ID equals item.Item_ID
                            join loc in db.tbl_stock_location
                            on st.Stock_Location_ID equals loc.Stock_Location_ID
                            group st by new { st.Item_ID, st.tbl_stock_location.Stock_Location_ID, st.tbl_item.Item_Name, st.tbl_stock_location.Stock_Location_Name } into a
                            select new StockViewModel
                            {

                                Item_ID = (int)a.Key.Item_ID,
                                Stock_Location_ID = (int)a.Key.Stock_Location_ID,
                                Item_Name = a.Key.Item_Name,
                                Stock_Location_Name = a.Key.Stock_Location_Name,
                                Quantity = (Decimal)(a.Sum(x => x.Stock_In) ?? 0) - (a.Sum(x => x.Stock_Out) ?? 0)
                            };
            return View(tbl_stock.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name");
            int cata_id = db.tbl_category.FirstOrDefault().Cata_ID;
            ViewBag.Item_ID = new SelectList(db.tbl_item.Where(a => a.Cata_ID == cata_id), "Item_ID", "Item_Name");
            ViewBag.unit = db.tbl_item.Where(a => a.Cata_ID == cata_id).FirstOrDefault().tbl_unit.Unit_Name;
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_stock stock)
        {
            if (ModelState.IsValid)
            {
                tbl_item tbl_item = db.tbl_item.Where(a => a.Item_ID == stock.Item_ID).FirstOrDefault();
                tbl_stock_location tbl_stock_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == stock.Stock_Location_ID).FirstOrDefault();
                stock.tbl_item = tbl_item;
                stock.tbl_stock_location = tbl_stock_location;
                stock.Stock_Out = 0;
                db.tbl_stock.Add(stock);
                db.SaveChanges();
            }
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name", stock.Item_ID);
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name", stock.Stock_Location_ID);
            return RedirectToAction("Index", "InventoryHome");
        }

        public ActionResult Edit(int id)
        {
            tbl_stock stock = db.tbl_stock.Find(id);
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name", stock.Item_ID);
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name", stock.Stock_Location_ID);
            return PartialView(stock);
        }

        [HttpPost]
        public ActionResult Edit(tbl_stock stock, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stock).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name", stock.Item_ID);
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name", stock.Stock_Location_ID);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult StockOut(int id)
        {
            ViewBag.Item_ID = id;
            ViewBag.Item_Name = db.tbl_item.Find(id).Item_Name;
            ViewBag.unit = db.tbl_item.Find(id).tbl_unit.Unit_Name;
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StockOut(tbl_stock stock)
        {
            tbl_item tbl_item = db.tbl_item.Where(a => a.Item_ID == stock.Item_ID).FirstOrDefault();
            tbl_stock_location tbl_stock_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == stock.Stock_Location_ID).FirstOrDefault();
            if (tbl_item != null && tbl_stock_location != null)
            {
                decimal stockin = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_item.Item_ID && a.Stock_Location_ID == tbl_stock_location.Stock_Location_ID).Select(b => b.Stock_In).Sum());
                decimal stockout = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_item.Item_ID && a.Stock_Location_ID == tbl_stock_location.Stock_Location_ID).Select(b => b.Stock_Out).Sum());
                decimal available_stock = stockin >= stockout ? (stockin - stockout) : 0;
                if (available_stock >= stock.Stock_Out)
                {
                    tbl_stock tblStock = new tbl_stock();
                    tblStock.Stock_Location_ID = stock.Stock_Location_ID;
                    tblStock.Item_ID = stock.Item_ID;
                    tblStock.Stock_In = 0;
                    tblStock.Stock_Out = stock.Stock_Out;
                    tblStock.note = stock.note;
                    tblStock.stock_change_date = stock.stock_change_date;
                    tblStock.tbl_item = tbl_item;
                    tblStock.tbl_stock_location = tbl_stock_location;
                    db.tbl_stock.Add(tblStock);
                    db.SaveChanges();
                    return RedirectToAction("Index", "InventoryHome");
                }
            }
            ViewBag.Item_ID = stock.Item_ID;
            ViewBag.Item_Name = db.tbl_item.Find(stock.Item_ID).Item_Name;
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            return PartialView();
        }

        [HttpGet]
        public ActionResult MoveStock()
        {
            ViewBag.unit = db.tbl_item.FirstOrDefault().tbl_unit.Unit_Name;
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name");
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            ViewBag.Stock_Loc_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MoveStock(tbl_stock stock, int Stock_Loc_ID)
        {
            tbl_item tbl_item = db.tbl_item.Where(a => a.Item_ID == stock.Item_ID).FirstOrDefault();
            tbl_stock_location tbl_stock_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == stock.Stock_Location_ID).FirstOrDefault();
            tbl_stock_location new_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == Stock_Loc_ID).FirstOrDefault();

            if (tbl_item != null && tbl_stock_location != null)
            {
                decimal stockin = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_item.Item_ID && a.Stock_Location_ID == tbl_stock_location.Stock_Location_ID).Select(b => b.Stock_In).Sum());
                decimal stockout = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_item.Item_ID && a.Stock_Location_ID == tbl_stock_location.Stock_Location_ID).Select(b => b.Stock_Out).Sum());
                decimal available_stock = stockin >= stockout ? (stockin - stockout) : 0;
                if (available_stock >= stock.Stock_Out)
                {
                    tbl_stock tblStock = new tbl_stock();
                    tblStock.Stock_Location_ID = stock.Stock_Location_ID;
                    tblStock.Item_ID = stock.Item_ID;
                    tblStock.Stock_In = 0;
                    tblStock.Stock_Out = stock.Stock_Out;
                    tblStock.note = stock.note + " -  Move from " + tbl_stock_location.Stock_Location_Name + " to " + new_location.Stock_Location_Name;
                    tblStock.stock_change_date = stock.stock_change_date;
                    tblStock.tbl_item = tbl_item;
                    tblStock.tbl_stock_location = tbl_stock_location;
                    db.tbl_stock.Add(tblStock);

                    tbl_stock newStock = new tbl_stock();
                    newStock.Stock_Location_ID = new_location.Stock_Location_ID;
                    newStock.Item_ID = stock.Item_ID;
                    newStock.Stock_In = stock.Stock_Out;
                    newStock.Stock_Out = 0;
                    newStock.note = stock.note + " -  Move from " + tbl_stock_location.Stock_Location_Name + " to " + new_location.Stock_Location_Name;
                    newStock.stock_change_date = stock.stock_change_date;
                    newStock.tbl_item = tbl_item;
                    newStock.tbl_stock_location = new_location;
                    db.tbl_stock.Add(newStock);

                    db.SaveChanges();
                    return RedirectToAction("Index", "InventoryHome");
                }
            }
            ViewBag.Item_ID = stock.Item_ID;
            ViewBag.Item_Name = db.tbl_item.Find(stock.Item_ID).Item_Name;
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            return PartialView();
        }

        public ActionResult Locate(int id)
        {
            var location_data = db.tbl_stock.Where(a => a.Item_ID == 5).GroupBy(b => b.Stock_Location_ID).Select(
                                g => new
                                {
                                    Item_Name = g.Select(s => s.tbl_item.Item_Name).FirstOrDefault(),
                                    Available_Stock = g.Sum(s => s.Stock_In) - g.Sum(s => s.Stock_Out),
                                    Stock_Location = g.Select(s => s.tbl_stock_location.Stock_Location_Name).FirstOrDefault(),
                                });
            var location_data2 = from st in db.tbl_stock
                                 join it in db.tbl_item on st.Item_ID equals it.Item_ID
                                 join loc in db.tbl_stock_location on st.Stock_Location_ID equals loc.Stock_Location_ID
                                 where st.Item_ID == id
                                 group st by st.Stock_Location_ID into g
                                 select new ItemLocateViewModel
                                 {
                                     Item_Name = g.Select(a => a.tbl_item).FirstOrDefault().Item_Name,
                                     Available_Stock = (g.Sum(a => a.Stock_In) - g.Sum(s => s.Stock_Out) ?? 0),
                                     Stock_Location = g.Select(a => a.tbl_stock_location).FirstOrDefault().Stock_Location_Name
                                 };
            ViewBag.unit = db.tbl_item.Where(a => a.Item_ID == id).FirstOrDefault().tbl_unit.Unit_Name;
            ViewBag.location_data = location_data2;
            return PartialView();
        }

        public JsonResult ExportStock()
        {
            var stock_items = (from item in db.tbl_item
                               join cat in db.tbl_category
                               on item.Cata_ID equals cat.Cata_ID
                               join sub in db.tbl_subcategory
                               on item.Subcata_ID equals sub.Subcata_ID
                               join brand in db.tbl_brand
                               on item.Brand_ID equals brand.Brand_ID
                               join unit in db.tbl_unit
                               on item.Unit_ID equals unit.Unit_ID
                               join stock in db.tbl_stock
                               on item.Item_ID equals stock.Item_ID
                               group stock by new { stock.Item_ID, item.Item_Name, cat.Cata_Name, sub.Subcata_Name, brand.Brand_Name, unit.Unit_Name, item.Reorder_Level } into g
                               select new
                               {
                                   item = g.Key.Item_Name,
                                   cata = g.Key.Cata_Name,
                                   subcata = g.Key.Subcata_Name,
                                   brand = g.Key.Brand_Name,
                                   unit = g.Key.Unit_Name,
                                   reorder = g.Key.Reorder_Level ?? 0,
                                   quantity = (g.Sum(a => a.Stock_In) ?? 0) - (g.Sum(a => a.Stock_Out) ?? 0)
                               }).ToList();

            var fileName = "InventoryStock_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
            CommonRepoprtData rpd = new CommonRepoprtData();
            LocalReport lc = new LocalReport();

            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "InventoryStock.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("");
            }

            ReportDataSource rd = new ReportDataSource("DataSet1", stock_items);
            lc.DataSources.Add(rd);

            ReportParameter[] param = new ReportParameter[4];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);
            
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            
            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            db.tbl_stock.Remove(db.tbl_stock.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult PullAvailableStock(int item_id, int loc_id)
        {
            //var stockqty = (from a in db.tbl_stock where a.Stock_Location_ID == stockId && a.Item_ID == itemId select a.Quantity).Sum();
            decimal stockin = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == item_id && a.Stock_Location_ID == loc_id).Select(b => b.Stock_In).Sum());
            decimal stockout = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == item_id && a.Stock_Location_ID == loc_id).Select(b => b.Stock_Out).Sum());
            if (stockin >= stockout)
            {
                return Json(stockin - stockout, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FillItem(int cata_id)
        {
            var items = db.tbl_item.Where(c => c.Cata_ID == cata_id).Select(c => new { Item_ID = c.Item_ID, Item_Name = c.Item_Name });
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
