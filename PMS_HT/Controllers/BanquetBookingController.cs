using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetBookingController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            var query = from tbl_banquet_booking in db.tbl_banquet_booking
                        join Prorgram_Purpose in db.Prorgram_Purpose on tbl_banquet_booking.P_Purpose_ID equals Prorgram_Purpose.P_Purpose_ID
                        join tbl_banquet_prog in db.tbl_banquet_prog on tbl_banquet_booking.Program_ID equals tbl_banquet_prog.Program_ID
                        join tbl_banquet_info in db.tbl_banquet_info on tbl_banquet_prog.Banquet_ID equals tbl_banquet_info.Banquet_ID
                        join tbl_banquet_transactions in db.tbl_banquet_transactions on tbl_banquet_booking.Banq_Booking_ID equals tbl_banquet_transactions.Banq_Booking_ID
                        where tbl_banquet_booking.Program_ID == id && tbl_banquet_transactions.Tran_Ref_Name.Equals("rent")
                        select new BookinViewModel
                        {
                            Start_Date = tbl_banquet_booking.Start_Date,
                            End_Date = tbl_banquet_booking.End_Date,
                            Purpose_Name = Prorgram_Purpose.Purpose_Name,
                            Particulars = tbl_banquet_transactions.Particulars,
                            Debit = tbl_banquet_transactions.Debit,
                            Program_ID = tbl_banquet_transactions.Program_ID,
                            Banq_Booking_ID = tbl_banquet_transactions.Banq_Booking_ID,
                            Status = tbl_banquet_info.Status
                        };

            ViewBag.Booking = query.ToList();
            ViewBag.Program_ID = id;
            Session["Program_ID"] = id;
            ViewBag.status = "Vacant";
            ViewBag.status2 = "Booked";

            return View(db.tbl_banquet_booking.ToList());
        }

        public ActionResult Add(int id)
        {
            ViewBag.P_Purpose_ID = new SelectList(db.Prorgram_Purpose, "P_Purpose_ID", "Purpose_Name");
            ViewBag.Program_ID = id;
            // ViewBag.rent = db.tbl_banquet_booking.Where(a => a.Program_ID == id).Select(a => a.tbl_banquet_prog.tbl_banquet_info.Rent).FirstOrDefault();
            ViewBag.rent = db.tbl_banquet_prog.Where(a => a.Program_ID == id).Select(a => a.tbl_banquet_info.Rent).FirstOrDefault();
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_banquet_booking booking, string particular, decimal? rent)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);
            //ViewBag.P_Purpose_ID = new SelectList(db.Prorgram_Purpose, "P_Purpose_ID", "Purpose_Name");
            if (ModelState.IsValid)
            {
                booking.Program_ID = parameter;
                db.tbl_banquet_booking.Add(booking);
                tbl_banquet_transactions temp = new tbl_banquet_transactions();
                temp.Debit = rent;
                temp.Particulars = particular;
                temp.Program_ID = parameter;
                temp.Tran_Ref_Name = "rent";
                temp.Banq_Booking_ID = booking.Banq_Booking_ID;
                db.tbl_banquet_transactions.Add(temp);

                db.SaveChanges();
            }
            ViewBag.P_Purpose_ID = new SelectList(db.Prorgram_Purpose, "P_Purpose_ID", "Purpose_Name", booking.P_Purpose_ID);
            return RedirectToAction("Index", new { id = parameter });
        }

        public JsonResult ErrorMessageAdd(DateTime Start_Date, DateTime End_Date)
        {
            var prev_program = db.tbl_banquet_booking.ToList();
            var message = "";
            if (Start_Date >= End_Date)
            {
                message = "Invalid date range.";
                return Json(message.ToList(), JsonRequestBehavior.AllowGet);
            }
            foreach (var item in prev_program)
            {
                if (item.Start_Date <= End_Date && item.End_Date >= Start_Date)
                {
                    message = "Booking already exists in selected date. Please choose another.";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(message.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int? id, int? id1)
        {
            tbl_banquet_booking booking = db.tbl_banquet_booking.Find(id);
            ViewBag.particular = db.tbl_banquet_transactions.Where(a => a.Program_ID == booking.Program_ID && a.Tran_Ref_Name.Equals("rent")).Select(a => a.Particulars).FirstOrDefault();
            ViewBag.rent = db.tbl_banquet_transactions.Where(a => a.Program_ID == booking.Program_ID && a.Tran_Ref_Name.Equals("rent") && a.Banq_Booking_ID == id1).Select(a => a.Debit).FirstOrDefault();

            ViewBag.P_Purpose_ID = new SelectList(db.Prorgram_Purpose, "P_Purpose_ID", "Purpose_Name", booking.P_Purpose_ID);
            return PartialView(booking);
        }

        [HttpPost]
        public ActionResult Edit(tbl_banquet_booking booking, decimal? Rent, string Particular)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);
            if (ModelState.IsValid)
            {
                booking.Program_ID = parameter;
                tbl_banquet_transactions a = db.tbl_banquet_transactions.Where(vv => vv.Banq_Booking_ID == booking.Banq_Booking_ID).Where(b => b.Program_ID == booking.Program_ID).Where(c => c.Tran_Ref_Name.Equals("rent")).FirstOrDefault();
                a.Debit = Rent;
                a.Particulars = Particular;
                a.Program_ID = booking.Program_ID;
                a.Banq_Booking_ID = booking.Banq_Booking_ID;
                a.Tran_Ref_Name = "rent";
                db.Entry(a).State = EntityState.Modified;
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.P_Purpose_ID = new SelectList(db.Prorgram_Purpose, "P_Purpose_ID", "Purpose_Name", booking.P_Purpose_ID);
            return RedirectToAction("Index", new { id = parameter });
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["Program_ID"]);
            var x = db.tbl_banquet_transactions.Where(a => a.Banq_Booking_ID == id).ToList();
            foreach (var item in x)
            {
                db.tbl_banquet_transactions.Remove(item);
            }
            db.tbl_banquet_booking.Remove(db.tbl_banquet_booking.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
