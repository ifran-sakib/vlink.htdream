using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Globalization;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Inventory")]
    public class InventoryPurchaseDetailsController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_purchase_master = db.tbl_purchase_master.Include(t => t.tbl_supplier);
            return View(tbl_purchase_master.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                tbl_purchase_master master = db.tbl_purchase_master.Find(id);
                if (master == null)
                {
                    return RedirectToAction("Index");
                }
                ViewBag.Memo_No = master.Memo_No;
                ViewBag.Sup_Name = master.tbl_supplier.Sup_Name;
                ViewBag.Supplier_ID = master.Supplier_ID;
                ViewBag.Purchase_Master_ID = id;

                decimal debit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == id).Select(b => b.Debit).Sum());
                decimal credit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == id).Select(b => b.Credit).Sum());
                ViewBag.Total = debit - credit;
                var tbl_purchase_details = db.tbl_purchase_details.Where(x => x.Purchase_Master_ID == id).Include(t => t.tbl_item).Include(t => t.tbl_purchase_master).Include(t => t.tbl_stock_location);

                return View(tbl_purchase_details.ToList());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult Add(int id)
        {
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name");
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name");
            ViewBag.Purchase_Master_ID = id;
            var tblPurchaseMaster = db.tbl_purchase_master.FirstOrDefault(a => a.Purchase_Master_ID == id);
            if (tblPurchaseMaster != null)
            {
                ViewBag.Memo_No = tblPurchaseMaster.Memo_No;
                ViewBag.Supplier_Name = tblPurchaseMaster.tbl_supplier.Sup_Name;
            }
            ViewBag.unit = db.tbl_item.FirstOrDefault().tbl_unit.Unit_Name;
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_purchase_details tbl_purchase_details)
        {
            tbl_supplier_ledger supplier_ledger_buy = new tbl_supplier_ledger();
            tbl_stock tbl_stock = new tbl_stock();
            tbl_purchase_details newrow = new tbl_purchase_details();

            tbl_purchase_master tbl_purchase_master = db.tbl_purchase_master.Find(tbl_purchase_details.Purchase_Master_ID);
            tbl_item tbl_item = db.tbl_item.Where(a => a.Item_ID == tbl_purchase_details.Item_ID).FirstOrDefault();
            tbl_stock_location tbl_stock_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == tbl_purchase_details.Stock_Location_ID).FirstOrDefault();
            tbl_supplier tbl_supplier = db.tbl_supplier.Where(a => a.Supplier_ID == tbl_purchase_master.Supplier_ID).FirstOrDefault();
            decimal vatpercent = Convert.ToDecimal(tbl_purchase_details.Item_Vat);
            decimal quantity = Convert.ToDecimal(tbl_purchase_details.Quantity);
            decimal price = Convert.ToDecimal(tbl_purchase_details.Purchase_Price);
            decimal vat = (price * quantity * vatpercent) / 100;

            tbl_purchase_details.Item_Vat = Decimal.Round(vat, 2);
            tbl_purchase_details.tbl_item = tbl_item;
            tbl_purchase_details.tbl_purchase_master = tbl_purchase_master;
            db.tbl_purchase_details.Add(tbl_purchase_details);
            db.SaveChanges();
            var query = "WITH x AS (SELECT *, r = RANK() OVER (ORDER BY Purchase_Details_ID DESC) from tbl_purchase_details) " +
            "SELECT * FROM x WHERE r = 1";
            var data = db.Database.SqlQuery<tbl_purchase_details>(query);
            newrow = data.FirstOrDefault();

            tbl_stock.Stock_Location_ID = tbl_purchase_details.Stock_Location_ID;
            tbl_stock.Item_ID = tbl_purchase_details.Item_ID;
            tbl_stock.Stock_In = tbl_purchase_details.Quantity;
            tbl_stock.Stock_Out = 0;
            tbl_stock.stock_change_date = DateTime.Now;
            tbl_stock.Purchase_Details_ID = newrow.Purchase_Details_ID;
            tbl_stock.tbl_item = tbl_item;
            tbl_stock.tbl_stock_location = tbl_stock_location;
            db.tbl_stock.Add(tbl_stock);

            decimal total = Convert.ToDecimal(tbl_purchase_master.Memo_Total);
            total = total + (quantity * price) + vat;
            tbl_purchase_master.Memo_Total = Decimal.Round(total, 2);
            tbl_purchase_master.status = "Due";
            db.Entry(tbl_purchase_master).State = EntityState.Modified;
            db.SaveChanges();

            supplier_ledger_buy.Supplier_ID = tbl_supplier.Supplier_ID;
            supplier_ledger_buy.tbl_supplier = tbl_supplier;
            supplier_ledger_buy.Purchase_Master_ID = tbl_purchase_master.Purchase_Master_ID;
            supplier_ledger_buy.Transaction_Date = DateTime.Now;
            supplier_ledger_buy.Tran_Ref_Name = "BuyProduct";
            supplier_ledger_buy.Tran_Ref_ID = 1;
            supplier_ledger_buy.Credit = 0;
            supplier_ledger_buy.Debit = Decimal.Round(((quantity * price) + vat), 2);
            supplier_ledger_buy.Particulars = "Add Product";
            supplier_ledger_buy.tbl_purchase_master = tbl_purchase_master;
            if (total > 0)
            {
                db.tbl_supplier_ledger.Add(supplier_ledger_buy);
            }
            db.SaveChanges();
            return RedirectToAction("Details", "InventoryPurchaseDetails", new { id = tbl_purchase_details.Purchase_Master_ID });
        }

        [HttpGet]
        public ActionResult Pay()
        {
            ViewBag.Supplier_ID = new SelectList(db.tbl_supplier, "Supplier_ID", "Sup_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Pay(tbl_supplier_ledger tbl_supplier_ledger, decimal? discount, decimal? paid, int source)
        {
            if (ModelState.IsValid)
            {
                tbl_purchase_master master = db.tbl_purchase_master.Find(tbl_supplier_ledger.Purchase_Master_ID);
                tbl_supplier_ledger supplier_ledger_paid = new tbl_supplier_ledger();
                tbl_supplier_ledger supplier_ledger_discount = new tbl_supplier_ledger();

                decimal debit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == master.Purchase_Master_ID).Select(b => b.Debit).Sum());
                decimal credit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == master.Purchase_Master_ID).Select(b => b.Credit).Sum());
                decimal total = debit - credit;
                decimal disc = Convert.ToDecimal(discount);
                decimal pa = Convert.ToDecimal(paid);

                if (total <= 0 && source == 1)
                {
                    return RedirectToAction("Details", "InventoryPurchaseDetails", new { id = master.Purchase_Master_ID });
                }
                if (total <= 0 && source == 2)
                {
                    return RedirectToAction("Index", "InventoryHome");
                }

                supplier_ledger_paid.Supplier_ID = tbl_supplier_ledger.Supplier_ID;
                supplier_ledger_paid.tbl_supplier = master.tbl_supplier;
                supplier_ledger_paid.Transaction_Date = tbl_supplier_ledger.Transaction_Date;
                supplier_ledger_paid.Tran_Ref_Name = "Paid";
                supplier_ledger_paid.Tran_Ref_ID = 2;
                supplier_ledger_paid.Debit = 0;
                supplier_ledger_paid.Purchase_Master_ID = master.Purchase_Master_ID;
                supplier_ledger_paid.tbl_purchase_master = master;
                supplier_ledger_paid.Particulars = tbl_supplier_ledger.Particulars;

                supplier_ledger_discount.Supplier_ID = tbl_supplier_ledger.Supplier_ID;
                supplier_ledger_discount.tbl_supplier = master.tbl_supplier;
                supplier_ledger_discount.Transaction_Date = tbl_supplier_ledger.Transaction_Date;
                supplier_ledger_discount.Tran_Ref_Name = "Discount";
                supplier_ledger_discount.Tran_Ref_ID = 3;
                supplier_ledger_discount.Debit = 0;
                supplier_ledger_discount.Purchase_Master_ID = master.Purchase_Master_ID;
                supplier_ledger_discount.tbl_purchase_master = master;
                supplier_ledger_discount.Particulars = tbl_supplier_ledger.Particulars;

                if (discount != null && discount > 0)
                {
                    supplier_ledger_discount.Credit = (discount <= total) ? discount : total;
                    db.tbl_supplier_ledger.Add(supplier_ledger_discount);
                }
                if (paid != null && paid > 0)
                {
                    supplier_ledger_paid.Credit = ((disc + paid) <= total) ? paid : (total - disc);
                    db.tbl_supplier_ledger.Add(supplier_ledger_paid);
                }

                master.status = ((disc + pa) < total) ? "Due" : "Paid";
                db.Entry(master).State = EntityState.Modified;
                db.SaveChanges();
                if (source == 1)
                {
                    return RedirectToAction("Details", "InventoryPurchaseDetails", new { id = master.Purchase_Master_ID });
                }
                else
                {
                    return RedirectToAction("Index", "InventoryHome");
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ReturnProduct(int id)
        {
            tbl_purchase_details tbl_purchase_details = db.tbl_purchase_details.Find(id);
            if (tbl_purchase_details != null)
            {
                decimal sin = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_purchase_details.Item_ID && a.Stock_Location_ID == tbl_purchase_details.Stock_Location_ID).Select(b => b.Stock_In).Sum());
                decimal sout = Convert.ToDecimal(db.tbl_stock.Where(a => a.Item_ID == tbl_purchase_details.Item_ID && a.Stock_Location_ID == tbl_purchase_details.Stock_Location_ID).Select(b => b.Stock_Out).Sum());
                ViewBag.Available = sin > sout ? (sin - sout) : 0;
                ViewBag.memo = tbl_purchase_details;
                return PartialView();
            }
            return RedirectToAction("Index", "InventoryHome");
        }

        [HttpPost]
        public ActionResult ReturnProduct(tbl_stock tbl_stock, int details_id)
        {
            if (ModelState.IsValid)
            {
                tbl_purchase_details tbl_purchase_details = db.tbl_purchase_details.Find(details_id);
                tbl_purchase_details newdetail = new tbl_purchase_details();
                tbl_purchase_details newrow = new tbl_purchase_details();
                tbl_supplier_ledger ledger = new tbl_supplier_ledger();
                if (tbl_purchase_details != null)
                {
                    newdetail = tbl_purchase_details;
                    decimal prevat = Convert.ToDecimal(newdetail.Item_Vat);
                    prevat /= (newdetail.Purchase_Price.Value * newdetail.Quantity.Value);
                    prevat *= (tbl_stock.Stock_Out.Value * tbl_purchase_details.Purchase_Price.Value);
                    if (newdetail.Item_Vat != null)
                    {
                        newdetail.Item_Vat = Decimal.Round(prevat, 2);
                    }
                    newdetail.Quantity = tbl_stock.Stock_Out * (-1);
                    db.tbl_purchase_details.Add(tbl_purchase_details);
                    db.SaveChanges();
                    var query = "WITH x AS (SELECT *, r = RANK() OVER (ORDER BY Purchase_Details_ID DESC) from tbl_purchase_details) " +
                    "SELECT * FROM x WHERE r = 1";
                    var data = db.Database.SqlQuery<tbl_purchase_details>(query);
                    newrow = data.FirstOrDefault();

                    tbl_purchase_master master = newdetail.tbl_purchase_master;

                    tbl_stock.Stock_In = 0;
                    tbl_stock.Item_ID = tbl_purchase_details.Item_ID;
                    tbl_stock.Stock_Location_ID = tbl_purchase_details.Stock_Location_ID;
                    tbl_stock.Purchase_Details_ID = newrow.Purchase_Details_ID;
                    tbl_stock.tbl_item = tbl_purchase_details.tbl_item;
                    tbl_stock.tbl_stock_location = tbl_purchase_details.tbl_stock_location;
                    db.tbl_stock.Add(tbl_stock);

                    decimal credit = Convert.ToDecimal(newdetail.Purchase_Price.Value* newdetail.Quantity.Value);
                    credit *= (-1);
                    credit += prevat;

                    ledger.Supplier_ID = master.Supplier_ID;
                    ledger.tbl_supplier = master.tbl_supplier;
                    ledger.Debit = 0;
                    ledger.Credit = credit;
                    ledger.Tran_Ref_ID = 6;
                    ledger.Tran_Ref_Name = "ReturnProduct";
                    ledger.Transaction_Date = tbl_stock.stock_change_date;
                    ledger.Particulars = "Return Product";
                    ledger.Purchase_Master_ID = master.Purchase_Master_ID;
                    ledger.tbl_purchase_master = master;
                    db.tbl_supplier_ledger.Add(ledger);
                    db.SaveChanges();
                    return RedirectToAction("Details", "InventoryPurchaseDetails", new { id = tbl_purchase_details.Purchase_Master_ID });
                }
            }

            return RedirectToAction("Index", "InventoryHome");
        }

        [HttpGet]
        public ActionResult CashBack()
        {
            ViewBag.Supplier_ID = new SelectList(db.tbl_supplier, "Supplier_ID", "Sup_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult CashBack(tbl_supplier_ledger ledger)
        {
            if (ModelState.IsValid)
            {
                tbl_purchase_master master = db.tbl_purchase_master.Find(ledger.Purchase_Master_ID);
                ledger.tbl_purchase_master = master;
                ledger.tbl_supplier = master.tbl_supplier;
                ledger.Credit = 0;
                ledger.Tran_Ref_ID = 4;
                ledger.Tran_Ref_Name = "Cashback";
                db.tbl_supplier_ledger.Add(ledger);
                db.SaveChanges();
            }
            return RedirectToAction("Index", "InventoryHome");
        }

        public JsonResult ExportMemo(int id)
        {
            tbl_purchase_master master = db.tbl_purchase_master.Find(id);
            string name = master.tbl_supplier.Sup_Name;
            string address = master.tbl_supplier.Sup_Address;
            string phone = master.tbl_supplier.Sup_Phone_No;
            if (phone == null)
            {
                phone = "";
            }
            string memo = master.Memo_No;
            DateTime pur_date = master.Purchase_Date.Value;

            var purchased_items = (from prod in db.tbl_purchase_details
                                  where prod.Purchase_Master_ID == id && prod.Quantity.Value > 0
                                  select new
                                  {
                                      Item_Name = prod.tbl_item.Item_Name,
                                      Quantity = prod.Quantity ?? 0,
                                      Unit_Price = prod.Purchase_Price ?? 0,
                                      Vat = prod.Item_Vat ?? 0,
                                      Total = ((prod.Quantity ?? 0) * (prod.Purchase_Price ?? 0)) + (prod.Item_Vat ?? 0)
                                  }).ToList();

            var returned_items = (from prod in db.tbl_purchase_details
                                   where prod.Purchase_Master_ID == id && prod.Quantity.Value < 0
                                   select new
                                   {
                                       Item_Name = prod.tbl_item.Item_Name,
                                       Quantity = (prod.Quantity ?? 0) * (-1),
                                       Unit_Price = prod.Purchase_Price ?? 0,
                                       Vat = prod.Item_Vat ?? 0,
                                       Total = (((prod.Quantity ?? 0) * (-1)) * (prod.Purchase_Price ?? 0)) + (prod.Item_Vat ?? 0)
                                   }).ToList();
            decimal CashBack = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == id && a.Tran_Ref_ID == 4).Select(b => b.Debit).Sum());
            decimal Paid = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == id && (a.Tran_Ref_ID == 2 || a.Tran_Ref_ID == 5)).Select(b => b.Credit).Sum());
            decimal Discount = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == id && a.Tran_Ref_ID == 3).Select(b => b.Credit).Sum());

            var fileName = "MemoDetail_" + memo + "_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf";



            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "MemoDetail.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("");
            }

            ReportDataSource DataSetAllProduct = new ReportDataSource("DataSetAllProduct", purchased_items);
            ReportDataSource DataSetReturnedProduct = new ReportDataSource("DataSetReturnedProduct", returned_items);

            lc.DataSources.Add(DataSetAllProduct);
            lc.DataSources.Add(DataSetReturnedProduct);

            ReportParameter[] param = new ReportParameter[12];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);
            param[4] = new ReportParameter("Supplier_Name", name, false);
            param[5] = new ReportParameter("Supplier_Address", address, false);
            param[6] = new ReportParameter("Supplier_Phone", phone, false);
            param[7] = new ReportParameter("Memo_No", memo, false);
            param[8] = new ReportParameter("Purchase_Date", pur_date.ToString(), false);
            param[9] = new ReportParameter("CashBack", CashBack.ToString("F", CultureInfo.InvariantCulture), false);
            param[10] = new ReportParameter("Paid", Paid.ToString("F", CultureInfo.InvariantCulture), false);
            param[11] = new ReportParameter("Discount", Discount.ToString("F", CultureInfo.InvariantCulture), false);

            lc.SetParameters(param);          

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }


        public ActionResult FillMemoNo(int supl_id)
        {
            var memos = db.tbl_purchase_master.Where(c => c.Supplier_ID == supl_id && c.status == "Due").Select(c => new { Purchase_Master_ID = c.Purchase_Master_ID, Memo_No = c.Memo_No });
            return Json(memos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillData(int master_id)
        {
            decimal debit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == master_id).Select(b => b.Debit).Sum());
            decimal credit = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == master_id).Select(b => b.Credit).Sum());
            decimal total = debit - credit;
            return Json(total, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillRecivable(int master_id)
        {
            var debit = db.tbl_purchase_details.Where(a => a.Purchase_Master_ID == master_id && a.Quantity < 0);
            decimal total = 0;
            if (debit != null)
            {
                foreach (var item in debit)
                {
                    decimal quantity = Convert.ToDecimal(item.Quantity);
                    decimal price = Convert.ToDecimal(item.Purchase_Price);
                    quantity *= (-1);
                    decimal vat = Convert.ToDecimal(item.Item_Vat);
                    total += (quantity * price) + vat;
                }
            }
            decimal preCashBack = Convert.ToDecimal(db.tbl_supplier_ledger.Where(a => a.Purchase_Master_ID == master_id && a.Tran_Ref_ID == 4).Select(b => b.Debit).Sum());
            total -= preCashBack;
            return Json(total, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
