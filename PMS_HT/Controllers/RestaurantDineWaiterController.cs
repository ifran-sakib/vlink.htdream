﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantDineWaiterController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantDineWaiter
        public ActionResult Index()
        {
            var tbl_dining_waiter = db.tbl_dining_waiter.Include(t => t.tbl_dining_set_up).Include(t => t.tbl_emp_info);
            return View(tbl_dining_waiter.ToList());
        }

        // GET: RestaurantDineWaiter/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_waiter tbl_dining_waiter = db.tbl_dining_waiter.Find(id);
            if (tbl_dining_waiter == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_waiter);
        }

        // GET: RestaurantDineWaiter/Create
        public ActionResult Create()
        {
            var existing = db.tbl_dining_waiter.Select(b => b.Emp_ID).ToList();
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info.Where(a => a.tbl_designation.Designation_Name == "Restaurant Waiter" && a.Emp_Status == "Active" && !existing.Contains(a.Emp_ID)), "Emp_ID", "Emp_Name");
            return View();
        }

        // POST: RestaurantDineWaiter/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Dining_Waiter_ID,Emp_ID,Status,Note")] tbl_dining_waiter tbl_dining_waiter)
        {
            if (ModelState.IsValid)
            {
                db.tbl_dining_waiter.Add(tbl_dining_waiter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var existing = db.tbl_dining_waiter.Select(b => b.Emp_ID).ToList();
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info.Where(a => a.tbl_designation.Designation_Name == "Restaurant Waiter" && a.Emp_Status == "Active" && !existing.Contains(a.Emp_ID)), "Emp_ID", "Emp_Name");
            return View(tbl_dining_waiter);
        }

        // GET: RestaurantDineWaiter/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_waiter tbl_dining_waiter = db.tbl_dining_waiter.Find(id);
            if (tbl_dining_waiter == null)
            {
                return HttpNotFound();
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info.Where(a => a.tbl_designation.Designation_Name == "Restaurant Waiter" && a.Emp_Status == "Active"), "Emp_ID", "Emp_Name", tbl_dining_waiter.Emp_ID);
            return PartialView(tbl_dining_waiter);
        }

        // POST: RestaurantDineWaiter/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Dining_Waiter_ID,Emp_ID,Status,Note")] tbl_dining_waiter tbl_dining_waiter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_dining_waiter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info.Where(a => a.tbl_designation.Designation_Name == "Restaurant Waiter" && a.Emp_Status == "Active"), "Emp_ID", "Emp_Name", tbl_dining_waiter.Emp_ID);
            return PartialView(tbl_dining_waiter);
        }

        // GET: RestaurantDineWaiter/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_waiter tbl_dining_waiter = db.tbl_dining_waiter.Find(id);
            if (tbl_dining_waiter == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_waiter);
        }

        // POST: RestaurantDineWaiter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_dining_waiter tbl_dining_waiter = db.tbl_dining_waiter.Find(id);
            db.tbl_dining_waiter.Remove(tbl_dining_waiter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
