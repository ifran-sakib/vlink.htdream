﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    public class FrontDeskFloorWiseDailyRoomChartController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_guest_stay_info = db.tbl_guest_stay_info.Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
            return View();
        }

        public ActionResult FirstFloor()
        {
            ViewBag.Floor1RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 1).ToList();
            ViewBag.Floor1 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 1).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        public ActionResult SecondFloor()
        {
            ViewBag.Floor2RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 2).ToList();
            ViewBag.Floor2 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 2).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        public ActionResult ThirdFloor()
        {
            ViewBag.Floor3RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 3).ToList();
            ViewBag.Floor3 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 3).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        public ActionResult FourthFloor()
        {
            ViewBag.Floor4RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 4).ToList();
            ViewBag.Floor4 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 4).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        public ActionResult FifthFloor()
        {
            ViewBag.Floor5RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 5).ToList();
            ViewBag.Floor5 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 5).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        public ActionResult SixthFloor()
        {
            ViewBag.Floor6RoomList = db.tbl_room_info.Where(a => a.Floor_ID == 6).ToList();
            ViewBag.Floor6 = db.tbl_guest_stay_info.Where(a => a.Status == "CheckIn" && a.tbl_room_info.Floor_ID == 6).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
