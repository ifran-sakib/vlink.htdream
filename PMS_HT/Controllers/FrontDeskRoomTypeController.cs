﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskRoomTypeController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_roomtype_setup.ToList());
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_roomtype_setup tbl_roomtype_setup = db.tbl_roomtype_setup.Find(id);
            if (tbl_roomtype_setup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_roomtype_setup);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Roomtype_ID,Roomtype_Name,YsnActive,Created_Date")] tbl_roomtype_setup tbl_roomtype_setup)
        {
            if (ModelState.IsValid)
            {
                tbl_roomtype_setup.Created_Date = DateTime.Now;
                db.tbl_roomtype_setup.Add(tbl_roomtype_setup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(tbl_roomtype_setup);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_roomtype_setup tbl_roomtype_setup = db.tbl_roomtype_setup.Find(id);
            if (tbl_roomtype_setup == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_roomtype_setup);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Roomtype_ID,Roomtype_Name,YsnActive,Created_Date")] tbl_roomtype_setup tbl_roomtype_setup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_roomtype_setup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_roomtype_setup);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_roomtype_setup tbl_roomtype_setup = db.tbl_roomtype_setup.Find(id);
            if (tbl_roomtype_setup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_roomtype_setup);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_roomtype_setup tbl_roomtype_setup = db.tbl_roomtype_setup.Find(id);
            db.tbl_roomtype_setup.Remove(tbl_roomtype_setup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public JsonResult UniqueRoomTypeCheck(string str)
        {
            tbl_roomtype_setup room = db.tbl_roomtype_setup.FirstOrDefault(r => r.Roomtype_Name == str);
            if (room == null)
            {
                return Json("unique", JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
