﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventoryCategoryController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_category.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_category category)
        {
            if (ModelState.IsValid)
            {
                db.tbl_category.Add(category);
                db.SaveChanges();
            }
            return RedirectToAction("Index", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_category.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_category category, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_category.Remove(db.tbl_category.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", "InventorySettings");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
