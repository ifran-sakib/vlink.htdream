﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantFoodController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantFood
        public ActionResult Index()
        {
            var tbl_food = db.tbl_food.OrderBy(a => a.tbl_food_category.Serial_No).Include(t => t.tbl_food_category);
            return View(tbl_food.ToList());
        }

        // GET: RestaurantFood/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food tbl_food = db.tbl_food.Find(id);
            if (tbl_food == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food);
        }

        // GET: RestaurantFood/Create
        public ActionResult Create()
        {
            ViewBag.Food_Cate_ID = new SelectList(db.tbl_food_category, "Food_Cate_ID", "Food_Cate_Name");
            return View();
        }

        // POST: RestaurantFood/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Food_ID,Food_No,Food_Name,Food_Cate_ID,Description,Image,Availability,Quantity,Price,Discount,Color,Note")] tbl_food tbl_food, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var img = "";
                if (file != null && file.ContentLength > 0)
                {
                    //DateTime nm = DateTime.Now;
                    string fileName = tbl_food.Food_ID.ToString();
                    var path = Server.MapPath("~/images/Restaurant/Foods/" + fileName + ".jpg");
                    img = "../../images/Restaurant/Foods/" + fileName + ".jpg";
                    tbl_food.Image = img;
                    file.SaveAs(path);
                }
                db.tbl_food.Add(tbl_food);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Food_Cate_ID = new SelectList(db.tbl_food_category, "Food_Cate_ID", "Food_Cate_Name", tbl_food.Food_Cate_ID);
            return View(tbl_food);
        }

        // GET: RestaurantFood/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food tbl_food = db.tbl_food.Find(id);
            if (tbl_food == null)
            {
                return HttpNotFound();
            }
            if (tbl_food.Image != null || tbl_food.Image != "")
            {
                ViewBag.Img = tbl_food.Image;
            }
            ViewBag.Food_Cate_ID = new SelectList(db.tbl_food_category, "Food_Cate_ID", "Food_Cate_Name", tbl_food.Food_Cate_ID);
            return PartialView(tbl_food);
        }

        // POST: RestaurantFood/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Food_ID,Food_No,Food_Name,Food_Cate_ID,Description,Image,Availability,Quantity,Price,Discount,Color,Note")] tbl_food tbl_food, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var img = "";
                if (file != null && file.ContentLength > 0)
                {
                    //DateTime nm = DateTime.Now;
                    string fileName = tbl_food.Food_ID.ToString();
                    var path = Server.MapPath("~/images/Restaurant/Foods/" + fileName + ".jpg");
                    img = "../../images/Restaurant/Foods/" + fileName + ".jpg";
                    tbl_food.Image = img;
                    file.SaveAs(path);
                }
                db.Entry(tbl_food).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Food_Cate_ID = new SelectList(db.tbl_food_category, "Food_Cate_ID", "Food_Cate_Name", tbl_food.Food_Cate_ID);
            return PartialView(tbl_food);
        }

        // GET: RestaurantFood/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food tbl_food = db.tbl_food.Find(id);
            if (tbl_food == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food);
        }

        // POST: RestaurantFood/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_food tbl_food = db.tbl_food.Find(id);
            db.tbl_food.Remove(tbl_food);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
