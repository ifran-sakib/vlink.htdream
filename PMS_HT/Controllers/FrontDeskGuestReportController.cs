﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using Microsoft.Reporting.WebForms;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskGuestReportController : CustomControllerBase
    {
        public ActionResult Arrival()
        {
            return View();
        }

        public ActionResult Departure()
        {
            return View();
        }

        public ActionResult InHouse()
        {
            return View();
        }

        public PartialViewResult Search(DateTime? dt, string type)
        {
            var today = DateTime.Today.Date;
            if (dt != null)
            {
                today = dt.Value;
            }
            if (type == "arrival")
            {
                var todayEntryCustomerInfo = db.tbl_guest_stay_info.Where((a => a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && DbFunctions.TruncateTime(a.Arrival_Date) == today)).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
                ViewBag.Data = todayEntryCustomerInfo.ToList();
                return PartialView(todayEntryCustomerInfo.ToList());
            }
            else if (type == "departure")
            {
                var totalCheckOutTodayInfo = db.tbl_guest_stay_info.Where(a => a.Check_Out_Date == today && a.Check_Out_Note != "Temp_Stay").OrderBy(a => a.Check_Out_Time).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
                ViewBag.Data = totalCheckOutTodayInfo.ToList();
                return PartialView(totalCheckOutTodayInfo.ToList());
            }
            else if (type == "inhouse")
            {
                var checkInCustomerInfo = db.tbl_guest_stay_info.Where(a => (a.Status == "CheckIn" || (a.Status == "Clear" && a.Check_Out_Date > today && a.Check_Out_Note != "Temp_Stay")) && DbFunctions.TruncateTime(a.Arrival_Date) <= today).OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info);
                ViewBag.Data = checkInCustomerInfo.ToList();
                return PartialView(checkInCustomerInfo.ToList());
            }
            return PartialView();
        }

        public JsonResult ExportArrivalReport(DateTime? dt, string type)
        {
            
            var today = DateTime.Today.Date;
            if (dt != null)
            {
                today = dt.Value;
            }
            var exportData = new List<GuestReportViewModel>();
            var thisTypeDataFromDb = new List<tbl_guest_stay_info>();
            switch (type)
            {
                case "Arrival":
                    thisTypeDataFromDb = db.tbl_guest_stay_info
                        .Where((a => a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" &&
                                     a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" &&
                                     a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" &&
                                     DbFunctions.TruncateTime(a.Arrival_Date) == today)).OrderBy(b => b.Arrival_Date)
                        .Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();
                    break;
                case "Departure":
                    thisTypeDataFromDb = db.tbl_guest_stay_info
                        .Where(a => a.Check_Out_Date == today && a.Check_Out_Note != "Temp_Stay")
                        .OrderBy(a => a.Check_Out_Time).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info)
                        .ToList();
                    break;
                case "InHouse":
                    thisTypeDataFromDb = db.tbl_guest_stay_info
                        .Where(a => (a.Status == "CheckIn" || (a.Status == "Clear" && a.Check_Out_Date > today && a.Check_Out_Note != "Temp_Stay")) && DbFunctions.TruncateTime(a.Arrival_Date) <= today)
                        .OrderBy(b => b.Arrival_Date).Include(t => t.tbl_guest_info).Include(t => t.tbl_room_info).ToList();
                    break;
                default:
                    return Json("error");
            }
            foreach (var item in thisTypeDataFromDb)
            {
                GuestReportViewModel thisRow = new GuestReportViewModel();
                thisRow.Room_No = item.tbl_room_info.Room_No;
                thisRow.Name = item.tbl_guest_info.Name;
                thisRow.Phone = item.tbl_guest_info.Phone;
                thisRow.Address = item.tbl_guest_info.Address;
                thisRow.Arrival_Date = item.Arrival_Date;
                thisRow.Departure_Date = item.Departure_Date;
                thisRow.Check_Out_Time = "";
                if (item.Check_Out_Date != null)
                {
                    DateTime checkoutDateTime = item.Check_Out_Date.Value.Add(item.Check_Out_Time.Value);
                    thisRow.Check_Out_Time = checkoutDateTime.ToString("dd/MM/yy HH:mm");
                }
                exportData.Add(thisRow);
            }

            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "TodaysGuestInfo.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }

            ReportDataSource DataSetTodaysGuestInfo = new ReportDataSource("DataSetTodaysGuestInfo", exportData);
            lc.DataSources.Add(DataSetTodaysGuestInfo);

            ReportParameter[] param = new ReportParameter[6];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);
            param[4] = new ReportParameter("ReportType", type + " Report", false);
            param[5] = new ReportParameter("ReportDataDate", today.ToString("dd/MM/yyyy"), false);
            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);



        }
    }
}