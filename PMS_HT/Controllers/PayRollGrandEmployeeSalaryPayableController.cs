﻿using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
    public class PayRollGrandEmployeeSalaryPayableController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var query = from emp in db.tbl_emp_info
                        join st in db.tbl_salary_transactions on emp.Emp_ID equals st.Emp_ID
                        group st by new { emp.Emp_Name } into g
                        select new PayableViewModel
                        {
                            EmpName = g.Key.Emp_Name,
                            Payable = (g.Sum(x => x.Debit) ?? 0),
                            Paid = (g.Sum(x => x.Credit) ?? 0),
                            Due = ((g.Sum(x => x.Debit) ?? 0) - (g.Sum(x => x.Credit) ?? 0))
                        };
            ViewBag.SalaryExpense = query.ToList();
            return View();
        }

        //public PartialViewResult DateRangeEmployeeSalaryPayable(DateTime? dob, DateTime? date)
        //{
        //    var query = from emp in db.tbl_emp_info
        //                            join st in db.tbl_salary_transactions on emp.Emp_ID equals st.Emp_ID where st.Date >= dob && st.Date <= date
        //                            group st by new { emp.Emp_Name } into g
        //                            select new PayableViewModel
        //                            {
        //                                EmpName = g.Key.Emp_Name,
        //                                Payable = (g.Sum(x => x.Debit) ?? 0),
        //                                Paid = (g.Sum(x => x.Credit) ?? 0),
        //                                Due = ((g.Sum(x => x.Debit) ?? 0) - (g.Sum(x => x.Credit) ?? 0))
        //                            };
        //    ViewBag.SalaryExpense = query.ToList();
        //    return PartialView();
        //}
    }
}