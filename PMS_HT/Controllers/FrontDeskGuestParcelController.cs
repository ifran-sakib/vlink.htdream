﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskGuestParcelController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_package = db.tbl_package.Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
            return View(tbl_package.ToList());
        }

        //public ActionResult Pending()
        //{
        //    var tbl_messages = db.tbl_package.Where(a => a.Status == "Pending").Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
        //    return View(tbl_messages.ToList());
        //}

        public ActionResult StayWiseParcel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var tbl_package = db.tbl_package.Where(a => a.Stay_Info_ID == id).Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
            ViewBag.Parcels = tbl_package.ToList();
            var tblGuestStayInfo = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id);
            if (tblGuestStayInfo != null)
            {
                ViewBag.RoomNo = tblGuestStayInfo.tbl_room_info.Room_No;
            }
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No", id);
            return PartialView();
        }

        public JsonResult Swap(int id)
        {
            tbl_package tbl_package = db.tbl_package.Find(id);
            if (tbl_package == null)
            {
                return Json("No Message", JsonRequestBehavior.AllowGet);
            }
            if (tbl_package.Status == "Delivered")
            {
                return Json("Already Delivered", JsonRequestBehavior.AllowGet);
            }

            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                tbl_package.Status = "Delivered";
                tbl_package.Delivering_Emp_ID = tblEmpInfo.Emp_ID;
                tbl_package.Delivery_Time = DateTime.Now;
                db.Entry(tbl_package).State = EntityState.Modified;
                db.SaveChanges();
                return Json("Changed", JsonRequestBehavior.AllowGet);
            }
            return Json("Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_package tbl_package = db.tbl_package.Find(id);
            if (tbl_package == null)
            {
                return HttpNotFound();
            }
            return View(tbl_package);
        }

        public ActionResult Create()
        {
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No");
            return PartialView();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Package_ID,Stay_Info_ID,Receiving_Emp_ID,Delivering_Emp_ID,Package_Details,Senders_Details,Receiving_Time,Delivery_Time,Status")] tbl_package tbl_package)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int user_id = tblEmpInfo.Emp_ID;
                    tbl_package.Receiving_Emp_ID = user_id;
                    tbl_package.Delivering_Emp_ID = null;
                    tbl_package.Receiving_Time = DateTime.Now;
                    tbl_package.Delivery_Time = null;
                    tbl_package.Status = "Pending";
                    db.tbl_package.Add(tbl_package);
                    db.SaveChanges();
                    return RedirectToAction("Index", "FrontDesk");
                }
            }
            ViewBag.Delivering_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Delivering_Emp_ID);
            ViewBag.Receiving_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Receiving_Emp_ID);
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "No_of_Nights", tbl_package.Stay_Info_ID);
            return View(tbl_package);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_package tbl_package = db.tbl_package.Find(id);
            if (tbl_package == null)
            {
                return HttpNotFound();
            }
            ViewBag.Delivering_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Delivering_Emp_ID);
            ViewBag.Receiving_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Receiving_Emp_ID);
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "No_of_Nights", tbl_package.Stay_Info_ID);
            return View(tbl_package);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Package_ID,Stay_Info_ID,Receiving_Emp_ID,Delivering_Emp_ID,Package_Details,Senders_Details,Receiving_Time,Delivery_Time,Status")] tbl_package tbl_package)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_package).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Delivering_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Delivering_Emp_ID);
            ViewBag.Receiving_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_package.Receiving_Emp_ID);
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "No_of_Nights", tbl_package.Stay_Info_ID);
            return View(tbl_package);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_package tbl_package = db.tbl_package.Find(id);
            if (tbl_package == null)
            {
                return HttpNotFound();
            }
            return View(tbl_package);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_package tbl_package = db.tbl_package.Find(id);
            db.tbl_package.Remove(tbl_package);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
