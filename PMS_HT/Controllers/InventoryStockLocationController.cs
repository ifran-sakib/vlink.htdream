using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventoryStockLocationController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_stock_location.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_stock_location stockLocation)
        {
            if (ModelState.IsValid)
            {
                db.tbl_stock_location.Add(stockLocation);
                db.SaveChanges();
            }
            return RedirectToAction("StockLocations", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_stock_location.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_stock_location stockLocation, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockLocation).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("StockLocations", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_stock_location.Remove(db.tbl_stock_location.Find(id));
            db.SaveChanges();
            return RedirectToAction("StockLocations", "InventorySettings");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
