using System;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class AccountsInventoryExpenseController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult DateRangeInventoryExpense(DateTime? dob, DateTime? date)
        {
            var query = from pm in db.tbl_purchase_master where pm.Purchase_Date >= dob && pm.Purchase_Date <= date
                        select new AccountsInventoryExpenseViewModel
                        {
                          Memo_No = pm.Memo_No,
                          PurchaseExpense = pm.Advanced_Amount,
                          PurchaseDate = pm.Purchase_Date
                        };

            ViewBag.InventoryExpense = query.ToList();
            return PartialView();
        }
    }
}