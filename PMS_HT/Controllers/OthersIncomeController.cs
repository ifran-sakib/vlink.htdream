﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class OthersIncomeController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_others_income = db.tbl_others_income.OrderByDescending(a => a.Created_Date).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_others_income_head);
            return View(tbl_others_income.ToList());
        }

        public ActionResult Add()
        {
            List<tbl_others_income_head> tbl_others_income_head = db.tbl_others_income_head.OrderBy(x => x.Others_Income_Head_Name).ToList();
            ViewBag.Others_Income_Head_ID = new SelectList(tbl_others_income_head, "Others_Income_Head_ID", "Others_Income_Head_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_others_income income)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Others_Income_Head_ID = new SelectList(db.tbl_others_income, "Others_Income_Head_ID", "Others_Income_Head_Name");
                db.tbl_others_income.Add(income);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            tbl_others_income income = db.tbl_others_income.Find(id);
            List<tbl_others_income_head> tbl_others_income_head = db.tbl_others_income_head.OrderBy(x => x.Others_Income_Head_Name).ToList();
            ViewBag.Others_Income_Head_ID = new SelectList(tbl_others_income_head, "Others_Income_Head_ID", "Others_Income_Head_Name", income.Others_Income_Head_ID);
            return PartialView(income);
        }

        [HttpPost]
        public ActionResult Edit(tbl_others_income income, int? id)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Others_Income_Head_ID = new SelectList(db.tbl_others_income_head, "Others_Income_Head_ID", "Others_Income_Head_Name", income.Others_Income_Head_ID);
                db.Entry(income).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult IncomeDayWise()
        {
            var incomeGroupWise = db.tbl_others_income.ToList();
            var results = from at in incomeGroupWise
                          group at by new
                          {
                              at.Created_Date
                          } into g
                          select new ExpenseGroupWise
                          {
                              Value = g.Sum(x => x.Amount),
                              date = g.Key.Created_Date
                          };
            ViewBag.IncomeGroupWise = results.ToList();
            return View();
        }

        public ActionResult Details(string date)
        {
            DateTime filterDate = Convert.ToDateTime(date);
            ViewBag.IncomeDetails = db.tbl_others_income.Where(a => a.Created_Date == filterDate).ToList();
            return View();
        }

        public ActionResult Delete(int id)
        {
            db.tbl_others_income.Remove(db.tbl_others_income.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
