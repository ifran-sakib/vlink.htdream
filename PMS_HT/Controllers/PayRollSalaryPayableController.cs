﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
    public class PayRollSalaryPayableController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var transactions = db.tbl_salary_transactions.ToList();
         
            var results = from at in transactions
                          group at by new
                          {
                              at.Emp_ID,
                              at.tbl_emp_info.Emp_Name,
                              at.Month_Name,
                              at.Year_Name
                          } into g
                         select new PayableViewModel
                          {
                              EmpId = g.Key.Emp_ID,
                              EmpName = g.Key.Emp_Name,
                              MonthName= g.Key.Month_Name,
                              YearName= g.Key.Year_Name,
                              Value = g.Sum(x => x.Debit) - g.Sum(x => x.Credit)
                          };          
                ViewBag.payable = results.ToList();


            return View();
        }
        
        public ActionResult Details(int? id, string month, string year)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //SELECT(SELECT   Earning_Head_Name  from tbl_earning_head where Earning_Head_ID in 
            //  (select Tran_Ref_ID from tbl_salary_transactions c where c.Tran_Ref_ID = tbl_salary_transactions.Tran_Ref_ID) )    Tran_Ref_ID, Particular , Debit
            // FROM            tbl_salary_transactions where Month_Name = 'January' and Year_Name = '2016' and Emp_ID = 1 and Tran_Ref_Name = 'SalaryEarning'


            // select sum (debit)from tbl_salary_transactions where Month_Name = 'January' and Year_Name = '2016' and Emp_ID = 1 and Tran_Ref_Name = 'SalaryEarning'
            
            var results = from c in db.tbl_salary_transactions.Where(a => a.Month_Name.Equals(month) && a.Year_Name.Equals(year) && a.Tran_Ref_Name.Equals("SalaryEarning") && a.Emp_ID == id)
                          select new PayableViewModel
                          {
                              Earning_Head =  c.tbl_earning_head.Earning_Head_Name,
                              VParticular = c.Particular,
                              Value = c.Debit
                          };                                                                          
            ViewBag.allowance = results.ToList();

            var Deduction = from c in db.tbl_salary_transactions.Where(a => a.Month_Name.Equals(month) && a.Year_Name.Equals(year) && a.Tran_Ref_Name.Equals("SalaryDeduction") && a.Emp_ID == id)
                          select new PayableViewModel
                          {
                              Deduction_Head = c.tbl_earning_head.Earning_Head_Name,
                              VParticular = c.Particular,
                              Value = c.Credit
                          };

            ViewBag.deduction = Deduction.ToList();

            var basic = from c in db.tbl_salary_transactions.Where(a => a.Month_Name.Equals(month) && a.Year_Name.Equals(year) && a.Tran_Ref_Name.Equals("AddBasic") && a.Emp_ID == id)
                            select new PayableViewModel
                            {
                                //EmpName = c.tbl_earning_head.Earning_Head_Name,
                                VParticular = c.Particular,
                                Value = c.Debit
                            };

            ViewBag.basic = basic.ToList();

            var paid = from c in db.tbl_salary_transactions.Where(a => a.Month_Name.Equals(month) && a.Year_Name.Equals(year) && a.Tran_Ref_Name.Equals("SalaryPayment") && a.Emp_ID == id)
                        select new PayableViewModel
                        {
                            //EmpName = c.tbl_earning_head.Earning_Head_Name,
                            VParticular = c.Particular,
                            Value = c.Credit
                        };

            ViewBag.paid = paid.ToList();

            //tbl_salary_transactions tbl_salary_transactions = db.tbl_salary_transactions.Find(id);
            //if (tbl_salary_transactions == null)
            //{
            //    return HttpNotFound();
            //}
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name");
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Salary_Tran_ID,Emp_ID,Tran_Ref_ID,Particular,Tran_Ref_Name,Debit,Credit,Balance,Month_Name,Year_Name,Date")] tbl_salary_transactions tbl_salary_transactions)
        {
            if (ModelState.IsValid)
            {
                db.tbl_salary_transactions.Add(tbl_salary_transactions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name", tbl_salary_transactions.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_salary_transactions.Emp_ID);
            return View(tbl_salary_transactions);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_salary_transactions tbl_salary_transactions = db.tbl_salary_transactions.Find(id);
            if (tbl_salary_transactions == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name", tbl_salary_transactions.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_salary_transactions.Emp_ID);
            return View(tbl_salary_transactions);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Salary_Tran_ID,Emp_ID,Tran_Ref_ID,Particular,Tran_Ref_Name,Debit,Credit,Balance,Month_Name,Year_Name,Date")] tbl_salary_transactions tbl_salary_transactions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_salary_transactions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name", tbl_salary_transactions.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_salary_transactions.Emp_ID);
            return View(tbl_salary_transactions);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_salary_transactions tbl_salary_transactions = db.tbl_salary_transactions.Find(id);
            if (tbl_salary_transactions == null)
            {
                return HttpNotFound();
            }
            return View(tbl_salary_transactions);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_salary_transactions tbl_salary_transactions = db.tbl_salary_transactions.Find(id);
            db.tbl_salary_transactions.Remove(tbl_salary_transactions);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
