﻿using System.Web;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult error404()
        {
            ErrorContextMaker();
            return View();
        }

        public ActionResult error405()
        {
            ErrorContextMaker();
            return View();
        }

        public ActionResult general()
        {
            ErrorContextMaker();
            return View();
        }

        private void ErrorContextMaker()
        {
            var error = System.Web.HttpContext.Current.AllErrors;
            if (error != null)
            {
                ViewBag.context = error[0];
            }
        }
    }
}