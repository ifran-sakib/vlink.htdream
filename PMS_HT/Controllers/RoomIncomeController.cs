using System;
using System.Web.Mvc;
using PMS_HT.Models;
using System.Linq;
using System.Data.Entity;
using System.IO;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using PMS_HT.CustomClass;
using PMS_HT.Areas.Services.Models;

namespace PMS_HT.Controllers
{
    public class RoomIncomeController : CustomControllerBase
    {
        private FrontDeskGuestStayController GStay = new FrontDeskGuestStayController();
        public ActionResult DailyRoomLettingReturn()
        {
            return View();
        }

        public PartialViewResult search(DateTime dt)
        {
            string id = dt.ToString("MM/dd/yy");
            DateTime StartDate = Convert.ToDateTime("2017-07-22");
            var query = "";
            //query = "SELECT tbl_guest_stay_info.Arrival_Date AS Arrival_Date, tbl_room_info.Room_No, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Time,Round( tbl_room_info.Rate*.15+tbl_room_info.Rate,-1) AS Rate,isnull((SELECT SUM(tbl_room_info.Rate * .1) FROM tbl_room_info JOIN tbl_room_transactions ON tbl_room_info.Room_ID = tbl_room_transactions.Room_ID WHERE tbl_room_transactions.Tran_Ref_ID = 3 AND tbl_room_transactions.Credit != 0 and DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ),0)AS Staff_Commission, tbl_guest_info.Name, tbl_guest_stay_info.Status, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "')AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0)AS PreviousBalance, Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) AS CurrentBill, 0.00 AS OtherBill, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0) + Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) AS Total,Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE Tran_Ref_ID=3 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) Group BY Guest_ID ),0) AS Payment, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0) + Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) - Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' )Group BY Guest_ID ),0) AS Balance, isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=1) ,0)AS Discount, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=8) ,0)AS Baddebt, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=7) ,0)AS Refund,isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=9) ,0)AS Surplus,isnull( (SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) AND Tran_Ref_ID=3 Group BY Guest_ID ) ,0)AS Advanced_Amount FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Room_ID = tbl_room_transactions.Room_ID and tbl_guest_stay_info.Guest_ID = tbl_room_transactions.Guest_ID WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_guest_stay_info.Arrival_Date)) <= '" + id + "' AND (tbl_guest_stay_info.Status = 'CheckIn' OR tbl_guest_stay_info.Check_Out_Date >='" + id + "') Group BY tbl_guest_stay_info.Arrival_Date, tbl_room_info.Room_No, tbl_room_info.Rate, tbl_guest_info.Name, tbl_guest_stay_info.Status, tbl_guest_stay_info.Guest_ID, tbl_guest_stay_info.Room_ID,tbl_guest_stay_info.Check_Out_Date, tbl_guest_stay_info.Check_Out_Time,tbl_guest_stay_info.Departure_Date order by tbl_guest_stay_info.Arrival_Date";
            //if (dt.Date >= StartDate.Date)
            //{
            //    query = "SELECT tbl_guest_stay_info.Arrival_Date AS Arrival_Date, tbl_room_info.Room_No, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Time, Round( tbl_room_info.Rate*1.15,-1) AS Rate, 0.00 AS Staff_Commission, tbl_guest_info.Name, tbl_guest_stay_info.Status, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') Group BY Stay_Info_ID) ,0)AS PreviousBalance, Round( tbl_room_info.Rate * 1.15,-1) AS CurrentBill, Isnull((SELECT isnull(SUM(Debit), 0) FROM tbl_room_transactions WHERE (Tran_Ref_ID=5 OR Tran_Ref_ID = 6) AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "') Group BY Stay_Info_ID ),0) AS OtherBill, 0.00 AS Total, Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE (Tran_Ref_ID=3 OR Tran_Ref_ID = 14) AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "') Group BY Stay_Info_ID ),0) AS Payment, 0.00 AS Balance, isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=1) ,0)AS Discount, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=8) ,0)AS Baddebt, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=7) ,0)AS Refund, isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=9) ,0)AS Surplus, isnull( (SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID=3 AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) Group BY Guest_ID ) ,0)AS Advanced_Amount FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_guest_stay_info.Arrival_Date)) <= '" + id + "' AND (tbl_guest_stay_info.Status = 'CheckIn' OR tbl_guest_stay_info.Check_Out_Date >='" + id + "') Group BY tbl_guest_stay_info.Arrival_Date, tbl_room_info.Room_No,tbl_guest_stay_info.Stay_Info_ID, tbl_room_info.Rate, tbl_guest_info.Name, tbl_guest_stay_info.Status, tbl_guest_stay_info.Guest_ID, tbl_guest_stay_info.Room_ID,tbl_guest_stay_info.Check_Out_Date, tbl_guest_stay_info.Check_Out_Time,tbl_guest_stay_info.Departure_Date Order By tbl_guest_stay_info.Arrival_Date";
            //}
            //var data = db.Database.SqlQuery<DailyRoomLettingViewModel>(query);
            //List<DailyRoomLettingViewModel> tempList = data.ToList();


            tbl_service roomData = db.tbl_service.Find(1);
            decimal vatPercent = Convert.ToDecimal(roomData.VAT) / 100;
            decimal scPercent = Convert.ToDecimal(roomData.SC) / 100;

            var dateParam = new SqlParameter
            {
                ParameterName = "date",
                Value = id
            };
            var vatParam = new SqlParameter
            {
                ParameterName = "vat",
                Value = vatPercent
            };
            var scParam = new SqlParameter
            {
                ParameterName = "sc",
                Value = scPercent
            };

            var tempList = db.Database.SqlQuery<DailyRoomLettingViewModel>("exec RoomLetting @date,@vat,@sc ", dateParam, vatParam, scParam).ToList<DailyRoomLettingViewModel>();

            for (int i = 0; i < tempList.Count; i++)
            {
                DailyRoomLettingViewModel thisrow = tempList[i];
                //tempList[i].Rate = tempList[i].Rate;
                //tempList[i].CurrentBill = tempList[i].CurrentBill;
                int distinctDays = 0;

                double secs = 0;
                if (tempList[i].CheckOut_Time.Date > dt || (tempList[i].CheckOut_Time.AddHours(3).Date > dt.Date && tempList[i].Status == "CheckIn"))
                {
                    tempList[i].Stay = (int)Math.Ceiling((dt - tempList[i].Arrival_Date).TotalHours) + 24;
                    secs = (double)(dt - tempList[i].Arrival_Date).TotalSeconds + 86400;
                    tempList[i].CheckOutString = "";
                    //distinctDays = (int)Math.Ceiling((dob1 - tempList[i].Arrival_Date).TotalDays)+1;
                    distinctDays = DayCounter(tempList[i].Arrival_Date, dt);
                }
                else
                {
                    tempList[i].Stay = (int)Math.Round((tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalHours);
                    secs = (double)(tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalSeconds;
                    tempList[i].CheckOutString = tempList[i].CheckOut_Time.ToString("dd/MM HH:mm");
                    //distinctDays = (int)Math.Ceiling((tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalDays);
                    distinctDays = DayCounter(tempList[i].Arrival_Date, tempList[i].CheckOut_Time);
                }
                

                int hour = Convert.ToInt32(tempList[i].Stay);
                double hour2 = Convert.ToDouble(hour);
                int days = (int)Math.Ceiling(hour2 / 24);

                if (secs > 86400 && (secs % 86400 < 10800 || days != distinctDays))
                {
                    tempList[i].CurrentBill = 0;
                }
                else if (hour <= 24 && tempList[i].Arrival_Date.Date != tempList[i].CheckOut_Time.Date && tempList[i].Arrival_Date.Date != dt.Date)
                {
                    tempList[i].CurrentBill = 0;
                }

                if (tempList[i].Arrival_Date.Date < dt.Date && tempList[i].CheckOut_Time.Date > dt.Date && tempList[i].CheckOut_Time.Hour >= 3 && tempList[i].CurrentBill == 0)
                {
                    tempList[i].CurrentBill = tempList[i].Rate;
                }


                tempList[i].Total = Rounder.Round(tempList[i].PreviousBalance + tempList[i].CurrentBill + tempList[i].OtherBill);
                if (tempList[i].CheckOut_Time.Date == dt && tempList[i].Status == "Clear" && tempList[i].Baddebt > 0)
                {
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Balance = tempList[i].Total - tempList[i].Payment - tempList[i].Discount;
                }
                if (tempList[i].Advanced_Amount >= tempList[i].Refund)
                {
                    tempList[i].Refund = 0;
                }
                else
                {
                    tempList[i].Refund -= tempList[i].Advanced_Amount;
                }
                if (tempList[i].Balance < 0)
                {
                    tempList[i].Advanced_Amount = tempList[i].Balance * (-1);
                    //tempList[i].Advanced_Amount -= tempList[i].Discount;
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Advanced_Amount = 0;
                }
                /*if (hour > 24 && (hour % 24 <= 3 || days != distinctDays))
                {
                    tempList[i].CurrentBill = 0;
                }
                else if (hour <= 24 && tempList[i].Arrival_Date.Date != tempList[i].CheckOut_Time.Date && tempList[i].Arrival_Date.Date != dt.Date)
                {
                    tempList[i].CurrentBill = 0;
                }

                tempList[i].Total = tempList[i].PreviousBalance + tempList[i].CurrentBill;
                if (tempList[i].CheckOut_Time.Date == dt)
                {
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Balance = tempList[i].Total - tempList[i].Payment - tempList[i].Discount;
                }

                if (tempList[i].Advanced_Amount >= tempList[i].Refund)
                {
                    tempList[i].Refund = 0;
                }
                else
                {
                    tempList[i].Refund -= tempList[i].Advanced_Amount;
                }
                if (tempList[i].Balance < 0)
                {
                    tempList[i].Advanced_Amount = tempList[i].Balance * (-1);
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Advanced_Amount = 0;
                }*/
            }
            query = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date,tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_room_transactions INNER JOIN tbl_guest_stay_info ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_room_transactions.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' AND tbl_room_transactions.Tran_Ref_ID = 12 ORDER BY tbl_room_transactions.Tran_Date";
            var outstandingPayList = db.Database.SqlQuery<OutStandingViewModel>(query);
            var totalOutstandingPay = Convert.ToDecimal(outstandingPayList.Select(a => a.Credit).Sum());

            if (totalOutstandingPay > 0)
            {
                DailyRoomLettingViewModel outPayRow = new DailyRoomLettingViewModel();
                outPayRow.Room_No = "";
                outPayRow.Name = "Outstanding Payment";
                outPayRow.Rate = 0;
                outPayRow.Arrival_Date = DateTime.Now;
                outPayRow.PreviousBalance = 0;
                outPayRow.CurrentBill = 0;
                outPayRow.OtherBill = 0;
                outPayRow.Total = 0;
                outPayRow.Payment = totalOutstandingPay;
                outPayRow.Balance = 0;
                outPayRow.Advanced_Amount = 0;
                outPayRow.Discount = 0;
                outPayRow.CheckOut_Time = DateTime.Now;
                outPayRow.Tran_Date = DateTime.Now;
                outPayRow.Stay = 0;
                outPayRow.Baddebt = 0;
                outPayRow.Refund = 0;
                outPayRow.Surplus = 0;
                outPayRow.CheckOutString = "";
                tempList.Add(outPayRow);
            }
            decimal booking_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) == dt.Date && a.Tran_Ref_ID == 14 && !(a.tbl_guest_stay_info.Status == "CheckIn" && DbFunctions.TruncateTime(a.tbl_guest_stay_info.Arrival_Date) == dt)).Sum(b => b.Credit));
            DailyRoomLettingViewModel bookPayRow = new DailyRoomLettingViewModel();
            if (booking_payment > 0)
            {
                bookPayRow.Room_No = "";
                bookPayRow.Name = "Booking Payment";
                bookPayRow.Rate = 0;
                bookPayRow.Arrival_Date = DateTime.Now;
                bookPayRow.PreviousBalance = 0;
                bookPayRow.CurrentBill = 0;
                bookPayRow.OtherBill = 0;
                bookPayRow.Total = 0;
                bookPayRow.Payment = booking_payment;
                bookPayRow.Balance = 0;
                bookPayRow.Advanced_Amount = 0;
                bookPayRow.Discount = 0;
                bookPayRow.CheckOut_Time = DateTime.Now;
                bookPayRow.Tran_Date = DateTime.Now;
                bookPayRow.Stay = 0;
                bookPayRow.Baddebt = 0;
                bookPayRow.Refund = 0;
                bookPayRow.Surplus = 0;
                bookPayRow.CheckOutString = "";
                tempList.Add(bookPayRow);
            }

            decimal booking_refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) == dt && a.Tran_Ref_ID == 13).Sum(b => b.Credit));
            DailyRoomLettingViewModel bookRefundRow = new DailyRoomLettingViewModel();
            if (booking_refund > 0)
            {
                bookRefundRow.Room_No = "";
                bookRefundRow.Name = "Cancellation Refund";
                bookRefundRow.Rate = 0;
                bookRefundRow.Arrival_Date = DateTime.Now;
                bookRefundRow.PreviousBalance = 0;
                bookRefundRow.CurrentBill = 0;
                bookRefundRow.OtherBill = 0;
                bookRefundRow.Total = 0;
                bookRefundRow.Payment = booking_refund * (-1);
                bookRefundRow.Balance = 0;
                bookRefundRow.Advanced_Amount = 0;
                bookRefundRow.Discount = 0;
                bookRefundRow.CheckOut_Time = DateTime.Now;
                bookRefundRow.Tran_Date = DateTime.Now;
                bookRefundRow.Stay = 0;
                bookRefundRow.Baddebt = 0;
                bookRefundRow.Refund = 0;
                bookRefundRow.Surplus = 0;
                bookRefundRow.CheckOutString = "";
                tempList.Add(bookRefundRow);
            }
            ViewBag.status = tempList;
            return PartialView();
        }

        [AllowAnonymous]
        public JsonResult ExportReport(string dob)
        {
            // DateTime dob1 = Convert.ToDateTime(dob);
            DateTime dob1 = DateTime.ParseExact(dob, "MM/dd/yyyy", null);
            DateTime today = dob1;
            DateTime StartDate = Convert.ToDateTime("2017-07-22");
            DateTime dt = dob1;
            String id = dob;
            var query = "";
            //query = "SELECT tbl_guest_stay_info.Arrival_Date AS Arrival_Date, tbl_room_info.Room_No, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Time,Round( tbl_room_info.Rate*.15+tbl_room_info.Rate,-1) AS Rate,isnull((SELECT SUM(tbl_room_info.Rate * .1) FROM tbl_room_info JOIN tbl_room_transactions ON tbl_room_info.Room_ID = tbl_room_transactions.Room_ID WHERE tbl_room_transactions.Tran_Ref_ID = 3 AND tbl_room_transactions.Credit != 0 and DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ),0)AS Staff_Commission, tbl_guest_info.Name, tbl_guest_stay_info.Status, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "')AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0)AS PreviousBalance, Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) AS CurrentBill, 0.00 AS OtherBill, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0) + Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) AS Total,Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE Tran_Ref_ID=3 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) Group BY Guest_ID ),0) AS Payment, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "' ) Group BY Guest_ID, Room_ID ) ,0) + Round( tbl_room_info.Rate * .15 + tbl_room_info.Rate,-1) - Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' )Group BY Guest_ID ),0) AS Balance, isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=1) ,0)AS Discount, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=8) ,0)AS Baddebt, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=7) ,0)AS Refund,isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=9) ,0)AS Surplus,isnull( (SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Guest_ID in (SELECT Guest_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Guest_ID = a.Guest_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) AND Room_ID in (SELECT Room_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Room_ID = a.Room_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) AND Tran_Ref_ID=3 Group BY Guest_ID ) ,0)AS Advanced_Amount FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Room_ID = tbl_room_transactions.Room_ID and tbl_guest_stay_info.Guest_ID = tbl_room_transactions.Guest_ID WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_guest_stay_info.Arrival_Date)) <= '" + id + "' AND (tbl_guest_stay_info.Status = 'CheckIn' OR tbl_guest_stay_info.Check_Out_Date >='" + id + "') Group BY tbl_guest_stay_info.Arrival_Date, tbl_room_info.Room_No, tbl_room_info.Rate, tbl_guest_info.Name, tbl_guest_stay_info.Status, tbl_guest_stay_info.Guest_ID, tbl_guest_stay_info.Room_ID,tbl_guest_stay_info.Check_Out_Date, tbl_guest_stay_info.Check_Out_Time,tbl_guest_stay_info.Departure_Date order by tbl_guest_stay_info.Arrival_Date";
            //if (dt.Date >= StartDate.Date)
            //{
            //    query = "SELECT tbl_guest_stay_info.Arrival_Date AS Arrival_Date, tbl_room_info.Room_No, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Time, Round( tbl_room_info.Rate*1.15,-1) AS Rate, 0.00 AS Staff_Commission, tbl_guest_info.Name, tbl_guest_stay_info.Status, isnull( (SELECT Round(isnull(SUM(Debit), 0) , -1) - isnull(sum(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID!=8 AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) < '" + id + "') Group BY Stay_Info_ID) ,0)AS PreviousBalance, Round( tbl_room_info.Rate * 1.15,-1) AS CurrentBill, Isnull((SELECT isnull(SUM(Debit), 0) FROM tbl_room_transactions WHERE (Tran_Ref_ID=5 OR Tran_Ref_ID = 6) AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "') Group BY Stay_Info_ID ),0) AS OtherBill, 0.00 AS Total, Isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE (Tran_Ref_ID=3 OR Tran_Ref_ID = 14) AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "') Group BY Stay_Info_ID ),0) AS Payment, 0.00 AS Balance, isnull((SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=1) ,0)AS Discount, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=8) ,0)AS Baddebt, isnull( (SELECT isnull(Credit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=7) ,0)AS Refund, isnull( (SELECT isnull(Debit, 0) FROM tbl_room_transactions WHERE Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' ) AND Tran_Ref_ID=9) ,0)AS Surplus, isnull( (SELECT isnull(SUM(Credit), 0) FROM tbl_room_transactions WHERE tbl_room_transactions.Tran_Ref_ID=3 AND Stay_Info_ID in (SELECT Stay_Info_ID FROM tbl_guest_stay_info a WHERE tbl_guest_stay_info.Stay_Info_ID = a.Stay_Info_ID AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) > '" + id + "' ) Group BY Guest_ID ) ,0)AS Advanced_Amount FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_guest_stay_info.Arrival_Date)) <= '" + id + "' AND (tbl_guest_stay_info.Status = 'CheckIn' OR tbl_guest_stay_info.Check_Out_Date >='" + id + "') Group BY tbl_guest_stay_info.Arrival_Date, tbl_room_info.Room_No,tbl_guest_stay_info.Stay_Info_ID, tbl_room_info.Rate, tbl_guest_info.Name, tbl_guest_stay_info.Status, tbl_guest_stay_info.Guest_ID, tbl_guest_stay_info.Room_ID,tbl_guest_stay_info.Check_Out_Date, tbl_guest_stay_info.Check_Out_Time,tbl_guest_stay_info.Departure_Date Order By tbl_guest_stay_info.Arrival_Date";
            //}
            //var data = db.Database.SqlQuery<DailyRoomLettingViewModel>(query);
            //List<DailyRoomLettingViewModel> tempList = data.ToList();
            tbl_service roomData = db.tbl_service.Find(1);
            decimal vatPercent = Convert.ToDecimal(roomData.VAT) / 100;
            decimal scPercent = Convert.ToDecimal(roomData.SC) / 100;

            var dateParam = new SqlParameter
            {
                ParameterName = "date",
                Value = id
            };
            var vatParam = new SqlParameter
            {
                ParameterName = "vat",
                Value = vatPercent
            };
            var scParam = new SqlParameter
            {
                ParameterName = "sc",
                Value = scPercent
            };

            var tempList = db.Database.SqlQuery<DailyRoomLettingViewModel>("exec RoomLetting @date,@vat,@sc ", dateParam, vatParam, scParam).ToList<DailyRoomLettingViewModel>();
            
            decimal baddebtTotal = 0;
            decimal refundTotal = 0;
            decimal surplusTotal = 0;

            for (int i = 0; i < tempList.Count; i++)
            {
                DailyRoomLettingViewModel thisrow = tempList[i];
                double secs = 0;
                int distinctDays = 0;
                if (tempList[i].CheckOut_Time.Date > dob1 || (tempList[i].CheckOut_Time.AddHours(3).Date > dob1.Date && tempList[i].Status == "CheckIn"))
                {
                    tempList[i].Stay = (int)Math.Ceiling((dob1 - tempList[i].Arrival_Date).TotalHours) + 24;
                    secs = (double)(dob1 - tempList[i].Arrival_Date).TotalSeconds + 86400;
                    tempList[i].CheckOutString = "";
                    //distinctDays = (int)Math.Ceiling((dob1 - tempList[i].Arrival_Date).TotalDays)+1;
                    distinctDays = DayCounter(tempList[i].Arrival_Date, dob1);
                }
                else
                {
                    tempList[i].Stay = (int)Math.Round((tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalHours);
                    secs = (double)(tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalSeconds;
                    tempList[i].CheckOutString = tempList[i].CheckOut_Time.ToString("dd/MM HH:mm");
                    //distinctDays = (int)Math.Ceiling((tempList[i].CheckOut_Time - tempList[i].Arrival_Date).TotalDays);
                    distinctDays = DayCounter(tempList[i].Arrival_Date, tempList[i].CheckOut_Time);
                }
                int hour = Convert.ToInt32(tempList[i].Stay);
                //int hr = secs / 3600;
                double hour2 = Convert.ToDouble(hour);
                int days = (int)Math.Ceiling(hour2 / 24);

                /*if (hour > 24 && (hour % 24 < 3 || days != distinctDays))
                {
                    tempList[i].CurrentBill = 0;
                }
                else if (hour <= 24 && tempList[i].Arrival_Date.Date != tempList[i].CheckOut_Time.Date && tempList[i].Arrival_Date.Date != dob1.Date)
                {
                    tempList[i].CurrentBill = 0;
                }*/

                if (secs > 86400 && (secs % 86400 < 10800 || days != distinctDays))
                {
                    tempList[i].CurrentBill = 0;
                }
                else if (hour <= 24 && tempList[i].Arrival_Date.Date != tempList[i].CheckOut_Time.Date && tempList[i].Arrival_Date.Date != dob1.Date)
                {
                    tempList[i].CurrentBill = 0;
                }


                if (tempList[i].Arrival_Date.Date < dob1.Date && tempList[i].CheckOut_Time.Date > dob1.Date && tempList[i].CheckOut_Time.Hour >= 3 && tempList[i].CurrentBill == 0)
                {
                    tempList[i].CurrentBill = tempList[i].Rate;
                }


                tempList[i].Total = Rounder.Round(tempList[i].PreviousBalance + tempList[i].CurrentBill + tempList[i].OtherBill);
                if (tempList[i].CheckOut_Time.Date == dob1 && tempList[i].Status == "Clear" && tempList[i].Baddebt > 0)
                {
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Balance = tempList[i].Total - tempList[i].Payment - tempList[i].Discount;
                }
                if (tempList[i].Advanced_Amount >= tempList[i].Refund)
                {
                    tempList[i].Refund = 0;
                }
                else
                {
                    tempList[i].Refund -= tempList[i].Advanced_Amount;
                }
                if (tempList[i].Balance < 0)
                {
                    tempList[i].Advanced_Amount = tempList[i].Balance * (-1);
                    //tempList[i].Advanced_Amount -= tempList[i].Discount;
                    tempList[i].Balance = 0;
                }
                else
                {
                    tempList[i].Advanced_Amount = 0;
                }
                refundTotal += tempList[i].Refund;
                baddebtTotal += tempList[i].Baddebt;
                surplusTotal += tempList[i].Surplus;
            }

            var vacantTotal = (from vacant in db.tbl_vacant_room where vacant.Date == dob1 select vacant.Rate).DefaultIfEmpty(0).Sum();
            var totalCharge = (from a in db.tbl_guest_stay_info join tran in db.tbl_room_transactions on a.Room_ID equals tran.Room_ID where a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note == "Temp_Stay" && DbFunctions.TruncateTime(a.Arrival_Date) == today && a.Departure_Date >= today select tran.Credit).Sum();

            var totalEntryToday = (from a in db.tbl_guest_stay_info where a.Status != "Reserved" && a.Status != "Booked" && a.Check_Out_Note != "Temp_Stay" && a.Check_Out_Note != "Reserved_Cancel" && a.Check_Out_Note != "Reserved_Auto_Cancel" && a.Check_Out_Note != "Booked_Cancel" && a.Check_Out_Note != "Booked_Auto_Cancel" && DbFunctions.TruncateTime(a.Arrival_Date) == today && a.Departure_Date >= today select a.Room_ID).Count();
            var totalCheckOutToday = (from a in db.tbl_guest_stay_info where a.Check_Out_Date == today && a.Check_Out_Note != "Temp_Stay" select a.Room_ID).Count();
            var checkInCustomerInfo = db.tbl_guest_stay_info.Where(a => DbFunctions.TruncateTime(a.Arrival_Date) <= today && ((a.Check_Out_Date.Value > today && a.Check_Out_Note != "Temp_Stay") || a.Status == "CheckIn")).Select(a => a.Room_ID);
            var checkinCount = checkInCustomerInfo.Count();

            //vacant room begin
            query = "SELECT tbl_room_info.Room_No ,round(tbl_room_info.Rate*.15+tbl_room_info.Rate,-1) as Rate, tbl_vacant_room.Date FROM tbl_vacant_room INNER JOIN  tbl_room_info ON tbl_vacant_room.Room_ID = tbl_room_info.Room_ID where tbl_vacant_room.Date = '" + id + "'";
            var vacantRoomList = db.Database.SqlQuery<VacantRoomViewModel>(query);

            //vacant room end
            //charge room begin
            query = "SELECT tbl_room_info.Room_No ,round(tbl_room_info.Rate * .15 + tbl_room_info.Rate, -1) as Rate, tbl_guest_stay_info.Check_Out_Date FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID where tbl_guest_stay_info.Check_Out_Date = '" + id + "' AND tbl_guest_stay_info.Check_Out_Note LIKE 'Entry Charge%' ORDER BY tbl_room_info.Room_No";
            var entryChargeRoomList = db.Database.SqlQuery<VacantRoomViewModel>(query);
            //charge room end
            query = "SELECT tbl_room_info.Room_No ,round(tbl_room_info.Rate * .15 + tbl_room_info.Rate, -1) as Rate, tbl_guest_stay_info.Check_Out_Date FROM tbl_guest_stay_info INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID where tbl_guest_stay_info.Check_Out_Date = '" + id + "' AND tbl_guest_stay_info.Check_Out_Note LIKE 'Running Charge%' ORDER BY tbl_room_info.Room_No";
            var runningChargeRoomList = db.Database.SqlQuery<VacantRoomViewModel>(query);

            query = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date, tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_guest_stay_info INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where tbl_guest_stay_info.Check_Out_Note LIKE '%Outstanding' AND tbl_guest_stay_info.Check_Out_Date <= '" + id + "' AND tbl_room_transactions.Tran_Ref_ID = 11 ORDER BY Checkout_Date";
            var outstandingChargeList = db.Database.SqlQuery<OutStandingViewModel>(query);

            query = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date,tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_room_transactions INNER JOIN tbl_guest_stay_info ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_room_transactions.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) = '" + id + "' AND tbl_room_transactions.Tran_Ref_ID = 12 ORDER BY tbl_room_transactions.Tran_Date";
            var outstandingPayList = db.Database.SqlQuery<OutStandingViewModel>(query);

            var totalOutstandingTodayCharge = Convert.ToDecimal(outstandingChargeList.Where(a => a.Checkout_Date.Date == dob1).Select(b => b.Credit).Sum());
            var totalOutstandingPay = Convert.ToDecimal(outstandingPayList.Select(a => a.Credit).Sum());

            var totalRoom = (from ri in db.tbl_room_info select ri.Room_ID).Count();
            var vacantCount = totalRoom - checkinCount;
            var previousVacant = totalRoom - (checkinCount - (totalEntryToday - totalCheckOutToday));
           
            var shiftRooms = (from e in db.tbl_guest_stay_history
                              where DbFunctions.TruncateTime(e.Shift_Date) == dob1
                select new
                {
                    Name = e.shift_from_stay_info.tbl_guest_info.Name ?? "",
                    Arrival_Date = e.shift_from_stay_info.Arrival_Date,
                    Previous_Room = e.shift_from_stay_info.tbl_room_info.Room_No ?? "",
                    New_Room = e.shift_to_stay_info.tbl_room_info.Room_No ?? "",
                    Shift_Date = e.Shift_Date.Value,
                    Reason = e.Reason ?? "",
                    User = e.tbl_emp_info.user_name ?? ""
                }).ToList();

            if (totalOutstandingPay > 0)
            {
                DailyRoomLettingViewModel outPayRow = new DailyRoomLettingViewModel();
                outPayRow.Room_No = "";
                outPayRow.Name = "Outstanding Payment";
                outPayRow.Rate = 0;
                outPayRow.Arrival_Date = DateTime.Now;
                outPayRow.PreviousBalance = 0;
                outPayRow.CurrentBill = 0;
                outPayRow.Total = 0;
                outPayRow.Payment = totalOutstandingPay;
                outPayRow.Balance = 0;
                outPayRow.Advanced_Amount = 0;
                outPayRow.Discount = 0;
                outPayRow.CheckOut_Time = DateTime.Now;
                outPayRow.Tran_Date = DateTime.Now;
                outPayRow.Stay = 0;
                outPayRow.Baddebt = 0;
                outPayRow.Refund = 0;
                outPayRow.Surplus = 0;
                outPayRow.CheckOutString = "";
                tempList.Add(outPayRow);
            }

            decimal booking_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) == dt.Date && a.Tran_Ref_ID == 14 && !(a.tbl_guest_stay_info.Status == "CheckIn" && DbFunctions.TruncateTime(a.tbl_guest_stay_info.Arrival_Date) == dt)).Sum(b => b.Credit));
            DailyRoomLettingViewModel bookPayRow = new DailyRoomLettingViewModel();
            if (booking_payment > 0)
            {
                bookPayRow.Room_No = "";
                bookPayRow.Name = "Booking Payment";
                bookPayRow.Rate = 0;
                bookPayRow.Arrival_Date = DateTime.Now;
                bookPayRow.PreviousBalance = 0;
                bookPayRow.CurrentBill = 0;
                bookPayRow.OtherBill = 0;
                bookPayRow.Total = 0;
                bookPayRow.Payment = booking_payment;
                bookPayRow.Balance = 0;
                bookPayRow.Advanced_Amount = 0;
                bookPayRow.Discount = 0;
                bookPayRow.CheckOut_Time = DateTime.Now;
                bookPayRow.Tran_Date = DateTime.Now;
                bookPayRow.Stay = 0;
                bookPayRow.Baddebt = 0;
                bookPayRow.Refund = 0;
                bookPayRow.Surplus = 0;
                bookPayRow.CheckOutString = "";
                tempList.Add(bookPayRow);
            }

            decimal booking_refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) == dt && a.Tran_Ref_ID == 13).Sum(b => b.Credit));
            DailyRoomLettingViewModel bookRefundRow = new DailyRoomLettingViewModel();
            if (booking_refund > 0)
            {
                bookRefundRow.Room_No = "";
                bookRefundRow.Name = "Cancellation Refund";
                bookRefundRow.Rate = 0;
                bookRefundRow.Arrival_Date = DateTime.Now;
                bookRefundRow.PreviousBalance = 0;
                bookRefundRow.CurrentBill = 0;
                bookRefundRow.OtherBill = 0;
                bookRefundRow.Total = 0;
                bookRefundRow.Payment = booking_refund * (-1);
                bookRefundRow.Balance = 0;
                bookRefundRow.Advanced_Amount = 0;
                bookRefundRow.Discount = 0;
                bookRefundRow.CheckOut_Time = DateTime.Now;
                bookRefundRow.Tran_Date = DateTime.Now;
                bookRefundRow.Stay = 0;
                bookRefundRow.Baddebt = 0;
                bookRefundRow.Refund = 0;
                bookRefundRow.Surplus = 0;
                bookRefundRow.CheckOutString = "";
                tempList.Add(bookRefundRow);
            }
            if (totalCharge == null)
            {
                totalCharge = 0;
            }
            


            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "RoomLettingReport.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }

            ReportDataSource DataSetRoomLetting = new ReportDataSource("DataSetRoomLetting", tempList);
            ReportDataSource DataSetOutstandingCharge = new ReportDataSource("DataSetOutstandingCharge", outstandingChargeList);
            ReportDataSource DataSetOutstandingPay = new ReportDataSource("DataSetOutstandingPay", outstandingPayList );
            ReportDataSource DataSetShiftRoom = new ReportDataSource("DataSetShiftRoom", shiftRooms);
            ReportDataSource DataSetVacantRoom = new ReportDataSource("DataSetVacantRoom", vacantRoomList );
            ReportDataSource DataSetEntryRoom = new ReportDataSource("DataSetEntryRoom", entryChargeRoomList);
            ReportDataSource DataSetRunningRoom = new ReportDataSource("DataSetRunningRoom", runningChargeRoomList );


            lc.DataSources.Add(DataSetRoomLetting);
            lc.DataSources.Add(DataSetOutstandingCharge);
            lc.DataSources.Add(DataSetOutstandingPay);
            lc.DataSources.Add(DataSetShiftRoom);
            lc.DataSources.Add(DataSetVacantRoom);
            lc.DataSources.Add(DataSetEntryRoom);
            lc.DataSources.Add(DataSetRunningRoom);

            int j = 0;
            ReportParameter[] param = new ReportParameter[17];
            param[j++] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[j++] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[j++] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[j++] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);

            param[j++] = new ReportParameter("BaddebtTotal", baddebtTotal.ToString(), false);
            param[j++] = new ReportParameter("RefundTotal", refundTotal.ToString(), false);
            param[j++] = new ReportParameter("SurplusTotal", surplusTotal.ToString(), false);
            param[j++] = new ReportParameter("Date", dob1.ToString("dd MMM yyyy"), false);

            param[j++] = new ReportParameter("PreviousVacant", previousVacant.ToString(), false);
            param[j++] = new ReportParameter("TodayEntry", totalEntryToday.ToString(), false);
            param[j++] = new ReportParameter("TodayDeparture", totalCheckOutToday.ToString(), false);
            param[j++] = new ReportParameter("TotalStay", checkinCount.ToString(), false);
            param[j++] = new ReportParameter("TotalVacant", vacantCount.ToString(), false);

            param[j++] = new ReportParameter("TotalOutstandingTodayCharge", totalOutstandingTodayCharge.ToString(), false);
            param[j++] = new ReportParameter("TotalOutstandingPay", totalOutstandingPay.ToString(), false);
            param[j++] = new ReportParameter("TotalBookingPayment", booking_payment.ToString(), false);
            param[j++] = new ReportParameter("TotalBookingRefund", booking_refund.ToString(), false);


            lc.SetParameters(param);



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public int DayCounter(DateTime start, DateTime end)
        {
            int counter = 0;
            for (var dt = start.Date; dt <= end.Date; dt = dt.AddDays(1))
            {
                counter++;
            }
            return counter;
        }
    }
}