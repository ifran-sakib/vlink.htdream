﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetInfoController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_banquet_info.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_banquet_info info)
        {

            if (ModelState.IsValid)
            {
                info.Status = "Vacant";
                db.tbl_banquet_info.Add(info);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_banquet_info.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_banquet_info info, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(info).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_banquet_info.Remove(db.tbl_banquet_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
