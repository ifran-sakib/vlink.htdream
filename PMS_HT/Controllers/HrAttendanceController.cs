﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class HrAttendanceController : CustomControllerBase
    {

        public ActionResult Index()
        {
            var tbl_attendance = db.tbl_attendance.Include(t => t.tbl_emp_info);
            return View(tbl_attendance.ToList());
        }
        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_attendance tbl_attendance = db.tbl_attendance.Find(id);
            if (tbl_attendance == null)
            {
                return HttpNotFound();
            }
            return View(tbl_attendance);
        }


        public ActionResult Create()
        {
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Attendance_ID,Emp_ID,Punch_In_Time,Punch_Out_Time,Date,Comments")] tbl_attendance tbl_attendance)
        {
            if (ModelState.IsValid)
            {
                db.tbl_attendance.Add(tbl_attendance);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_attendance.Emp_ID);
            return View(tbl_attendance);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_attendance tbl_attendance = db.tbl_attendance.Find(id);
            if (tbl_attendance == null)
            {
                return HttpNotFound();
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_attendance.Emp_ID);
            return View(tbl_attendance);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Attendance_ID,Emp_ID,Punch_In_Time,Punch_Out_Time,Date,Comments")] tbl_attendance tbl_attendance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_attendance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_attendance.Emp_ID);
            return View(tbl_attendance);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_attendance tbl_attendance = db.tbl_attendance.Find(id);
            if (tbl_attendance == null)
            {
                return HttpNotFound();
            }
            return View(tbl_attendance);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_attendance tbl_attendance = db.tbl_attendance.Find(id);
            db.tbl_attendance.Remove(tbl_attendance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
