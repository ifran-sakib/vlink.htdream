﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class HouseKeepingTaskScheduleController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_task_schdule = db.tbl_task_schdule.Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_maid_info).Include(t => t.tbl_room_info).Include(t => t.tbl_task_setup);
            ViewBag.status = "In Process";
            ViewBag.status2 = "Done";
            return View(tbl_task_schdule.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name");
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No");
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_task_schdule schedule)
        {
            if (ModelState.IsValid)
            {
                schedule.Task_Schdule_Status = "In Process";
                db.tbl_task_schdule.Add(schedule);
                db.SaveChanges();
            }
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", schedule.Updated_By);
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", schedule.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", schedule.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", schedule.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", schedule.Task_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            tbl_task_schdule taskSchedule = db.tbl_task_schdule.Find(id);
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskSchedule.Updated_By);
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskSchedule.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskSchedule.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskSchedule.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskSchedule.Task_ID);
            return PartialView(taskSchedule);
        }

        [HttpPost]
        public ActionResult Edit(tbl_task_schdule schedule, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schedule).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", schedule.Updated_By);
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", schedule.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", schedule.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", schedule.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", schedule.Task_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_task_schdule.Remove(db.tbl_task_schdule.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
