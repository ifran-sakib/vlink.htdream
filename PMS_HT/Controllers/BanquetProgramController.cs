﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetProgramController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_banquet_prog = db.tbl_banquet_prog.Include(t => t.tbl_banquet_info).Include(t => t.tbl_country);
            return View(tbl_banquet_prog.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_banquet_prog tbl_banquet_prog = db.tbl_banquet_prog.Find(id);
            if (tbl_banquet_prog == null)
            {
                return HttpNotFound();
            }
            return View(tbl_banquet_prog);
        }

        public ActionResult Add()
        {
            ViewBag.Banquet_ID = new SelectList(db.tbl_banquet_info, "Banquet_ID", "Banquet_Name");
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_banquet_prog program)
        {
            if (ModelState.IsValid)
            {
                db.tbl_banquet_prog.Add(program);
                db.SaveChanges();
            }
            ViewBag.Banquet_ID = new SelectList(db.tbl_banquet_info, "Banquet_ID", "Banquet_Name", program.Banquet_ID);
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", program.Country_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            tbl_banquet_prog program = db.tbl_banquet_prog.Find(id);
            ViewBag.Banquet_ID = new SelectList(db.tbl_banquet_info, "Banquet_ID", "Banquet_Name", program.Banquet_ID);
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", program.Country_ID);
            return PartialView(program);
        }

        [HttpPost]
        public ActionResult Edit(tbl_banquet_prog program, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(program).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Banquet_ID = new SelectList(db.tbl_banquet_info, "Banquet_ID", "Banquet_Name", program.Banquet_ID);
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", program.Country_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_banquet_prog.Remove(db.tbl_banquet_prog.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
