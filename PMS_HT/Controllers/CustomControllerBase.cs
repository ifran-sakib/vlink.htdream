﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.CustomClass;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [SessionExpire]
    public class CustomControllerBase : Controller
    {
        protected readonly PMS_HT_DB db = new PMS_HT_DB();
        protected readonly CommonRepoprtData rpd = new CommonRepoprtData();

        public CustomControllerBase()
        {
            this.SubscriptionCheck();
        }

        protected byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        protected DateTime FrontDeskRoomColor()
        {
            var preferences = db.tbl_preferences.Where(a => a.Type == "Color" || a.Type == "DateTime").ToList();
            ViewBag.ClearColor = preferences.FirstOrDefault(a => a.Preference_Name == "Clear Room").Value;
            ViewBag.OccupiedColor = preferences.FirstOrDefault(a => a.Preference_Name == "Occupied Room").Value;
            ViewBag.ReservedColor = preferences.FirstOrDefault(a => a.Preference_Name == "Reserved Room").Value;
            ViewBag.BookedColor = preferences.FirstOrDefault(a => a.Preference_Name == "Booked Room").Value;
            ViewBag.DirtyColor = preferences.FirstOrDefault(a => a.Preference_Name == "Dirty Room").Value;
            ViewBag.OutOfOrderColor = preferences.FirstOrDefault(a => a.Preference_Name == "OutOfOrder Room").Value;
            ViewBag.HouseUseColor = preferences.FirstOrDefault(a => a.Preference_Name == "House Use").Value;
            ViewBag.UpcomingDepartColor = preferences.FirstOrDefault(a => a.Preference_Name == "Expected Departure").Value;
            string lastdt = preferences.FirstOrDefault(a => a.Preference_ID == 7).Value;
            return DateTime.ParseExact(lastdt, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        }
    }
}