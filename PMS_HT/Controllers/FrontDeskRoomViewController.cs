﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskRoomViewController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_room_view_setup.ToList());
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_view_setup tbl_room_view_setup = db.tbl_room_view_setup.Find(id);
            if (tbl_room_view_setup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_room_view_setup);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Room_view_ID,View_type,View_details,YsnActive,Created_Date")] tbl_room_view_setup tbl_room_view_setup)
        {
            if (ModelState.IsValid)
            {
                tbl_room_view_setup.Created_Date = DateTime.Now;
                db.tbl_room_view_setup.Add(tbl_room_view_setup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(tbl_room_view_setup);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_view_setup tbl_room_view_setup = db.tbl_room_view_setup.Find(id);
            if (tbl_room_view_setup == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_room_view_setup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Room_view_ID,View_type,View_details,YsnActive,Created_Date")] tbl_room_view_setup tbl_room_view_setup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_room_view_setup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_room_view_setup);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_view_setup tbl_room_view_setup = db.tbl_room_view_setup.Find(id);
            if (tbl_room_view_setup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_room_view_setup);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_room_view_setup tbl_room_view_setup = db.tbl_room_view_setup.Find(id);
            db.tbl_room_view_setup.Remove(tbl_room_view_setup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult UniqueRoomViewCheck(string str)
        {
            tbl_room_view_setup room = db.tbl_room_view_setup.FirstOrDefault(r => r.View_type == str);
            if (room == null)
            {
                return Json("unique", JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
