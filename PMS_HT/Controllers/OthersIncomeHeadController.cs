﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class OthersIncomeHeadController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_others_income_head.ToList());
        }

        public ActionResult Add()
        {

            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_others_income_head income)
        {
            if (ModelState.IsValid)
            {

                db.tbl_others_income_head.Add(income);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_others_income_head tbl_others_income_head = db.tbl_others_income_head.Find(id);
            if (tbl_others_income_head == null)
            {
                return HttpNotFound();
            }
            return View(tbl_others_income_head);
        }
        
        public ActionResult Edit(int id)
        {

            return PartialView(db.tbl_others_income_head.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_others_income_head income, int id)
        {
            if (ModelState.IsValid)
            {

                db.Entry(income).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_others_income_head.Remove(db.tbl_others_income_head.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
