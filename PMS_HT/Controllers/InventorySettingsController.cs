﻿using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Inventory")]
    public class InventorySettingsController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_category.ToList());
        }

        public ActionResult SubCategory()
        {
            return View(db.tbl_subcategory.ToList());
        }

        public ActionResult Items()
        {
            return View(db.tbl_item.ToList());
        }

        public ActionResult Brands()
        {
            return View(db.tbl_brand.ToList());
        }

        public ActionResult StockLocations()
        {
            return View(db.tbl_stock_location.ToList());
        }

        public ActionResult Units()
        {
            return View(db.tbl_unit.ToList());
        }

        public ActionResult Vendors()
        {
            return View(db.tbl_supplier.ToList());
        }
    }
}