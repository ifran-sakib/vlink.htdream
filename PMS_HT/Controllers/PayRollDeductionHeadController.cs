﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
    public class PayRollDeductionHeadController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_earning_head.Where(a => a.Type.Equals("Deduction")).ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_earning_head tbl_earning_head = db.tbl_earning_head.Find(id);
            if (tbl_earning_head == null)
            {
                return HttpNotFound();
            }
            return View(tbl_earning_head);
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Earning_Head_ID,Earning_Head_Name,Type")] tbl_earning_head tbl_earning_head)
        {
            if (ModelState.IsValid)
            {
                tbl_earning_head.Type = "Deduction";
                db.tbl_earning_head.Add(tbl_earning_head);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_earning_head);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_earning_head tbl_earning_head = db.tbl_earning_head.Find(id);
            if (tbl_earning_head == null)
            {
                return HttpNotFound();
            }
            return View(tbl_earning_head);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Earning_Head_ID,Earning_Head_Name")] tbl_earning_head tbl_earning_head)
        {
            if (ModelState.IsValid)
            {
                tbl_earning_head.Type = "Deduction";
                db.Entry(tbl_earning_head).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_earning_head);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_earning_head tbl_earning_head = db.tbl_earning_head.Find(id);
            if (tbl_earning_head == null)
            {
                return HttpNotFound();
            }
            return View(tbl_earning_head);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_earning_head tbl_earning_head = db.tbl_earning_head.Find(id);
            db.tbl_earning_head.Remove(tbl_earning_head);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}