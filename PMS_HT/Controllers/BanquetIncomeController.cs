﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetIncomeController : CustomControllerBase
    {
        public ActionResult Index()
        {

            return View();   
        }
        public PartialViewResult ViewDateWiseBanquetIncome(DateTime? dob, DateTime? date)
        {
            var tbl_banquet_transactions = db.tbl_banquet_transactions.Where(a => a.Tran_Ref_Name.Equals("payment") && a.Tran_Date >= dob && a.Tran_Date <= date).Include(t => t.tbl_banquet_amenities).Include(t => t.tbl_banquet_booking).Include(t => t.tbl_banquet_prog);
            ViewBag.transactions = tbl_banquet_transactions.ToList();
            return PartialView();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_banquet_transactions tbl_banquet_transactions = db.tbl_banquet_transactions.Find(id);
            if (tbl_banquet_transactions == null)
            {
                return HttpNotFound();
            }
            return View(tbl_banquet_transactions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
