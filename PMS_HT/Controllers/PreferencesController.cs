﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class PreferencesController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var preferences = db.tbl_preferences.OrderBy(a => a.Type).ThenBy(b => b.Preference_Name).ToList();
            return View(preferences);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_preferences tbl_preferences = db.tbl_preferences.Find(id);
            if (tbl_preferences == null)
            {
                return HttpNotFound();
            }
            return View(tbl_preferences);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Create()
        {
            return PartialView();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult Create([Bind(Include = "Preference_ID,Type,Preference_Name,Value")] tbl_preferences tbl_preferences)
        {
            if (ModelState.IsValid)
            {
                db.tbl_preferences.Add(tbl_preferences);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(tbl_preferences);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_preferences tbl_preferences = db.tbl_preferences.Find(id);
            if (tbl_preferences == null || tbl_preferences.Preference_Name == "Color")
            {
                return HttpNotFound();
            }
            return PartialView(tbl_preferences);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Preference_ID,Type,Preference_Name,Value")] tbl_preferences tbl_preferences)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_preferences).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_preferences);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_preferences tbl_preferences = db.tbl_preferences.Find(id);
            if (tbl_preferences == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_preferences);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_preferences tbl_preferences = db.tbl_preferences.Find(id);
            db.tbl_preferences.Remove(tbl_preferences);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
