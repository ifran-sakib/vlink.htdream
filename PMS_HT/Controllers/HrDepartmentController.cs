﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
	[Authorize(Roles = "Super Admin")]
    public class HrDepartmentController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_department.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_department tbl_department = db.tbl_department.Find(id);
            if (tbl_department == null)
            {
                return HttpNotFound();
            }
            return View(tbl_department);
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Department_ID,Department_Name")] tbl_department tbl_department)
        {
            if (ModelState.IsValid)
            {
                db.tbl_department.Add(tbl_department);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_department);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_department tbl_department = db.tbl_department.Find(id);
            if (tbl_department == null)
            {
                return HttpNotFound();
            }
            return View(tbl_department);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Department_ID,Department_Name")] tbl_department tbl_department)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_department).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_department);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_department tbl_department = db.tbl_department.Find(id);
            if (tbl_department == null)
            {
                return HttpNotFound();
            }
            return View(tbl_department);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_department tbl_department = db.tbl_department.Find(id);
            db.tbl_department.Remove(tbl_department);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
