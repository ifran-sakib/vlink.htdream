﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    public class OutstandingChargeController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var query = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date, tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_guest_stay_info INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where tbl_guest_stay_info.Check_Out_Note LIKE '%Outstanding' AND tbl_room_transactions.Tran_Ref_ID = 11 ORDER BY Checkout_Date";
            var data = db.Database.SqlQuery<OutStandingViewModel>(query);
            List<OutStandingViewModel> tempList = data.ToList();

            ViewBag.OutStandingList = tempList;
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            decimal roomCharge = 0, vat = 0, discount = 0, paid = 0, otherBill = 0, refund = 0, roomPayment = 0, roomVat = 0, roomDiscount = 0, roomOtherBill = 0, selectedRoomCharge = 0, roomDue = 0;
            var guest1 = db.tbl_guest_stay_info.Where(a => a.Stay_Info_ID == id).FirstOrDefault();
            ViewBag.GuestName = guest1.tbl_guest_info.Name;
            ViewBag.Phone = guest1.tbl_guest_info.Phone;
            ViewBag.Address = guest1.tbl_guest_info.Address;
            ViewBag.ArrivalDate = guest1.Arrival_Date;
            ViewBag.DepartureDate = guest1.Departure_Date;
            DateTime checkOutDateTime = guest1.Check_Out_Date.Value;
            checkOutDateTime = checkOutDateTime.Add(guest1.Check_Out_Time.Value);
            ViewBag.checkOutDateTime = checkOutDateTime;
            ViewBag.id = id;
            if (guest1 != null)
            {
                var noOfNight = (int)Math.Round((DateTime.Now - guest1.Arrival_Date).TotalHours);
                int extraHour = noOfNight % 24;
                noOfNight /= 24;
                if (extraHour >= 3)
                {
                    noOfNight++;
                }
                ViewBag.NoOfNight = noOfNight;
                ViewBag.RoomChargePerDayS = guest1.tbl_room_info.Rate;
                var roomChargesS = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 4))).ToList();
                ViewBag.RoomChargeListS = roomChargesS;
                if (roomChargesS != null)
                {
                    foreach (var item in roomChargesS)
                    {
                        if (item.Tran_Date.Value.Date == guest1.Arrival_Date.Date || item.Tran_Date.Value.AddHours(3) < checkOutDateTime)
                        {
                            roomCharge += (decimal)item.Debit;
                        }
                    }
                    ViewBag.RoomChargeS = roomCharge;
                }

                var vats = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 2))).ToList();
                if (vats != null)
                {
                    foreach (var item in vats)
                    {
                        if (item.Tran_Date.Value.Date == guest1.Arrival_Date.Date || item.Tran_Date.Value.AddHours(3) < checkOutDateTime)
                        {
                            vat += (decimal)item.Debit;
                        }
                    }
                    ViewBag.Vat = vat;
                    ViewBag.RoomVat = roomVat;
                    ViewBag.VatPercent = vats.FirstOrDefault().Debit;
                }

                var discounts = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 1))).ToList();
                ViewBag.DiscountList = discounts;
                if (discounts != null && discounts.Count != 0)
                {
                    foreach (var item in discounts)
                    {
                        discount += (decimal)item.Credit;
                    }
                }
                ViewBag.Discount = discount;

                var otherBillsS = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 5))).ToList();
                ViewBag.OtherBillListS = otherBillsS;
                if (otherBillsS != null)
                {
                    foreach (var item in otherBillsS)
                    {
                        otherBill += (decimal)item.Debit;
                    }
                }
                ViewBag.OtherBillS = otherBill;

                var refunds = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 7))).ToList();
                if (refunds != null)
                {
                    foreach (var item in refunds)
                    {
                        refund += (decimal)item.Credit;
                    }
                }
                ViewBag.refund = refund;

                var paidTransactionsS = (db.tbl_room_transactions.Where(a => a.Stay_Info_ID == (guest1.Stay_Info_ID) && (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 12))).ToList();
                ViewBag.paidTransactionsS = paidTransactionsS;
                if (paidTransactionsS != null)
                {
                    foreach (var item in paidTransactionsS)
                    {
                        paid += (decimal)item.Credit;
                    }
                    ViewBag.PaidS = paid;
                }
            }
            roomDue = (roomCharge + vat + otherBill) - (paid + discount);
            ViewBag.RoomTotalCharge = (roomCharge + vat + otherBill) - discount;
            ViewBag.RoomDueS = roomDue;
            return PartialView();
        }

        [HttpPost]
        public ActionResult Edit(int stayId, decimal paid, string particulars, decimal netPayable)
        {
            if (ModelState.IsValid)
            {
                var guest1 = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayId);

                bool isFullPaid = false;
                tbl_room_transactions paidTransaction = new tbl_room_transactions();
                paidTransaction.Stay_Info_ID = guest1.Stay_Info_ID;
                paidTransaction.Room_ID = guest1.Room_ID;
                paidTransaction.Guest_ID = guest1.Guest_ID;
                paidTransaction.Debit = 0;

                if (guest1 != null)
                {
                    decimal outstand_charge = 0;
                    tbl_room_transactions outRow = db.tbl_room_transactions.FirstOrDefault(a => a.Stay_Info_ID == stayId && a.Tran_Ref_ID == 11);
                    if (outRow != null)
                    {
                        outstand_charge += (decimal)outRow.Credit;
                    }

                    if (paid != 0)
                    {
                        if (paid < netPayable)
                        {
                            paidTransaction.Credit = paid;
                            paidTransaction.Tran_Date = DateTime.Now;
                            paidTransaction.Tran_Ref_ID = 12;
                            paidTransaction.Tran_Ref_Name = "OutStanding Paid";
                            paidTransaction.Particulars = particulars;
                            db.tbl_room_transactions.Add(paidTransaction);
                            db.SaveChanges();

                            outRow.Credit = netPayable - paid;
                            db.tbl_room_transactions.Attach(outRow);
                            var updatedOutRow = db.Entry(outRow);
                            updatedOutRow.State = EntityState.Modified;


                            //db.SaveChanges();

                        }
                        else if (paid > netPayable)
                        {
                            paidTransaction.Credit = netPayable;
                            paidTransaction.Tran_Date = DateTime.Now;
                            paidTransaction.Tran_Ref_ID = 12;
                            paidTransaction.Tran_Ref_Name = "OutStanding Paid";
                            paidTransaction.Particulars = particulars;
                            db.tbl_room_transactions.Add(paidTransaction);
                            db.SaveChanges();

                            outRow.Credit = 0;
                            db.tbl_room_transactions.Attach(outRow);
                            var updatedOutRow = db.Entry(outRow);
                            updatedOutRow.State = EntityState.Modified;

                            paidTransaction.Debit = paid - netPayable;
                            paidTransaction.Credit = 0;
                            paidTransaction.Tran_Ref_ID = 9;
                            paidTransaction.Tran_Ref_Name = "surPlus";
                            db.tbl_room_transactions.Add(paidTransaction);
                            isFullPaid = true;
                        }
                        else
                        {
                            paidTransaction.Credit = netPayable;
                            paidTransaction.Tran_Date = DateTime.Now;
                            paidTransaction.Tran_Ref_ID = 12;
                            paidTransaction.Tran_Ref_Name = "OutStanding Paid";
                            paidTransaction.Particulars = particulars;
                            db.tbl_room_transactions.Add(paidTransaction);
                            db.SaveChanges();

                            outRow.Credit = 0;
                            db.tbl_room_transactions.Attach(outRow);
                            var updatedOutRow = db.Entry(outRow);
                            updatedOutRow.State = EntityState.Modified;

                            isFullPaid = true;
                        }
                    }
                    string note = guest1.Check_Out_Note;
                    if (note == "Entry Charge Outstanding" && isFullPaid == true)
                    {
                        guest1.Check_Out_Note = "Entry Charge";
                    }
                    else if (note == "Running Charge Outstanding" && isFullPaid == true)
                    {
                        guest1.Check_Out_Note = "Running Charge";
                    }
                    else if (note == "Outstanding" && isFullPaid == true)
                    {
                        guest1.Check_Out_Note = null;
                    }
                    db.tbl_guest_stay_info.Attach(guest1);
                    var updatedGuestInfo = db.Entry(guest1);
                    updatedGuestInfo.State = EntityState.Modified;
                }
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}