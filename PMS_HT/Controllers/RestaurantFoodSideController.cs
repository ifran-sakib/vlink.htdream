﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantFoodSideController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantFoodSide
        public ActionResult Index()
        {
            return View(db.tbl_food_side_extra.ToList());
        }

        // GET: RestaurantFoodSide/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_side_extra tbl_food_side_extra = db.tbl_food_side_extra.Find(id);
            if (tbl_food_side_extra == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food_side_extra);
        }

        // GET: RestaurantFoodSide/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RestaurantFoodSide/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Food_Side_ID,Food_Side_Name,Description,Image,Availability,Price,Discount")] tbl_food_side_extra tbl_food_side_extra)
        {
            if (ModelState.IsValid)
            {
                db.tbl_food_side_extra.Add(tbl_food_side_extra);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_food_side_extra);
        }

        // GET: RestaurantFoodSide/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_side_extra tbl_food_side_extra = db.tbl_food_side_extra.Find(id);
            if (tbl_food_side_extra == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food_side_extra);
        }

        // POST: RestaurantFoodSide/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Food_Side_ID,Food_Side_Name,Description,Image,Availability,Price,Discount")] tbl_food_side_extra tbl_food_side_extra)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_food_side_extra).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_food_side_extra);
        }

        // GET: RestaurantFoodSide/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_side_extra tbl_food_side_extra = db.tbl_food_side_extra.Find(id);
            if (tbl_food_side_extra == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food_side_extra);
        }

        // POST: RestaurantFoodSide/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_food_side_extra tbl_food_side_extra = db.tbl_food_side_extra.Find(id);
            db.tbl_food_side_extra.Remove(tbl_food_side_extra);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
