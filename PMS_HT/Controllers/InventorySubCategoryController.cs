﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventorySubCategoryController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_subcategory.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_subcategory sbuCategory)
        {
            if (ModelState.IsValid)
            {
                db.tbl_subcategory.Add(sbuCategory);
                db.SaveChanges();
            }
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", sbuCategory.Cata_ID);
            return RedirectToAction("SubCategory", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            tbl_subcategory subcategory = db.tbl_subcategory.Find(id);
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", subcategory.Cata_ID);
            return PartialView(subcategory);
        }

        [HttpPost]
        public ActionResult Edit(tbl_subcategory sbuCategory, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sbuCategory).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", sbuCategory.Cata_ID);
            return RedirectToAction("SubCategory", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_subcategory.Remove(db.tbl_subcategory.Find(id));
            db.SaveChanges();
            return RedirectToAction("SubCategory", "InventorySettings");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
