﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantDineSetupController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantDineSetup
        public ActionResult Index()
        {
            var tbl_dining_set_up = db.tbl_dining_set_up.Include(t => t.tbl_dining_location);
            return View(tbl_dining_set_up.ToList());
        }

        // GET: RestaurantDineSetup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_set_up tbl_dining_set_up = db.tbl_dining_set_up.Find(id);
            if (tbl_dining_set_up == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_set_up);
        }

        // GET: RestaurantDineSetup/Create
        public ActionResult Create()
        {
            ViewBag.Dining_Location_ID = new SelectList(db.tbl_dining_location, "Dining_Location_ID", "Location_Name");
            ViewBag.Dining_Waiter_ID = new SelectList(db.tbl_dining_waiter, "Dining_Waiter_ID", "tbl_emp_info.Emp_Name");
            return View();
        }

        // POST: RestaurantDineSetup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Dining_ID,Dining_No,Capacity,Status,Dining_Location_ID,Dining_Waiter_ID")] tbl_dining_set_up tbl_dining_set_up)
        {
            if (ModelState.IsValid)
            {
                db.tbl_dining_set_up.Add(tbl_dining_set_up);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Dining_Location_ID = new SelectList(db.tbl_dining_location, "Dining_Location_ID", "Location_Name", tbl_dining_set_up.Dining_Location_ID);
            ViewBag.Dining_Waiter_ID = new SelectList(db.tbl_dining_waiter, "Dining_Waiter_ID", "tbl_emp_info.Emp_Name");
            return View(tbl_dining_set_up);
        }

        // GET: RestaurantDineSetup/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_set_up tbl_dining_set_up = db.tbl_dining_set_up.Find(id);
            if (tbl_dining_set_up == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dining_Location_ID = new SelectList(db.tbl_dining_location, "Dining_Location_ID", "Location_Name", tbl_dining_set_up.Dining_Location_ID);
            ViewBag.Dining_Waiter_ID = new SelectList(db.tbl_dining_waiter, "Dining_Waiter_ID", "tbl_emp_info.Emp_Name", tbl_dining_set_up.Dining_Waiter_ID);
            return PartialView(tbl_dining_set_up);
        }

        // POST: RestaurantDineSetup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Dining_ID,Dining_No,Capacity,Status,Dining_Location_ID,Dining_Waiter_ID")] tbl_dining_set_up tbl_dining_set_up)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_dining_set_up).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Dining_Location_ID = new SelectList(db.tbl_dining_location, "Dining_Location_ID", "Location_Name", tbl_dining_set_up.Dining_Location_ID);
            ViewBag.Dining_Waiter_ID = new SelectList(db.tbl_dining_waiter, "Dining_Waiter_ID", "tbl_emp_info.Emp_Name", tbl_dining_set_up.Dining_Waiter_ID);
            return PartialView(tbl_dining_set_up);
        }

        // GET: RestaurantDineSetup/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_dining_set_up tbl_dining_set_up = db.tbl_dining_set_up.Find(id);
            if (tbl_dining_set_up == null)
            {
                return HttpNotFound();
            }
            return View(tbl_dining_set_up);
        }

        // POST: RestaurantDineSetup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_dining_set_up tbl_dining_set_up = db.tbl_dining_set_up.Find(id);
            db.tbl_dining_set_up.Remove(tbl_dining_set_up);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
