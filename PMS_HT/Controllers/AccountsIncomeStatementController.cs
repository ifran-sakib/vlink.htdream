using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Web.Mvc;
using PMS_HT.Models;
using Microsoft.Reporting.WebForms;
using PMS_HT.Areas.Services.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk,Account")]
    public class AccountsIncomeStatementController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ViewDateRangeIncome(DateTime? dob, DateTime? date)
        {
            tbl_service roomData = db.tbl_service.Find(1);
            decimal vatPercent = Convert.ToDecimal(roomData.VAT) / 100;
            decimal scPercent = Convert.ToDecimal(roomData.SC) / 100;
            decimal totalVatScPercent = 1 + vatPercent + scPercent;

            DateTime todaysDate = DateTime.Now.Date;
            DateTime StartDate = Convert.ToDateTime("2017-07-22");
            string dobStr = dob.Value.ToString("yyyy-MM-dd");
            string dateStr = date.Value.ToString("yyyy-MM-dd");
            var tbl_room_transactions = db.tbl_room_transactions
                                            .Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob 
                                                            && DbFunctions.TruncateTime(a.Tran_Date) <= date 
                                                            && (a.Tran_Ref_ID == 3 || a.Tran_Ref_ID == 7))
                                            .Include(t => t.tbl_guest_info)
                                            .Include(t => t.tbl_room_info)
                                            .ToList();

            
            var currentEntryIncome = (from tran in db.tbl_room_transactions
                                 join guest in db.tbl_guest_stay_info on tran.Stay_Info_ID equals guest.Stay_Info_ID
                                 where (DbFunctions.TruncateTime(guest.Arrival_Date) == DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                 (DbFunctions.TruncateTime(guest.Arrival_Date) >= dob) &&
                                 (DbFunctions.TruncateTime(guest.Arrival_Date) <= date) &&
                                 (DbFunctions.TruncateTime(tran.Tran_Date) >= dob) &&
                                 (DbFunctions.TruncateTime(tran.Tran_Date) <= date) && tran.Tran_Ref_ID == 3
                                 group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                 select new DailyRoomLettingViewModel
                                 {
                                     Room_No = g.Key.Room_No,
                                     Payment = (g.Sum(a => a.Credit) ?? 0),
                                     Tran_Date = (g.Key.Tran_Date).Value
                                 }).ToList();
            var runningEntryIncome = (from tran in db.tbl_room_transactions
                                 join guest in db.tbl_guest_stay_info on tran.Stay_Info_ID equals guest.Stay_Info_ID
                                 where (DbFunctions.TruncateTime(guest.Arrival_Date) < DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                 //(DbFunctions.TruncateTime(guest.Arrival_Date) >= dob) && 
                                 //(DbFunctions.TruncateTime(guest.Arrival_Date) <= date) && 
                                 (DbFunctions.TruncateTime(tran.Tran_Date) >= dob) &&
                                 (DbFunctions.TruncateTime(tran.Tran_Date) <= date) && tran.Tran_Ref_ID == 3
                                 group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                 select new DailyRoomLettingViewModel
                                 {
                                     Room_No = g.Key.Room_No,
                                     Payment = (g.Sum(a => a.Credit) ?? 0),
                                     Tran_Date = (g.Key.Tran_Date).Value
                                 }).ToList();

            if (dob.Value.Date < StartDate.Date)
            {
                 currentEntryIncome = (from tran in db.tbl_room_transactions
                                         join guest in db.tbl_guest_stay_info on new { tran.Room_ID, tran.Guest_ID } equals new { guest.Room_ID, guest.Guest_ID }
                                         where (DbFunctions.TruncateTime(guest.Arrival_Date) == DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                         (DbFunctions.TruncateTime(guest.Arrival_Date) >= dob) &&
                                         (DbFunctions.TruncateTime(guest.Arrival_Date) <= date) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) >= dob) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) <= date) && tran.Tran_Ref_ID == 3
                                         group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                         select new DailyRoomLettingViewModel
                                         {
                                             Room_No = g.Key.Room_No,
                                             Payment = (g.Sum(a => a.Credit) ?? 0),
                                             Tran_Date = (g.Key.Tran_Date).Value
                                         }).ToList();

                 runningEntryIncome = (from tran in db.tbl_room_transactions
                                         join guest in db.tbl_guest_stay_info on new { tran.Room_ID, tran.Guest_ID } equals new { guest.Room_ID, guest.Guest_ID }
                                         where (DbFunctions.TruncateTime(guest.Arrival_Date) < DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                         //(DbFunctions.TruncateTime(guest.Arrival_Date) >= dob) && 
                                         //(DbFunctions.TruncateTime(guest.Arrival_Date) <= date) && 
                                         (DbFunctions.TruncateTime(tran.Tran_Date) >= dob) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) <= date) && tran.Tran_Ref_ID == 3
                                         group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                         select new DailyRoomLettingViewModel
                                         {
                                             Room_No = g.Key.Room_No,
                                             Payment = (g.Sum(a => a.Credit) ?? 0),
                                             Tran_Date = (g.Key.Tran_Date).Value
                                         }).ToList();
            }
            var qq = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date,tbl_room_transactions.Tran_Date,  isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date,tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_room_transactions INNER JOIN tbl_guest_stay_info ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_room_transactions.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) >= '" + dobStr + "' AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) <= '" +dateStr + "' AND tbl_room_transactions.Tran_Ref_ID = 12 ORDER BY tbl_room_transactions.Tran_Date";
            var outstandingPayList = db.Database.SqlQuery<OutStandingViewModel>(qq);

            var tbl_banquet_transactions = db.tbl_banquet_transactions
                                                .Where(a => a.Tran_Ref_Name.Equals("payment") && DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date)
                                                .Include(t => t.tbl_banquet_amenities)
                                                .Include(t => t.tbl_banquet_booking)
                                                .Include(t => t.tbl_banquet_prog)
                                                .ToList();
            var tbl_others_income = db.tbl_others_income
                                        .Where(a => a.Created_Date >= dob && a.Created_Date <= date)
                                        .Include(t => t.tbl_emp_info)                                        
                                        .Include(t => t.tbl_emp_info1)
                                        .Include(t => t.tbl_others_income_head)
                                        .ToList();
            var tbl_expense = db.tbl_expense
                                .Where(a => a.Created_Date >= dob && a.Created_Date <= date)
                                .Include(t => t.tbl_emp_info)
                                .Include(t => t.tbl_emp_info1)
                                .Include(t => t.tbl_expense_head)
                                .ToList();
            var salaryExpense = db.tbl_salary_transactions
                                    .Where(a => a.Tran_Ref_Name.Equals("SalaryPayment") && a.Date >= dob && a.Date <= date)
                                    .Include(t => t.tbl_emp_info)
                                    .ToList();
            var bankExpense = db.tbl_bank_transaction
                                .Where(a => a.Tran_Type.Equals("Deposit") && a.Created_Date >= dob && a.Created_Date <= date && a.tbl_account_info.tbl_bank_info.Bank_Name != "Cash Bank")
                                .Include(x => x.tbl_account_info)
                                .Include(x => x.tbl_account_info.tbl_bank_info)
                                .ToList();
            var bankIncome = db.tbl_bank_transaction
                                .Where(a => a.Tran_Type.Equals("Withdrawn") && a.Created_Date >= dob && a.Created_Date <= date && a.tbl_account_info.tbl_bank_info.Bank_Name != "Cash Bank")
                                .Include(x => x.tbl_account_info)
                                .ToList();
            var cashBankExpense = db.tbl_bank_transaction
                                        .Where(a => a.Tran_Type.Equals("Deposit") && a.Created_Date >= dob && a.Created_Date <= date && a.tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank")
                                        .Include(x => x.tbl_account_info)
                                        .Include(x => x.tbl_account_info.tbl_bank_info)
                                        .ToList();
            var cashBankIncome = db.tbl_bank_transaction
                                    .Where(a => a.Tran_Type.Equals("Withdrawn") && a.Created_Date >= dob && a.Created_Date <= date && a.tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank")
                                    .Include(x => x.tbl_account_info)
                                    .ToList();
            decimal refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => (a.Tran_Ref_ID == 7) && (DbFunctions.TruncateTime(a.Tran_Date) >= dob) && (DbFunctions.TruncateTime(a.Tran_Date) <= date)).ToList().Sum(b => b.Credit));
            decimal booking_refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 13).Sum(b => b.Credit));
            decimal booking_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 14).Sum(b => b.Credit));


            decimal totalIncome = Convert.ToDecimal(currentEntryIncome.Sum(a => a.Payment));
            totalIncome += Convert.ToDecimal(runningEntryIncome.Sum(a => a.Payment));
            totalIncome += Convert.ToDecimal(outstandingPayList.Sum(a => a.Credit));
            totalIncome -= refund;
            totalIncome += booking_payment;
            totalIncome -= booking_refund;

            decimal totalIncomeWithoutVatSc = totalIncome / totalVatScPercent;
            decimal vat = totalIncomeWithoutVatSc * vatPercent;
            decimal sc = totalIncomeWithoutVatSc * scPercent;
            decimal comission = totalIncomeWithoutVatSc * (decimal)0.1;



            ViewBag.Expense = tbl_expense;
            ViewBag.RoomIncome = tbl_room_transactions;
            ViewBag.CurrentEntryIncome = currentEntryIncome;
            ViewBag.RunningEntryIncome = runningEntryIncome;
            ViewBag.OutstandingPayment = outstandingPayList.ToList();
            ViewBag.BanquetIncome = tbl_banquet_transactions;
            ViewBag.OthersIncome = tbl_others_income;
            ViewBag.SalaryExpense = salaryExpense;
            ViewBag.BankExpense = bankExpense;
            ViewBag.BankIncome = bankIncome;
            ViewBag.CashBankExpense = cashBankExpense;
            ViewBag.CashBankIncome = cashBankIncome;
            ViewBag.refund = refund;
            ViewBag.bookingpayment = booking_payment;
            ViewBag.cancellationrefund = booking_refund;

            ViewBag.totalIncomeWithoutVatSc = totalIncomeWithoutVatSc;
            ViewBag.vat = vat;
            ViewBag.sc = sc;
            ViewBag.comission = comission;

            var query = (from pm in db.tbl_supplier_ledger
                        where DbFunctions.TruncateTime(pm.Transaction_Date) >= dob && DbFunctions.TruncateTime(pm.Transaction_Date) <= date
                        && pm.Tran_Ref_ID == 2
                        select new AccountsInventoryExpenseViewModel
                        {
                            Memo_No = pm.tbl_purchase_master.Memo_No ?? "",
                            PurchaseExpense = pm.Credit ?? 0,
                            PurchaseDate = pm.Transaction_Date.Value
                        }).ToList();
            ViewBag.InventoryExpense = query;

            ViewBag.RoomIncomeCount = tbl_room_transactions.Count();
            ViewBag.RunningEntryCount = runningEntryIncome.Count();
            ViewBag.CurrentEntryCount = currentEntryIncome.Count();
            ViewBag.OutstandingPayCount = outstandingPayList.Count();
            ViewBag.BanquetIncomeCount = tbl_banquet_transactions.Count();
            ViewBag.OthersIncomeCount = tbl_others_income.Count();
            ViewBag.ExpenseCount = tbl_expense.Count();
            ViewBag.SalaryExpenseCount = salaryExpense.Count();
            ViewBag.InventoryExpenseCount = query.Count();
            ViewBag.BankExpenseCount = bankExpense.Count();
            ViewBag.BankIncomeCount = bankIncome.Count();
            ViewBag.CashBankExpenseCount = cashBankExpense.Count();
            ViewBag.CashBankIncomeCount = cashBankIncome.Count();

            return PartialView();
        }

        [AllowAnonymous]
        public JsonResult ExportReport(string dob, string date)
        {
            tbl_service roomData = db.tbl_service.Find(1);
            decimal vatPercent = Convert.ToDecimal(roomData.VAT) / 100;
            decimal scPercent = Convert.ToDecimal(roomData.SC) / 100;
            decimal totalVatScPercent = 1 + vatPercent + scPercent;

            DateTime dob1 = DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime date1 = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime todaysDate = DateTime.Now.Date;
            DateTime StartDate = Convert.ToDateTime("2017-07-22");
            string dobStr = dob1.ToString("yyyy-MM-dd");
            string dateStr = date1.ToString("yyyy-MM-dd");            


            var currentEntryIncome = (from tran in db.tbl_room_transactions
                                  join guest in db.tbl_guest_stay_info on tran.Stay_Info_ID equals guest.Stay_Info_ID
                                  where (DbFunctions.TruncateTime(guest.Arrival_Date) == DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                 (DbFunctions.TruncateTime(guest.Arrival_Date) >= dob1) &&
                                 (DbFunctions.TruncateTime(guest.Arrival_Date) <= date1) &&
                                 (DbFunctions.TruncateTime(tran.Tran_Date) >= dob1) && (DbFunctions.TruncateTime(tran.Tran_Date) <= date1) && tran.Tran_Ref_ID == 3
                                  group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                  select new
                                  {
                                      Room_No = g.Key.Room_No,
                                      Credit = (g.Sum(a => a.Credit) ?? 0),
                                      Tran_Date = (g.Key.Tran_Date).Value
                                  }).ToList();

            var runningEntryIncome = (from tran in db.tbl_room_transactions
                                 join guest in db.tbl_guest_stay_info on tran.Stay_Info_ID equals guest.Stay_Info_ID
                                 where (DbFunctions.TruncateTime(guest.Arrival_Date) < DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                (DbFunctions.TruncateTime(tran.Tran_Date) >= dob1) &&
                                (DbFunctions.TruncateTime(tran.Tran_Date) <= date1) && tran.Tran_Ref_ID == 3
                                 group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                 select new
                                 {
                                     Room_No = g.Key.Room_No,
                                     Credit = (g.Sum(a => a.Credit) ?? 0),
                                     Tran_Date = (g.Key.Tran_Date).Value
                                 }).ToList();

            if (dob1.Date < StartDate.Date)
            {
                currentEntryIncome = (from tran in db.tbl_room_transactions
                                          join guest in db.tbl_guest_stay_info on new { tran.Room_ID, tran.Guest_ID } equals new { guest.Room_ID, guest.Guest_ID }
                                          where (DbFunctions.TruncateTime(guest.Arrival_Date) == DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                         (DbFunctions.TruncateTime(guest.Arrival_Date) >= dob1) &&
                                         (DbFunctions.TruncateTime(guest.Arrival_Date) <= date1) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) >= dob1) && (DbFunctions.TruncateTime(tran.Tran_Date) <= date1) && tran.Tran_Ref_ID == 3
                                          group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                          select new
                                          {
                                              Room_No = g.Key.Room_No,
                                              Credit = (g.Sum(a => a.Credit) ?? 0),
                                              Tran_Date = (g.Key.Tran_Date).Value
                                          }).ToList();

                runningEntryIncome = (from tran in db.tbl_room_transactions
                                         join guest in db.tbl_guest_stay_info on new { tran.Room_ID, tran.Guest_ID } equals new { guest.Room_ID, guest.Guest_ID }
                                         where (DbFunctions.TruncateTime(guest.Arrival_Date) < DbFunctions.TruncateTime(tran.Tran_Date)) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) >= dob1) &&
                                         (DbFunctions.TruncateTime(tran.Tran_Date) <= date1) && tran.Tran_Ref_ID == 3
                                         group tran by new { tran.Tran_Date, tran.tbl_room_info.Room_No } into g
                                         select new
                                         {
                                             Room_No = g.Key.Room_No,
                                             Credit = (g.Sum(a => a.Credit) ?? 0),
                                             Tran_Date = (g.Key.Tran_Date).Value
                                         }).ToList();
            }
            var qq = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date,tbl_room_transactions.Tran_Date,  isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date,tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_room_transactions INNER JOIN tbl_guest_stay_info ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_room_transactions.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) >= '" + dobStr + "' AND DATEADD(dd, 0, DATEDIFF(dd, 0, tbl_room_transactions.Tran_Date)) <= '" + dateStr + "' AND tbl_room_transactions.Tran_Ref_ID = 12 ORDER BY tbl_room_transactions.Tran_Date";
            var outstandingPayList = db.Database.SqlQuery<OutStandingViewModel>(qq);

            var dailyExpense = (from exp in db.tbl_expense
                                where exp.Created_Date >= dob1 && exp.Created_Date <= date1
                                select new
                                {
                                    Exp_Head_ID = exp.Exp_Head_ID ?? 0,
                                    Exp_Head_Name = exp.tbl_expense_head.Exp_Head_Name,
                                    Note = exp.Note,
                                    Amount = exp.Amount ?? 0,
                                    Created_Date = (exp.Created_Date).Value
                                }).ToList();


            var bankIncome = (from tran in db.tbl_bank_transaction
                              where tran.Tran_Type == "Withdrawn" && tran.Created_Date >= dob1 && tran.Created_Date <= date1 && tran.tbl_account_info.tbl_bank_info.Bank_Name != "Cash Bank"
                              select new
                              {
                                  Tran_Ref_ID = tran.Tran_Ref_ID ?? 0,
                                  Acc_ID = tran.tbl_account_info.Acc_ID ?? 0,
                                  Credit = tran.Credit ?? 0,
                                  Particulars = tran.Particulars,
                                  Created_Date = (tran.Created_Date).Value,
                                  Acc_No = tran.tbl_account_info.Acc_No,
                                  Bank_Name = tran.tbl_account_info.tbl_bank_info.Bank_Name
                              }).ToList();

            var bankExpense = (from tran in db.tbl_bank_transaction
                               where tran.Tran_Type == "Deposit" && tran.Created_Date >= dob1 && tran.Created_Date <= date1 && tran.tbl_account_info.tbl_bank_info.Bank_Name != "Cash Bank"
                               select new
                               {
                                   Tran_Ref_ID = tran.Tran_Ref_ID ?? 0,
                                   Acc_ID = tran.tbl_account_info.Acc_ID ?? 0,
                                   Debit = tran.Debit ?? 0,
                                   Particulars = tran.Particulars,
                                   Created_Date = (tran.Created_Date).Value,
                                   Acc_No = tran.tbl_account_info.Acc_No,
                                   Bank_Name = tran.tbl_account_info.tbl_bank_info.Bank_Name
                               }).ToList();

            var cashBankIncome = (from tran in db.tbl_bank_transaction
                                  where tran.Tran_Type == "Withdrawn" && tran.Created_Date >= dob1 && tran.Created_Date <= date1 && tran.tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank"
                                  select new
                                  {
                                      Tran_Ref_ID = tran.Tran_Ref_ID ?? 0,
                                      Acc_ID = tran.tbl_account_info.Acc_ID ?? 0,
                                      Credit = tran.Credit ?? 0,
                                      Particulars = tran.Particulars,
                                      Created_Date = (tran.Created_Date).Value,
                                      Acc_No = tran.tbl_account_info.Acc_No,
                                      Bank_Name = tran.tbl_account_info.tbl_bank_info.Bank_Name
                                  }).ToList();

            var cashBankExpense = (from tran in db.tbl_bank_transaction
                                   where tran.Tran_Type == "Deposit" && tran.Created_Date >= dob1 && tran.Created_Date <= date1 && tran.tbl_account_info.tbl_bank_info.Bank_Name == "Cash Bank"
                                   select new
                                   {
                                       Tran_Ref_ID = tran.Tran_Ref_ID ?? 0,
                                       Acc_ID = tran.tbl_account_info.Acc_ID ?? 0,
                                       Debit = tran.Debit ?? 0,
                                       Particulars = tran.Particulars,
                                       Created_Date = (tran.Created_Date).Value,
                                       Acc_No = tran.tbl_account_info.Acc_No,
                                       Bank_Name = tran.tbl_account_info.tbl_bank_info.Bank_Name
                                   }).ToList();


            var banquetTransactions = (from banq in db.tbl_banquet_transactions
                                      where banq.Tran_Ref_Name == "payment" && DbFunctions.TruncateTime(banq.Tran_Date) >= dob1 && DbFunctions.TruncateTime(banq.Tran_Date) <= date1
                                      select new
                                      {
                                          Program_ID = banq.Program_ID ?? 0,
                                          Banquet_Name = banq.tbl_banquet_prog.tbl_banquet_info.Banquet_Name,
                                          Credit = banq.Credit ?? 0,
                                          Tran_Date = (banq.Tran_Date).Value
                                      }).ToList();

            var othersIncome = (from inc in db.tbl_others_income
                                where inc.Created_Date >= dob1 && inc.Created_Date <= date1
                                select new
                                {
                                    Others_Income_Head_ID = inc.Others_Income_Head_ID ?? 0,
                                    Others_Income_Head_Name = inc.tbl_others_income_head.Others_Income_Head_Name,
                                    Note = inc.Note,
                                    Amount = inc.Amount ?? 0,
                                    Created_Date = (inc.Created_Date).Value
                                }).ToList();

            var salaryExpense = (from salary in db.tbl_salary_transactions
                                 where salary.Tran_Ref_Name == "SalaryPayment" && salary.Date >= dob1 && salary.Date <= date1
                                 select new
                                 {
                                     Emp_ID = salary.Emp_ID ?? 0,
                                     Emp_Name = salary.tbl_emp_info.Emp_Name,
                                     Credit = salary.Credit ?? 0,
                                     Date = (salary.Date).Value,
                                 }).ToList();

            var inventoryExpense = (from pm in db.tbl_supplier_ledger
                                    where DbFunctions.TruncateTime(pm.Transaction_Date) >= dob1 && DbFunctions.TruncateTime(pm.Transaction_Date) <= date1
                                    && pm.Tran_Ref_ID == 2
                                    select new AccountsInventoryExpenseViewModel
                                    {
                                        Memo_No = pm.tbl_purchase_master.Memo_No ?? "",
                                        PurchaseExpense = pm.Credit ?? 0,
                                        PurchaseDate = pm.Transaction_Date.Value
                                    }).ToList();

            var tbl_expense2 = (from exp in db.tbl_expense_head
                                select new
                                {
                                    Exp_Head_ID = exp.Exp_Head_ID ?? 0,
                                    Exp_Head_Name = exp.Exp_Head_Name,
                                    Exp_Description = exp.Exp_Description
                                }).ToList();

            decimal refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => (a.Tran_Ref_ID == 7) && (DbFunctions.TruncateTime(a.Tran_Date) >= dob1) && (DbFunctions.TruncateTime(a.Tran_Date) <= date1)).ToList().Sum(b => b.Credit));
            decimal booking_refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob1 && DbFunctions.TruncateTime(a.Tran_Date) <= date1 && a.Tran_Ref_ID == 13).Sum(b => b.Credit));
            decimal booking_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob1 && DbFunctions.TruncateTime(a.Tran_Date) <= date1 && a.Tran_Ref_ID == 14).Sum(b => b.Credit));

            //var fileName = "DailyStatement_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
            decimal totalIncome = currentEntryIncome.Sum(a => a.Credit);
            totalIncome += runningEntryIncome.Sum(a => a.Credit);
            totalIncome += outstandingPayList.Sum(a => a.Credit);
            totalIncome -= refund;
            totalIncome += booking_payment;
            totalIncome -= booking_refund;

            decimal totalIncomeWithoutVatSc = totalIncome / totalVatScPercent;
            decimal vat = totalIncomeWithoutVatSc * vatPercent;
            decimal sc = totalIncomeWithoutVatSc * scPercent;
            decimal comission = totalIncomeWithoutVatSc * (decimal)0.1;


            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "DailyStatement.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("error");
            }

            ReportDataSource DataSetCurrentRoomIncome = new ReportDataSource("DataSetCurrentRoomIncome", currentEntryIncome);
            ReportDataSource DataSetRunningRoomIncome = new ReportDataSource("DataSetRunningRoomIncome", runningEntryIncome);
            ReportDataSource DataSetExpense = new ReportDataSource("DataSetExpense", dailyExpense);
            ReportDataSource DataSetBankIncome = new ReportDataSource("DataSetBankIncome", bankIncome);
            ReportDataSource DataSetBankExpense = new ReportDataSource("DataSetBankExpense", bankExpense);
            ReportDataSource DataSetSalaryExpense = new ReportDataSource("DataSetSalaryExpense", salaryExpense);
            ReportDataSource DataSetOthersIncome = new ReportDataSource("DataSetOthersIncome", othersIncome);
            ReportDataSource DataSetInventoryExpense = new ReportDataSource("DataSetInventoryExpense", inventoryExpense);
            ReportDataSource DataSetOutstandingCharge = new ReportDataSource("DataSetOutstandingCharge", outstandingPayList);
            ReportDataSource DataSetBanquetTransaction = new ReportDataSource("DataSetBanquetTransaction", banquetTransactions);
            ReportDataSource DataSetCashBankIncome = new ReportDataSource("DataSetCashBankIncome", cashBankIncome);
            ReportDataSource DataSetCashBankExpense = new ReportDataSource("DataSetCashBankExpense", cashBankExpense);


            lc.DataSources.Add(DataSetCurrentRoomIncome);
            lc.DataSources.Add(DataSetRunningRoomIncome);
            lc.DataSources.Add(DataSetExpense);
            lc.DataSources.Add(DataSetBankIncome);
            lc.DataSources.Add(DataSetBankExpense);
            lc.DataSources.Add(DataSetSalaryExpense);
            lc.DataSources.Add(DataSetOthersIncome);
            lc.DataSources.Add(DataSetInventoryExpense);
            lc.DataSources.Add(DataSetOutstandingCharge);
            lc.DataSources.Add(DataSetBanquetTransaction);
            lc.DataSources.Add(DataSetCashBankIncome);
            lc.DataSources.Add(DataSetCashBankExpense);


            ReportParameter[] param = new ReportParameter[12];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);            
            param[4] = new ReportParameter("DateRange", dob + " - " + date, false);
            param[5] = new ReportParameter("Refund", refund.ToString(), false);
            param[6] = new ReportParameter("TotalBookingPayment", booking_payment.ToString(), false);
            param[7] = new ReportParameter("TotalCancellationRefund", booking_refund.ToString(), false);
            param[8] = new ReportParameter("TotalVat", vat.ToString(), false);
            param[9] = new ReportParameter("TotalSc", sc.ToString(), false);
            param[10] = new ReportParameter("TotalWithoutVatSc", totalIncomeWithoutVatSc.ToString(), false);
            param[11] = new ReportParameter("StaffComission", comission.ToString(), false);

            lc.SetParameters(param);



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }
    }
}
