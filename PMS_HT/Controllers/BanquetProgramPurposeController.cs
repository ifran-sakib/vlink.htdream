﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetProgramPurposeController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.Prorgram_Purpose.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(Prorgram_Purpose progPurpose)
        {
            if (ModelState.IsValid)
            {
                db.Prorgram_Purpose.Add(progPurpose);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.Prorgram_Purpose.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(Prorgram_Purpose progPurpose, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(progPurpose).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.Prorgram_Purpose.Remove(db.Prorgram_Purpose.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
