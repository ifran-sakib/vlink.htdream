﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class LaundryItemController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_laundry_item = db.tbl_laundry_item.Include(t => t.tbl_laundry_item_category);
            return View(tbl_laundry_item.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Laundry_Item_Cata_ID = new SelectList(db.tbl_laundry_item_category, "Laundry_Item_Cata_ID", "Laundry_Item_Cata_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_laundry_item item)
        {
            if (ModelState.IsValid)
            {
                db.tbl_laundry_item.Add(item);
                db.SaveChanges();
            }
            ViewBag.Laundry_Item_Cata_ID = new SelectList(db.tbl_laundry_item_category, "Laundry_Item_Cata_ID", "Laundry_Item_Cata_Name", item.Laundry_Item_Cata_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            tbl_laundry_item item = new tbl_laundry_item();
            ViewBag.Laundry_Item_Cata_ID = new SelectList(db.tbl_laundry_item_category, "Laundry_Item_Cata_ID", "Laundry_Item_Cata_Name", item.Laundry_Item_Cata_ID);
            return PartialView(db.tbl_laundry_item.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_laundry_item item, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Laundry_Item_Cata_ID = new SelectList(db.tbl_laundry_item_category, "Laundry_Item_Cata_ID", "Laundry_Item_Cata_Name", item.Laundry_Item_Cata_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_laundry_item.Remove(db.tbl_laundry_item.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
