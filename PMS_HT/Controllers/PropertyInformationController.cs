﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager")]
    public class PropertyInformationController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_property_information = db.tbl_property_information.Include(t => t.tbl_country);
            return View(tbl_property_information.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_property_information property)
        {
            if (ModelState.IsValid)
            {
                db.tbl_property_information.Add(property);
                db.SaveChanges();
            }
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", property.Country_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            tbl_property_information property = db.tbl_property_information.FirstOrDefault();
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", property.Country_ID);
            return View(property);
        }

        [HttpPost]
        public ActionResult Edit(tbl_property_information property, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(property).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Country_ID = new SelectList(db.tbl_country, "Country_ID", "Country_Name", property.Country_ID);
            return View();
        }

        public ActionResult Delete(int id)
        {
            db.tbl_property_information.Remove(db.tbl_property_information.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [Authorize(Roles = "Super Admin")]
        public ActionResult Fixer()
        {
            DateTime boundaryDate = Convert.ToDateTime("2018-06-29");
            var exisdata = db.tbl_guest_attendee.Where(a => a.Purpose == "checkout" && a.Changed_Date < boundaryDate).ToList();
            foreach (var item in exisdata)
            {
                var timeOfDay = item.Changed_Date.Value.TimeOfDay;
                var stayInfoId = item.Stay_Info_ID;

                var StayInfo = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == stayInfoId);
                if (StayInfo != null)
                {
                    StayInfo.Check_Out_Time = timeOfDay;
                    db.Entry(StayInfo).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return View();
        }

        [Authorize(Roles = "Super Admin")]
        public async Task<ActionResult> BackupDatabase()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PMS_HT_DB"].ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            string databaseName = builder.InitialCatalog;

            string backupDirectory = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/DB_Backup"));
            var fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".bak";
            string fullFilePath = backupDirectory + "\\" + fileName;

            if (!Directory.Exists(backupDirectory))
            {
                try
                {
                    Directory.CreateDirectory(backupDirectory);
                }
                catch (Exception exception)
                {
                    return Content("Directory Not Available!!");
                }
            }


            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand sqlcmd = new SqlCommand("backup database " + databaseName + " to disk='" + fullFilePath + "'", connection);
                await sqlcmd.ExecuteNonQueryAsync();
                connection.Close();
            }
            catch (Exception exception)
            {
                return Content("Error in backup!!");
            }


            byte[] fileData = System.IO.File.ReadAllBytes(fullFilePath);
            string contentType = MimeMapping.GetMimeMapping(fullFilePath);
            var cd = new ContentDisposition
            {
                FileName = fileName,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            //Delete Created file after 10 minutes
            FileInfo file = new FileInfo(fullFilePath);
            file.Delete();

            return File(fileData, contentType);
        }

        //[Authorize(Roles = "Super Admin")]
        //[HttpGet]
        //public ActionResult DeleteDataBase()
        //{
        //    Random random = new Random();
        //    Basedate basedate = new Basedate()
        //    {
        //        A = random.Next(1, 10),
        //        B = random.Next(1, 10)
        //    };
        //    basedate.Sum = basedate.A + basedate.B;

        //    return View();
        //}

        [Authorize(Roles = "Super Admin")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteDataBase(/*DateTime? basedate*/)
        {
            var dtStr = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString();

            //if (basedate != null)
            //{
                DateTime baseDate = DateTime.ParseExact(dtStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            //if (!basedate.ValidateSum())
            //{
            //    ModelState.AddModelError("Sum", "Wrong");
            //}

            var allAttendeeRows = db.tbl_guest_attendee
                                    .Where(a => a.Changed_Date <= baseDate)
                                    .ToList();
                db.tbl_guest_attendee.RemoveRange(allAttendeeRows);

                var vacantRoomData = db.tbl_vacant_room
                                        .Where(a => a.Date <= baseDate)
                                        .ToList();
                db.tbl_vacant_room.RemoveRange(vacantRoomData);

                var allShiftRows = db.tbl_guest_stay_history
                                        .Where(a => a.Shift_Date <= baseDate)
                                        .ToList();
                db.tbl_guest_stay_history.RemoveRange(allShiftRows);
                db.SaveChanges();

                var allStayToDelete = db.tbl_guest_stay_info
                                        .Where(a => a.Arrival_Date <= baseDate && a.Status != "CheckIn")
                                        .Select(a => a.Stay_Info_ID)
                                        .ToList();
                //finding outstanding staysbegin
                var outQuery = "SELECT tbl_room_info.Room_No, tbl_guest_info.Name, tbl_guest_info.Phone, tbl_guest_info.Address, tbl_room_transactions.Credit, tbl_guest_stay_info.Arrival_Date, isnull(DATEADD(SECOND,DATEDIFF(SECOND,0,tbl_guest_stay_info.Check_Out_Time),tbl_guest_stay_info.Check_Out_Date),tbl_guest_stay_info.Departure_Date) AS Checkout_Date, tbl_room_transactions.Particulars, tbl_guest_stay_info.Stay_Info_ID FROM tbl_guest_stay_info INNER JOIN tbl_room_transactions ON tbl_guest_stay_info.Stay_Info_ID = tbl_room_transactions.Stay_Info_ID INNER JOIN tbl_room_info ON tbl_guest_stay_info.Room_ID = tbl_room_info.Room_ID INNER JOIN tbl_guest_info ON tbl_guest_stay_info.Guest_ID = tbl_guest_info.Guest_ID where tbl_guest_stay_info.Check_Out_Note LIKE '%Outstanding' AND tbl_room_transactions.Tran_Ref_ID = 11 ORDER BY Checkout_Date";
                var outData = db.Database.SqlQuery<OutStandingViewModel>(outQuery);
                List<OutStandingViewModel> tempList = outData.ToList();
                var otStayIds = tempList.Select(a => a.Stay_Info_ID).ToList();

                foreach (var item in otStayIds)
                {
                    if (allStayToDelete.Contains(item))
                    {
                        allStayToDelete.Remove(item);
                    }
                }                
                //finding outstanding stays end


                //need code for deleting data from tbl_messages
                //need code for deleting data from tbl_package

                foreach (var item in allStayToDelete)
                {
                    int row_delete = db.Database.ExecuteSqlCommand("DELETE FROM tbl_room_transactions where Stay_Info_ID = " + item);
                    row_delete = db.Database.ExecuteSqlCommand("DELETE FROM tbl_guest_stay_info where Stay_Info_ID = " + item);
                }

            //}
            return RedirectToAction("Index", "Dashboard");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class Basedate
    {
        public DateTime? BaseDate { get; set; }

        public int A { get; set; }

        public int B { get; set; }

        public int? Sum { get; set; }

        public bool ValidateSum()
        {
            return (A + B) == Sum ? true : false;
        }
    }

    public class CommonRepoprtData
    {
        private PMS_HT_DB db = new PMS_HT_DB();
        public string PropertyName { get; set; }
        public string PropertyPhone { get; set; }
        public string PropertyAddress { get; set; }
        public string FooterCopyright { get; set; }
        public string PropertyLogo { get; set; }
        public string VATRegistrationNumber { get; set; }

        public string DeviceInfo = "<DeviceInfo>" +
                "<OutputFormat>PDF</OutputFormat>" +
                "<PageWidth></PageWidth>" +
                "<PageHeight></PageHeight>" +
                "<MarginTop>0.2in</MarginTop>" +
                "<MarginLeft>0.0in</MarginLeft>" +
                "<MarginRight>0.0in</MarginRight>" +
                "<MarginBottom>0.1in</MarginBottom>" +
                "</DeviceInfo>";
        public string ReportType = "pdf";
        public string MimeType = "";
        public string Encoding = "";
        public string FileNameExtension = "";

        public string BackUpDrive { get; set; }

        public CommonRepoprtData()
        {
            tbl_property_information prop = db.tbl_property_information.FirstOrDefault();
            FooterCopyright = "©Copyright 2017";
            if (DateTime.Now.Year > 2017)
            {
                FooterCopyright += "-";
                FooterCopyright += DateTime.Now.Year.ToString();
            }
            FooterCopyright += " | All Rights Reserved To ";
            //prop = null;
            if (prop != null)
            {
                PropertyName = prop.Property_Name;
                PropertyPhone = prop.Phone;
                PropertyAddress = prop.Address + ", " + prop.City + " - " + prop.Postal_Code + ", " + prop.tbl_country.Country_Name + ".";
                FooterCopyright += prop.Property_Name;
                PropertyLogo = prop.Logo;
                VATRegistrationNumber = prop.Registraion_VAT;
                BackUpDrive = prop.Logo_Location;
            }
            else
            {
                PropertyName = "VLink Hotel";
                PropertyPhone = "+880312868319-20";
                PropertyAddress = "3rd Floor, VIP Tower, 125 Kazir Dowry, Chittagong - 4000, Bangladesh.";
                FooterCopyright += "VLink Hotel";
                PropertyLogo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAACMCAYAAACqNZEDAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAKRNJREFUeNrsfQm4XFWV7tr7nKo7JGQAlEExEZOAoE14oq3StgEUCU6AiODXSsIgKPYDbH32e7YCbWt/PuEh3c4BBEFFpRkcSEAgARlFhWgDmSAhhCSEJHeue6vqnL3eWns459SpU/fWvbl1byWp9eWk6taZ9vDvNe211wZoUYta1KIWtahFLWpRi1rUoha1qEUtalGLWtSiFrUoSaKei/56xdFyml+YpwRM6W/3oN+Xkr63CQToLCN0FAXkQ3qUUCBlAChR34eBEEpBWx2vkHRUXCdEdemEQL4uX3GjknSZqqgNYvXzsojKpp8nZeJHrLN8GRfRez0qd06Ysuo6KLoS4/PmXgSfPnLJe0tC6us8+p8PqS8WHkiR02+jEoR0m6LCYvrlIvCp7XPUFBDINij3TA07nhx4BPoKt829f41qBHD8Oq/DXL86Ajvga9MHUXZKIQgTHuEE8gEhI0TIKQTlAZSpmmEgqEO5gVAQwDzkmqIFQ9bDQbfuiGWhzuAneFjRwcp1hv7RfEJ8nXtHxst95OvQEyHE5cNKUKB7nqgsn6gGjbvOE8lrqEDJ8qJBoTRQiKvhU2MyyBg+wlbL44fy/RaAUpRpUApIPp+vR7oOPTPawlIJ4cXuAvSXH6o5DCaK4zDt+Nc3tHs59VclxRyuT0iIYODkeJiFpsMUNW3gCwhQo8X8Jup4Bdb4CatLa/s2Gr3JigjX6dFJMQIjyWgQVXkx1tF4pjwis1Ex62KI6+EogIgbJeqCMegZ4oK5jrmbASMtK0Qqc5H6I09DpecVT6kN+HtZguMbxW1GBRymvstffw6N+qtEADNMa1U+iQVJAIaVMqvlCqfZdCSG6gASYnaJEasfIKIOYcCaUcv34zBVxGQPYuK5GP1U826s9SNCNtrtCa47qoyHYDWYFIq4jAngOa4kbT35vyCQkEeF3S94q8t94kICzQON1HH80VysAvg53fAhDODDEIq4ZZnnSzNspDKs1cl5hDpGL1a0rQWNqBYHNVmRBQ7akWqRKUZgGSLxYF0VB/aEuBI1HiEy/hZJeZSudEKGohIVIk7rNkanqcAeitT70ZTRgxg8ru0EtXvfTg+DgriN/nyoKZTjJHV/8dATZIA/JXH7aqc2IqmDWsbSSMKwstIS49ZXWANBmOLpGaLGPROxSq8w7BoTnEMmz4sKjlRZeYyu09JA2DImRrfCkRvK3SswzS1FTbmX5prSIgDB6DNMoeWYwt4sbF0daATEnDUgA6V7i3cftf95xG02NBo4/mhvIPb5hCjCj0l5+by2npQpeDEnNer9ADVYlBRV8sYpyViD74sajRo9JoMFqQQTQqzRT7W4TvI9bnRjtpKCGWCJmJ9IcBz7zMiCsg9TGYBPKeoaeEYRJtDQC5QdKE6XActZNNA9CyXm6sT9B7tkkUBzDV3yYtOY41Vc56JD55KBs5Iq1hEwcEghLtGRY+sqMMhQtqYyIbtZtmMtfcGKJoRqACS5Qq17KxSTrBE+rJJTQ8moy94UsSjCSjAipgA5DHnKDDinAJel0IaFsDqNBg9dIErmebk8sjsBPALQ4KDAgZ3yXgLOycRtgqYFDtPWc+f8rzaJX+c6F6mCgQWOM20xNj0TlsfwHZNUZFFlKMkym+skASNUpplcg3tWcozhRFEWB0yqW0LUZ75hDdFn3BdW0aWjbA0L6cQ9+yvoD25fPiOtlcU62cAO8SK114lz71uzaqIcgP5YbwzK8mbfU+fmhZrnM1gCw2MVWhltG0GldRwcefS5kYwpMSFgBDMHEaq8Y5iwjkRGJ+LIQ2i4ezGp59Rgp3VVNzRcRes4hIpQCzkjlpQV31w1Nrn5y2CZOFKOzhXpVAjX0iVrm85zXIs2nzP3tHxZ3apIIJdZ1FigsBkpEkNLWG6CqnafKyeOkgVLN7qobRZrli5qj/B6wFrTQYm1wZoEjgqHf17Cc1ztmlaxpAyl4TjC6jdscbF3m0WT75kHFcmMZeVZDuCfiQu9nURUeSKB4+/KzVS5uyCQj3glfCfJaO3XVZ7UzizPKcKiutFEHewbaxhfVS6SxIUq4flNm+Iio8NRVOnHNblGLZeNSH63hoLWTZyCpzL0tWHAqccWAZDdzwwgpcsktJvDU0a+Mmi0+3kIthBorp5o0OwycGZdt2boxdPn/kAEcJgMYX9mscp1iLLmeYYfph6/SJa7BqG2PoJZnAAzxBxW+S3H4tiu1HVSjkM9DeD8NXUq24jOHBeVg8y2JTenFDE352kdEeDv6Of/gkkgf1cfUOqUP1Hb1YK8gnN4hLjOVRY4ImtEiTp7BmvrG9FIF3XchikvMWQr0sOKw4wyo/PsWi6nnG8lUWnE4Y2AKhVN6zVGn5Fhov0SPizmPCrEdfTTt4jbDE4GcMR4POT5hfP+nrjyz+lpB6IczqQW9RgeNRGgZ5wT4MFaLKiKq2E2u6mb44hhzPm4bKF17mh9RRhlN0xwkOFIT9EARNM0aCvKKo2eLVfGKx8QFwuN5+8i+n/JRJnf485xNBdpF48FBbiKWOg3PXQeWBFxl4p5KoxN9OGUzyxuIssxaLQSTi0o3HRHsjwgUs67hMqdduiJDLdMOpwjrC5f6DCbYAnKch49UU0drFQcXlH5DlHTvFcYcxeONuC/JU/lBOwjY/8OncvD4/TY6ycLNOPGcZhWLTzsYLIqHp+C6rVsEQQQj5xI6cQ4FKDmyJVWphvPaYkuLjoFU8+4q8rnhflKcBrva0bH1HI+WqBEIlQkpwFii6diRl6Y2WzhPMgYo4m5TpuPnhdgJ3MbLzEtkFUu1xbuHew45WfoCWNpxFU7gaadzfUCwFC76ApzcCaB5h6YRPLH60GHL129+ekTD7sSA3GV8oSHKYds1fdaeogFCXe08GAdNeB3A0nWPjek8y67OSru8JztfBVbUkLUb1KjSBwpq89xkDCB7TChK0l7vTLKjel81OWcQ6L7QjKh9xGpaYsk0ETaQqww7wzn4qgw5uIePZCndYgLPUg/PwCTTP54PiyXg5uCIpxJo+btIuVqrysyBpONRjqDgkPCsnj2zb9bdT/sRvT0yYcfXg7hrDbEfRzb4vaQYviqo2Vh0jpLhTW/OUiuzOy3DbbS6SuJ2xQnu45yXOXeNNhZ6hCXMbdIxopI4RoOMxvPhYmymBIJdcQXsA+16JWrTn9jbncCjpD4BenBa4SsYRmK+Ihm9DP8Sb4yUzjM0UrsG5LwHTr1RFPUsREPXX38vNup3h9GTwjpfBuIwwdwJR0u1vvM7vWQlIl+IS4Kp4glb/n1qnIzA2b1Z98jhtZvfo/sD+/J5ZzJbweLnVdKT9YKVRnllySOrAytlYYerKafjiFu098MdZWNeGjgie+RcrzBTAPYwGs7iZf2HDuFkWNp2YGI1uvqCS2qDCdS8GkchPnNzmn6N7w0e3AwvMQFVgmoDrp3Og5i2trESIFHq2CxqOJ5QNLv+ukZX2kW0DQMOLifuJfY6m+FBYtW7oZjcw48Ina2uVha/iEv4E2EqX9Y+fH5spmBI0L8ENXzffmcAUU4TOxpRji1cSKmpkyEtu3hIfp+VzPVtSEd8aZbVikySX8gfHiWJ/6km36oYZJmO4esP4TuaeNJPoVnDvUONS3Xeeojb3orluAz7XZ1BXNQU4E6dQSr42EiTEPoGG7ooT8XNRO3aRhwdL9PFc8O+eLbBa3lgp7dFaNRqoQZbU6BzAO8Gsr41VUXvacpgRMOBZ/3AedIz3BOT8ZRfcMqxyL2Q2EitJa5bWhi/9nR93Kz1bdhwJl36+qwfSZeG+TgqWJiqkEIrBs4HA+vrKVFFhZMLcLJO5596YxHFx3fVCLr4Y8e/UFVwtMJNNIBgXUWNqPrra4Op1BuItPIbfq3nE58vRkHSkM7YN5PVpfyQlzmBdinnHNPGUeeWfohEvHGRol2czahnRUWyoUsmKmFdg//sX3n1sOapQGfPPWIg/yuwf+T80xbikSwmnLcJe2vSSjHyrWFMIpdjh6Q022A3XQpc5vtex1wmNo68V7sgEfLxDJCPoRIR3tGLSqyorZEvGqhnRq0M8BjvTJ+dPVnT2qKBvQH1BnTi3hMW2BDPJPxQcM5QUTldx1r7MVOUxokt9Ef90CTUsOBc9jtqwswU3ySLIwBPbdUj4mB8TIRLa5s4/LscEeIwiuqcwe3vPDeyW68J05581yvT105k4aEFyYWBeLIupxIHTLkFbGgG4hA1Effvjz3vjU791rgaJH1s9UvE7O5mScpvcQ800iNG+qJPuvbsUtulBmVh5SG8POT2XAPfvLt04tFdUkuB37Rl4C+9Y5DvF5LDmOKa6kt4oPrlyvrlQt8+isEms3N7HqYMCUzPwW/Gvji0XCkwKvEhCC6xnWrG9BYKySyhD+AJz58+pGL1/zzxyan5br7ThOl4FxB5l7JZwsKI18VrySNLKdayEkevPbbE7qe6MGj9KAbmt3ZOWHAaZvmv+T5cAMH6Ac6R4SZhvDtkhrllvwKw1lYrzFBTBA5B3V8ijJ+oTZq8PZCeGlp/arZE25FnXH0odP71dn7KGjjDi97IkquwHXiNVKs+A9SPQrEavmawIXVqjjyUNnK8ZxUzjgLC/T/v5C46m4Bx9LBP36GRc+vqXnv9ZRRJPXyVpvzQ2AcnYeiWg9wCqT75DVGUIYjewvlxRPdaO39pfN8VH/bzuUOjJgJpLEEmc241ZiYjh+LFGc7b2d1mhwfClki30MiajlZUs2Om8ZMcg5HGxfOm1UK4FkaeR1lm4WIg5mipSXCel3dZKdbg27nfnQEFZpgJ87F08vzOa9qe++MOUfcN+/fbsFGl//Ppxx1bMeWwfun5DDP4OWgcY4JKuftkhYAy3FsPRLWItrYHZfIidfZt9lALwLeo8SFPzb3d2tehN2AJtyRJqd3bsSc/Aa1V6DnZqS1mlJWatZSXuG8yZ7NxESd0ME39JYvKT6/Zlajy/6nD8zfV+0oXk6AyYcsohg4NpBMhonVmGDmHfJlsySaxTEDXy/lFSaAnUUWB2cpvYYKBugZ1xH32S1AMynAee0tTyF6uIRe/BzL+lCYgG5tbsuMFZGJ78yVWLQpu/iOHWXt9PuUsjoJCuUPNrrs5WL47o4Q36WmCOhvE9DTLqFvKvV6u9BrvTV3sbPaDCQGDB8smn1rZUVTL27pixkty+jjN7uDiJo04Gjz/NerN4scXEKKQOCXjZxnn41KZGESiRQmWmpZx6EOAGe9yHIe/qSO8XAwvGjVOX/3loZxm9OOOVAUSv8kp0CbzMVWns86TknnDjKhEXQi4JRqCQcgX1qicg7lRRTELxCdw7BIFsCX5tzXfPNRTQcczcr3nX43Ne49oYodfFHQuaitkUUWrGX3smwUTRGEcwZf6WuYb6fUNfjZmQKP2aekyJpTMIUK0MnZOUoIHSSOOgnRvor9T+wpD6zFFVr0u4V1nDaynIsWL36D9JrVsJuRmMyXrzr+sLd4ZbwJ8/BGNsFd3kCXLDFdOo1yVenoySmTsJJ1ikCJsDzrwA8decMD4xq78sez3np498s9fzigTewT8Lptkq9tOVs81mNMsBkMEUgG6fcB35R1CnEhnopoV3HQe67MYgu1w5CU47/QifeSJbVtdwOOP5kv75jiPxX2lX8WKPgygSUXrfa0Kxmz/GbChdDZ887q0sO3jF6wbce/bFx87J9f96OHt45HGR87/Zip5S0DV7T7Yp8uAkUPWU/9ngkHlc4nwxyGlBdOOdudp6PdeP9yXCliqUdvD+DAQaWnFNosZxoSokz3XUHlfgV2Q5pU4Mz69dPh+hPm/YxM64XUlu/wUo7VzJXCwoLHhsoFnvEJ6Tkt0iFkoXzMziH5EbryO+NRxrCk3pUrqpP7pwBspGPTtDbY2pmDAgEl8KSeuB2iMvQTC+rNedDFn3kPSjmpnZxTiiGc/5ftcNr6AuQJJTxRW5JaX3to7r1rbmtk+x503WlioFzY1/BFMt5CDHovuntc4rZFM6B3w/vmHdcXiN+R0unxak29aJ+XiYjKWXSRsWZKJ6jk1Ll2BUWpSB05RZTyc2e98chr7n5+V8r10Blvn17ctvOZGSAOvv2wqXDt/P1IwfUqOWDGfS7K0aV6OXz7AHzv/m2wbyHUOaELefkcVXLRofevaViSx2nff/8CKsBXhIfHCRmnoLEJMjmacIi+D1KzDbT5U/96/sEnnXn63Heqo17/xuZWjivM3HbxIFlHt/OKRW1uS5NlWlidRw6TwFG78O2JkE1gn3SOAuZLm3ZcsvpzZ0wba5n+cP4H/MLW3sX7DImDp1OLH//yAMzuLVVm/4LshX6m3PGJNft2woapSgO7wIvqJNxERX24gaBhu28JIeQ44ZmZV6wE9VQ69qcCHUKfc0ne/uqrJ55bN2iaBjhz71wdtiF8URZhg7SpNV2u4jinb22m6RayCWkWcbE4yPcNnl/e/NIpYy3TwMvrF3T44f+emUeYRpbUa3oQTnihr0Juijp5Ng+EH/7NAdDTKTnm5ldUXk4WgA0CDffpWXTMkX5sSKDCisEXhXNIeKZQGLh7tzHHqxo3BxtJ9l9Dclj5iUq6DOOi1hpekcjHJ83KAhYmbcVyOxQGz153ycdmj7YsKz75tulDg8EiAuurPdICWY9hTvjmriJMGwoqU7fgMAkxIZ6jWrdfG27u8FkRvpL+3tqodiSRTfXFs83yEqhemgNxrLNpN3kjlNSO3RY48+5aHYj94Xt+CM/4QSo1PdaO34nCMJM+IsUmMEL7UOkdpW3bRz0JOthTOJUYzUkd0jzL7FmBMK+rDG/YWaxYOxhtypAqUzJ5NdMrHTn4zlH7PcaqUyNT5RMtoBf/vZDp3ESx3hVN7YS4vbNj5nW9F9+jdlvgaPD812qOa/8y9dEWp0qUbSn1dIN258eHSzCgGyLUq/316keOhSpyj5ZLHV7f9n9af8XiuuexnjnzTVO9QunyToD92mjElujdRd/Mrebpv4+v2QYSq0NqIL3lAFZxRbHiNVNOOuasN0xtmG7zg5MPINX3PCqAL7zYoJCJ6ZwI9B4EUsqbNp1185hCOJpugZtoh7tI4b1TpPY70GuO0qsibVZOTFgyXKOhvIRy3sT0qMLQlMEXnrlqxzfObR8RNJ99nwi3BF/af1DM2pcnUQOjn3Aq3pA+2dl4VBfC7O5ClNyrYnUqxn8nxZTLCcSPoI8zGtd6+A/031FkSWUvt+ZyeIYVqRKuV+XwlrG+qemA8/qla0rSh2/TiHhSi2lluA1PhKrEetooR04CTC4cQyeStiNNc6Btfe/v2bhpREW5sOmFt5GOcME+OdCBVbwMmefClM1Vw8/uJNP/xBf7oaNk4kAUVLoMMKV8QoozURU+MX3Jwtnj3W4zfriwnbN00fM7PS9bYzcLIlErymRt/Xq/tpl/2GOAYxRlsYY6/WqWOW41J3hxunrXMzIpCkRsbkq0m5GA3o0KcChsH+zq//KWy855Va13/nnx33cU+vHykgczVd6mbdbrdOymG47j0d9v2VaAWX3FzOymxvNdCahkIkvqvLfTx9vGs70IiFyMRfTfbDFsvkpTCAxhK5bwN88t/hnsUcCZd/fqsp/DW6nPn8x51reTWO2gd0hIupedlWB/4tlptJOMmDcL94NXdhzR37XtzOeuujhf5bP5zKliYEfXcaTDnNRuEveBW/fO3zlEQoexcohnEeCgPgVveXkAMjBTtS+FqM70zzvtnWo7e3zEO8L+9HGhfq1XGziJWOjfErKf3JV3Nu0i/tfetXawYwjP7xhSOwOMN9LQKVOk0W2ChCnugOW5jdd4QxI0m5L40vw9+PIrX8IXN76zyorasOoIHCh9bZpnRJveAYeflTO6FevdeWWSVPNM9zQCz7EvDcFB3cUkJ4mtO1HFZSCFsnfRsXAcm+s0Ot7Mzj5p9nOMIgiSBxL/xkCbVT/s/vTS7j0SOLqd2/GvyhM/b1cKvMDsvSgxDr2Idt+rUEhNuhRIJuZWZlZadPcdEHR3nf/yNV+o5DqB+kQbwlEmpFOY1Cw2KZJL4siL5RSYTGGcj+8IMs0XbOqvxQGyEyrFvx1MxwfGSUx10ri6TI8nL26b7IScIS8MeMz3vT/t6nubGjgH3L+uXOiQX/DLsJanI3I2/NIsERZR3jyJ8aGiZJCVaWI9j2NnQlC9XR/oe37lZ9zvj77v8A9MLcMnptll7cok59EjNLDTGcquUmDlm8HbQcDZr6jg1A29USy0qGM7xsRGM2wQfoIU2o5daZ+Z1y7k4iyi46B0TzqHadK6kogFkfMu2372b8I9GjhMs+5ZM0AN/R9kCu/UwFGVe32ICDQY+VdUlAzS5h1GoyRr3ba/OK3wyvYLXrrkvfs9+8HDcn4ZL80jHKjDPQGjQPIIeFYJd6sW7IaA+pmvKZCmu7mvah+RpIkuoDLVv4jLzv6ci4hjjJ0jA8yhj08IGzFQISqdPgOJfUoRVgbF4E/j0S9NDxzjrMKbaGQ/mEMDHDd642U0GM1lJbLjRx5caUdgnjNIFAlOPYOHF3dsvzgcgI90CjjOt6qBSDnxhHSZ000csbL+JGdJsTf5g+t7oS1QFVMQkNBzkr8JrHIWnsnSZheAcyLV928r9u2UkLkY0A62G3ovXLZzrwHOQb9f1yMDuFKUoS+nPcNC+3e0jwYxyicr7R7Uvl1qLFzePTt1oGN3ePacxMzg1sIXZQ5u5I39ONeg76HWjUQqcQDaDQxdXwdgM2kIE3141I4SzOkaqhZJCJnpeSvWikmYR9ceOUYxNZuqdbbnwpPAxGPr8FS7FQ9a21zn6gFc1w4zloxXn+wWwNHTKh78kVrnLs11hFlbU5GcKDmyoMaGvJU9yApyfrRDvAIcdEyjTjrhxa5Kz8BwYSDSHkaB3Ye+njcmbiPgbHrNkQA1spxFG4zqYpeVkLdsPfdnuNcB58BH1hWVLy6ifnpJJkVR5kZlospjW2EeA4y4O7BIeX89m8tQr9oUJgidvcltxO0+9kIRpg8FNbdJcjP36JvQD+nHa8uJPjnt2oWHjspLfN3CafTQCwk8bVgjs4FLBaf1tRBXhkN43Xj2x24DHG3D/n7tjrIQ36cO7MvbiTuo3hAv3jgtsWw42nVGjBRHg1mQq7BU3D5S/Bevm5pRULBg80CUFAqgctM2meaKISZBzNtqnkKiJ18naDzt7BOk0CfKlOkkRiuxwFs2xZu6ca8Fjuk0sYQU4cf9ECsmQCvSwGYAQ4jK5NujqXhqSsw4G2NdFPIlhAUbB9R+Q2HUi3qey214kgXSyvmk0+k4pF4vBR3nuEqNlIwTETdjqH679cJfqr0aOLMfWPNyvqiubC9hl46KRDMBWgLja3E5eLTybKcN2OcTWt+O2y/UpbzXiyOEsLvu2hw3dscWESQYjs1yrn1Inskoyu9l0Ph0HL9T3Uga9q+0ePDcbn3VHcsbmrm9SUW0zzi8g64/oc4meD8dh+npg+E2/LRxrdIT15MB8eR498NuBxwm0i9+ryQs0XucQxycBMPlFXYz54kNNmTywDgVW4V5G/mFbI4+mz5Xz9p7ZhOSwX3J1tt/33/b3i7+L92zzckndLvbphvdE1WcjAB+ComhjhHE1Ey652MVVv8I2epJv/l57wVLiy3gEL3uwbUFsqy+SVxnEy/qjxyAI4ibjHnR7JEKlcHdaM1whWbKgf03nYGCfKj3xywXD9zvc6+/+QFeUfEEXXJNBBWrBFeKzETSJTS+IktvoyqcNkLVP0VXv0OafR3iLR2zOpanHwLxk57zlj7diD7YLYGjfTsr1m2XIV5B7VPwo1GLw8YAZ8XK1ARYYisil4Ui2n/Lbi7PQfHYkXukjEIHRHWfu7RMF/H3gbSTEiDDZVAZAjGTOuMjw3Ab5kYfp6NT3xsmgtsySJWwhyq5pFHtv9sCB4xucis13hPSmslRkmkc3sYWNiePW8aSPNKQ0o4+MDPtcXYJk2WLLi2KV834ymE3PRQt4aUR/jyd+Wnyfkwr6bacmvtEm7hqLL6XTPNaMUMfpIv+BmyOHQe+WqKK3rGSlOI/t4CT5dt5dF23349n+RK3degsV2YiUrq4ZAsGwzVMijVpYwxqLprAeJJJz4bzXlFWvOhlN2abX94BcKCrre3rb/jRQw9mPOZrdKx3gNUL4lRidQHEn0YUChezM5U46JV28jLmNtcufB2d/HcnOoVVzKJETZ4N5rcL70hs89ad/7P308v6WsCpQa96Yt0Wv4jXEgcZ4D2OvLB6mjraO2uUHmJ3D+syfsQtDBilFCu8fLZTjbC2iS75ZVI2CRs9yOlQsNYOIOb3d9P9s1NXfDQy14cRfVYR5z8fJoD9tZHtvtsDR7tEivg9heJP0f4JkMrsNYZYu6QYcNabi/shnaZbeN61b7zj6Zey7u09byl7AL5JX/ujjnUTprXW+bgddBAOoW+RaU7chydBL0WbX7LmltoqQm0fhPCLnnPuUi3gjED7PvHcplIgrvJK1GjJRWiJWenRcpuKWW47x6EziJEMKE71vlGcLpcO9xhSlDmV/vVgE46x/0bmTJBYtjYbvZZ1ncunL1nYQQevTVxEt75Gd1auBuiUjRDI6Qf8QQ2GNze6zfcI4GiuUMblIoQbQpvFNL3OSYwBO/HDzQQ8N1ZJiU2qo+37837538U6HsQd+HwFOFS22NSqky8cyA+k43g65tDfi9wFGNbyDtvYI52tTNzWe/E9vS3g1EmvfWxtnyzBv7eVcBvXSvpxdk8Xc1uRMMClloV4MxKMRn3s8eXQCS8wDsMSih7lwaJ5t66sK163+7ylvH/mHRHXo0Iokb2BvdurCuN9PM+hey6moswXDlSihhXlvJghPEh61LKJaO89BjhMQYfYQrrO12jkDVVMZKqMXDtYraRWdEpCUeIFeQyqMsKywIdHR6UrIdxJQNjouIYIsbb/KOEQJNCwnnMWYGIuroY1KOzoUIi30DWbWsAZJc1avoYV2Z9QJ6902zKmfTggYMRgHSHi7Z6jyMAQt1BjXTvvvjWFUTbwSvq4DVOe6czux3gfT7p+Oh9mwzfBUwc1nTaaWykYUGW8vfuCZeUWcMZABz+ydgc18QXUkIMud05ym+aqqe6MoV+xgx2Y0AnSbf51wJPLR1uervOW9tNjvoe8dTgOlxcz3lo7+X4GjQ7DGG7P8gCRFPcrey9ctnWi2nmPAw7TG5av4VH+S+0PlHYVZmoqQspE9tKMCSwBsV0fBOK/w7y46c3LV41pdQDdtJY+bo1WWWbIG7a6nMmuA7444MhZUVIMv91OCC9TT/5iItt4jwSObstAXOEpeFxgHCNckWzbJn2UkQfWpI6LdoDhbKElZB/RK56H5xy+fPXAWMvSe95SfjNnk9/upkRY1xEQ6y+QWKWAdr08quRiujg4LNqa2nYg+nAnFX91CzjjQJiD9RjAjaoEBWFWxsahF4kjWgnhUuVLmy7fcCMse+K6shJ/GYcicdr0+ytMfrdfNI6yxzwR7xoMOECg+w92Ok5k+wrYg2nTsXN5odp3QylOAbsSUy+IiOJszCjXIRnWDOaUsjypyWu4yiie7++Q/+Pwe1b3jEd5pi9Z+Fb6YKtMMzadn8/KSayzN2ILy7FWXEZm/8KJblu5JwPntQ+v3dJWxi93KDWUs2vKvSh3n7DuD4wchkLYeBuO6ivCK/kQvzReoGHqOX/pE8TYlgq02A3s1gOy/p5w1qK9fyuW8QeT0bZ7NHB0Q4fwdEisnDOSRtF+6TW7VufRP7WBzkNcluK2wBO3N6BI7E1+BWwqFBg+L0k1x0GMY6sVPkH33t0CTgPo1U+sw3xJ/T/iNE/pzTlsrLDWY2w6XB0+4ZuAraAkOPbmDz7gtw7+/dpiA4p0h9N1tHQMk+m76hgIYLOrIhalL37c85llgy3gNIjInN5GAPm6L7EINteOZ7iRnlH07aoHTp/iScJOXn6z5It1jShL96d0/C9nVO+L/JBB/Qqy8yAT2u9XRXx8stp0rwDOAX9cx8nXb6Wvj2ixBIa7SLvIzrd7S3FjkC60vOjBncRtgkaVh/r9ITqWxz6c+jmOm5sg/Nwyta3zpclqUwF7Ea1/97x3CB9vCEDO4x1cNHgURlMLhJ+NpLWecMj9a9c1uizTf7jwROr/u6VLle/XN4VvFfktwUB4aN8/3j3U4jgTIbIAHpch/JTBEmK0j4hZPAc6Ou9KEmvPT0RZCDQr6ONplVi3VSfDYQb1jckEzV4HnLkPrFEygOvziHezC6Uk7J6anIizLO4NcuLbr3+wocmrI+q9YGmJPv4ZUA45M7sieXVaHLhESQqfI5DdOtltuVcBx+o3m0ksXS09LLElpacaFDyXA/zmoQ3aX2EYeowQsVJYuGDo1mHFeYrdbHlEnlihAtzaAs4E0+seWhuqMtzrK1jGC9p0eh2EX3g+3jvRZSGus51gcT0mth9wOwRXZkWySXAC7A9LeEfvhcvCFnAmgQ4h8ASD8DliNhv9HCzFnPj+zBXPqUkqDi+aW4WJbF16iXBgEllHM+ksT0P8hUR8pBnacK8EDlMpFOuVD/+JAr8WKtg0WeXovfC3jIybiacMafAEULFwUMfjWB2IdJtb8p3tXc3QfgJaNOk07fvv5+B0Xof1d7pTfNQx0y5fsckeAI9jX3hsz6X3hC2O0yJHvOf4nWAm77VHGwMbRG9ww5bX1c0CmhZwmoSsuOLVCasMUISOCBQ25y39e45E1fJmKnMLOM1Dz9Dx3SpdglcvKPjPnk8t3dYCTouyuA6rvxw3XDDiyoZbBLCZ/v9xs5W3BZzmAg8vG74cXA6BMpYgBzf0nL90sAWcFo1EHK/zApneW8JB8XSpV/2yGQvpt/qp6YgnWX8UDsIz5X7vVdDRsbLVJC1qUYta1KIWtahFLWpRE1NrknMSaeb1J8+gj/n8veucu1bsscChiiYj5GZSZbtT53lTeLeI7Q46f2o9z6LrxDDX8RzNAvvnca6BU2XhchxN5zbUeMcKOnec/Y2ftTz9uz23iE3h4Z45XJlGcf5y+jibjtmpRz9Fx6XJ61PPGo70e1LtkqYr6JrLR9P+tWi0DsA7Et9PyTj/4cT3OydwAPDIvWwXRz+P/KsTP12aBZpd5TB0PGnLOjvjEi7DcgvgpqbROgDvTADm3XTckDp/Sg2QTQQtoga/cSws34qM2y0Amb5Fz7mhAWW8zIkmS/yOB+x7z06c+xGV6SkqA3OgG+01TLMAwIFqgz0Hib8h9feNqd9WTBZw7kiwcgbJ4pSYmpEQU92TMBCYYxw9hvtuT3AAFl+XNgrcWWLDgdVyIweei7l9kwC2YjYCTur+NI10fpdoVKLKgsFxkhkWLJMtpp5KjLb5o2XzVt9YkBilpzZIEV6QGFjdNTr1isT3BdDENJa5qqS4+nACSJMlprptgzul/GrqpHo53vxUB506Bk7JOgnU+a4k2GsNAkezd7FdFmQoyseNl/U2ltnxKgXZKpaTJqbofXck5DeX45JRKNUwjp01mndl1WMD7Cbkj6GTunlEW9DMsKA5O0tMZZmGYzH96iTWS9wWgpfRu8ei3DK3WjFK4PN7Xkj9lmVqj/hMq6SPF2UpxxsmDTgZ4uqUFLu/YzJGAFsgFixOx/mRbajZIzTu0RZws+1xmQVhvXRjhp/m3TV8NFliC2roNbvKtZtHOU500g2Jin040RBpMXVFxtFIujRRrgV1iIcNtryLE79dYrloI5R4SBgWWeL04skegI3mOK5ii1Kj584UwOpCvLU4qhp6tLqSFaPXQOwMnFHnfSsS4tdxq6PHs6Ft2ZIckcXiLNtmMyxoku1w467qVBntuqGGJ3zU7b8roaN3jqA4j8oyyTjmj7GDLh+jLF+c4FbzrZneCI6Y5DyX2LrengLNt8bB+pmf0aaLxqv95S6MoDtScniynH61OmjUHCElSi8m8Mweb67DJjFUe9yTOtfiBjogx41as+OTSCkR0W2nGFrUoha1qEUtalGLWtSiFrWoRS1qUYta1KIWtahFLWpRi/YW+v8CDAAkV88HCMpAQgAAAABJRU5ErkJggg==";
                VATRegistrationNumber = "";
                BackUpDrive = "D";
            }

            FooterCopyright += " | Powered By V-Link Network";
        }
    }
}
