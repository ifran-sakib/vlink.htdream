using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using System.Web.Security;
using System.Web.Helpers;
using System.Globalization;
using PMS_HT.CustomClass;

namespace PMS_HT.Controllers
{

    public class HrEmployeeController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();
        // GET: HrEmployee
        [SessionExpire]
        [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            this.SubscriptionCheck();
            if (User.IsInRole("Super Admin"))
            {
                var emp = db.tbl_emp_info.Include(t => t.tbl_department).Include(t => t.tbl_designation);
                return View(emp.ToList());
            }
            else
            {
                var emp = db.tbl_emp_info.Where(a => a.tbl_department.Department_Name != "Super Admin");
                return View(emp.ToList());
            }
        }

        [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
        public ActionResult Add()
        {
            ViewBagDepartmentListGenerator();
            ViewBag.Designation_ID = new SelectList(db.tbl_designation, "Designation_ID", "Designation_Name");
            return PartialView();
        }

        [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
        [HttpPost]
        public ActionResult Add(tbl_emp_info emp, bool? Is_Authenticated)
        {
            var userAllowedDepartmentIdList = CurrentUserAllowedDepartmentIdList();
            if (ModelState.IsValid && userAllowedDepartmentIdList.Contains(emp.Department_ID.Value))
            {
                if (emp.File != null)
                {
                    emp.Image = "";
                    if (emp.File.ContentLength > 0)
                    {
                        var filename = emp.Emp_Name + "_" + Path.GetFileName(emp.File.FileName);
                        var path = Path.Combine(Server.MapPath("~/images/employee"), filename);
                        emp.File.SaveAs(path);
                        emp.Image = filename;
                    }
                }
                if (Is_Authenticated != null && Is_Authenticated == true)
                {
                    emp.IsAuthenticated = true;
                    emp.password = Encode(emp.password);
                }
                else
                {
                    emp.IsAuthenticated = false;
                    emp.user_name = null;
                    emp.password = null;
                }
                if (emp.Department_ID == 2 || emp.Department_ID == 4 || emp.Department_ID == 7 || emp.Department_ID == 10)
                {
                    decimal dlevel = Convert.ToDecimal(emp.Discount_Lavel);
                    emp.Discount_Lavel = dlevel;
                }
                else
                {
                    emp.Discount_Lavel = null;
                }
                db.tbl_emp_info.Add(emp);
                db.SaveChanges();
            }
            ViewBagDepartmentListGenerator();
            ViewBag.Designation_ID = new SelectList(db.tbl_designation, "Designation_ID", "Designation_Name", emp.Designation_ID);
            return RedirectToAction("Index");
        }

        [SessionExpire]
        [Authorize]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        [HttpGet]
        public ActionResult Edit(int id, string caller)
        {
            int user_id = 0;
            tbl_emp_info emp = db.tbl_emp_info.Where(a => a.Emp_ID == id).FirstOrDefault();
            if (emp == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (User.IsInRole("Super Admin") ||
                (User.IsInRole("Admin") && emp.tbl_department.Department_Name != "Super Admin") ||
                (User.IsInRole("Manager") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                (User.IsInRole("HR") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                user_id == id)
            {
                user_id = id;
            }
            else
            {
                user_id = db.tbl_emp_info.Where(a => a.user_name == User.Identity.Name).Select(b => b.Emp_ID).FirstOrDefault();
            }
            tbl_emp_info tbl_emp_info = db.tbl_emp_info.Find(user_id);
            if (tbl_emp_info == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBagDepartmentListGenerator(tbl_emp_info.Department_ID);
            ViewBag.Designation_ID = new SelectList(db.tbl_designation, "Designation_ID", "Designation_Name", tbl_emp_info.Designation_ID);
            ViewBag.IsAuthenticated = tbl_emp_info.IsAuthenticated.HasValue ? tbl_emp_info.IsAuthenticated : false;
            ViewBag.Caller = caller;
            return PartialView(tbl_emp_info);
        }

        [SessionExpire]
        [Authorize]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        [HttpPost]
        public ActionResult Edit(tbl_emp_info emp, int id, bool? Is_Authenticated, string caller)
        {
            var userAllowedDepartmentIdList = CurrentUserAllowedDepartmentIdList();
            var userName = db.tbl_emp_info.Where(a => a.Emp_ID == id).Select(b => b.user_name).FirstOrDefault();
            var password = db.tbl_emp_info.Where(a => a.Emp_ID == id).Select(b => b.password).FirstOrDefault();
            var discount = db.tbl_emp_info.Where(a => a.Emp_ID == id).Select(b => b.Discount_Lavel).FirstOrDefault();
            var isAuthorize = db.tbl_emp_info.Where(a => a.Emp_ID == id).Select(b => b.IsAuthenticated).FirstOrDefault();

            if (ModelState.IsValid && userAllowedDepartmentIdList.Contains(Convert.ToInt32(emp.Department_ID)))
            {
                if (emp.File != null)
                {
                    if (emp.File.ContentLength > 0)
                    {
                        var filename = emp.Emp_Name + "_" + Path.GetFileName(emp.File.FileName);
                        var path = Path.Combine(Server.MapPath("~/images/employee"), filename);
                        emp.File.SaveAs(path);
                        emp.Image = filename;
                    }
                }
                else
                {
                    emp.Image = db.tbl_emp_info.Where(d => d.Emp_ID == id).Select(a => a.Image).FirstOrDefault();
                }

                if (User.IsInRole("Manager") || User.IsInRole("Admin") || User.IsInRole("Super Admin") ||
                    User.IsInRole("HR"))
                {
                    if (Is_Authenticated != null && Is_Authenticated == true)
                    {
                        emp.IsAuthenticated = true;
                    }
                    else
                    {
                        emp.IsAuthenticated = false;
                    }
                }
                else
                {
                    emp.IsAuthenticated = isAuthorize;
                }

                if (User.IsInRole("Manager") || User.IsInRole("Admin") || User.IsInRole("Super Admin") || User.IsInRole("HR"))
                {
                    if (emp.Department_ID == 2 || emp.Department_ID == 4 || emp.Department_ID == 7 || emp.Department_ID == 10)
                    {
                        decimal dlevel = Convert.ToDecimal(emp.Discount_Lavel);
                        emp.Discount_Lavel = dlevel;
                    }
                    else
                    {
                        emp.Discount_Lavel = null;
                    }
                }
                else
                {
                    emp.Discount_Lavel = discount;
                }


                if (string.IsNullOrEmpty(emp.user_name))
                {
                    emp.user_name = userName;
                }
                emp.password = password;

                db.Entry(emp).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBagDepartmentListGenerator(emp.Department_ID);
            ViewBag.Designation_ID = new SelectList(db.tbl_designation, "Designation_ID", "Designation_Name", emp.Designation_ID);
            if (caller == "DT")
            {
                return RedirectToAction("Details", "HrEmployee", new { id = id });
            }
            return RedirectToAction("Index", "HrEmployee");
        }

        [NonAction]
        private void ViewBagDepartmentListGenerator(int? id)
        {
            if (User.IsInRole("Super Admin"))
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department, "Department_ID", "Department_Name", id);
            }
            else if (User.IsInRole("Admin"))
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department.Where(a => a.Department_Name != "Super Admin"), "Department_ID", "Department_Name", id);
            }
            else
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department.Where(a => a.Department_Name != "Super Admin" && a.Department_Name != "Admin"), "Department_ID", "Department_Name", id);
            }
        }

        [NonAction]
        private void ViewBagDepartmentListGenerator()
        {
            if (User.IsInRole("Super Admin"))
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department, "Department_ID", "Department_Name");
            }
            else if (User.IsInRole("Admin"))
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department.Where(a => a.Department_Name != "Super Admin"), "Department_ID", "Department_Name");
            }
            else
            {
                ViewBag.Department_ID = new SelectList(db.tbl_department.Where(a => a.Department_Name != "Super Admin" && a.Department_Name != "Admin"), "Department_ID", "Department_Name");
            }
        }

        [SessionExpire]
        [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Delete(int id)
        {
            db.tbl_emp_info.Remove(db.tbl_emp_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [SessionExpire]
        [Authorize]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Details(int id)
        {
            this.SubscriptionCheck();
            int user_id = 0;
            tbl_emp_info emp = db.tbl_emp_info.Find(id);
            if (emp == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (User.IsInRole("Super Admin") ||
                (User.IsInRole("Admin") && emp.tbl_department.Department_Name != "Super Admin") ||
                (User.IsInRole("Manager") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                (User.IsInRole("HR") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                user_id == id)
            {
                user_id = id;
            }
            else
            {
                user_id = db.tbl_emp_info.Where(a => a.user_name == User.Identity.Name).Select(b => b.Emp_ID).FirstOrDefault();
            }

            ViewBag.Emp_ID = user_id;
            ViewBag.Image = db.tbl_emp_info.Where(x => x.Emp_ID == user_id).Select(x => x.Image).FirstOrDefault();
            tbl_emp_info tbl_emp_info = db.tbl_emp_info.Find(user_id);
            if (tbl_emp_info == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id == user_id)
            {
                return View(emp);
            }
            else
            {
                return RedirectToAction("Details", "HrEmployee", new { id = user_id });
            }
        }

        public JsonResult ChangePassword(int id, string new_password)
        {
            tbl_emp_info emp = db.tbl_emp_info.Where(a => a.Emp_ID == id).FirstOrDefault();
            int user_id = db.tbl_emp_info.Where(a => a.user_name == User.Identity.Name).Select(b => b.Emp_ID).FirstOrDefault();
            if (emp == null)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (User.IsInRole("Super Admin") ||
                (User.IsInRole("Admin") && emp.tbl_department.Department_Name != "Super Admin") ||
                (User.IsInRole("Manager") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                (User.IsInRole("HR") && emp.tbl_department.Department_Name != "Super Admin" && emp.tbl_department.Department_Name != "Admin") ||
                user_id == id)
            {
                emp.password = Encode(new_password);
                db.Entry(emp).State = EntityState.Modified;
                db.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private List<int> CurrentUserAllowedDepartmentIdList()
        {
            if (User.IsInRole("Super Admin"))
            {
                return db.tbl_department.Select(a => a.Department_ID).ToList();
            }
            else if (User.IsInRole("Admin"))
            {
                return db.tbl_department.Where(a => a.Department_Name != "Super Admin").Select(a => a.Department_ID).ToList();
            }
            else if (User.IsInRole("Manager") || User.IsInRole("HR"))
            {
                return db.tbl_department.Where(a => a.Department_Name != "Super Admin" && a.Department_Name != "Admin").Select(a => a.Department_ID).ToList();
            }
            else
            {
                return db.tbl_emp_info.Where(a => a.user_name == User.Identity.Name).Select(b => b.Department_ID.Value).ToList();
            }
        }


        [AllowAnonymous]
        public ActionResult Login()
        {
            this.SubscriptionCheck();
            var id = Session["Emp_ID"];
            var name = Session["UserName"];
            tbl_property_information prop = db.tbl_property_information.FirstOrDefault();
            //prop = null;
            if (prop != null)
            {
                ViewBag.HotelName = prop.Property_Name;
                ViewBag.Logo = "../images/Property/PropertyLogo.png";
            }
            else
            {
                ViewBag.HotelName = "VLink Hotel";
                ViewBag.Logo = "../images/Property/VLinkHotelLogo.png";
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(tbl_emp_info u)
        {
            DateTime SubEnd = this.SubscriptionCheck();
            tbl_property_information prop = db.tbl_property_information.FirstOrDefault();
            //prop = null;
            if (prop != null)
            {
                ViewBag.HotelName = prop.Property_Name;
                ViewBag.Logo = "../images/Property/PropertyLogo.png";
            }
            else
            {
                ViewBag.HotelName = "VLink Hotel";
                ViewBag.Logo = "../images/Property/VLinkHotelLogo.png";
            }
            if (DateTime.Now > SubEnd)
            {
                ViewBag.SubscriptionEnd = "Subscription ended. Contact VLink Network.";
                return View();
            }
            if (u.user_name == null || u.password == null)
            {
                ModelState.AddModelError("", "Username and password cannot be empty.");
            }
            else
            {
                var password = Encode(u.password);
                tbl_emp_info user = db.tbl_emp_info.FirstOrDefault(a => a.user_name == u.user_name && a.password == password);
                if (user != null)
                {
                    if (user.IsAuthenticated.HasValue && (bool)user.IsAuthenticated)
                    {
                        Session["UserID"] = user.Emp_ID;
                        Session["UserName"] = user.user_name;
                        Session["Image"] = user.Image;
                        if (user.Image == null)
                        {
                            Session["Image"] = "no-image1.jpg";
                        }

                        Session.Timeout = 60;
                        FormsAuthentication.SetAuthCookie(user.user_name, false);
                        if (user.tbl_department.Department_Name == "Frontdesk")
                        {
                            return RedirectToAction("Index", "FrontDesk");
                        }
                        if (user.tbl_department.Department_Name == "Inventory")
                        {
                            return RedirectToAction("Index", "InventoryHome");
                        }
                        if (user.tbl_department.Department_Name == "HR")
                        {
                            return RedirectToAction("Index", "HrEmployee");
                        }
                        if (user.tbl_department.Department_Name == "Manager" || user.tbl_department.Department_Name == "Super Admin" || user.tbl_department.Department_Name == "Admin")
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                        if (user.tbl_department.Department_Name == "Housekeeping")
                        {
                            return RedirectToAction("Index", "HouseKeepingTaskAssignment");
                        }
                        if (user.tbl_department.Department_Name == "Account")
                        {
                            return RedirectToAction("Index", "AccountsIncomeStatement");
                        }
                        if (user.tbl_department.Department_Name == "Restaurant")
                        {
                            return RedirectToAction("Index", "RestaurantDashboard");
                        }
                        return RedirectToAction("Details", "HrEmployee", new { id = user.Emp_ID });
                    }
                    ModelState.AddModelError("", "User is not authorized.");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            return View();
        }

        [NonAction]
        private static string Encode(string password)
        {
            return Crypto.Hash(password);
        }

        public ActionResult LockScreen()
        {
            if (Session["UserID"] != null)
            {
                var id = (int)Session["UserID"];

                var user = (from userlist in db.tbl_emp_info
                            where (userlist.Emp_ID == id)
                            select new
                            {
                                userlist.user_name,
                                userlist.Emp_Name,
                                userlist.Image
                            }).ToList();
                Session["Emp_Name"] = user.FirstOrDefault().Emp_Name;
                Session["Emp_Uname"] = user.FirstOrDefault().user_name;
            }

            return View();
        }

        [HttpGet]
        public ActionResult Logoff()
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Login", "HrEmployee");
            }
            else
            {
                var username = Session["UserName"].ToString();
                Session.Clear();
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("Login", "HrEmployee");
            }
        }

        public JsonResult UserNameChecker(string u_name)
        {
            tbl_emp_info emp = db.tbl_emp_info.Where(a => a.user_name == u_name).FirstOrDefault();
            if (emp == null)
            {
                return Json("right", JsonRequestBehavior.AllowGet);
            }
            return Json("wrong", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
