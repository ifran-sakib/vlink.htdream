﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Frontdesk")]
    public class FrontDeskGuestMessageController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_messages = db.tbl_messages.Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
            return View(tbl_messages.ToList());
        }

        //public ActionResult Pending()
        //{
        //    var tbl_messages = db.tbl_messages.Where(a => a.Status == "Pending").Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
        //    return View(tbl_messages.ToList());
        //}

        public ActionResult StayWiseMessage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbl_messages = db.tbl_messages.Where(a => a.Stay_Info_ID == id).Include(t => t.delivering_employee).Include(t => t.receiving_employee).Include(t => t.tbl_guest_stay_info);
            ViewBag.Messages = tbl_messages.ToList();
            var tblGuestStayInfo = db.tbl_guest_stay_info.FirstOrDefault(a => a.Stay_Info_ID == id);
            if (tblGuestStayInfo != null)
            {
                ViewBag.RoomNo = tblGuestStayInfo.tbl_room_info.Room_No;               
            }
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No", id);
            return PartialView();
        }

        public JsonResult Swap(int id)
        {
            tbl_messages tbl_messages = db.tbl_messages.Find(id);
            if (tbl_messages == null)
            {
                return Json("No Message", JsonRequestBehavior.AllowGet);
            }
            if (tbl_messages.Status == "Delivered")
            {
                return Json("Already Delivered", JsonRequestBehavior.AllowGet);
            }

            var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
            if (tblEmpInfo != null)
            {
                tbl_messages.Status = "Delivered";
                tbl_messages.Delivering_Emp_ID = tblEmpInfo.Emp_ID;
                tbl_messages.Delivery_Time = DateTime.Now;
                db.Entry(tbl_messages).State = EntityState.Modified;
                db.SaveChanges();
                return Json("Changed", JsonRequestBehavior.AllowGet);
            }
            return Json("Error", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_messages tbl_messages = db.tbl_messages.Find(id);
            if (tbl_messages == null)
            {
                return HttpNotFound();
            }
            return View(tbl_messages);
        }

        public ActionResult Create()
        {
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No");
            return PartialView();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Message_ID,Stay_Info_ID,Receiving_Emp_ID,Delivering_Emp_ID,Message,Senders_Details,Receiving_Time,Delivery_Time,Status")] tbl_messages tbl_messages)
        {
            if (ModelState.IsValid)
            {
                var tblEmpInfo = db.tbl_emp_info.FirstOrDefault(a => a.user_name == User.Identity.Name);
                if (tblEmpInfo != null)
                {
                    int userId = tblEmpInfo.Emp_ID;
                    tbl_messages.Receiving_Emp_ID = userId;
                    tbl_messages.Delivering_Emp_ID = null;
                    tbl_messages.Receiving_Time = DateTime.Now;
                    tbl_messages.Delivery_Time = null;
                    tbl_messages.Status = "Pending";
                    db.tbl_messages.Add(tbl_messages);
                    db.SaveChanges();
                    return RedirectToAction("Index", "FrontDesk");
                }
            }

            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No", tbl_messages.Stay_Info_ID);
            return View(tbl_messages);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_messages tbl_messages = db.tbl_messages.Find(id);
            if (tbl_messages == null)
            {
                return HttpNotFound();
            }
            ViewBag.Delivering_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_messages.Delivering_Emp_ID);
            ViewBag.Receiving_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_messages.Receiving_Emp_ID);
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No", tbl_messages.Stay_Info_ID);
            return View(tbl_messages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Message_ID,Stay_Info_ID,Receiving_Emp_ID,Delivering_Emp_ID,Message,Senders_Details,Receiving_Time,Delivery_Time,Status")] tbl_messages tbl_messages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_messages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Delivering_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_messages.Delivering_Emp_ID);
            ViewBag.Receiving_Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_messages.Receiving_Emp_ID);
            ViewBag.Stay_Info_ID = new SelectList(db.tbl_guest_stay_info, "Stay_Info_ID", "tbl_room_info.Room_No", tbl_messages.Stay_Info_ID);
            return View(tbl_messages);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_messages tbl_messages = db.tbl_messages.Find(id);
            if (tbl_messages == null)
            {
                return HttpNotFound();
            }
            return View(tbl_messages);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_messages tbl_messages = db.tbl_messages.Find(id);
            db.tbl_messages.Remove(tbl_messages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
