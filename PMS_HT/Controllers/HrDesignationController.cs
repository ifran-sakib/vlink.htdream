﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,HR")]
    public class HrDesignationController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_designation.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_designation tbl_designation = db.tbl_designation.Find(id);
            if (tbl_designation == null)
            {
                return HttpNotFound();
            }
            return View(tbl_designation);
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Designation_ID,Designation_Name,Basic")] tbl_designation tbl_designation)
        {
            if (ModelState.IsValid)
            {
                db.tbl_designation.Add(tbl_designation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_designation);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_designation tbl_designation = db.tbl_designation.Find(id);
            if (tbl_designation == null)
            {
                return HttpNotFound();
            }
            return View(tbl_designation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Designation_ID,Designation_Name,Basic")] tbl_designation tbl_designation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_designation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_designation);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_designation tbl_designation = db.tbl_designation.Find(id);
            if (tbl_designation == null)
            {
                return HttpNotFound();
            }
            return View(tbl_designation);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_designation tbl_designation = db.tbl_designation.Find(id);
            db.tbl_designation.Remove(tbl_designation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
