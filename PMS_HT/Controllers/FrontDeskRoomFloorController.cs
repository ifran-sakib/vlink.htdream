﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager")]
    public class FrontDeskRoomFloorController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_floor_setup.ToList());
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_floor_setup tbl_floor_setup = db.tbl_floor_setup.Find(id);
            if (tbl_floor_setup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_floor_setup);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Create()
        {
            return PartialView();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult Create([Bind(Include = "Floor_ID,Floor_No,Description,YsnActive,Created_Date")] tbl_floor_setup tbl_floor_setup)
        {
            if (ModelState.IsValid)
            {
                tbl_floor_setup.Created_Date = DateTime.Now;
                db.tbl_floor_setup.Add(tbl_floor_setup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(tbl_floor_setup);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_floor_setup tbl_floor_setup = db.tbl_floor_setup.Find(id);
            if (tbl_floor_setup == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_floor_setup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Floor_ID,Floor_No,Description,YsnActive,Created_Date")] tbl_floor_setup tbl_floor_setup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_floor_setup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_floor_setup);
        }

        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_floor_setup tbl_floor_setup = db.tbl_floor_setup.Find(id);
            if (tbl_floor_setup == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_floor_setup);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_floor_setup tbl_floor_setup = db.tbl_floor_setup.Find(id);
            db.tbl_floor_setup.Remove(tbl_floor_setup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult UniqueRoomFloorCheck(string str)
        {
            tbl_floor_setup room = db.tbl_floor_setup.FirstOrDefault(r => r.Floor_No == str);
            if (room == null)
            {
                return Json("unique", JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
