﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class BankInfoController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_bank_info.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_bank_info tbl_bank_info = db.tbl_bank_info.Find(id);
            if (tbl_bank_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_bank_info);
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_bank_info bankinfo)
        {
            if (ModelState.IsValid)
            {
                db.tbl_bank_info.Add(bankinfo);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }   
        
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_bank_info bankInfo = db.tbl_bank_info.Find(id);
            if (bankInfo == null)
            {
                return HttpNotFound();
            }
            if (!User.IsInRole("Super Admin") && bankInfo.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }
            return PartialView(bankInfo);
        }

        [HttpPost]
        public ActionResult Edit(tbl_bank_info bankInfo, int? id)
        {
            if (!User.IsInRole("Super Admin") && bankInfo.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(bankInfo).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            tbl_bank_info bankInfo = db.tbl_bank_info.Find(id);
            if (bankInfo == null)
            {
                return HttpNotFound();
            }
            if (!User.IsInRole("Super Admin") && bankInfo.Bank_Name == "Cash Bank")
            {
                return HttpNotFound();
            }
            db.tbl_bank_info.Remove(db.tbl_bank_info.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
