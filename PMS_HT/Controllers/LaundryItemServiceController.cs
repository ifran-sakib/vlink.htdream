﻿using PMS_HT.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PMS_HT.Controllers
{
    public class LaundryItemServiceController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ServiceList()
        {
            var transaction = (from c in db.tbl_room_transactions group c by new
            {
                c.Room_ID,
                c.tbl_room_info.Room_No,
                c.Guest_ID,
                c.tbl_guest_info.Name
            } into grp select new LaundryTransactionViewModel
              {
                Room_ID = grp.Key.Room_ID,
                Room_No = grp.Key.Room_No,
                Guest_ID = grp.Key.Guest_ID,
                Name = grp.Key.Name,
                Debit = grp.Sum(t => t.Debit)
               });
            ViewBag.result = transaction.ToList();
            return View();
        }
        
        public JsonResult getRoom()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var room = from g in db.tbl_guest_stay_info
                   join r in db.tbl_room_info on g.Room_ID equals r.Room_ID
                   where g.Status.Equals("CheckIn")
                   select new { g.Stay_Info_ID, r.Room_No, r.Room_ID, g.Guest_ID };
            return new JsonResult { Data = room.ToList(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getGuest(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var guest = (from s in db.tbl_guest_stay_info
                        join g in db.tbl_guest_info on s.Guest_ID equals g.Guest_ID
                        where (s.Room_ID == id && s.Status.Equals("CheckIn"))
                        select new { s.Guest_ID, g.Name }).Distinct();
            return new JsonResult { Data = guest, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getLaundryCategory()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<tbl_laundry_item_category> categories = new List<tbl_laundry_item_category>();
            categories = db.tbl_laundry_item_category.OrderBy(a => a.Laundry_Item_Cata_Name).ToList();
            return new JsonResult { Data = categories, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getItem(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<tbl_laundry_item> products = new List<tbl_laundry_item>();
            products = db.tbl_laundry_item.Where(d => d.Laundry_Item_Cata_ID == id).OrderBy(a => a.Laundry_Item_Name).ToList();
            return new JsonResult { Data = products, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getService()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<tbl_laundry_service> service = new List<tbl_laundry_service>();
            service = db.tbl_laundry_service.OrderBy(d => d.Service_Heading_Name).ToList();
            return new JsonResult { Data = service, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getServicePrice(int Laundry_Service_ID, int itemId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var serviceCost = db.tbl_laundry_service_cost.Where(a => a.Laundry_Service_ID == Laundry_Service_ID && a.Laundry_Item_ID == itemId).Select(a => a.Cost);
            return new JsonResult { Data = serviceCost, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult getReturn()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<tbl_return_germent_in> ret = new List<tbl_return_germent_in>();
            ret = db.tbl_return_germent_in.OrderBy(d => d.Return_In_Name).ToList();
            return new JsonResult { Data = ret, JsonRequestBehavior = JsonRequestBehavior.AllowGet };        
        }

        public JsonResult getReturnCost(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<tbl_return_germent_in> ret = new List<tbl_return_germent_in>();
            ret = db.tbl_return_germent_in.Where(d => d.Garment_In_ID == id).OrderBy(d => d.Cost).ToList();
            return new JsonResult { Data = ret, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult Save(tbl_room_transactions room)
        {
            bool status = false;
            var isValidModel = TryUpdateModel(room);
            if (isValidModel)
            {
                int garmentId = (int)room.Garment_In_ID;
                int returnCost = (int)room.Credit;
                room.Credit = null;
                room.Garment_In_ID = null;
                db.tbl_room_transactions.Add(room);
                room.Tran_Ref_Name = "Laundry";                
                db.SaveChanges();
                status = true;

                if(garmentId > 0)
                {
                    db.tbl_room_transactions.Add(room);
                    room.Laundry_Item_ID = null;
                    room.Laundry_Service_ID = null;
                    room.Garment_In_ID = garmentId;
                    room.Tran_Ref_Name = "ReturnGerment";
                    room.Debit = returnCost;
                    room.Credit = null;
                    db.SaveChanges();
                }

                //var room_Details = db.tbl_room_transactions.Where(a => a.Guest_Tran_ID == room.Guest_Tran_ID).ToList();
                //foreach (var item in room_Details)
                //{
                //    tbl_laundry_expense temp = db.tbl_laundry_expense.Find(item.Guest_Tran_ID);
                //    temp.Guest_Tran_ID = room.Guest_Tran_ID;
                //    db.Entry(temp).State = EntityState.Modified;
                //    db.SaveChanges();
                //}
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}