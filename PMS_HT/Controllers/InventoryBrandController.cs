﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventoryBrandController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_brand.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_brand brand)
        {
            if (ModelState.IsValid)
            {
                db.tbl_brand.Add(brand);
                db.SaveChanges();
            }
            return RedirectToAction("Brands", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_brand.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_brand brand, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(brand).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Brands", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_brand.Remove(db.tbl_brand.Find(id));
            db.SaveChanges();
            return RedirectToAction("Brands", "InventorySettings");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
