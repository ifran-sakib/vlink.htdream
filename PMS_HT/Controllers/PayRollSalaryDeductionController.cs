﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class PayRollSalaryDeductionController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            var tbl_salary_transactions = db.tbl_salary_transactions.Where(a => a.Tran_Ref_Name.Equals("SalaryDeduction") && a.Emp_ID ==  id).Include(t => t.tbl_earning_head).Include(t => t.tbl_emp_info);
            ViewBag.EmpId = id;
            Session["EmployeeID"] = id;
            return View(tbl_salary_transactions.ToList());
        }  

        public ActionResult Add(int? id)
        {
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head.Where(a => a.Type.Equals("Deduction")), "Earning_Head_ID", "Earning_Head_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_salary_transactions empSalaryDeduction)
        {
            int parameter = Convert.ToInt32(Session["EmployeeID"]);
            if (ModelState.IsValid)
            {
                empSalaryDeduction.Emp_ID = parameter;
                empSalaryDeduction.Tran_Ref_Name = "SalaryDeduction";
                db.tbl_salary_transactions.Add(empSalaryDeduction);
                db.SaveChanges();
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name", empSalaryDeduction.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", empSalaryDeduction.Emp_ID);
            return RedirectToAction("Index", new { id = parameter });
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeID"]);
            db.tbl_salary_transactions.Remove(db.tbl_salary_transactions.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter });
        }
    
        public ActionResult Edit(int id)
        {
            tbl_salary_transactions salary = db.tbl_salary_transactions.Find(id);
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head.Where(a=> a.Type.Equals("Deduction")), "Earning_Head_ID", "Earning_Head_Name", salary.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salary.Emp_ID);
            return PartialView(salary);
        }

        [HttpPost]
        public ActionResult Edit(tbl_salary_transactions salary, int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeID"]);
            if (ModelState.IsValid)
            {
                salary.Emp_ID = parameter;
                salary.Tran_Ref_Name = "SalaryDeduction";
                db.Entry(salary).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_earning_head, "Earning_Head_ID", "Earning_Head_Name", salary.Tran_Ref_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salary.Emp_ID);
            return RedirectToAction("Index", new { id = parameter});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
