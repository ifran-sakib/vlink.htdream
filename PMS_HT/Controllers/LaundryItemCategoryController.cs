﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class LaundryItemCategoryController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_laundry_item_category.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_laundry_item_category category)
        {
            if (ModelState.IsValid)
            {
                db.tbl_laundry_item_category.Add(category);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_laundry_item_category.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_laundry_item_category category, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_laundry_item_category.Remove(db.tbl_laundry_item_category.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
