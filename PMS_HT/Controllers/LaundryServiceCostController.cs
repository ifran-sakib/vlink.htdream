﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class LaundryServiceCostController : CustomControllerBase
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: LaundryServiceCost
        public ActionResult Index()
        {
            var tbl_laundry_service_cost = db.tbl_laundry_service_cost.Include(t => t.tbl_laundry_item).Include(t => t.tbl_laundry_service);
            return View(tbl_laundry_service_cost.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name");
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_laundry_service_cost laundryCost)
        {
            if (ModelState.IsValid)
            {
                db.tbl_laundry_service_cost.Add(laundryCost);
                db.SaveChanges();
            }
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", laundryCost.Laundry_Item_ID);
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", laundryCost.Laundry_Service_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            tbl_laundry_service_cost laundryCost = new tbl_laundry_service_cost();
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", laundryCost.Laundry_Item_ID);
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", laundryCost.Laundry_Service_ID);
            return PartialView(db.tbl_laundry_service_cost.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_laundry_service_cost laundryCost, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(laundryCost).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", laundryCost.Laundry_Item_ID);
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", laundryCost.Laundry_Service_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_laundry_service_cost.Remove(db.tbl_laundry_service_cost.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        // GET: LaundryServiceCost/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_laundry_service_cost tbl_laundry_service_cost = db.tbl_laundry_service_cost.Find(id);
        //    if (tbl_laundry_service_cost == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_laundry_service_cost);
        //}

        //// GET: LaundryServiceCost/Create
        //public ActionResult Create()
        //{
        //    ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name");
        //    ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name");
        //    return View();
        //}

        //// POST: LaundryServiceCost/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Laundry_Sevice_Cost_ID,Laundry_Item_ID,Laundry_Service_ID,Cost,Note")] tbl_laundry_service_cost tbl_laundry_service_cost)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.tbl_laundry_service_cost.Add(tbl_laundry_service_cost);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", tbl_laundry_service_cost.Laundry_Item_ID);
        //    ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", tbl_laundry_service_cost.Laundry_Service_ID);
        //    return View(tbl_laundry_service_cost);
        //}

        //// GET: LaundryServiceCost/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_laundry_service_cost tbl_laundry_service_cost = db.tbl_laundry_service_cost.Find(id);
        //    if (tbl_laundry_service_cost == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", tbl_laundry_service_cost.Laundry_Item_ID);
        //    ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", tbl_laundry_service_cost.Laundry_Service_ID);
        //    return View(tbl_laundry_service_cost);
        //}

        //// POST: LaundryServiceCost/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Laundry_Sevice_Cost_ID,Laundry_Item_ID,Laundry_Service_ID,Cost,Note")] tbl_laundry_service_cost tbl_laundry_service_cost)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(tbl_laundry_service_cost).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", tbl_laundry_service_cost.Laundry_Item_ID);
        //    ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", tbl_laundry_service_cost.Laundry_Service_ID);
        //    return View(tbl_laundry_service_cost);
        //}

        //// GET: LaundryServiceCost/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    tbl_laundry_service_cost tbl_laundry_service_cost = db.tbl_laundry_service_cost.Find(id);
        //    if (tbl_laundry_service_cost == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(tbl_laundry_service_cost);
        //}

        //// POST: LaundryServiceCost/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    tbl_laundry_service_cost tbl_laundry_service_cost = db.tbl_laundry_service_cost.Find(id);
        //    db.tbl_laundry_service_cost.Remove(tbl_laundry_service_cost);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
