using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class FrontDeskCashCollectionController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ViewDateWiseCollection(DateTime? dob, DateTime? date)
        {
            tbl_room_transactions outrow = new tbl_room_transactions();
            tbl_room_transactions bookpayrow = new tbl_room_transactions();
            tbl_room_transactions refundrow = new tbl_room_transactions();
            tbl_room_transactions bookrefundrow = new tbl_room_transactions();

            var tbl_room_transactions = db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 3).OrderBy(p => DbFunctions.TruncateTime(p.Tran_Date)).ThenBy(b => b.Tran_Ref_ID).Include(t => t.tbl_room_info).ToList();

            decimal outstanding_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 12).Sum(b => b.Credit));
            if (outstanding_payment > 0)
            {
                outrow.tbl_room_info = new tbl_room_info();
                outrow.tbl_room_info.Room_No = "Outstanding Payment";
                outrow.Credit = outstanding_payment;
                tbl_room_transactions.Add(outrow);
            }

            decimal booking_payment = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 14).Sum(b => b.Credit));
            if (booking_payment > 0)
            {
                bookpayrow.tbl_room_info = new tbl_room_info();
                bookpayrow.tbl_room_info.Room_No = "Booking Payment";
                bookpayrow.Credit = booking_payment;
                tbl_room_transactions.Add(bookpayrow);
            }

            decimal refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 7).Sum(b => b.Credit));
            if (refund > 0)
            {
                refundrow.tbl_room_info = new tbl_room_info();
                refundrow.tbl_room_info.Room_No = "Checkout Refund";
                refundrow.Credit = refund * (-1);
                tbl_room_transactions.Add(refundrow);
            }

            decimal booking_refund = Convert.ToDecimal(db.tbl_room_transactions.Where(a => DbFunctions.TruncateTime(a.Tran_Date) >= dob && DbFunctions.TruncateTime(a.Tran_Date) <= date && a.Tran_Ref_ID == 13).Sum(b => b.Credit));
            if (booking_refund > 0)
            {
                bookrefundrow.tbl_room_info = new tbl_room_info();
                bookrefundrow.tbl_room_info.Room_No = "Booking Refund";
                bookrefundrow.Credit = booking_refund * (-1);
                tbl_room_transactions.Add(bookrefundrow);
            }

            ViewBag.transactions = tbl_room_transactions.ToList();

            return PartialView();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
