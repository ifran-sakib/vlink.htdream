﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class HouseKeepingTaskAssignmentController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_task_assignment = db.tbl_task_assignment.Include(t => t.tbl_emp_info).Include(t => t.tbl_maid_info).Include(t => t.tbl_room_info).Include(t => t.tbl_task_setup);
            ViewBag.status = "In Process";
            ViewBag.status2 = "Done";
            return View(tbl_task_assignment.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name");
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No");
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_task_assignment taskAssign)
        {
            if (ModelState.IsValid)
            {
                taskAssign.Task_Status = false;
                db.tbl_task_assignment.Add(taskAssign);
                db.SaveChanges();
            }
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskAssign.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskAssign.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskAssign.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskAssign.Task_ID);
            return RedirectToAction("Index");
        }

        //public ActionResult Edit(int id)
        //{
        //    tbl_task_assignment taskAssign = db.tbl_task_assignment.Find(id);
        //    ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskAssign.Created_By);
        //    ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskAssign.Maid_ID);
        //    ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskAssign.Room_ID);
        //    ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskAssign.Task_ID);
        //    return PartialView(taskAssign);
        //}
        //[HttpPost]
        //public ActionResult Edit(tbl_task_assignment taskAssign )
        //{
        //    if (ModelState.IsValid)
        //    {
        //      //  taskAssign.Task_Status = Done;
        //        db.Entry(taskAssign).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }

        //    ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskAssign.Created_By);
        //    ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskAssign.Maid_ID);
        //    ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskAssign.Room_ID);
        //    ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskAssign.Task_ID);
        //    return RedirectToAction("Index");
        //}


        public ActionResult Edit(int id)
        {
            tbl_task_assignment taskAssign = db.tbl_task_assignment.Find(id);
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskAssign.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskAssign.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskAssign.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskAssign.Task_ID);
            return PartialView(taskAssign);
        }

        [HttpPost]
        public ActionResult Edit(tbl_task_assignment taskAssign, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(taskAssign).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", taskAssign.Created_By);
            ViewBag.Maid_ID = new SelectList(db.tbl_maid_info, "Maid_ID", "Name", taskAssign.Maid_ID);
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", taskAssign.Room_ID);
            ViewBag.Task_ID = new SelectList(db.tbl_task_setup, "Task_ID", "Task_Heading", taskAssign.Task_ID);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_task_assignment.Remove(db.tbl_task_assignment.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult Done(IEnumerable<int> done)
        {
            try
            {
                var some = db.tbl_task_assignment.Where(x => done.Contains(x.Task_Assignment_ID)).ToList();
                some.ForEach(a => a.Task_Status = true);
                db.SaveChanges();
            }
            catch { }
                return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
