﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventoryItemController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_item = db.tbl_item.Include(t => t.tbl_brand).Include(t => t.tbl_category).Include(t => t.tbl_subcategory).Include(t => t.tbl_unit);
            return View(tbl_item.ToList());
        }

        public ActionResult Add()
        {
            ViewBag.Brand_ID = new SelectList(db.tbl_brand, "Brand_ID", "Brand_Name");
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name");
            int cata_id = db.tbl_category.FirstOrDefault().Cata_ID;
            ViewBag.Subcata_ID = new SelectList(db.tbl_subcategory.Where(a => a.Cata_ID == cata_id), "Subcata_ID", "Subcata_Name");
            ViewBag.Unit_ID = new SelectList(db.tbl_unit, "Unit_ID", "Unit_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_item item)
        {
            if (ModelState.IsValid)
            {
                db.tbl_item.Add(item);
                db.SaveChanges();
            }
            ViewBag.Brand_ID = new SelectList(db.tbl_brand, "Brand_ID", "Brand_Name", item.Brand_ID);
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", item.Cata_ID);
            ViewBag.Subcata_ID = new SelectList(db.tbl_subcategory, "Subcata_ID", "Subcata_Name", item.Subcata_ID);
            ViewBag.Unit_ID = new SelectList(db.tbl_unit, "Unit_ID", "Unit_Name", item.Unit_ID);
            return RedirectToAction("Items", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            tbl_item item = db.tbl_item.Find(id);
            ViewBag.Brand_ID = new SelectList(db.tbl_brand, "Brand_ID", "Brand_Name", item.Brand_ID);
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", item.Cata_ID);
            ViewBag.Subcata_ID = new SelectList(db.tbl_subcategory, "Subcata_ID", "Subcata_Name", item.Subcata_ID);
            ViewBag.Unit_ID = new SelectList(db.tbl_unit, "Unit_ID", "Unit_Name", item.Unit_ID);
            return PartialView(item);
        }

        [HttpPost]
        public ActionResult Edit(tbl_item item, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Brand_ID = new SelectList(db.tbl_brand, "Brand_ID", "Brand_Name", item.Brand_ID);
            ViewBag.Cata_ID = new SelectList(db.tbl_category, "Cata_ID", "Cata_Name", item.Cata_ID);
            ViewBag.Subcata_ID = new SelectList(db.tbl_subcategory, "Subcata_ID", "Subcata_Name", item.Subcata_ID);
            ViewBag.Unit_ID = new SelectList(db.tbl_unit, "Unit_ID", "Unit_Name", item.Unit_ID);
            return RedirectToAction("Items", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_item.Remove(db.tbl_item.Find(id));
            db.SaveChanges();
            return RedirectToAction("Items", "InventorySettings");
        }

        public ActionResult FillSubCategory(int cata_id)
        {
            var subcategories = db.tbl_subcategory.Where(c => c.Cata_ID == cata_id).Select(c => new { Subcata_ID = c.Subcata_ID, Subcata_Name = c.Subcata_Name });
            return Json(subcategories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillUnit(int item_id)
        {
            var item = db.tbl_item.FirstOrDefault(c => c.Item_ID == item_id);
            if (item != null)
            {
                var unit = item.tbl_unit.Unit_Name;
                return Json(unit, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
