﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class RestaurantFoodCategoryController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: RestaurantFoodCategory
        public ActionResult Index()
        {
            var categories = db.tbl_food_category.OrderBy(a => a.Serial_No).ToList();
            return View(categories);
        }

        // GET: RestaurantFoodCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_category tbl_food_category = db.tbl_food_category.Find(id);
            if (tbl_food_category == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food_category);
        }

        // GET: RestaurantFoodCategory/Create
        public ActionResult Create()
        {
            ViewBag.Total = db.tbl_food_category.Count() + 1;
            return View();
        }

        // POST: RestaurantFoodCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Food_Cate_ID,Food_Cate_Name,Note,Serial_No")] tbl_food_category tbl_food_category)
        {
            if (ModelState.IsValid)
            {
                int new_serial = (int)tbl_food_category.Serial_No;
                int dR1 = db.Database.ExecuteSqlCommand("UPDATE tbl_food_category SET Serial_No = Serial_No + 1 WHERE Serial_No >= " + new_serial.ToString());
                db.tbl_food_category.Add(tbl_food_category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_food_category);
        }

        // GET: RestaurantFoodCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_category tbl_food_category = db.tbl_food_category.Find(id);
            if (tbl_food_category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Total = db.tbl_food_category.Count();
            return PartialView(tbl_food_category);
        }

        // POST: RestaurantFoodCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Food_Cate_ID,Food_Cate_Name,Note,Serial_No")] tbl_food_category tbl_food_category)
        {
            if (ModelState.IsValid)
            {
                int dR1 = 0;
                int new_serial = (int)tbl_food_category.Serial_No;
                string query = "SELECT Serial_No FROM tbl_food_category WHERE Food_Cate_ID = " + tbl_food_category.Food_Cate_ID.ToString();
                var nn = db.Database.SqlQuery<int>(query);
                int old_serial = Convert.ToInt32(nn.FirstOrDefault());
                if (new_serial < old_serial)
                {
                    dR1 = db.Database.ExecuteSqlCommand("UPDATE tbl_food_category SET Serial_No = Serial_No + 1 WHERE Serial_No >= " + new_serial.ToString() + " AND Serial_No < " + old_serial.ToString());
                }
                else if (new_serial > old_serial)
                {
                    dR1 = db.Database.ExecuteSqlCommand("UPDATE tbl_food_category SET Serial_No = Serial_No - 1 WHERE Serial_No > " + old_serial.ToString() + "AND Serial_No <= " + new_serial.ToString());
                }

                db.Entry(tbl_food_category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return PartialView(tbl_food_category);
        }

        // GET: RestaurantFoodCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_food_category tbl_food_category = db.tbl_food_category.Find(id);
            if (tbl_food_category == null)
            {
                return HttpNotFound();
            }
            return View(tbl_food_category);
        }

        // POST: RestaurantFoodCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_food_category tbl_food_category = db.tbl_food_category.Find(id);
            db.tbl_food_category.Remove(tbl_food_category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
