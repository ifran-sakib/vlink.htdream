﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetAmenitiesController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_banquet_amenities.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_banquet_amenities banAmenities)
        {
            if (ModelState.IsValid)
            {
                db.tbl_banquet_amenities.Add(banAmenities);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_banquet_amenities.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(tbl_banquet_amenities banAmenities, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(banAmenities).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_banquet_amenities.Remove(db.tbl_banquet_amenities.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
