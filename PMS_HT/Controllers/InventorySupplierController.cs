﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class InventorySupplierController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View(db.tbl_supplier.ToList());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_supplier supplier)
        {
            if (ModelState.IsValid)
            {
                db.tbl_supplier.Add(supplier);
                db.SaveChanges();
            }
            return RedirectToAction("Vendors", "InventorySettings");
        }

        public ActionResult Edit(int id)
        {
            return PartialView(db.tbl_supplier.Find(id));
        }
        [HttpPost]
        public ActionResult Edit(tbl_supplier supplier, int id)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplier).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Vendors", "InventorySettings");
        }

        public ActionResult Delete(int id)
        {
            db.tbl_supplier.Remove(db.tbl_supplier.Find(id));
            db.SaveChanges();
            return RedirectToAction("Vendors", "InventorySettings");
        }
     
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
