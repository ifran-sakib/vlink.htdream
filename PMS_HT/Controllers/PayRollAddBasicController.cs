﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class PayRollAddBasicController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            Session["EmployeeId"] = id;
            var tbl_salary_transactions = db.tbl_salary_transactions.Where(a=> a.Tran_Ref_Name.Equals("AddBasic") && a.Emp_ID==id).Include(t => t.tbl_earning_head).Include(t => t.tbl_emp_info);
            ViewBag.EmpId = id;
            ViewBag.Basic = db.tbl_emp_info.Where(t => t.Emp_ID == id).Select(abs => abs.tbl_designation.Basic).FirstOrDefault();
            return View(tbl_salary_transactions.ToList());
        }

        public ActionResult Add(int? id)
        {      
            ViewBag.Emp_ID = id;
            ViewBag.Debit = db.tbl_emp_info.Where(t => t.Emp_ID == id).Select(abs => abs.tbl_designation.Basic).FirstOrDefault();
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_salary_transactions addBasic)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            if (ModelState.IsValid)
            {
                addBasic.Emp_ID = parameter;
                addBasic.Tran_Ref_Name = "AddBasic";
                db.tbl_salary_transactions.Add(addBasic);
                db.SaveChanges();
            }
            return RedirectToAction("Index" , new { id = parameter } );
        }

        public ActionResult Edit(int id)
        {
            tbl_salary_transactions salary = db.tbl_salary_transactions.Find(id); 
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salary.Emp_ID);
            return PartialView(salary);
        }

        [HttpPost]
        public ActionResult Edit(tbl_salary_transactions salary, int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            if (ModelState.IsValid)
            {
                salary.Emp_ID = parameter;
                salary.Tran_Ref_Name = "AddBasic";
                db.Entry(salary).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", salary.Emp_ID);
            return RedirectToAction("Index", new { id = parameter});
        }

        public ActionResult Delete(int id)
        {
            int parameter = Convert.ToInt32(Session["EmployeeId"]);
            db.tbl_salary_transactions.Remove(db.tbl_salary_transactions.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index", new { id = parameter});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
