using System;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetAvailableController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult SearchBanquet(DateTime dob, DateTime date)
        {
            var query = "SELECT distinct(tbl_banquet_info.Banquet_Name) FROM tbl_banquet_prog INNER JOIN tbl_banquet_booking ON tbl_banquet_prog.Program_ID = tbl_banquet_booking.Program_ID  right outer JOIN tbl_banquet_info ON tbl_banquet_prog.Banquet_ID = tbl_banquet_info.Banquet_ID where(tbl_banquet_booking.Start_Date IS NULL and tbl_banquet_booking.End_Date IS NULL) or tbl_banquet_info.Banquet_Name not in (SELECT tbl_banquet_info.Banquet_Name FROM tbl_banquet_prog INNER JOIN tbl_banquet_booking ON tbl_banquet_prog.Program_ID = tbl_banquet_booking.Program_ID JOIN tbl_banquet_info ON tbl_banquet_prog.Banquet_ID = tbl_banquet_info.Banquet_ID where(tbl_banquet_booking.Start_Date <= '" + date.ToString("yyyy-MM-dd HH:mm:ss") + "' and tbl_banquet_booking.End_Date >= '" + dob.ToString("yyyy-MM-dd HH:mm:ss") + "'))";
            var data = db.Database.SqlQuery<BanquetVacantViewModel>(query);
            ViewBag.Banquet = data;
            return PartialView();
        }
    }
}