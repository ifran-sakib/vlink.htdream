﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Models;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace PMS_HT.Controllers
{
    public class BankTransactionsController : CustomControllerBase
    {
        public ActionResult Index(int? id)
        {
            ViewBag.Acc_ID = id;
            Session["Acc_ID"] = id;
            var tbl_bank_transaction = db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id).Include(t => t.tbl_account_info).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_account_info.tbl_bank_info).OrderBy(a => a.Created_Date);

            var accountInfo = db.tbl_account_info.FirstOrDefault(a => a.Acc_ID == id);
            ViewBag.Bank = accountInfo.tbl_bank_info.Bank_Name;
            ViewBag.Branch = accountInfo.tbl_branch_info.Branch_Name;
            ViewBag.AccountNo = accountInfo.Acc_No;
            return View(tbl_bank_transaction.ToList());
        }

        public ActionResult Statement(int? id, string dob, string date)
        {
            if (id == null || dob == "" || date == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateTime dob1 = Convert.ToDateTime(dob);
            DateTime date1 = Convert.ToDateTime(date);
            decimal totalDeposit = Convert.ToDecimal(db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date < dob1).Select(a => a.Debit).Sum());
            decimal totalWithdrawn = Convert.ToDecimal(db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date < dob1).Select(a => a.Credit).Sum());
            decimal balance = totalDeposit - totalWithdrawn;
            ViewBag.PreBalance = balance;
            var tbl_bank_transaction = db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date >= dob1 && a.Created_Date <= date1).Include(t => t.tbl_account_info).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_account_info.tbl_bank_info).OrderBy(a => a.Created_Date);

            var accountInfo = db.tbl_account_info.FirstOrDefault(a => a.Acc_ID == id);
            ViewBag.Bank = accountInfo.tbl_bank_info.Bank_Name;
            ViewBag.Branch = accountInfo.tbl_branch_info.Branch_Name;
            ViewBag.AccountNo = accountInfo.Acc_No;
            ViewBag.StartDate = dob;
            ViewBag.EndDate = date;
            ViewBag.id = id;
            return View(tbl_bank_transaction.ToList());
        }

        public JsonResult ExportStatement(int? id, string dob, string date)
        {
            if (id == null || dob == "" || date == "")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            DateTime dob1 = Convert.ToDateTime(dob);
            DateTime date1 = Convert.ToDateTime(date);
            decimal totalDeposit = Convert.ToDecimal(db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date < dob1).Select(a => a.Debit).Sum());
            decimal totalWithdrawn = Convert.ToDecimal(db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date < dob1).Select(a => a.Credit).Sum());
            decimal balancea = totalDeposit - totalWithdrawn;
            var tbl_bank_transaction = db.tbl_bank_transaction.Where(a => a.Tran_Ref_ID == id && a.Created_Date >= dob1 && a.Created_Date <= date1).Include(t => t.tbl_account_info).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_account_info.tbl_bank_info).OrderBy(a => a.Created_Date);
            var transactions = (from tran in db.tbl_bank_transaction
                                where tran.Tran_Ref_ID == id && tran.Created_Date >= dob1 && tran.Created_Date <= date1
                                orderby tran.Created_Date
                                select new BankStatementViewModel
                                {
                                    date = tran.Created_Date.Value,
                                    particular = tran.Particulars,
                                    debit = tran.Debit.Value,
                                    credit = tran.Credit.Value,
                                    balance = 0
                                }).ToList();

            transactions.Insert(0, new BankStatementViewModel
            {
                date = dob1,
                particular = "Previous Balance",
                debit = 0,
                credit = 0,
                balance = balancea
            });
            for (int i = 1; i < transactions.Count; i++)
            {
                balancea += (transactions[i].debit - transactions[i].credit);
                transactions[i].balance = balancea;
            }
            var accountInfo = db.tbl_account_info.FirstOrDefault(a => a.Acc_ID == id);

            CommonRepoprtData rpd = new CommonRepoprtData();
            LocalReport lc = new LocalReport();
            string reportPath = Path.Combine(Server.MapPath("~/Reports"), "BankStatement.rdlc");
            if (System.IO.File.Exists(reportPath))
            {
                lc.ReportPath = reportPath;
            }
            else
            {
                return Json("");
            }

            ReportDataSource DataSetBankStatement = new ReportDataSource("DataSetBankStatement", transactions);

            lc.DataSources.Add(DataSetBankStatement);

            ReportParameter[] param = new ReportParameter[9];
            param[0] = new ReportParameter("PropertyName", rpd.PropertyName, false);
            param[2] = new ReportParameter("PropertyPhone", rpd.PropertyPhone, false);
            param[1] = new ReportParameter("PropertyAddress", rpd.PropertyAddress, false);
            param[3] = new ReportParameter("FooterCopyright", rpd.FooterCopyright, false);
            param[4] = new ReportParameter("BankName", accountInfo.tbl_bank_info.Bank_Name, false);
            param[5] = new ReportParameter("BranchName", accountInfo.tbl_branch_info.Branch_Name, false);
            param[6] = new ReportParameter("AccountName", accountInfo.Acc_Name, false);
            param[7] = new ReportParameter("AccountNo", accountInfo.Acc_No, false);
            param[8] = new ReportParameter("StatementPeriod", dob + " to " + date, false);

            lc.SetParameters(param);

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lc.Render(
                rpd.ReportType,
                rpd.DeviceInfo,
                out rpd.MimeType,
                out rpd.Encoding,
                out rpd.FileNameExtension,
                out streams,
                out warnings);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            String base64 = Convert.ToBase64String(renderedBytes);
            return Json(base64, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_bank_transaction tbl_bank_transaction = db.tbl_bank_transaction.Find(id);
            if (tbl_bank_transaction == null)
            {
                return HttpNotFound();
            }
            return View(tbl_bank_transaction);
        }

        public ActionResult Add(int? id)
        {
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No");
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Acc_ID = id;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(tbl_bank_transaction tbl_bank_transaction, decimal? amount)
        {
            int parameter = Convert.ToInt32(Session["Acc_ID"]);
            var sum_deposit = Convert.ToDecimal((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == parameter select tran.Debit).Sum() - (from tr in db.tbl_bank_transaction where tr.Tran_Ref_ID == parameter select tr.Credit).Sum());
            if (ModelState.IsValid)
            {
                if (tbl_bank_transaction.Tran_Type == "Deposit")
                {
                    tbl_bank_transaction.Debit = amount;
                    tbl_bank_transaction.Credit = 0;
                }
                if (tbl_bank_transaction.Tran_Type == "Withdrawn")
                {
                    if (sum_deposit - amount >= 0)
                    {
                        tbl_bank_transaction.Credit = amount;
                        tbl_bank_transaction.Debit = 0;
                    }
                    else
                    {
                        ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
                        ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
                        ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
                        return RedirectToAction("Index", new { id = parameter });
                    }
                }
                tbl_bank_transaction.Tran_Ref_ID = parameter;
                db.tbl_bank_transaction.Add(tbl_bank_transaction);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = parameter });
            }

            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
            return RedirectToAction("Index", new { id = parameter });
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_bank_transaction tbl_bank_transaction = db.tbl_bank_transaction.Find(id);
            if (tbl_bank_transaction.Tran_Type == "Withdrawn")
            {
                ViewBag.amount = tbl_bank_transaction.Credit;
            }
            if (tbl_bank_transaction.Tran_Type == "Deposit")
            {
                ViewBag.amount = tbl_bank_transaction.Debit;
            }
            if (tbl_bank_transaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
            return PartialView(tbl_bank_transaction);

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_bank_transaction tbl_bank_transaction, decimal? amount)
        {
            var sum_deposit = Convert.ToDecimal((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == tbl_bank_transaction.Tran_Ref_ID && tran.Bnk_Tran_ID != tbl_bank_transaction.Bnk_Tran_ID select tran.Debit).Sum() - (from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == tbl_bank_transaction.Tran_Ref_ID && tran.Bnk_Tran_ID != tbl_bank_transaction.Bnk_Tran_ID select tran.Credit).Sum());

            if (ModelState.IsValid)
            {
                if (tbl_bank_transaction.Tran_Type == "Deposit")
                {
                    if ((amount + sum_deposit) >= 0)
                    {
                        tbl_bank_transaction.Debit = amount;
                        tbl_bank_transaction.Credit = 0;
                    }
                    if ((amount + sum_deposit) < 0)
                    {
                        ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
                        ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
                        ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
                        return RedirectToAction("Index", new { id = tbl_bank_transaction.Tran_Ref_ID });
                    }
                }
                if (tbl_bank_transaction.Tran_Type == "Withdrawn")
                {
                    if ((sum_deposit - amount) >= 0)
                    {
                        tbl_bank_transaction.Credit = amount;
                        tbl_bank_transaction.Debit = 0;
                    }
                    if ((sum_deposit - amount) < 0)
                    {
                        ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
                        ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
                        ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
                        return RedirectToAction("Index", new { id = tbl_bank_transaction.Tran_Ref_ID });
                    }
                }
                db.Entry(tbl_bank_transaction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = tbl_bank_transaction.Tran_Ref_ID });
            }

            ViewBag.Tran_Ref_ID = new SelectList(db.tbl_account_info, "Acc_ID", "Acc_No", tbl_bank_transaction.Tran_Ref_ID);
            ViewBag.Creted_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Creted_By);
            ViewBag.Modified_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_bank_transaction.Modified_By);
            return RedirectToAction("Index", new { id = tbl_bank_transaction.Tran_Ref_ID });
        }

        public JsonResult ErrorMeassageAdd(decimal? amount, string trantype)
        {
            int parameter = Convert.ToInt32(Session["Acc_ID"]);
            var balance = ((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == parameter select tran.Debit).Sum()) - ((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == parameter select tran.Credit).Sum());
            if (balance == null)
            {
                balance = 0;
            }
            if (trantype == "Deposit")
            {
                if ((amount + balance) >= 0)
                {
                    var message = "s";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
                if ((amount + balance) < 0)
                {
                    var message = "Invalid Amount";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }

            }
            if (trantype == "Withdrawn")
            {
                if ((balance - amount) >= 0)
                {
                    var message = "s";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
                if ((balance - amount) < 0)
                {
                    var message = "Invalid Amount";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ErrorMeassage(decimal? amount, string trantype, int? bnktranid, int? tranrefid)
        {
            var balance = ((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == tranrefid && tran.Bnk_Tran_ID != bnktranid select tran.Debit).Sum()) - ((from tran in db.tbl_bank_transaction where tran.Tran_Ref_ID == tranrefid && tran.Bnk_Tran_ID != bnktranid select tran.Credit).Sum());
            if (balance == null)
            {
                balance = 0;
            }
            if (trantype == "Deposit")
            {
                if ((amount + balance) >= 0)
                {
                    var message = "s";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
                if ((amount + balance) < 0)
                {
                    var message = "Invalid Amount";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }

            }
            if (trantype == "Withdrawn")
            {
                if ((balance - amount) >= 0)
                {
                    var message = "s";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
                if ((balance - amount) < 0)
                {
                    var message = "Invalid Amount";
                    return Json(message.ToList(), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_bank_transaction tbl_bank_transaction = db.tbl_bank_transaction.Find(id);
            if (tbl_bank_transaction == null)
            {
                return HttpNotFound();
            }
            int Acc_ID = tbl_bank_transaction.tbl_account_info.Acc_ID.Value;
            db.tbl_bank_transaction.Remove(tbl_bank_transaction);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = Acc_ID });
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_bank_transaction tbl_bank_transaction = db.tbl_bank_transaction.Find(id);
            int Acc_ID = tbl_bank_transaction.tbl_account_info.Acc_ID.Value;
            db.tbl_bank_transaction.Remove(tbl_bank_transaction);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = Acc_ID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
