using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Account")]
    public class ExpenseController : CustomControllerBase
    {
        public ActionResult Index()
        {
            var tbl_expense = db.tbl_expense.OrderByDescending(a => a.Created_Date).Include(t => t.tbl_emp_info).Include(t => t.tbl_emp_info1).Include(t => t.tbl_expense_head);
            return View(tbl_expense.ToList());
        }
        
        public ActionResult Add()
        {
            List<tbl_expense_head> tbl_expense_head = db.tbl_expense_head.OrderBy(x => x.Exp_Head_Name).ToList();
            ViewBag.Exp_Head_ID = new SelectList(tbl_expense_head, "Exp_Head_ID", "Exp_Head_Name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(tbl_expense expense)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Exp_Head_ID = new SelectList(db.tbl_expense_head, "Exp_Head_ID", "Exp_Head_Name");
                db.tbl_expense.Add(expense);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            tbl_expense expanse = db.tbl_expense.Find(id);
            List<tbl_expense_head> tbl_expense_head = db.tbl_expense_head.OrderBy(x => x.Exp_Head_Name).ToList();
            ViewBag.Exp_Head_ID = new SelectList(tbl_expense_head, "Exp_Head_ID", "Exp_Head_Name", expanse.Exp_Head_ID);
            return PartialView(expanse);
        }

        [HttpPost]
        public ActionResult Edit(tbl_expense expense, int? id)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Exp_Head_ID = new SelectList(db.tbl_expense_head, "Exp_Head_ID", "Exp_Head_Name", expense.Exp_Head_ID);
                db.Entry(expense).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult ExpenseDayWise()
        {
            var ExpenseGroupWise = db.tbl_expense.ToList();
            var results = from at in ExpenseGroupWise
                          group at by new
                          {
                              at.Created_Date
                          } into g
                          select new ExpenseGroupWise
                          {
                              Value = g.Sum(x => x.Amount),
                              date = g.Key.Created_Date
                          };
            ViewBag.ExpenseGroupWise = results.ToList();
            return View();
        }

        public ActionResult Details(string date)
        {
            DateTime filterDate = Convert.ToDateTime(date);
            ViewBag.expenseDetails = db.tbl_expense.Where(a => a.Created_Date == filterDate).ToList();
            return View();
        }

        public ActionResult Delete(int id)
        {
            db.tbl_expense.Remove(db.tbl_expense.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
