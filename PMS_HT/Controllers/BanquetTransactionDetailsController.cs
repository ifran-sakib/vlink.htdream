﻿using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class BanquetTransactionDetailsController : CustomControllerBase
    {
        public ActionResult Index(int? id,int? id1)
        {
            var rent = from tran in db.tbl_banquet_transactions where tran.Program_ID == id && tran.Banq_Booking_ID == id1 && tran.Tran_Ref_Name == "rent"
                       select new BanquetTransactionDetailsViewModel
                       {
                          Debit = tran.Debit,
                          Tran_Date = tran.Tran_Date
                       }; 
            ViewBag.Rent = rent.ToList();

            var payment = from tran in db.tbl_banquet_transactions
                          where tran.Program_ID == id && tran.Banq_Booking_ID == id1 && tran.Tran_Ref_Name == "payment"
                          select new BanquetTransactionDetailsViewModel
                          {
                              Credit = tran.Credit,
                              Tran_Date = tran.Tran_Date
                          };
            ViewBag.Payment = payment.ToList();

            var amenites = from trans in db.tbl_banquet_transactions
                           join amn in db.tbl_banquet_amenities on trans.Tran_Ref_ID equals amn.Banquet_Amenities_ID
                           where trans.Program_ID == id && trans.Banq_Booking_ID == id1 && trans.Tran_Ref_Name == "amenites"
                           select new BanquetTransactionDetailsViewModel
                           {
                               Amenities_Heading = amn.Amenities_Heading,
                               Debit = trans.Debit,
                               Tran_Date = trans.Tran_Date };
            ViewBag.Amenites = amenites.ToList();

            return View();
        }
    }
}