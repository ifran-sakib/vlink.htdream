﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Models;

namespace PMS_HT.Controllers
{
    public class AccountsSalaryExpenseController : CustomControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult DateRangeSalaryExpense(DateTime? dob, DateTime? date)
        {
            var query = db.tbl_salary_transactions.Include(t => t.tbl_emp_info).Where(a => a.Tran_Ref_Name.Equals("SalaryPayment") && a.Date >= dob && a.Date <= date);
            ViewBag.SalaryExpense = query.ToList();
            return PartialView();
        }
    }
}