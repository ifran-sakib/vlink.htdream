﻿using System;
using System.Linq;
using System.Web.Mvc;
using PMS_HT.Models;
using System.Data.Entity;

namespace PMS_HT.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Manager,Inventory")]
    public class InventoryMasterdetailsController : CustomControllerBase
    {
        public ActionResult Index()
        {
            tbl_item item = new tbl_item();
            tbl_stock_location location = new tbl_stock_location();
            tbl_supplier supplier = new tbl_supplier();
            ViewBag.supplier = new SelectList(db.tbl_supplier, "Supplier_ID", "Sup_Name", supplier.Supplier_ID);
            ViewBag.Item_ID = new SelectList(db.tbl_item, "Item_ID", "Item_Name", item.Item_ID);
            ViewBag.Stock_Location_ID = new SelectList(db.tbl_stock_location, "Stock_Location_ID", "Stock_Location_Name", location.Stock_Location_ID);
            return View();
        }

        [HttpPost]
        public JsonResult Save(tbl_purchase_master master)
        {
            bool status = false;
            var isValidModel = TryUpdateModel(master);
            if (isValidModel)
            {
                tbl_supplier_ledger supplier_ledger_buy = new tbl_supplier_ledger();
                tbl_supplier_ledger supplier_ledger_paid = new tbl_supplier_ledger();
                tbl_supplier_ledger supplier_ledger_discount = new tbl_supplier_ledger();
                tbl_purchase_master newrow = new tbl_purchase_master();

                DateTime change_date = master.Purchase_Date.Value;
                tbl_supplier tbl_supplier = db.tbl_supplier.Find(master.Supplier_ID);
                master.tbl_supplier = tbl_supplier;


                decimal total = Convert.ToDecimal(master.Memo_Total);
                decimal advanced_amount = Convert.ToDecimal(master.Advanced_Amount);
                decimal discount = Convert.ToDecimal(master.Discount);
                master.Advanced_Amount = advanced_amount;
                master.Discount = discount;

                supplier_ledger_buy.Supplier_ID = master.Supplier_ID;
                supplier_ledger_buy.tbl_supplier = tbl_supplier;
                supplier_ledger_buy.Transaction_Date = change_date;
                supplier_ledger_buy.Tran_Ref_Name = "BuyProduct";
                supplier_ledger_buy.Tran_Ref_ID = 1;
                supplier_ledger_buy.Credit = 0;
                supplier_ledger_buy.Debit = total;

                supplier_ledger_paid.Supplier_ID = master.Supplier_ID;
                supplier_ledger_paid.tbl_supplier = tbl_supplier;
                supplier_ledger_paid.Transaction_Date = change_date;
                supplier_ledger_paid.Tran_Ref_Name = "AdvancePaid";
                supplier_ledger_paid.Tran_Ref_ID = 5;
                supplier_ledger_paid.Credit = advanced_amount;
                supplier_ledger_paid.Debit = 0;

                supplier_ledger_discount.Supplier_ID = master.Supplier_ID;
                supplier_ledger_discount.tbl_supplier = tbl_supplier;
                supplier_ledger_discount.Transaction_Date = change_date;
                supplier_ledger_discount.Tran_Ref_Name = "Discount";
                supplier_ledger_discount.Tran_Ref_ID = 3;
                supplier_ledger_discount.Credit = discount;
                supplier_ledger_discount.Debit = 0;

                if (total <= (advanced_amount - discount))
                {
                    master.status = "Paid";
                }
                else
                {
                    master.status = "Due";
                }
                db.tbl_purchase_master.Add(master);
                db.SaveChanges();
                var query = "WITH x AS (SELECT *, r = RANK() OVER (ORDER BY Purchase_Master_ID DESC) from tbl_purchase_master) " + "SELECT * FROM x WHERE r = 1";
                var data = db.Database.SqlQuery<tbl_purchase_master>(query);
                newrow = data.FirstOrDefault();

                supplier_ledger_buy.Purchase_Master_ID = newrow.Purchase_Master_ID;
                //supplier_ledger_buy.tbl_purchase_master = newrow;
                supplier_ledger_paid.Purchase_Master_ID = newrow.Purchase_Master_ID;
                //supplier_ledger_paid.tbl_purchase_master = newrow;
                supplier_ledger_discount.Purchase_Master_ID = newrow.Purchase_Master_ID;
                //supplier_ledger_discount.tbl_purchase_master = newrow;

                status = true;
                var details_for_master = db.tbl_purchase_details.Where(a => a.Purchase_Master_ID == master.Purchase_Master_ID).ToList();
                foreach (var item in details_for_master)
                {
                    tbl_purchase_details temp = db.tbl_purchase_details.Find(item.Purchase_Details_ID);
                    tbl_stock stockAdd = new tbl_stock();
                    tbl_item tbl_item = db.tbl_item.Where(a => a.Item_ID == stockAdd.Item_ID).FirstOrDefault();
                    tbl_stock_location tbl_stock_location = db.tbl_stock_location.Where(a => a.Stock_Location_ID == stockAdd.Stock_Location_ID).FirstOrDefault();

                    stockAdd.Stock_Location_ID = temp.Stock_Location_ID;
                    stockAdd.Item_ID = temp.Item_ID;
                    stockAdd.Stock_In = temp.Quantity;
                    stockAdd.Stock_Out = 0;
                    stockAdd.stock_change_date = change_date;
                    stockAdd.Purchase_Details_ID = item.Purchase_Details_ID;
                    stockAdd.tbl_item = tbl_item;
                    stockAdd.tbl_stock_location = tbl_stock_location;
                    db.tbl_stock.Add(stockAdd);

                    decimal vatpercent = Convert.ToDecimal(temp.Item_Vat);
                    decimal quantity = Convert.ToDecimal(temp.Quantity);
                    decimal price = Convert.ToDecimal(temp.Purchase_Price);
                    decimal vat = (price * quantity * vatpercent) / 100;

                    temp.Memo_No = master.Memo_No;
                    temp.tbl_item = tbl_item;
                    temp.Item_Vat = Decimal.Round(vat, 2);
                    //temp.Purchase_Master_ID = newrow.Purchase_Master_ID;
                    //temp.tbl_purchase_master = newrow;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
                if (total > 0)
                {
                    db.tbl_supplier_ledger.Add(supplier_ledger_buy);
                }
                if (discount > 0)
                {
                    db.tbl_supplier_ledger.Add(supplier_ledger_discount);
                }
                if (advanced_amount > 0)
                {
                    db.tbl_supplier_ledger.Add(supplier_ledger_paid);
                }
                db.SaveChanges();
            }
            return new JsonResult { Data = new { status = status } };
        }

    }
}