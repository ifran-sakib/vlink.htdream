﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PMS_HT
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalFilters.Filters.Add(new AuthorizeAttribute());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;
        }
        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    Exception ex = Server.GetLastError();
        //    HttpException httpex = ex as HttpException;
        //    RouteData data = new RouteData();
        //    data.Values.Add("controller", "Error");
        //    if (httpex == null)
        //    {
        //        data.Values.Add("action", "general");
        //    }
        //    else
        //    {
        //        switch(httpex.GetHttpCode())
        //        {
        //            case 404:
        //                data.Values.Add("action", "error404");
        //                break;
        //            case 405:
        //                data.Values.Add("action", "error405");
        //                break;
        //            default:
        //                data.Values.Add("action", "general");
        //                break;
        //        }
        //    }
        //    Server.ClearError();
        //    Response.TrySkipIisCustomErrors = true;
        //    IController error = new ErrorController();
        //    error.Execute(new RequestContext(new HttpContextWrapper(Context), data));
        //}
    }
}
