﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PMS_HT
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Order",
                "Axd/RestaurantOrder/TableEdit/{Table_id}",
                defaults:new {controller = "RestaurantOrder", action = "Edit", Table_id = UrlParameter.Optional}
                );
            routes.MapRoute(
                "Pay",
                "Axd/RestaurantOrder/TablePay/{Table_id}",
                defaults: new { controller = "RestaurantOrder", action = "Pay", Table_id = UrlParameter.Optional }
                );
            //routes.MapRoute(
            //    "Order2",
            //    "RestaurantOrder/Edit/id",
            //    defaults: new { controller = "RestaurantOrder", action = "Edit", id = UrlParameter.Optional }
            //    );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrEmployee", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
