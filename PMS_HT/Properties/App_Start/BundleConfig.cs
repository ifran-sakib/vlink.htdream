﻿using System.Web;
using System.Web.Optimization;

namespace PMS_HT
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                        "~/assets/global/plugins/jquery.min.js",
                        "~/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                        "~/assets/global/plugins/js.cookie.min.js",
                        "~/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                        "~/assets/global/plugins/jquery.blockui.min.js",
                        "~/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                        //END CORE PLUGINS-- >
                        //BEGIN PAGE LEVEL PLUGINS-- >
                        "~/assets/global/plugins/moment.min.js",
                        "~/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js",
                        "~/assets/global/plugins/morris/morris.min.js",
                        "~/assets/global/plugins/morris/raphael-min.js",
                        "~/assets/global/plugins/counterup/jquery.waypoints.min.js",
                        "~/assets/global/plugins/counterup/jquery.counterup.min.js",
                        "~/assets/global/plugins/fullcalendar/fullcalendar.min.js",
                        //"~/assets/global/plugins/flot/jquery.flot.min.js",
                        //"~/assets/global/plugins/flot/jquery.flot.resize.min.js",
                        //"~/assets/global/plugins/flot/jquery.flot.categories.min.js",
                        //"~/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js",
                        "~/assets/global/plugins/jquery.sparkline.min.js",
                        "~/assets/global/plugins/moment.min.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js",
                        //"~/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js",
                        "~/assets/global/scripts/datatable.js",
                        "~/assets/global/plugins/datatables/datatables.min.js",
                        "~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
                        "~/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
                        //"~/assets/apps/scripts/jquery-ui.js",
                        //END PAGE LEVEL PLUGINS
                        //BEGIN THEME GLOBAL SCRIPTS
                        "~/assets/global/scripts/app.min.js",
                        //END THEME GLOBAL SCRIPTS-- >
                        //BEGIN PAGE LEVEL SCRIPTS-- >
                        "~/assets/pages/scripts/components-date-time-pickers.min.js",
                        "~/assets/pages/scripts/table-datatables-rowreorder.js",
                        "~/assets/pages/scripts/dashboard.min.js",
                        "~/assets/pages/scripts/table-datatables-editable.js",
                        //END PAGE LEVEL SCRIPTS-- >
                        //BEGIN THEME LAYOUT SCRIPTS-- >
                        "~/assets/layouts/layout3/scripts/layout.min.js",
                        "~/assets/layouts/layout3/scripts/demo.min.js",
                        "~/assets/layouts/global/scripts/quick-sidebar.min.js",
                        "~/Scripts/GlobalClock.js",
                        "~/Scripts/Sites.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include(
                        "~/Scripts/Chart.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.12.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/loginlib").Include(
                      "~/assets/global/plugins/jquery.min.js",
                        "~/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                        "~/assets/global/plugins/js.cookie.min.js",
                        "~/assets/global/plugins/backstretch/jquery.backstretch.js",
                        "~/assets/global/scripts/app.js",
                        "~/assets/pages/scripts/login-4.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepaginator").Include(
                      "~/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                      "~/assets/global/plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/assets/global/plugins/font-awesome/css/font-awesome.css",
                        "~/assets/global/plugins/simple-line-icons/simple-line-icons.css",
                        "~/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                        "~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css",
                        //< !--END GLOBAL MANDATORY STYLES-- >
                        //< !--BEGIN PAGE LEVEL PLUGINS-- >
                        "~/assets/global/plugins/morris/morris.css",
                        "~/assets/global/plugins/fullcalendar/fullcalendar.css",
                        "~/assets/global/plugins/jqvmap/jqvmap/jqvmap.css",
                        "~/assets/global/plugins/datatables/datatables.min.css",
                        "~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
                        "~/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css",
                        //< !--END PAGE LEVEL PLUGINS-- >
                        //< !--BEGIN THEME GLOBAL STYLES-- >
                        "~/assets/global/css/components-rounded.css",
                        "~/assets/global/css/plugins.css",//culprit sort icon
                                                          //< !--END THEME GLOBAL STYLES-- >
                                                          //< !--BEGIN THEME LAYOUT STYLES-- >
                        "~/assets/layouts/layout3/css/layout.css",
                        "~/assets/layouts/layout3/css/themes/default.css",
                        "~/assets/layouts/layout3/css/custom.css",
                        //< !--END THEME LAYOUT STYLES-- >
                        "~/Content/jquery-ui/jquery-ui.css",
                        "~/assets/global/css/CustomGlobal.css",
                        "~/assets/pages/css/error.css"));

            bundles.Add(new StyleBundle("~/Content/datpaginator").Include(
                        "~/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
                        "~/assets/global/plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.css",
                        "~/assets/global/plugins/bootstrap-toastr/toastr.min.css"));

            bundles.Add(new StyleBundle("~/Content/logincss").Include(
                        "~/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                        "~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                        "~/assets/global/css/components-rounded.css",
                        "~/assets/global/css/plugins.css",
                        "~/assets/pages/css/login-4.css",
                        "~/assets/global/css/CustomGlobal.css"));
        }
    }
}
