﻿$(document).ready(function () {
    $('.portlet-title #Add').click(function (event) {
        event.preventDefault();
        $.get(this.href, function (response) {
            $('.divForAdd').html(response);
        });
        $('#Add-Model').modal({
            backdrop: 'static',
        }, 'show');
    });

});