﻿$('#gPDF').click(function () {

    var dt = new Date();
    var year = dt.getFullYear();
    var month = dt.getMonth();
    var day = dt.getDate();
    var hour = dt.getHours();
    var miniute = dt.getMinutes();
    month++;
    var months = month.toString();
    if (months.length == 1) { months = '0' + months; }
    day = day.toString();
    if (day.length == 1) { day = '0' + day; }
    hour = hour.toString();
    if (hour.length == 1) { hour = '0' + hour; }
    miniute = miniute.toString();
    if (hour.length == 1) { miniute = '0' + miniute; }
    year = year % 2000;
    year = year.toString();
    dt = dt.toString();
    dt = dt.slice(3, 24);
    var user = document.getElementById("userName").innerHTML;
    var invoiceNumber = year + months + day + hour + miniute;
    var name = document.getElementById("Name").value;

           

    var Phone = document.getElementById("Phone").value;
    var arraivalDate = document.getElementById("Arrival_Date").value;
    var departureDate = document.getElementById("Departure_Date").value;
    var checkOutDate = document.getElementById("checkOutDate").value;
    var roomRate = document.getElementById("roomRate").value;

    var discount = Number($("#discount").val());
    var discountAdjustment = Number($("#discountAdjustment").val());
    var totalDiscount = discount + discountAdjustment;

    var refund = Number($("#refund").val());
    var vat = Number($("#vat").val());

    var previousPaid = Number($("#previousPaid").val());
    var paid = Number($("#paid").val());
    var totalPaid = previousPaid + paid;

    var fileName = invoiceNumber + '.pdf';
    var doc = new jsPDF('p', 'pt', 'a4');
    //load logo
    var imData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAACMCAYAAACqNZEDAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAH2dJREFUeNrsXQd4VFXaPjMkAQEFFVnbYhQU10UJLnZcYl0LJQg2EA1YUFw1wAquNay9B9sqP2oQwQoEu64lVlRaothRg110FRRhDWTm/17mPeZwvZnMnTl3Zm6Y8zzfc+eWufeU93ztfOeckNpA0uQxo8Ny2EHoT0I7ChUK/VHoD0KdhDoKtRdqIxQSigrVC60SWiH0X6FlQl8ILRX6WOg9ofdPu/G2X9UGlkItGCgby+EQof2E9hLqSWDYTr8I1Qi9KfSK0MsCpO9ywAkWWLrKYaDQEQRMG5fHwEU+IX0m9I0QGnq50M9Cq8ltwKEKhNqSG20utKXQtkLbC3UT2sTl/RGhWqEnhR4BoARI0Rxwsg8sW8thmNDxQkWOMkGELBCaKzSfDfqRNORaC99VFHW9CdK9+bu149HPhR4Umibfrc0BJ/P6yuFCp/PYyrgNPeQJoUfR66WxVhv/Q6PuTD0H3Gk7oW2EOgttJgTx1o7cRus4qw0d51sCoU7oQ+o4n2qOIu/fSA59hP4m1E+ouyPrAPEdBNH/csBJH2AgekYIjaGCq9MP7NUPUMdo4PPgCMVsTOg5uwjlW87Wd+Rorwm9JDRPczT5Pr53LDliV+M/HwndIFQZVACFAgIYcISxQicKbcXLXwvNQEMJVcGy4XMltJgOEtrDUUboH58KvcvG+8nQe37gOZTdBv6vgByog9Cm5EQh6jrgJj1okZkJ3OlZobfJYZ7h+//KPCF/uxlluEdouuT/7Rxw7AEGrH+c0HiKEaT3ha4GaKSy6+WZfDZGqdChQnkOi+dlcgJwhQXyn58t5k+Rk+xPOoCKs3Jww/uF7pRvL+R/oI9dTLGpCNRpQhfIM1/lgJNao8A6qiD3UNQpLoFIksqNkLugFUaRA+gEYDwm9DCOAFea870j9ZtBVJrDxm2Y7DdRpEYoxsqFduL9lUL/Qrkl32tywPFW8RBFt7LitRjB+XjoDnK/i/yeQF1nI6PHQiRUQik2FeIMlwWm+1DmdWfjFsTrRKHHySFHEUBa7L0ldIqUY14OOIlV9HEEyWa89JzQmVKBH8i9LeT3haxkbfL+KPR/QrfJM0uzmHsqirGzhfobVuDrQudJ3l+UZ6BDXS50Gu+vpUiemI3cJ5QlFduegDnR0AugDE+VSsP9/tQBOvA+nHbXwbSV+ysDZhnuRBF8OC/BlJ8DXU7K8onc3wf6EF0GWrwdj3s54KxfkaigmUZFPS00EkoizelbhAbw3nL2wpvk/qog+0GkbOBAVwntyUuryXGuIce5Qugc6kgo96HZJLpCGa48AOJeWkxgx+cLXc9eeAp/a7c+HGcXSuV9r1pIkvKHqAMBLFvzMrzLJ8HLLPcPJaftTFfBCXL90Q0WOJT541hh6FEwQY+WSnlN7nUiqx5gONjGyL3pqoUmDsiC25zJ+sBQyQUq5iQEoB4S2oeW2Lm4DhG+IQLnUiq6WoYPomjal6bqtrwHEXbGhjDazHrpQ8tQe5lhdQ2nPwqDpgfy+iR2powNnoYzUDmjKJKQZgsVEzS4/gJBg4oaKTRkQwENkpQVYRkYqL2Hl45UscHZnegbms3r0H0qpc5abRAcRwp6Ni2KEHWW0bx1AysDCZ7hwVKJ76oNOEldjaSlifE5ODX7Uv+5jS4JJIzPDbMx2p+1wCFH+Te/eROBAn8MxpuOMljzUKmIn1Quoc72IJfBCP4ygwNdRx0RCbrfifCmtyjgUBG+lOIpTND8U8ViWaD07SqEEeKzhO5KdwWkmobsvgc8v+1J7YxjO8e15x5eOO+9JOqvM10QpeQ8F1HHwflkFRvtf4pcelVLAs5YmtXapD6dMvsFWgyojIFS6BeSbLgjVGMcjdlQ7WnK60bcSBruQMugGUzwJ1KPJ8j3p6fQ+Wap2DAMFOIJUl/XyvVh1IfQIeFEPCpdHS/PZ9DA43utoQiPpnh6iKBBCMJhUtjXU/jM/apx5Dxe8sMCWZ2Ozkfv+VBamegoV8v5arl+CzkSdMSB5EznBtqqkgJhUO9efgMez2G8hWu7kdMcniJoFC2whLircAjbVsjKdHVSBnxBF/wPwToJY3ty/UYV866v843JtRMCCxzJfDuyVogKOPdKOGINh98QOrhwbW6QGi8F0NriPOvqTcVii8I0yREHhIjIZ7W1Ktd6BJXjwIzE2BOGEY6hnwbm5VjeP1uuPZ+BxsvPIMexBR4owFABPqTYn0nfF4LDECGAWRkPSn23DRRwJMMITjqJp+dLQV+lWXkrewQGMe8MorhIEbQhi+D5L01zRBFsQe6+kuBZw057Q2CAYwRhISGw6nq5hjlJD9CRtUToOB1MnoHGy8sgx2llmfOsq0sVC2LrBZ2Hor9cP8L2CATHgZKGiWsIsBpJSwYm+Pa0QDCEsDyoCmqmdRwX8PzHAMqpApQhtKzmksPdmvXmOEMktAd4rBTqS/oZjtEav08T0rw03mZiWcE6ieeij3cvQjJNfIS2FmQQP4jbQWwPfFS3q9g0ZISqYlryIMRuS73PyUqOw/lOk3j6PLV9TOa/idcwQe7fPlWcF47zDrnhz3FodRz6lTqESQXprGsXrhOhTvkjuT3CaD8goJAqOFskK0XVP1RsNgJ635mMFYF/AbHDEE2n+hg/klFxkQUWLMDzBU1yRS4ziK6PerbLuKwrDD2X47UZLoV4X64dSMUNaYLPc4WCEnP8BxGTfnqZp9KXs47LUBnXOs54tlNW9QL4ZjamXwEZhNPvfipnmKE42ecGCQpwMB2mQcDzqtBhQra5Dg5Hq9goehcaJePZLmifyVkDHOo2emZCOeNCxtO3AEfVBL9aAb1XCKGWF/nw+hqKWNsJnQlRjojmmyv5/6tl8CDPF/MUY1v5htU1QNqrKFusqlIVm8uN4KsHOPdJB2RhNuKXPoGmC1lzcTL/j0ajKhoOLY9GolUN0UhNfrhVzc/19eqZxbUvxvlmX+oLoCIxp4pDsXVzkk1YFqVa3ovR7XMeXjhvhaXqmSJUpmITAEdT19TTjRHOcqytHpAstwkTMJjyiuksd8u1K+X3eeyt2/vgs0ED9iNoNvP6X5gf0ZCqbAipikfmz0vZNdCvZ6+erfPzy0ORaEmKr8KycCUCnsWWrFy4QOB0xfyzHQiWu+lq2FHapS6ToupwggYrLkynbnMG791sGzTQCYQQ+T8nGdAIl6lpCIeKZi+YN8IGaJAeq11UW5CXV2PhVQhOf1nKt7el6sK8eQSNbUmpMIPtBAlzZqZ1nNN5nMGJ/Ser2EzLVYb/xmbCVN/Lks1zKBSqmjP/TesOyHC+NR8qxN7jAp7ulnw71/C0jP6mO7R6wQWm0g8cLp92GE/ncWKZDjyfZnvSnFQmONkp2emdsToEBU76sJS3jYV3gcvAs41oy0Op+0BUYd5aSaY4zjCyPYzOVqnYgkHdmNFbLYMG/qBbstXGbog02H5lDyqxqXIdSIFX9SkNlWd4fmKmgHM8jw8yuKiU52/YXFlKQNObSl04W4Gz5ldflt8ZJ2XvZENP1no8Z8hO4/khcr55WoHDJWG1P+ABRvtp1ldpETR4LxyJbVQWp4a1DX68th11xlQT4nQw/obxNDgHsXzuL/TvDEw3xxlIMx4eSiyTdiQLWk9t3la6UK2/4GJWpmjUt0kFx1gQV78QLOvex+jBp412TCtwjuDxCQZk7cLzakam2eA2YKtne/xbTWaQ49ubi6QeNrFkmiP1objSq10clIp15Qk49NXsx1OdgT0dGbSREATmJWa2lEp62pOPo5ZhKsqpJijEP9GYgfP0KZrnkBJ908VxDqbOAYX4SQIJ+yU0WAbOII/Pb6QylPLCnqoQS+V6kW1bWhBXEE+P8RQT9uBN1lOS+iVdbo/P9+FxPqa7cFWpPJ7/aLE9tvf4PIY66uI9sDaSFTOLETOzUMUCrzZlz6+nf2WNCy2w9F2spYhBz/05VATddH9DevgOHC2W9Hwo/eGXMtwgHQ1LL82qiEeFY+E8LOl/WZo/+7JRTz0MjrMrogOTWaU1YT5LpOrGme8A0uuWC/pxkJSRAOQCq8h/b7SZXksQZvmufnOcrlSokGo5Eb6XA0i2EkzGfe1qmqFChkbYToUens3IQkicew6xh81Jesn5FDnHhiaIC8d07Df9BI5eFRSKMeb0IA5nc2rsn1ouKwY0J3i0rOK3WChUqho93BtiqiVwNId5h8D5k99Wld6t5RNG+ukPvmc7EF30AIQATFS5ZDPptXn0Cu8f8tjNb+BolvyJIbq0/PQjYXmUWS2s8fIy+G3dTp3oRvkkSQvWM3D0SqCf89iFx898sj6iNCEfyTELK6nOUNC7GO24rd/A0c6or3nUCzr7tjuLgAf61FGqcXGmXEo+wfHXYLSdbseOyQw9eAGOHubXy8d2TkdpBTwNQpg5AefZLwFvvIwtL8txxe+NtvvO4ECd/ASO3oBDxxJvymM0TQCqoEKOyLZIQIGTaW/Sj0bbLXdpW1+Ao/fs1hPgNnacpwM8nwsh+hD+o3t9cAO0ZOUYaYXRdj+7tK0vwNEBVXrzUT2wmPZdXAQ8bwkNpymJgbrnAgKc24fsvsedQttm6PurjbYzN5HdyE/gaDYbcfw3Y8NAAp6IEBbVvi0gwEHnQ8jIBwKeSUJbp/n7aw3Ol1K7eQFO1AGgaJbIbaQ/BkxkwSOOQLUlAp5rhDZNs3Le4Gg3zyDyInN/ZY9pY5wjtQ44cOpIrvPFo9Fox1AoVMTfxfLbZr4hIrAu8QgBD+bATwYX9bGe9Fo+znV96v0EziqCpq1Dt2kbFOCsaWhQK9fUL49EotWt81pVRVSo+pnFtZ728jyqV+/t5FAsIEKAfkmrsJUJGDCHsfDUSQKgkcks3Z9g0oPUvxi/k9JTvQAHGjkmjHV0aOgFQQHOyoa1pc+9u3hqKh+atWg+gIZ3TN1rx506bNt+kxLhQhUqtQUIdMIU4AUCHvis7hAA2a4ns+1ME9zzpiteuosORNfzcbJpi8MuiTy0aUHrOpsffeOjD1fMXDR/qrIbKA/xhbX8YH3ZXpd5C6PtOrm0rS/AWcajHnr4JhuUY+7esqVqeQnW1xxL04H19o1aPOlYHC2mfvYTOF/wuK3jfIsMV/A2KoOufJ8TVgS5T8BjQ5EyufLnhnj/MpmwGC8Z0kqkHoZ3hlfkTPHErDevCUr4hRa+39WwhjGPvNDRrr4BR8cBd2PYqA4E6p4DTsLAgVlfrrwvE3eBcJ0dUvy+DuD6mMug6MC8Jcm8zItVpU3EjdlYH9Ef0AWBQaluh0hZXkC/UIELtSblUy6/QqsjMM4/Ltc2UcoKEFV69L8cZHD5ZJKe3Kf3OtUzcN/3m+N8oBrDGvbgMho1fMeeFur1AZqJy6g/fcJCvaViwfBYsgOLbyOQHdM9npMGGGH0HBUgAMESK/X4txEWTH2kN7lShVY5FvoKHC5nos1OPQNBr79iY/XMGz0+j7WU71J2VnXIFHi8mPG9k1WSuRlIN6PN9mHbr1FJzlDxmpE3eNyHx9d43N9CXb6okotfDqngJi+6Tr5Kfr+tPqwnjIgvMLjP4mQm4yUDHM1h/sL9ATCDEwNke6e6XwBjjO9UG1Yq9PDsDwylTSbpTWzfoOTQEuKVZDPuFTgASoRK6v6SCTiSsMQqFFsbk92mkn22+CRiZ6BH4LycpJjC4W88fZYLYe1lcHn/gcNFIfXKnb9lhsd+qVam9Ch4ox8LGgii3kHTU3lfvez+JLP3Z0MRfpLcB1ZaA42NtHAcpCd4PJJHvXp6CVcfTTVNCRxwop7DWbwOiq5MgTvoPcS+oAWlO/jrqawwkgxw9IJK3QUoQPPjPN/GkpKMhX8+DxJwIgkCRzjNdkKLlPetBMZzdmsyYkovwT+LCvIARzumDThvGg2LdeXga1nE8+EWxBV0qLtbIHAKaX573YgDel+ym8TtTlGFdB/10C0pXWelFTgCFHz0QZ4OI6rv0UCS8/YW2gL+mVWBAY5KGDheY3awPN2pKcTlaB8XhoewFI3ezH6htONH6eY4SHq93K407e6ljwBzkodZ4DoYeMOqCk8HQseJ+BKvj854tNRFKlambgu4OdChhzjaL73A4aasepmxg2ht6TUAz7KhJEuFYcgBy/5jMe5vWoKOk2DCTATEIZ8kdbA22ZdIG6DjdWCHvov1iI4NP870jABH543HgRRXkyg7IVOPsFGDYNFCMEMxg7M62wDTa4eu2/Xv2eucjfLzbW0ghs7SV8p8nYWw0X/wiJmv6Nh/5/lMG3ttpLJfFZx+GIDEIONQycx9cg0KGPZewIDornLNmjOPWxHCGrlEJbnBGVO12zm4Rn1Dw/I2TWwjJPcK88PhQsxywCZnkUhk3eyHsJ1ZD1jxAzE3swQwKc+Pl3bYl+XCQHR3qhMzyM16cJfgzACHGcSuLggfe5+cBgNpGLZHRN5oyaD1LaMJoFIVW8Gikwp2gn/meqFrBDBWjAFy/2paUHg3FmzA6luIx3lA2uQ4G99JNSQRZvPXzNSxkqkPDcXrYksWlpv4qqT4mqqyZ0FRLwlcBU7AblKWclugYTpSNQ7/XE0/zs6sp6tsfSRkAeHIzASafOA6W1NUQZRdIWC6wM8WEA50AK26rQMAmNX0p1wiYPnC9sulLfKpPgAoL6jYPlXgNtizao60RYmtb9kIgobsbGDmsC8S5PUNvDdWCuNroJU0ACpobkA4DQBzsh+gYdKbuKI9sEn9KLYLzq3ulpwycAQob7HHI00UoGAe9BUq5l0G17mdctdvXSEIaa1fL5Y63p6GAxJWbYU3v1z7bWzuI4Zka72W84UGU1m9XDI5Wgpylop5PjEae5qyvGG6i86QkiuG7wAAfzJ+/+L47TwivOQmj9/xAzQhgqUdfV56m+hOzKt1dSFkMfPjqYyBLSJWZ65cQxwx9l3ChC8szPyxHxUnes7VtB4SSYOpj5kAWJWM30S+i/rD9pKJDiWUyXcm+QAcrHwxySjf87RuETI6Xurd+hqKNrcsRMzw2zTFp9DPcyZ7AGZGzJBrBRnmOLAsnsD+3kKfCn0HiyZZZxujFheqDCap057ssEjTBSSzqGNuRUW5wo/vWgMOnX0nU47vQpH1PX0uYNF7Kv9WD00UON9KY//P8re9ACdqGTQYQniQuiSWtTtTrg1QjTMoTrHphPWL4wA88wz0l0khDpVrTxvXMI413IdyJKocf5bBbyvVuFysDdCEaZTAavqVOg10nClUQV5ie6isB462rFRsdxK8exr3KIcp+CwLNFmudc8Qx1nqQ3k/U5lJ2KOrv2GGL6JrBHP5MXviRD8/bh04ZI3HMfNYT/ch6j3oEUvIVisFPG0z0Ov9AM6CdCOGQz3n8vR2qfPbydX7UhxCRC0NFHAIHozyHkprCgNuiFNezYJBJ8C8nlelAmytvpno9GM/9p2AZzbR9WUiKQIGhE3Sbib3voHiH9xnDEGDMcKZfoM35HPPgKI2mwDFcQhZKXrpNmzIA6SgX6ZojkP0weno3Jqw3nE+TZTjD/wss+SllaNezfM1WCk+WdBIuk7FPMKKusypQmNVbDBz3X2py3PTwfV8nwUpBR7HAiPdIXS6ik2tqaLYgm/nYClwncqlePV4uYo5WhV1GRgZCM66hx1zjopt1pqWVefDafjG9YZzahRBBEtrEMUXwk9fkYrZLQePJkGDIKwJBmig+IKb3802xFTsoekCTVo4DguO71Qamv4IKSQU5GL2lE2opyA046kcVNYzua+lOFp3SegMggZeeThU36K4/yGdeQulsRJasbccQ30D4Jku17EvA2ZvwmyH8/A8cCnbu+4FEDQYLEZs8OFaf6ElNYycBiEU7xI0y9Kdv3C6PsRtb4axMlDoe6Ryxsr1RbSycMxjBT0s9zpswKD5i4r5wjRobmGHGkOdJt/gNMsykcdQBioFYMXsz8N46VY6sApoKRzP61CWh0vFvLIBASZMq+ky1geGR/4udXCn3IPV+E8+Cp2mf7rFU0aBwwpqS86jI9Kw+wuchhjbGk2FGhZXAxXri6SSVrVw0HRnx+lj+JyOJWe5mnpOiDrh0EzXRzgTH2WhB1MswWl1EH07+8g9KDcYEK2lDwQVtlgqdpcWCpg2QgjAqiFo9DpBmL77LTvVOEPPOSobOlEoCypuGK2FtlSaMdZ1FUFzAWV7Ae/Btf4vG/OCsqDcIRoK8PrqZUgQNXm6lO8Jud+fANpCNXqEb8+W/IeypBKxIibCA/Re5oghHokFDchpoAcV8x4WmESMSYXcXx5Q0BxMwPTmJXQKDCNcQgMBonoE2wdlPCUdwwiBA46h96DCRqnG9equpHz/lZwH1MEAEHrgzakOWaSpfDjsp2IzLAdQTYjSUICZ/T450I2qccbGyzQQlmZbeUJZWMFb0fzUCwLVk+OMp5/ncPbMvYy/fUnfxlSp5CVZVBaAA9N3RrI8el+GH6n0g3MiJOQ0img9wRCRlCf7GU/T4oBjVPoAVm4hLyFOuFzFPKYRNsg4Asnc5hHr9yCUYzZH6dOd71bkLBhSOVrFBnOVwSWxAABM6x9oNaFMO/E+wkMuBdfxK3KvxQNHWxxk7eA2etdhsPRrVCy+tl6ewVgXRomHq/Un5cGUn0erBIteYsXNFT4B5c+0iA6ghWhulajBDMDMIDjgq7pYNS6Tj7wimu98yeNXQdDTArFGsDROZ1peA4w8Y+oxRtunQMdhAx5IPWGg+v2uNhFyLZj579JPUqdia+N9y5Xi4+knGE/DalbYhQX7KsDv0oMK7maOv0QpbhA4fj8m+XM6NACDAP6exnOPAEScnxaYFKjFpaXyi6ggD1aNc8Kg9zyjYnPWH4GPg7oFxMUQWmM9mvFZRalrgCOt5jvDdANAae+omt8JcBk5GzjcUwgTYT4QvHYC87KJkWeA6kp5rkYFMAVyVXJpEOg9o2mymitWoPExuv4oG+9bPg9LbA861Xal2d9VJbcdYoTKODjWexRDGAJYgoFZriMMztePHNLchA3W4Uz6oj5QAU5BXs4ejYSZlBi2QLjGIWr9JeuhXL5OLoDjfGmsbxz/70DltTP1EnAErBDfilwI4msVORHCQ/H/r0yxJu8AcLFFAQZq96e1V+DgZguow0xvCc7LwAPHAYLNqduUsMe3c3kMDf8OdR1YXJ9TV/qOjraV9B9FWTetDVEFgGA7QsRJw9OLxRR2odXnFIMNBCs438xschHkgNM8J+pLcdGHOk6+j59cQ2UYe11UC72QyuLTOeBkD5AgfnajfgMu0Y1cA9yjg4d6WEUrDJ7cJXQNYNbGgmR3YckBJ7igakMxBAC1p3gKGcrsbzqOgGOlyqVcyqVcyqVcyqVcyqWWqxwP7tV7O2XsSDtz0fyJzofkmZNU4yh1tTzjuneSPIcxGB1HjOfrSPjPUsezMJeLE8znum8m8h+d/0TKlUiS93RgmQqNy65lMv4Df1KRM/9NPHtJImV3ebZSf9+lXurk3lSXb5nPeclThTy73gBxHiuk3LjmVsGlxgfx7IsugKlUcbbUkWfKHY1X7PhuvKS/mch/JhrAba5czYEGHabJTcnkPip0jMutCgfQqlTTG5WVJ1h257PVqnH1jd/Vi+StRvJW63iX+Vy5W54ILmee0FHWA2LKweoETbVqfh+mcnk2MPtQkWM1t5NdTRPcptBxuYScK50p2SXcSl2u/W595LClDHZ09K5iUinR+lumiGhFDlVsUI2jQcx7lU00WrEL2UolRrmWG1y3hPmpY1njVXJdMw3iTGUu5alMMv/FBHEy5VZGuV2Bn2eB2xQ75O4I4xx6SRUrsKNROS9SPi813mUGni9vSv56fCaVZHaGGofOMCfBiq8wej6A09yKozWWy1Qh9Vrt1E/itOdAo9w1pFK3/Ict9Mq48pqZrozH9gKQiqnveKp4Kfsko9cWUfz5ncy6LmRHTaY9Kx0ctTQux5HCveDywqIEemVdU1YGdaAyU3+I82yiqcglr5Vu1kQKDWB2hEr5HiqvPA5XKHGUWR9LjPuTmimTs+N55UB15HK6vsvknZXN1TdFUakj/3WOvP3Wbm4cx01vaEpBLGpCnv9OrDjOCy2JEmc+C211W1ZQqUvdVAOwTu7hUvGVhs5n6jDN6YvVDkomlRt13jFB67XEwQRqKS2q3J7JyBTgoCRyr2KXBlynzDvA87uKdwFOIfVCv/O9wgFS0yhJBDhVTfwua1JUyUdDTYiv4iZEUHEz4syNw9iIs4XSd0AaGgGi4gBWfIVRTt2TR7hUPAASjWPujokjHutsgV7yUGbkt6IJK9B0cipDvLlxx3XAR6dIdRMQs5AdUblNyOQShzW0IoDcB+XqJWWcrdb3jrtVfHM9u0ngWLaqygxuWeTByGnOzzMmVVFV7WL+dXCg+aQ4bDBrE/LdhFipScBpVh1HVylMQGzYBHtVAkZOmYMZOPNe5wRZXooZWwq3u/HhIsr+KipnRS4OpXJL9VLoNs7T1JhUE2NCdXGssFKa4XWs/OUUT6UuHNe8ViXvHOTy/U8NkY3nX0xTHyiLx1Gop5mAKnfWCTtQjQl8G/tVlVPPKUrAd1BmwQw39SY3EE6Mk083jjk1ge+UNWEpVrhUfFMctcp4T4mhG/nNddC5y+N02BKXfDrfUcsO9BvwwxYytkKol8MEdFWiLfpY0pGqEihPrUvFVycg1jsmORyQbKqIU5ZSh8GxIoH8l1iPOSZbc7rrA6cMxylPnUWuGdj0/wIMAB44E7+L7GEwAAAAAElFTkSuQmCC';
    doc.addImage(imData, 'jpeg', 30, 10, 88, 88);
    //set Hotel name
    doc.setFontSize(30);
    doc.setFont("arial", "bold");
    doc.setTextColor(84, 50, 53);
    doc.text(180, 45, 'Hotel Safina LTD.');
    //show date
    doc.setFont("arial", "bold");
    doc.setFontSize(10);
    doc.setTextColor(0, 0, 0);
    //doc.text(240, 50, '50,Jubilee road. Chittagong : 4000');
    //doc.text(265, 65, 'Chittagong, Bangladesh.');
    doc.text(205, 60, '50,Jubilee road. Chittagong, Bangladesh');
    //doc.text(257, 75, 'Chittagong, Bangladesh.');
    doc.text(205, 75, 'Phone : +88 031-616476, +88 031-614317');
    doc.text(255, 90, '+88 031-614319');
    doc.text(490, 75, 'Invoice Number');
    doc.text(432, 90, 'Date : ' + dt);
    doc.text(120, 820, "This is a computer generated invoice. For any query, please contact with us.");

    doc.setDrawColor(255, 0, 0); // 	draw red lines
    doc.setLineWidth(2);
    doc.line(30, 100, 565, 100);
    doc.line(30, 810, 565, 810);	//footer line

    /* Name table begin */
    doc.setDrawColor(0);
    doc.setFillColor(146, 158, 178);
    doc.rect(30, 103, 535, 20, 'F');
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(1);
    doc.rect(30, 103, 535, 70, 'S');

    doc.setFontSize(12);
    doc.setFont("arial", "bold");
    doc.text(35, 117, 'Guest Name'); doc.text(135, 117, ':');
    doc.text(340, 117, 'Mobile : ');
    doc.text(35, 135, 'Arrival Date'); doc.text(135, 135, ':');
    doc.text(35, 150, 'Departure Date'); doc.text(135, 150, ':');
    doc.text(35, 165, 'Check Out Date'); doc.text(135, 165, ':');
    /* name table end */

    /* room section begin */
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(30, 190, 535, 35, 'DF');
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(1);
    doc.rect(30, 190, 535, 175, 'S');
    doc.setDrawColor(90, 104, 93);
    doc.line(30, 245, 565, 245); //rows
    doc.line(30, 265, 565, 265);
    doc.line(30, 285, 565, 285);
    doc.line(30, 305, 565, 305);
    doc.line(30, 325, 565, 325);
    doc.line(30, 345, 565, 345);
    doc.line(210, 190, 210, 365); //vertical lines
    doc.line(310, 190, 310, 365);
    doc.line(385, 190, 385, 365);
    doc.line(455, 190, 455, 365);

    doc.text(90, 212, 'Room No');
    doc.text(221, 205, 'Room Charge\n    Per Day');
    doc.text(320, 205, ' Number\nof Rooms');
    doc.text(395, 205, ' Number\nof Nights');
    doc.text(490, 212, '---------');
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(455, 365, 110, 25, 'DF');
    doc.text(350, 384, 'Total Room Rent');
    /* Room section end */

    /* service charge section begin */
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(30, 410, 535, 25, 'DF');
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(1);
    doc.rect(30, 410, 535, 165, 'S');
    doc.setLineWidth(1);
    doc.line(200, 410, 200, 575); //vertical lines
    doc.line(400, 410, 400, 575);
    doc.text(100, 428, 'Date');
    doc.text(260, 428, 'Particulars');
    doc.text(432, 428, 'Charged Amount');
    doc.setDrawColor(90, 104, 93);
    doc.line(30, 455, 565, 455);
    doc.line(30, 475, 565, 475);
    doc.line(30, 495, 565, 495);
    doc.line(30, 515, 565, 515);
    doc.line(30, 535, 565, 535);
    doc.line(30, 555, 565, 555);
    /* service charge section end */

    /* calculation section begin */
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(400, 575, 165, 25, 'DF');
    doc.text(303, 594, 'Total Other Bill');
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(211, 216, 237);
    doc.rect(400, 600, 165, 25, 'DF');
    doc.text(294, 618, 'Total Room Rent');
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(211, 216, 237);
    doc.rect(400, 625, 165, 25, 'DF');
    doc.text(335, 643, "Total VAT");
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(400, 650, 165, 25, 'DF');
    doc.text(272, 668, "Total (Inclusive VAT)");      //+20
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(211, 216, 237);
    doc.rect(400, 675, 165, 25, 'DF');
    doc.text(339, 693, "Discount");
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(211, 216, 237);
    doc.rect(400, 700, 165, 25, 'DF');
    doc.text(350, 718, "Refund");
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(211, 216, 237);
    doc.rect(400, 725, 165, 25, 'DF');
    doc.text(318, 743, "Paid Amount");
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(146, 158, 178);
    doc.rect(400, 750, 165, 25, 'DF');
    doc.text(320, 768, "Due Amount");

    /* calculation section end */

    /* Signature and footer note Begin */
    doc.line(487, 66, 565, 66);
    doc.line(30, 716, 150, 716);
    doc.text(30, 730, "Signature & Date");
    doc.text(30, 743, "Prepared By : ");
    doc.text(111, 800, "Thank you for staying with us. Hope to see you again in the future.");
    /* Signature and footer note end */

    /* dynamic data section begin */
    doc.setFont("courier", "normal");
    //name table
    doc.text(492, 63, invoiceNumber);
    doc.text(147, 117, name);
    doc.text(392, 117, Phone);
    doc.text(147, 135, arraivalDate);
    doc.text(147, 150, departureDate);
    doc.text(147, 165, checkOutDate);

    // Room Table data begin
    var roomTop = 240;
    var lRoomNo = 35;
    var lRoomCharge = 240;
    var lNoOfRooms = 340;
    var lNoOfNights = 410;
    var lLineTotal = 475;
    var totalRoomChargeTable = 0;
    var room_no_array = [];
    var rate_array = [];
    var rnoOfNight_array = [];
    var noOfRoom_array = [];

    var data = @Html.Raw(Json.Encode(((IEnumerable<PMS_HT.Models.tbl_guest_stay_info>)ViewBag.RoomListS).Select(user => new
          {
              room_No = user.tbl_room_info.Room_No,
              room_charge = user.tbl_room_info.Rate,
              Departure_Date = user.Departure_Date,
              Arrival_Date = user.Arrival_Date
          }))
               );



    function daydiff(first, second) {
        var dt1 = first;
        var dt2 = second;
        dt1 = dt1.replace(/[^0-9]/igm, '');
        dt2 = dt2.replace(/[^0-9]/igm, '');
        dt1 = parseInt(dt1);
        dt2 = parseInt(dt2);
        //var d1 = new Date(dt1);
        //var d2 = new Date(dt2);
        var res =  Math.round((dt2-dt1)/(1000*60*60*24));
        if (res == 0) {
            res = 1;
        }
        return res.toString();
    }
    data.forEach(function (item) {
        var arr_temp = item.Arrival_Date;
        var depar_temp = item.Departure_Date;
        //var noOfNightTemp = daydiff(arr_temp,dt.getTime());
        var noOfNightTemp = '@ViewBag.NoOfNight';
        var roomTemp = item.room_No;
        var charge = item.room_charge.toString();
        // tempNoOfNight = (int)Math.Round((item.Departure_Date - item.Arrival_Date).TotalDays);
        var flag = 0;
        var last = rate_array.length;
        for(var i=0;i<last;i++)
        {
            if(charge==rate_array[i] && noOfNightTemp==rnoOfNight_array[i])
            {
                room_no_array[i] = room_no_array[i]+','+roomTemp;
                var noRoomTemp = parseInt(noOfRoom_array[i]);
                noRoomTemp++;
                noOfRoom_array[i] = noRoomTemp.toString();
                flag=1;
            }
        }
        if(flag==0)
        {
            room_no_array[last] = roomTemp;
            rate_array[last] = charge;
            rnoOfNight_array[last] = noOfNightTemp;
            noOfRoom_array[last] = '1';
        }
    });
    for(var j=0;j<rate_array.length;j++)
    {
        doc.setFontSize(10);
        doc.text(lRoomNo, roomTop, room_no_array[j]); 		            //room table _ rrom no column row 1
        doc.setFontSize(12);
        doc.text(lRoomCharge, roomTop, rate_array[j]);		// room table _ room charge per day row 1
        doc.text(lNoOfRooms, roomTop, noOfRoom_array[j]);			        // room table _ number of rooms row 1
        doc.text(lNoOfNights, roomTop, rnoOfNight_array[j]);			    // room table _ number of nights row 1
        var l = calculateLineTotal(rate_array[j], noOfRoom_array[j], rnoOfNight_array[j]);
        totalRoomChargeTable += l;
        var lineTotal = l.toString();
        doc.text(lLineTotal, roomTop, lineTotal);	        // room table _ Line total row 1

        roomTop += 20;
    }

    doc.text(lLineTotal, 383, totalRoomChargeTable.toString());	// room table _ Total Room Rent
    //Room Table Data End

    //Particulars Table Data Begin
    var particularTop = 450;
    var lDate = 35;
    var lParticular = 215;
    var lChargedAmount = 455;
    var totalOtherBill = 0;
    var data = @Html.Raw(Json.Encode(((IEnumerable<PMS_HT.Models.tbl_room_transactions>)ViewBag.OtherBillListS).Select(user => new
          {
              date = user.Tran_Date,
              particular = user.Particulars,
              amount = user.Debit
          }))
               );

    data.forEach(function (item) {
        var otherDate = "";
        if(item.date!=null)
        {
            otherDate = item.date;
            otherDate = jsonToDate(otherDate);
            otherDate = otherDate.slice(4,24);
        }
        doc.text(lDate, particularTop, otherDate); 					//particular table _ date column row 1
        doc.text(lParticular, particularTop, item.particular); 					//particular table _ particular column row 1
        doc.text(lChargedAmount, particularTop, item.amount.toString()); 	//particular table _ charged amount column row 1
        particularTop += 20;
        totalOtherBill += item.amount;
    });
    var extraCharge = Number($("#extraCharge").val());
    if (extraCharge > 0) {
        doc.text(lDate, particularTop, "At CheckOut"); 					//particular table _ date column row 1
        doc.text(lParticular, particularTop, "Extra Charge");			//particular table _ particular column row 1
        doc.text(lChargedAmount, particularTop, extraCharge.toString()); 	//particular table _ charged amount column row 1
        particularTop += 20;
        totalOtherBill += extraCharge;
    }
    totalInclusiveVat = totalOtherBill + totalRoomChargeTable + vat;
    totalInclusiveVat = Math.round10(totalInclusiveVat, 1);
    due = totalInclusiveVat - totalDiscount - refund - totalPaid;
    doc.text(lChargedAmount, 593, totalOtherBill.toString()); 	//Total service charges
    doc.text(lChargedAmount, 618, totalRoomChargeTable.toString()); 	//Total room rent
    doc.text(lChargedAmount, 643, vat.toString()); 	        //previously paid
    doc.text(lChargedAmount, 668, totalInclusiveVat.toString()); 	            //discount
    doc.text(lChargedAmount, 693, totalDiscount.toString()); 	    //total without vat
    doc.text(lChargedAmount, 718, refund.toString()); 	                //vat
    doc.text(lChargedAmount, 743, totalPaid.toString()); 	        //total inclusive vat
    doc.text(lChargedAmount, 768, due.toString()); 	        //total inclusive vat

    //Particulars Table Data End

    doc.text(113, 743, user);
    /* Dynamic Data Section End */

    //doc.save(fileName);
    var base64 = doc.output('datauristring');
    var object = '<iframe width="100%" height="100%" src="'+base64+'"></iframe>';
    var generator = window.open("","","top=100,left=500,width=500,height=500");
    generator.document.write(object);
    generator.document.close();


    /* html2canvas($("#printArea"), {
         onrendered: function (canvas) {
             //var fileName1 = invoiceNumber + name + '.pdf';
             var imgData2 = canvas.toDataURL('image/png',1.0);
             var doc2 = new jsPDF('p', 'mm');
             doc2.addImage(imgData2, 'PNG', 10, 10);
             //doc.save(fileName);
             var base642 = doc2.output('datauristring');
             var object2 = '<iframe width="100%" height="100%" src="'+base642+'"></iframe>';
             var generator2 = window.open("","","top=100,left=500,width=500,height=500");
             generator2.document.write(object2);
             generator2.document.close();
         }
     });*/
});