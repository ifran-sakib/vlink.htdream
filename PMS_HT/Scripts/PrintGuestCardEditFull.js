﻿function printCard(alloted_room)
{
    
    var dt = new Date;
    var fileName = dt.getTime();
    dt = dt.toString();
    dt = dt.slice(3, 24);
    var user = document.getElementById("userName").innerHTML; 
    var name = $("#Name").val();
    var sign = $("#Signature").val();
    var father_name = $("#Father_Name").val();
    var mother_name = $("#Mother_Name").val();
    var address = $("#Address").val();
    var age = $("#Age").val();
    var feet = $("#feet").val();
    var inch = $("#inch").val();
    var height = feet + "'" + inch + '"'; 
    var marital_status = $("#Maritial_Status").val();
    var spouse_name = $("#Spouse_Name").val();
    var occupation = $("#Occupation").val();
    var nid = $("#National_ID").val();
    var passport = $("#Passport_No").val();
    if (passport == null) {
        passport = "";
    }
    var drivingLicense = $("#Driving_License").val();
    var phone = $("#Phone").val();
    var arrival_date = $("#Arrival_Date").val();
    fileName = arrival_date + "_" + alloted_room + "_Guest_Card.pdf";
    var npd = nid; 
    if (passport.length > npd.length) {
        npd = passport;
    } 
    if (drivingLicense.length > npd.length) {
        npd = drivingLicense;
    }
    var PropertyName = $("#PropertyName").html();
    var PropertyAddress = $("#PropertyAddress").html();
    var PropertyPhone = $("#PropertyPhone").html();
    var PropertyLogo = $("#PropertyLogo").html();
    var PropertyAddressPhone = PropertyAddress + " " + PropertyPhone;
    var serial_no = "0";
    $.ajax({
        type: "POST",
        url: "/FrontDeskGuestOverview/SerialFinder",
        data: {
            types: "E",
            dt: arrival_date,
            roomNo: alloted_room
        },
        success: function (data) {
            serial_no = data.toString();
        },
        async: false
    }); 
    var doc = new jsPDF('p', 'pt', 'a4');
    var imData = PropertyLogo;
    doc.addImage(imData, 'jpeg', 23, 15, 60, 60);
    //set Hotel name		
    doc.setFontSize(30);
    doc.setFont("arial", "normal");
    doc.text(95, 37, PropertyName);
    //show date
    doc.setFont("arial", "bold");
    doc.setFontSize(10);
    doc.setTextColor(0, 0, 0);
    doc.setFont("arial", "normal");
    doc.text(95, 52, PropertyAddressPhone);
    doc.setFontSize(13);
    doc.text(95, 70, 'Guest Information');
    doc.setFont("arial", "normal");
    doc.setFontSize(9);
    doc.text(33, 80, 'Money and other valuable things must be deposited in the office safe. Otherwise the management will not be responsible for any loss');
    doc.setFontSize(10);
    doc.setLineWidth(2);
    doc.line(15, 85, 580, 85);
    doc.line(15, 10, 580, 10);
    doc.line(15, 10, 15, 380);
    doc.line(580, 10, 580, 380);
    doc.line(15, 380, 580, 380);
    doc.setFont("arial", "bold");
    doc.text(555, 30, serial_no);
    doc.text(467, 70, dt);
    doc.setFontSize(12);
    doc.setFont("arial", "bold");
    doc.text(25, 100, '1.  Name'); doc.text(140, 100, ":");
    doc.text(25, 120, "2.  Father's Name"); doc.text(140, 120, ":");
    doc.text(375, 120, "3. Height"); doc.text(435, 120, ":");
    doc.text(25, 140, "4.  Mother's Name"); doc.text(140, 140, ":");
    doc.text(375, 140, "5. Age"); doc.text(435, 140, ":");
    doc.text(25, 160, "6.  Address"); doc.text(140, 160, ":");
    doc.text(25, 200, "7.  Spouse Name"); doc.text(140, 200, ":");
    doc.text(375, 195, "8. Marital \n    Status"); doc.text(435, 195, ":");
    doc.text(25, 220, "9.  Occupation\n     (With Address)"); doc.text(140, 220, ":");
    doc.text(20, 250, "10.  NID/Passport/\n       Driving License"); doc.text(140, 250, ":");
    doc.text(370, 250, "11. Phone"); doc.text(435, 250, ":");
    doc.text(20, 280, "12.  Arrival Date"); doc.text(140, 280, ":");
    doc.text(370, 280, "13. CheckOut\n      Date"); //doc.text(435, 280, ":");

    if ($('#VIP:checked').length != 0 || $('#DNR:checked').length != 0) {
        doc.text(20, 308, "Remarks"); doc.text(80, 308, ":");
        var leftVD = 62;
        doc.setFontSize(16);
        if($('#VIP:checked').length != 0)
            doc.text(leftVD+=30, 310, "VIP");
        if ($('#DNR:checked').length != 0)
            doc.text(leftVD+=30, 310, "DNR");
    }

    doc.setDrawColor(1);
    doc.setFillColor(0, 0, 0);
    doc.rect(450, 262, 125, 40, 'S');
    doc.setFontSize(15);
    doc.text(242, 325, "Alloted Room");
    doc.setDrawColor(0);
    doc.setFillColor(211, 211, 211);
    doc.rect(240, 330, 100, 40, 'F');
    doc.setFontSize(12);
    doc.line(35, 345, 185, 345);
    doc.text(40, 360, "Served By : ");
    doc.line(408, 345, 550, 345);
    doc.text(410, 360, "Signature of the Boarder"); 
    /* Data section Begin */
    doc.setFont("arial", "normal");
    doc.text(155, 100, name);
    doc.text(155, 120, father_name);
    doc.text(450, 120, height);
    doc.text(155, 140, mother_name);
    doc.text(450, 140, age);
    doc.text(155, 160, address);
    doc.text(155, 200, spouse_name);
    doc.text(450, 195, marital_status);
    doc.text(155, 220, occupation);
    doc.text(155, 250, npd);
    doc.text(450, 250, phone);
    doc.text(155, 280, arrival_date);
    doc.text(110, 360, user);
    doc.setFontSize(35);
    doc.setFont("arial", "bold");
    doc.text(260, 362, alloted_room);
    if (sign != "" && sign != null) {
        doc.addImage(sign, 'png', 419, 304, 120, 40);
    }
    /* Data section end */

    //doc.save(fileName);
    var base64 = doc.output('datauristring');
    var object = '<iframe width="100%" height="100%" src="' + base64 + '"></iframe>';
    var generator = window.open("", "", "top=100,left=500,width=500,height=500");
    generator.document.write(object);
    generator.document.close();
}