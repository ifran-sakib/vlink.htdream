﻿function updateClock() {
    var currentTime = getValidTime();

    $('.clock-content').html(currentTime.toString());

    setTimeout('updateClock()', 1000);
}

function getValidTime() {
    var currentTime = new Date();

    var hoursValue = currentTime.getHours();
    var minutesValue = currentTime.getMinutes();
    var secondsValue = currentTime.getSeconds();

    if (hoursValue < 10) {
        hoursValue = '0' + hoursValue;
    }
    if (minutesValue < 10) {
        minutesValue = '0' + minutesValue;
    }
    if (secondsValue < 10) {
        secondsValue = '0' + secondsValue;
    }

    return hoursValue + ':' + minutesValue + ':' + secondsValue;
}

$(document).ready(function () {
    updateClock();
});