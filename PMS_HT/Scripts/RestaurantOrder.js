$(document).ready(function() {

/*START ORDER ITEM*/

    $('.food-table tbody').on('click', '.item-btn', function() {
        //get selected food item details
        var currow = $(this).closest('tr');
        var col1 = currow.find('td:eq(0)').text();
        var col2 = currow.find('td:eq(1)').text();
        var col3 = currow.find('td:eq(2)').text();
        var col4 = currow.find('td:eq(4)').text();

        //set selected item in the right side order table
        var Orow1 = $('.order-table tbody').find('tr:eq(0)');
        Orow1.find('td:eq(0)').text(col1);
        Orow1.find('td:eq(1)').text(col2);
        Orow1.find('td:eq(2)').text(col3);
        Orow1.find('td:eq(3)').text(col4);

        //Make a clone of first row of the order table
        var orderRow = $("#order-row").clone();
        orderRow.prop("hidden", false);
        $(".order-table tbody").append(orderRow);

        Orow1.find('td:eq(0)').text("");
        Orow1.find('td:eq(1)').text("");
        Orow1.find('td:eq(2)').text("");
        Orow1.find('td:eq(3)').text("");
        orderRow.removeAttr('id');

        $(this).prop("disabled", true);
        $('#OrderButton').siblings('span.error').css('visibility', 'hidden');
    })

    $('.order-table tbody').on('click', '.order-btn', function() {
        var currow = $(this).closest('tr');
        var OitemNo = currow.find('td:eq(0)').text();

        $('.food-table tbody tr').each(function() {
            if ($(this).find('td:eq(0)').text() == OitemNo) {
                $(this).find('td:eq(6)').find('.item-btn').prop("disabled", false);
            }
        })
        currow.remove();
    });

/*END ORDER ITEM*/

    

})
