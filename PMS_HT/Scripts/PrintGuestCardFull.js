﻿function Shortifier(src, chopPoint) {
    if (src.length <= chopPoint)
        return src;
    return finalStr = src.substr(0, chopPoint) + '\n' + src.substr(chopPoint, src.length)
}
function printCard(alloted_room, status)
{
    var dt = new Date();
    //var fileName = dt.getTime();
    dt = dt.toString();
    dt = dt.slice(3, 24);
    var checkedInBy = $("#CheckedInBy").html();
    if (checkedInBy == null || checkedInBy == "") {
        checkedInBy = $("#userName").html();
    }
    var checkedOutBy = "";
    var name = $("#Name").val();
    var sign = "";
    var st = $("#Signature").val();
    if (st != "" && st != null)
    {
        sign = st;
    }
    var father_name = $("#tbl_guest_info_Father_Name").val();
    var mother_name = $("#tbl_guest_info_Mother_Name").val();
    var address = $("#tbl_guest_info_Address").val();
    var age = $("#tbl_guest_info_Age").val();
    var feet = $("#feet").val();
    var inch = $("#inch").val();
    var height = feet + "'" + inch + "\"";
    var marital_status = $("#tbl_guest_info_Maritial_Status").val();
    var spouse_name = $("#tbl_guest_info_Spouse_Name").val();
    var occupation = $("#tbl_guest_info_Occupation").val();
    var nid = $("#National_ID").val();
    var passport = $("#tbl_guest_info_Passport_No").val();
    var drivingLicense = $("#Driving_License").val();
    var phone = $("#tbl_guest_info_Phone").val();
    var arrival_date = $("#Arrival_Date").val(); 
    var departure_date = $("#Departure_Date").val();
    var checkout_date = "";

   
    var PropertyName = $("#PropertyName").html();
    var PropertyAddress = $("#PropertyAddress").html();
    var PropertyPhone = $("#PropertyPhone").html();
    var PropertyLogo = $("#PropertyLogo").html();
    var PropertyAddressPhone = PropertyAddress + " " + PropertyPhone;

    //var room_rent = $("#rate").val();
    var room_advance = $("#paid").val();
    var total_rent = $("#totalPayable").val();
    var total_days = $("#noOfNight").val();
    var room_rent = (Number($("#totalPayable").val()) / Number($("#noOfNight").val())).toString();
    var nationality = $("#tbl_guest_info_Nationality").val();
    var country = $("#Country_ID option:selected").text();

    
    var purposeOfVisit = $("#tbl_guest_info_Purpose_of_visit").val();
    var arraived_from = $("#tbl_guest_info_Arrived_From").val();
    var going_to = $("#tbl_guest_info_Going_To").val();
    var arraival_date_in_country = "";
    var visa = $("#tbl_guest_info_Visa_No").val();

    var serial_no = "0";
    var type = "A";
    if (status == "Edit" || status == "BR")
    {
        father_name = $("#Father_Name").val();
        mother_name = $("#Mother_Name").val();
        address = $("#Address").val();
        age = $("#Age").val();

        marital_status = $("#Maritial_Status").val();
        spouse_name = $("#Spouse_Name").val();
        occupation = $("#Occupation").val();

        passport = $("#Passport_No").val();
        phone = $("#Phone").val();

        nationality = $("#Nationality").val();
        country = $("#Country_Name").val();

        purposeOfVisit = $("#Purpose_of_visit").val();
        arraived_from = $("#Arrived_From").val();
        going_to = $("#Going_To").val();
        visa = $("#Visa_No").val();

        room_advance = $("#previousPaid").val();
        total_rent = $("#previousTotal").val();
        room_rent = (Number($("#previousTotal").val()) / Number($("#noOfNight").val())).toString();
        type = "E";
        if (status == "BR")
        {
            var nowPaid = Number($('#paid').val());
            var prePaid = Number($("#previousPaid").val());
            room_advance = (nowPaid + prePaid).toString();
            total_rent = $("#totalPayable").val();
            room_rent = (Number($("#totalPayable").val()) / Number($("#noOfNight").val())).toString();
        }
    }
    
    $.ajax({
        type: "POST",
        url: "/FrontDeskGuestOverview/SerialFinder",
        data: {
            types: type,
            dt: arrival_date,
            roomNo: alloted_room
        },
        success: function (data) {
            serial_no = data.toString();
        },
        async: false
    });

    if (nationality == "Foreigner") {
        nationality = country;
    } else {
        arraived_from = "";
        going_to = "";
        visa = "";
    }
    arrival_date = Shortifier(arrival_date, 13);
    departure_date = Shortifier(departure_date, 13);
    checkout_date = Shortifier(checkout_date, 13);
    //fileName = arrival_date + "_" + alloted_room + "_Guest_Card.pdf";
    if (passport == null) {
        passport = "";
    }
    var npd = nid;
    if (passport.length > npd.length) {
        npd = passport;
    }
    if (drivingLicense.length > npd.length) {
        npd = drivingLicense;
    } 
    if (address.length >= 38) {
        var str = address;
        var res = str.substr(1, 38);
        var lastSpace = res.lastIndexOf(" ") + 1;
        var finalStr = str.substr(0, lastSpace) + '\n' + str.substr(lastSpace + 1, res.length);
        address = finalStr;
    }
    var doc = new jsPDF('p', 'pt', 'a4');
    //load logo
    var imData = PropertyLogo;
    doc.addImage(imData, 'jpeg', 23, 15, 60, 60);
    //set Hotel name		
    doc.setFontSize(30);
    doc.setFont("arial", "normal");
    doc.text(95, 37, PropertyName);
    //show date
    doc.setFont("arial", "bold");
    doc.setFontSize(10);
    doc.setTextColor(0, 0, 0);
    doc.setFont("arial", "normal");
    doc.text(95, 52, PropertyAddressPhone);
    doc.setFont("arial", "bold");
    doc.setFontSize(13);
    doc.text(95, 70, 'Guest Information');
    doc.setFont("arial", "normal");
    doc.setFontSize(9);
    doc.text(33, 80, 'Money and other valuable things must be deposited in the office safe. Otherwise the management will not be responsible for any loss');
    doc.setFontSize(10);
    doc.setLineWidth(2);
    doc.line(15, 85, 580, 85);
    doc.line(15, 10, 580, 10);
    doc.line(15, 10, 15, 390);
    doc.line(580, 10, 580, 390);
    doc.line(15, 390, 580, 390);
    doc.setLineWidth(1);
    doc.line(15, 205, 580, 205);
    doc.setFont("arial", "bold");
    doc.text(555, 30, serial_no);
    doc.text(467, 70, dt); 
    doc.setFontSize(12);
    doc.setFont("arial", "bold");
    doc.text(25, 100, '1.  Name'); doc.text(140, 100, ":");
    doc.text(25, 120, "2.  Father's Name"); doc.text(140, 120, ":");
    doc.text(375, 120, "3. Height"); doc.text(455, 120, ":");
    doc.text(25, 140, "4.  Mother's Name"); doc.text(140, 140, ":");
    doc.text(375, 140, "5. Age"); doc.text(455, 140, ":");
    doc.text(25, 160, "6.  Address"); doc.text(140, 160, ":");
    doc.text(375, 160, "7. Phone"); doc.text(455, 160, ":");
    doc.text(375, 180, "8. Profession"); doc.text(455, 180, ":");
    doc.text(25, 200, "9.  Spouse Name"); doc.text(140, 200, ":");
    doc.text(369, 200, "10. Nationality"); doc.text(455, 200, ":");

    doc.text(20, 220, "11. NID/Passport/Driving Lic."); doc.text(190, 220, ":");
    doc.text(369, 220, "12. Visa No."); doc.text(455, 220, ":");
    doc.text(20, 240, "13. Coming From"); doc.text(190, 240, ":");
    doc.text(369, 240, "14. Going To"); doc.text(455, 240, ":");
    doc.text(20, 260, "15. Purpose of Visit"); doc.text(190, 260, ":");
    doc.text(370, 259, "Remarks"); doc.text(430, 259, ":");

    if ($('#VIP:checked').length != 0) {
        var leftVD = 432;
        doc.setFontSize(16);
        if ($('#VIP:checked').length != 0)
            doc.text(leftVD += 30, 260, "VIP");
    }
   
    var dtTop = 270;
    var dtHeight = 58;
    doc.setDrawColor(0, 0, 0);
    doc.setFillColor(255, 255, 255);
    doc.rect(16, dtTop, 564, dtHeight, 'DF');
    doc.setFillColor(255, 255, 220);
    doc.rect(16, dtTop, 564, 28, 'DF');

    doc.line(100, dtTop, 100, dtTop + dtHeight);
    doc.line(260, dtTop, 260, dtTop + dtHeight);
    doc.line(420, dtTop, 420, dtTop + dtHeight);
    //doc.line(289, dtTop, 289, dtTop + dtHeight);
    //doc.line(376, dtTop, 376, dtTop + dtHeight);
    //doc.line(462, dtTop, 462, dtTop + dtHeight);
    //doc.line(498, dtTop, 498, dtTop + dtHeight);

    doc.setFillColor(255, 255, 220);
    //doc.rect(16, dtTop + dtHeight + 2, 564, 20, 'DF');

    dtTop += 18;
    var cRoomNo = 57;
    //var cRoomRent = 110;
    //var cAdvanced = 174;
    var cArrDate = 180;
    var cDepdate = 340;
    var cCkOut = 500;
    //var cTotalDays = 480;
    //var cTotalAmount = 539;
    doc.setFontSize(12);

    doc.text(cRoomNo, dtTop, "Room No", "center");
    //doc.text(cRoomRent, dtTop, "Room Rent", "center");
    //doc.text(cAdvanced, dtTop, "Advance", "center");
    doc.text(cArrDate, dtTop, "Arr. Date & Time", "center");
    doc.text(cDepdate, dtTop, "Dep. Date & Time", "center");
    doc.text(cCkOut, dtTop, "CkOut Date & Time", "center");
    //doc.text(cTotalDays, dtTop - 7, "Total\nDays", "center");
    //doc.text(cTotalAmount, dtTop, "Total Amount", "center");
    //doc.text(297, dtTop + dtHeight - 3, "CHECK OUT TIME : 11AM(LOCAL TIME) (Late Check out will be charged accordingly)", "center");

    dtTop += 30;
    doc.setFontSize(20);
    doc.text(cRoomNo, dtTop, alloted_room, "center"); 
    doc.setFontSize(12);
    doc.setFont("arial", "normal");
    //doc.text(cRoomRent, dtTop, room_rent, "center");
    //doc.text(cAdvanced, dtTop, room_advance, "center");
    doc.text(cArrDate, dtTop - 7, arrival_date, "center");
    doc.text(cDepdate, dtTop - 7, departure_date, "center");
    doc.text(cCkOut, dtTop - 7, checkout_date, "center");
    //doc.text(cTotalDays, dtTop, total_days, "center");
    //doc.text(cTotalAmount, dtTop, total_rent, "center");
   

    var lowerTop = dtTop + dtHeight + 8;
    doc.setFont("arial", "bold");
    doc.setFontSize(12);
    doc.text(18, lowerTop, "Checked in by"); doc.text(103, lowerTop, ":");
    doc.text(297, lowerTop, "Guest Signature", "center");
    doc.text(400, lowerTop, "Checked out by"); doc.text(490, lowerTop, ":");
    doc.line(18, lowerTop - 12, 180, lowerTop - 12);
    doc.line(210, lowerTop - 12, 380, lowerTop - 12);
    doc.line(400, lowerTop - 12, 578, lowerTop - 12);

    doc.setFont("arial", "normal"); 
    doc.text(108, lowerTop, checkedInBy); 
    doc.text(495, lowerTop, checkedOutBy); 

    doc.setDrawColor(1);
    doc.setFillColor(0, 0, 0);
    doc.setFontSize(15);
    doc.setDrawColor(0);
    doc.setFillColor(211, 211, 211);
    doc.setFontSize(12);

    
    doc.text(155, 100, name);
    doc.text(155, 120, father_name);
    doc.text(462, 120, height);
    doc.text(155, 140, mother_name);
    doc.text(462, 140, age);
    doc.text(155, 160, address);
    doc.text(462, 160, phone);
    doc.text(462, 180, occupation);
    doc.text(155, 200, spouse_name);
    doc.text(462, 200, nationality);
   
    doc.text(195, 220, npd); 
    doc.text(462, 220, visa); 
    doc.text(195, 240, arraived_from);
    doc.text(462, 240, going_to);
    doc.text(195, 260, purposeOfVisit);

    

    doc.setFontSize(35);
    doc.setFont("arial", "bold");
    if (sign != "" && sign != null) {
        doc.addImage(sign, 'png', 220, 330, 120, 40);
    }
    /* Data section end */

    //doc.save(fileName);
    var base64 = doc.output('datauristring');
    var object = '<iframe width="100%" height="100%" src="' + base64 + '"></iframe>';
    var generator = window.open("", "", "top=100,left=500,width=500,height=500");
    generator.document.write(object);
    generator.document.close();
}