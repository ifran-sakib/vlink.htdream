﻿
    $(document).ready(function () {
        $("#Laundry_Item_ID").change(function () {
            $("#log").ajaxError(function (event, jqxhr, settings, exception) {
                alert(exception);
            });
            var a = document.getElementById('Laundry_Item_ID');
            var ItemSelected = a.options[a.selectedIndex].value;
            var b = document.getElementById('Laundry_Service_ID');
            var serviceSelected = b.options[b.selectedIndex].value;
            //$.get('@Url.Action("getServicePrice")',
            $.get(("getServicePrice"),
                { Laundry_Service_ID: serviceSelected, itemId: ItemSelected }, function (data) {
                    $("#Debit").val(data);
                });
        });
    });

$(document).ready(function () {
    $("#Laundry_Service_ID").change(function () {

        $("#log").ajaxError(function (event, jqxhr, settings, exception) {
            alert(exception);
        });
        var a = document.getElementById('Laundry_Item_ID');
        var ItemSelected = a.options[a.selectedIndex].value;
        var b = document.getElementById('Laundry_Service_ID');
        var serviceSelected = b.options[b.selectedIndex].value;
        $.get(("getServicePrice"),
            { Laundry_Service_ID: serviceSelected, itemId: ItemSelected }, function (data) {
                $("#Debit").val(data);
            });
    });
});

var Rooms = []
//fetch categories from database
function LoadRoom(element) {
    if (Rooms.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/LaundryItemService/getRoom',
            success: function (data) {
                Rooms = data;
                //render catagory
                renderRoom(element);
            }
        })
    }
    else {
        //render catagory to the element
        renderRoom(element);
    }
}

function renderRoom(element) {
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select Room'));
    $.each(Rooms, function (i, val) {
        $ele.append($('<option/>').val(val.Room_ID).text(val.Room_No));
    })
}

LoadRoom($('#Room_ID'));

//fetch Guest
function LoadGuestName(categoryDD) {
    $.ajax({
        type: "GET",
        url: "/LaundryItemService/getGuest",
        data: { 'id': $(categoryDD).val() },
        success: function (data) {
            renderGuest($(categoryDD).parents('.roomguest').find('select.guestClass'), data);
            //document.getElementById('#returnCostId').innerHTML = $(categoryDD).val();
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function renderGuest(element, data) {
    //render product
    var $ele = $(element);
    $ele.empty();
    $.each(data, function (i, val) {
        $ele.append($('<option/>').val(val.Guest_ID).text(val.Name));
    })
}



var Categories = []
//fetch categories from database
function LoadCategory(element) {
    if (Categories.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/LaundryItemService/getLaundryCategory',
            success: function (data) {
                Categories = data;
                //render catagory
                renderCategory(element);
            }
        })
    }
    else {
        //render catagory to the element
        renderCategory(element);
    }
}

function renderCategory(element) {
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select'));
    $.each(Categories, function (i, val) {
        $ele.append($('<option/>').val(val.Laundry_Item_Cata_ID).text(val.Laundry_Item_Cata_Name));
    })
}

//fetch products
function LoadItem(categoryDD) {
    //alert($(categoryDD).val());
    $.ajax({
        type: "GET",
        url: "/LaundryItemService/getItem",
        data: { 'id': $(categoryDD).val() },
        success: function (data) {
            //render products to appropriate dropdown
            renderProduct($(categoryDD).parents('.mycontainer').find('select.item'), data);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function renderProduct(element, data) {
    //render product
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select Item'));
    $.each(data, function (i, val) {
        $ele.append($('<option/>').val(val.Laundry_Item_ID).text(val.Laundry_Item_Name));
    })
}
LoadCategory($('#LaundryCategory'));



//Service and service Cost

var Services = []
//fetch categories from database
function LoadService(element) {
    if (Services.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/LaundryItemService/getService',
            success: function (data) {
                Services = data;
                //render catagory
                renderService(element);
            }
        })
    }
    else {
        //render catagory to the element
        renderService(element);
    }
}

function renderService(element) {
    var $ele = $(element);
    $ele.empty();
    //$ele.append($('<option/>').val('0').text('Select Service'));
    $.each(Services, function (i, val) {
        $ele.append($('<option/>').val(val.Laundry_Service_ID).text(val.Service_Heading_Name));
    })
}
LoadService($('#Laundry_Service_ID'));

//fetch Guest
function LoadServiceCost(serviceDD, itemDD) {
    $.ajax({
        type: "GET",
        url: "/LaundryItemService/getServicePrice",
        data: { 'id': $(categoryDD).val() },
        success: function (data) {
            renderServiceCost($(categoryDD).parents('.ServiceCost').find('select.price'), data);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function renderServiceCost(element, data) {
    //render product
    var $ele = $(element);
    $ele.empty();
    $.each(data, function (i, val) {
        $ele.append($('<option/>').val(val.Cost).text(val.Cost));
    })
}


//Load return and Return Cost
var Returns = []
//fetch categories from database
function LoadReturn(element) {
    if (Returns.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/LaundryItemService/getReturn',
            success: function (data) {
                Returns = data;
                //render catagory
                renderReturn(element);
            }
        })
    }
    else {
        //render catagory to the element
        renderReturn(element);
    }
}

function renderReturn(element) {
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select Return Option'));
    $.each(Returns, function (i, val) {
        $ele.append($('<option/>').val(val.Garment_In_ID).text(val.Return_In_Name));
    })
}
LoadReturn($('#Garment_In_ID'));

//fetch Return Cost
function LoadReturnCost(categoryDD) {
    $.ajax({
        type: "GET",
        url: "/LaundryItemService/getReturnCost",
        data: { 'id': $(categoryDD).val() },
        success: function (data) {
            renderReturnCost($(categoryDD).parents('.retCost').find('select.returnCost'), data);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function renderReturnCost(element, data) {
    //render return cost
    var $ele = $(element);
    $ele.empty();
    $.each(data, function (i, val) {
        $ele.append($('<option/>').val(val.Cost).text(val.Cost));
    })
}

$('#submit').click(function () {
    var isAllValid = true;

    if (isAllValid) {
        var data = {
            Room_ID: $('#Room_ID').val().trim(),
            Guest_ID: $('#Guest_ID').val().trim(),
            Particulars: $('#Particulars').val().trim(),
            Debit: $('#Debit').val().trim(),
            Laundry_Item_ID: $('#Laundry_Item_ID').val().trim(),
            Laundry_Service_ID: $('#Laundry_Service_ID').val().trim(),
            Garment_In_ID: $('#Garment_In_ID').val().trim(),
            Credit: $('#returnCostId').val().trim(),
            Tran_Date: new Date($.now())
            //tbl_purchase_details: list
        }
        
        //$(this).val('Save Order');
        $.ajax({
            type: 'POST',
            url: '/LaundryItemService/Save',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function (data) {
                if (data.status) {
                    alert('Successfully saved');
                    //here we will clear the form
                    list = [];
                    $('#Room_ID,#Guest_ID,#Particulars,#Debit,#Laundry_Item_ID,#Laundry_Service_ID').val('');
                    $('#orderdetailsItems').empty();
                    $('#submit').text("Save Order");
                    $(this).val('Save Order');
                }
                else {
                    alert('Error');
                }
                $('#submit').text('Save');
            },
            error: function (error) {
                console.log(error);
                $('#submit').text('Save');
            }
        });
    }

}); 