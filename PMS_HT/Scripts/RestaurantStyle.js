﻿$(document).ready(function () {

    /*START NAV STYLE*/

    var navItem = $('menu .navbar-default .navbar-nav li a ');
    navItem.mouseover(function (event) {

        $(this).find('i').stop().animate({ backgroundColor: "#ff6f1a" }, 30);
        $(this).stop().animate({ color: "#ff6f1a" }, 200);

    }).mouseleave(function (event) {
        $(this).find('i').stop().animate({ backgroundColor: "#00a651" }, 30);
        $(this).stop().animate({ color: "#fff" }, 200);
    });

    navItem.eq(0).css({ 'margin-left': '0' })


    var navDropDownItem = $('.header-right .navbar-right .dropdown-menu li');
    navDropDownItem.find('a').css({ 'color': '#00a651' });
    navDropDownItem.mouseover(function () {
        $(this).find('a').css({ 'color': '#fff' });
        $(this).stop().animate({ 'backgroundColor': '#00a651', 'color': '#fff' }, 100);
    }).mouseleave(function () {
        $(this).find('a').css({ 'color': '#00a651' });
        $(this).stop().animate({ 'backgroundColor': '#ffff', 'color': '#00a651' }, 100);
    })




    /*END NAV STYLE*/

    /*START RES TABLE*/

    var resTableContainer = $('.res-table-container');
    var TableContent = $('.table-content');

    TableContent.draggable({
        containment: resTableContainer,
        stop: function (event, ui) {
            var thisDiv = ui.helper.css("margin");
            //alert(thisDiv.height);
            //alert(ui.position.left + " " + ui.position.top + " " + ui.offset.left + " " + ui.offset.top);

        }

    });

    /*END RES TABLE*/


    /*START HOMPAGE TABLE*/

    var homeOTRow1Th = $('.hompage-order-table thead tr th').eq(0);
    var homeOTRow2Th = $('.hompage-order-table thead tr').eq(1).find('th');
    tableHeaderTitle(homeOTRow1Th, homeOTRow2Th);

    function tableHeaderTitle(tableRow1, tableRow2) {
        var tableRow2L = tableRow2.length;
        tableRow1.prop('colspan', tableRow2L);
    }

    /*END HOMPAGE TABLE*/

    /*START SUB-PAGE TABLE*/

    var itemSTblRow1Th = $('.item-selected-table tr th').eq(0);
    var itemSTblRow2Th = $('.item-selected-table tr').eq(1).find('th');
    tableHeaderTitle(itemSTblRow1Th, itemSTblRow2Th);

    /*END SUB-PAGE TABLE*/

    /*START STICKY HEADER*/

    var mainMenu = $('menu .navbar-default');

    $(window).scroll(function () {
        var top = $('header').height();
        if ($(this).scrollTop() >= top/2) {
            mainMenu.addClass("navbar-sticky");
        }
        else {
            mainMenu.removeClass("navbar-sticky");
        }
    });

    /*END STICKY HEADER*/



    /*START DATA TABLE*/

    var itemTableDT = $('#itemtable');
    itemTableDT.DataTable(
    {
        "paging": false,
        "ordering": false,
        "info": false
    });

    var itemTbl = $('.item-table');
    itemTbl.find('.row').eq(2).remove();
    itemTbl.find('.row').eq(0).find('.col-sm-6').eq(0).remove();
    itemTbl.find('.row').eq(0).find('.col-sm-6').eq(0).removeClass('col-sm-6').addClass('col-md-12');

    var itemTblLbl = $('.item-table .dataTables_filter label');
    itemTblLbl.contents().filter(function () {
        return this.nodeType == 3;
    }).remove();
    itemtableIcon = '<i class="fa fa-search"> </i>';
    itemTblTitle = '<h4> ITEMS </h4>';
    itemTblLbl.append(itemtableIcon);
    itemTblDF = $('.item-table .dataTables_filter');
    itemTblDF.prepend(itemTblTitle);

    $('.item-table .dataTables_filter .form-control').attr({
        placeholder: 'Search'
    });

    /*END DATA TABLE*/

    /*START RESPONSIVE SECTION*/

    //variable declartion

    var mediaQ = Modernizr.mq('only screen and (max-width:767px)');

    $(window).resize(function () {
        if (mediaQ) {
            navItem.eq(0).css({ 'margin-left': '15px' })
            $('.page-title .breadcrumb').removeClass('pull-right').addClass('pull-left');

            $('.footer-copyright').find('span').eq(1).removeClass('pull-right').css({ 'margin': '0 auto', 'display': 'table' });

            mainMenu.find('.navbar-nav li').last().css({ 'border-bottom': '0' });
            // mainMenu.find('.navbar-nav li a').last().css({'padding-bottom':'0'}) ;
        }

    }).resize()



    /*END RESPONSIVE SECTION*/



})
