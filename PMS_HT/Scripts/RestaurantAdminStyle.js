﻿$(document).ready(function () {

    //MODAL DECLARTION

    $('.modal').modal();

    //SIDEBAR DROPDOWN
    var sidebarDropdown = $('#sidebar-dropdown');

    var sidebarDropdownMenu = $('#sidebar-dropdown-menu');


    sidebarDropdown.unbind('click').click(function (event) {
        event.stopPropagation();
        $(this).find(sidebarDropdownMenu).unbind('click').slideToggle(500);
    });

    //RESPONSIVE SECTION

    var mediaQ = Modernizr.mq('only screen and (max-width:767px)');

    var navMenu = $("#nav-menu");

    var aTxt = $(".admin-text");

    var navRightDropdown = $('.nav-right-dropdown ');

    var navRightDropdownM = $('.nav-dropdownmenu-right');

    navMenu.hide();

    $(window).resize(function () {
        if (mediaQ) {
            navMenu.insertBefore("#logo");
            var navC = aTxt.contents().filter(function () {
                return this.nodeType == 3;
            }).remove();

            navMenu.show();

            $('.nav-dropdownmenu-right li a i').css({ 'padding-left': '10px' });

            navRightDropdownM.hide();
            navRightDropdown.unbind('click').click(function (event) {
                event.stopPropagation();
                $(this).find(navRightDropdownM).unbind('click').slideToggle(20);
            });

            $('.page-title-wrapper').remove();
            aTxt.css({ 'display': 'flex', 'margin': '0px', 'padding': '0px' });
            aTxt.find('.mdi').removeClass('left');
            aTxt.find('.mdi').removeClass('right');
            navMenu.click(function (event) {

                var sidebarTrans = $('.side-nav.fixed').css('-webkit-transform').split(/[()]/)[1];
                var posx = sidebarTrans.split(',')[4];
                $('.side-nav.fixed').css({ 'opacity': '0', 'left': '-400px', 'top': '56px' });
                if (posx == 0) {
                    $('.side-nav.fixed').css({ 'transform': 'translateX(-105%)' }).stop().animate({ 'opacity': '1', 'left': '-400px' }, 500);
                } else {
                    $('.side-nav.fixed').css({ 'transform': 'translateX(0%)' }).stop().animate({ 'opacity': '1', 'left': '0px' }, 500);;
                }
            });

        }

    }).resize()


    /*START ITEM DATA TABLE*/


    var itemDT = $("#itemtbl");

    itemDT.DataTable({
        columnDefs: [{
            targets: [0, 1, 2],
            className: 'mdl-data-table__cell--non-numeric'
        }]
    });

    /*END ITEM DATA TABLE*/

})
