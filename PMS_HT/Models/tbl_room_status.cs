namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_room_status
    {
        [Key]
        public int Room_Status_ID { get; set; }

        [StringLength(50)]
        public string Status_Heading { get; set; }

        public string Status_Color { get; set; }

        [StringLength(100)]
        public string Status_Description { get; set; }

        public bool? YsnActive { get; set; }
    }
}
