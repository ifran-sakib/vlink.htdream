﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class ItemLocateViewModel
    {
        public string Item_Name { get; set; }
        public decimal? Available_Stock { get; set; }
        public string Stock_Location { get; set; } 
    }
}