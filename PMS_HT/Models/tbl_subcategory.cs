namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_subcategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_subcategory()
        {
            tbl_item = new HashSet<tbl_item>();
        }

        [Key]
        public int Subcata_ID { get; set; }

        [StringLength(100)]
        public string Subcata_Name { get; set; }
        
        public int? Cata_ID { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item> tbl_item { get; set; }
        [ForeignKey("Cata_ID")]
        public virtual tbl_category tbl_category { get; set; }
    }
}
