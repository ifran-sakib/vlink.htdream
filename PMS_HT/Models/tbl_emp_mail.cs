namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_emp_mail
    {
        [Key]
        public int Emp_Mail_ID { get; set; }

        [StringLength(50)]
        public string Sent_To_Mail { get; set; }

        [StringLength(50)]
        public string Sent_From_Mail { get; set; }

        public int? Sender_ID { get; set; }

        public int? Receiver_ID { get; set; }

        [StringLength(50)]
        public string Mail_Sub { get; set; }

        [StringLength(255)]
        public string Mail_Body { get; set; }

        public DateTime? Sent_Date { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_emp_info tbl_emp_info1 { get; set; }
    }
}
