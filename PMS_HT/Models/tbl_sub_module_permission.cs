namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_sub_module_permission
    {
        [Key]
        public int Sub_Module_ID { get; set; }

        [StringLength(50)]
        public string Sub_Module_Name { get; set; }

        public int? Module_ID { get; set; }

        public bool? Viewable { get; set; }

        public bool? Editable { get; set; }
    }
}
