﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class BookinViewModel
    {
        public DateTime? Start_Date { get; set; }


        public DateTime? End_Date { get; set; }
        public string Purpose_Name { get; set; }
        public string Particulars { get; set; }
        public decimal? Debit { get; set; }

        public int? Program_ID { get; set; }
        public int? Banq_Booking_ID { get; set; }
        public string Status { get; set; }






    }
}