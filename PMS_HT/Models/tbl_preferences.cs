﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_preferences
    {
        [Key]
        public int Preference_ID { get; set; }

        public string Type { get; set; }

        [Display(Name = "Preference Name")]
        public string Preference_Name { get; set; }

        public string Value { get; set; }
    }
}