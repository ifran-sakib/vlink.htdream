namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_return_germent_in
    {
        [Key]
        public int Garment_In_ID { get; set; }

        [StringLength(100)]
        public string Return_In_Name { get; set; }

        public decimal? Cost { get; set; }
    }
}
