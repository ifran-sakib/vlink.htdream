﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class BanquetTransactionDetailsViewModel
    {
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public DateTime? Tran_Date { get; set; }
        public string Amenities_Heading { get; set; }
    }
}