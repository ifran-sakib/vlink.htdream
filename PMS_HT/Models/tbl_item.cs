namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_item()
        {
            tbl_stock = new HashSet<tbl_stock>();
        }

        [Key]
        public int Item_ID { get; set; }

        public int? Cata_ID { get; set; }

        public int? Subcata_ID { get; set; }

        public int? Brand_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(100)]
        public string Item_Name { get; set; }

        public decimal? Sales_Price { get; set; }

        public decimal? Purchase_Price { get; set; }

        public decimal? Reorder_Level { get; set; }

        [ForeignKey("Brand_ID")]
        public virtual tbl_brand tbl_brand { get; set; }

        [ForeignKey("Cata_ID")]
        public virtual tbl_category tbl_category { get; set; }

        [ForeignKey("Subcata_ID")]
        public virtual tbl_subcategory tbl_subcategory { get; set; }

        [ForeignKey("Unit_ID")]
        public virtual tbl_unit tbl_unit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_stock> tbl_stock { get; set; }
    }
}
