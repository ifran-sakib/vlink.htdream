﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
namespace PMS_HT.Models
{
    public class tbl_bank_transaction
    {
        [Key]
        public int? Bnk_Tran_ID { get; set; }

        public int? Tran_Ref_ID { get; set; }

        [StringLength(50)]
        public string Tran_Type { get; set; }

        [StringLength(500)]
        public string Particulars { get; set; }

        [Display(Name ="Deposite")]
        public decimal? Debit { get; set; }

        [Display(Name = "Withdrawn")]
        public decimal? Credit { get; set; }

        public int? Creted_By { get; set; }
        [Display(Name ="Date")]
  
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Created_Date { get; set; }

        public int? Modified_By { get; set; }

        public DateTime? Modified_Date { get; set; }




        [ForeignKey("Tran_Ref_ID")]
        public virtual tbl_account_info tbl_account_info { get; set; }

        [ForeignKey("Creted_By")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        [ForeignKey("Modified_By")]
        public virtual tbl_emp_info tbl_emp_info1 { get; set; }
    }
}