namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_laundry_item_category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_laundry_item_category()
        {
            tbl_laundry_item = new HashSet<tbl_laundry_item>();
        }

        [Key]
        public int Laundry_Item_Cata_ID { get; set; }

        [StringLength(100)]
        public string Laundry_Item_Cata_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_laundry_item> tbl_laundry_item { get; set; }
    }
}
