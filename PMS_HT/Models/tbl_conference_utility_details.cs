namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_utility_details
    {
        [Key]
        public int Utility_Details_ID { get; set; }

        [StringLength(50)]
        public string Utility_Order_No { get; set; }

        public int? Utility_Master_ID { get; set; }

        public int? Conf_Item_ID { get; set; }

        public decimal? Quantity { get; set; }

        public DateTime? Start_Date { get; set; }

        public DateTime? End_Date { get; set; }

        public TimeSpan? Strat_Time { get; set; }

        public TimeSpan? End_Time { get; set; }

        public decimal? Total_day { get; set; }

        public decimal? Cost { get; set; }

        public virtual tbl_conf_item_setup tbl_conf_item_setup { get; set; }

        public virtual tbl_conference_utility_master tbl_conference_utility_master { get; set; }
    }
}
