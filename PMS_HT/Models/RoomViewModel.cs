﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class RoomViewModel
    {
        public string Room_No { get; set; }
        public int Room_ID { get; set; }
        public int Floor_ID { get; set; }
        public string Status { get; set; }
        public string Roomtype_Name { get; set; }
        public string arrival_date { get; set; }
    }
}