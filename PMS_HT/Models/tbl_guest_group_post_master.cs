﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_guest_group_post_master
    {
        [Key]
        public int Post_Master_ID { get; set; }        

        public int? Stay_Info_ID { get; set; }

        public int? Guest_Group_ID { get; set; }


        [ForeignKey("Guest_Group_ID")]
        public virtual tbl_guest_group_info tbl_guest_group_info { get; set; }

        [ForeignKey("Stay_Info_ID")]
        public virtual tbl_guest_stay_info tbl_guest_stay_info { get; set; }



        public virtual ICollection<tbl_guest_group_post_master_task> tbl_guest_group_post_master_tasks { get; set; }



        public tbl_guest_group_post_master()
        {
            tbl_guest_group_post_master_tasks = new HashSet<tbl_guest_group_post_master_task>();
        }
    }
}