namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_dining_waiter
    {
        public tbl_dining_waiter()
        {
            tbl_dining_set_up = new HashSet<tbl_dining_set_up>();
            tbl_item_order_master = new HashSet<tbl_item_order_master>();
        }
        [Key]
        public int Dining_Waiter_ID { get; set; }

        public int? Emp_ID { get; set; }

        public string Status { get; set; }

        [StringLength(50)]
        public string Note { get; set; }

        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual ICollection<tbl_dining_set_up> tbl_dining_set_up { get; set; }

        public virtual ICollection<tbl_item_order_master> tbl_item_order_master { get; set; }
    }
}
