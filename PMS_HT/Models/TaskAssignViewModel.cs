using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class TaskAssignViewModel
    {
        public string Task_Heading { get; set; }
        public string RoomNo { get; set; }
        public string Note { get; set; }
        public DateTime? Task_Assignment_Date_Time { get; set; }
        public bool? Task_Status { get; set; }
        public int Task_Assignment_ID { get; set; }
    }

}