namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_room_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_room_info()
        {
            tbl_guest_stay_history = new HashSet<tbl_guest_stay_history>();
            tbl_guest_stay_info = new HashSet<tbl_guest_stay_info>();
            tbl_package = new HashSet<tbl_package>();
            tbl_register_book = new HashSet<tbl_register_book>();
            tbl_room_transactions = new HashSet<tbl_room_transactions>();
            tbl_tariff = new HashSet<tbl_tariff>();
            tbl_task_assignment = new HashSet<tbl_task_assignment>();
            tbl_task_schdule = new HashSet<tbl_task_schdule>();
            tbl_room_temp_monitor = new HashSet<tbl_room_temp_monitor>();
        }

        [Key]
        public int Room_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Room No")]
        public string Room_No { get; set; }

        public int? Roomtype_ID { get; set; }

        public int? Floor_ID { get; set; }

        public int? Room_view_ID { get; set; }

        public decimal? Rate { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public bool? YsnActive { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? Created_Date { get; set; }

        public virtual tbl_floor_setup tbl_floor_setup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_guest_stay_history> tbl_guest_stay_history { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_guest_stay_info> tbl_guest_stay_info { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_package> tbl_package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_register_book> tbl_register_book { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_room_transactions> tbl_room_transactions { get; set; }

        public virtual tbl_room_view_setup tbl_room_view_setup { get; set; }

        public virtual tbl_roomtype_setup tbl_roomtype_setup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_tariff> tbl_tariff { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_assignment> tbl_task_assignment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_schdule> tbl_task_schdule { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_room_temp_monitor> tbl_room_temp_monitor { get; set; }
    }
}
