﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_order_method
    {
        [Key]
        public int Order_Method_ID { get; set; }

        [StringLength(20)]
        [Display(Name = "Order Type")]
        public string Order_Method_Name { get; set; }

        public bool? IsBuiltIn { get; set; }
    }
}