namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_program
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_conference_program()
        {
            tbl_conference_transactions = new HashSet<tbl_conference_transactions>();
            tbl_conference_utility_master = new HashSet<tbl_conference_utility_master>();
        }

        [Key]
        public int Conf_Program_ID { get; set; }

        public int? Conf_Room_ID { get; set; }

        [StringLength(50)]
        public string Person_Name { get; set; }

        [StringLength(100)]
        public string Person_Address { get; set; }

        [StringLength(50)]
        public string Person_Phone { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? Country_ID { get; set; }

        [StringLength(50)]
        public string Company_Name { get; set; }

        [StringLength(50)]
        public string Company_Phone { get; set; }

        [StringLength(100)]
        public string Company_Adress { get; set; }

        [StringLength(50)]
        public string Company_Details { get; set; }

        [StringLength(50)]
        public string Contact_Person_Phone { get; set; }

        [StringLength(100)]
        public string Contact_Person_Address { get; set; }

        public int? Conf_Purpose_ID { get; set; }

        public virtual tbl_conference_purpose tbl_conference_purpose { get; set; }

        public virtual tbl_conference_room tbl_conference_room { get; set; }

        public virtual tbl_country tbl_country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_transactions> tbl_conference_transactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_utility_master> tbl_conference_utility_master { get; set; }
    }
}
