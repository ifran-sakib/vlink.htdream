﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class tbl_bank_info
    {
        [Key]
        public int? Bank_ID { get; set; }
      
        [StringLength(50)]
        public string Bank_Name { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
    }
}