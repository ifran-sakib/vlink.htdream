namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_item_order_details
    {
        [Key]
        public int Order_Details_ID { get; set; }             

        public int? Food_ID { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Price { get; set; }

        public int? Food_Side_ID { get; set; }

        public decimal? Side_Quantity { get; set; }

        public decimal? Side_Price { get; set; }

        public decimal? VAT { get; set; }

        public decimal? SC { get; set; }

        public decimal? Total { get; set; }

        public int? Order_Master_ID { get; set; }

        public DateTime? Order_Date { get; set; }

        public string Note { get; set; }

        public bool? YsnActive { get; set; }

        [ForeignKey("Order_Master_ID")]
        public virtual tbl_item_order_master tbl_item_order_master { get; set; }

        [ForeignKey("Food_ID")]
        public virtual tbl_food tbl_food { get; set; }

        [ForeignKey("Food_Side_ID")]
        public virtual tbl_food_side_extra tbl_food_side_extra { get; set; }
    }
}
