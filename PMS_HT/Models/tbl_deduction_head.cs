namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_deduction_head
    {
        [Key]
        public int Deduction_Head_ID { get; set; }

        [StringLength(50)]
        public string Deduction_Head_Name { get; set; }
    }
}
