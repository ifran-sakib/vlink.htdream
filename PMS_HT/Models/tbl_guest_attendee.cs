﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_guest_attendee
    {
        public tbl_guest_attendee()
        {

        }
        [Key]
        public int? Attendee_ID { get; set; }
        public int? Stay_Info_ID { get; set; }
        public int? Emp_ID { get; set; }
        public string Purpose { get; set; }
        public DateTime? Changed_Date { get; set; }

        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}