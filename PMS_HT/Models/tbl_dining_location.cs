using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public partial class tbl_dining_location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_dining_location()
        {
            tbl_dining_set_up = new HashSet<tbl_dining_set_up>();
        }

        [Key]
        public int Dining_Location_ID { get; set; }

        [StringLength(50)]
        [Display(Name ="Location Name")]
        public string Location_Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Location Type")]
        public string Location_Type { get; set; }

        public int? Point_A { get; set; }

        public int? Point_B { get; set; }

        public int? Point_C { get; set; }

        public int? Radius { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_dining_set_up> tbl_dining_set_up { get; set; }
    }
}
