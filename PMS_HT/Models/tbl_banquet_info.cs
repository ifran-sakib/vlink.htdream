namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_banquet_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_banquet_info()
        {
            tbl_banquet_prog = new HashSet<tbl_banquet_prog>();
        }

        [Key]
        public int Banquet_ID { get; set; }

        [StringLength(50)]

        public string Banquet_Name { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(50)]
        public string Capacity { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public decimal? Rent { get; set; }

        public int? Tax_ID { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_banquet_prog> tbl_banquet_prog { get; set; }
    }
}
