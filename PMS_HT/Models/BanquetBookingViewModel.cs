﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace PMS_HT.Models
{
    public class BanquetBookingViewModel
    {
        public int? Banq_Booking_ID { get; set; }

        public int? Program_ID { get; set; }

        public DateTime? Start_Date { get; set; }

        public DateTime? End_Date { get; set; }
        public string Tran_Ref_Name { get; set; }
        public string Particulars { get; set; }

        public int? P_Purpose_ID { get; set; }
        public int B_Tran_ID { get; set; }
        public string Purpose_Name { get; set; }

        public int? Tran_Ref_ID { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public DateTime? Tran_Date { get; set; }
    }
}