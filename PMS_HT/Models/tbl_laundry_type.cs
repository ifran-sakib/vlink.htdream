namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_laundry_type
    {
        [Key]
        public int Laundry_Type_ID { get; set; }

        [StringLength(50)]
        public string Type_Name { get; set; }
    }
}
