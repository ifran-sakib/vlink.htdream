﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_company_info
    {
        [Key]
        public int? Company_ID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        [Range(0, 100)]
        public decimal? Discount { get; set; }

        public string Note { get; set; }
    }
}