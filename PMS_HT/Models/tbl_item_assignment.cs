namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_item_assignment
    {
        [Key]
        public int Assignment_ID { get; set; }

        public int? Report_ID { get; set; }

        [StringLength(50)]
        public string Item_Name { get; set; }

        public decimal? Service_Cost { get; set; }

        public decimal? Carrying_Cost { get; set; }

        public virtual tbl_report tbl_report { get; set; }
    }
}
