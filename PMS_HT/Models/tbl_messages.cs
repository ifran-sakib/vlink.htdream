﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_messages
    {
        [Key]
        public int Message_ID { get; set; }

        public int? Stay_Info_ID { get; set; }

        [Display(Name = "Received By")]
        public int? Receiving_Emp_ID { get; set; }

        [Display(Name = "Delivered By")]
        public int? Delivering_Emp_ID { get; set; }

        public string Message { get; set; }

        [StringLength(255)]
        [Display(Name = "Sender's Detail")]
        public string Senders_Details { get; set; }

        [Display(Name = "Receiving Time")]
        public DateTime? Receiving_Time { get; set; }

        [Display(Name = "Delivery Time")]
        public DateTime? Delivery_Time { get; set; }

        public string Status { get; set; }

        [ForeignKey("Receiving_Emp_ID")]
        public virtual tbl_emp_info receiving_employee { get; set; }

        [ForeignKey("Delivering_Emp_ID")]
        public virtual tbl_emp_info delivering_employee { get; set; }

        [ForeignKey("Stay_Info_ID")]
        public virtual tbl_guest_stay_info tbl_guest_stay_info { get; set; }
    }
}