﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class tbl_laundry_service_cost
    {
        [Key]
        public int Laundry_Sevice_Cost_ID { get; set; }
        public int? Laundry_Item_ID { get; set; }
        public int? Laundry_Service_ID { get; set; }
        public decimal? Cost { get; set; }
        [StringLength(100)]
        public string Note { get; set; }
        [ForeignKey("Laundry_Item_ID")]
        public virtual tbl_laundry_item tbl_laundry_item { get; set; }
        [ForeignKey("Laundry_Service_ID")]
        public virtual tbl_laundry_service tbl_laundry_service { get; set; }


    }
}