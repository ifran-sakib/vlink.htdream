namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_travel_agent_info
    {
        [Key]
        public int? Travel_Agent_ID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        [Range(0, 100)]
        public decimal? Discount { get; set; }

        [Range(0, 100)]
        public decimal? Comission { get; set; }

        public string Note { get; set; }
    }
}
