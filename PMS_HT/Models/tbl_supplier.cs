namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_supplier
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_supplier()
        {
            tbl_purchase_master = new HashSet<tbl_purchase_master>();
            tbl_supplier_ledger = new HashSet<tbl_supplier_ledger>();
        }

        [Key]
        public int Supplier_ID { get; set; }

        [StringLength(50)]
        public string Sup_Name { get; set; }

        [StringLength(100)]
        public string Sup_Address { get; set; }

        [StringLength(50)]
        public string Sup_Phone_No { get; set; }

        public decimal? Opening_Balance { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchase_master> tbl_purchase_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_supplier_ledger> tbl_supplier_ledger { get; set; }
    }
}
