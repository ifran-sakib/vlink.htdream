namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_tariff
    {
        [Key]
        public int Tarrif_ID { get; set; }

        public int? Room_ID { get; set; }

        [StringLength(50)]
        public string Tariff_Name { get; set; }

        [StringLength(155)]
        public string Tariff_Details { get; set; }

        public bool? YsnActive { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }
    }
}
