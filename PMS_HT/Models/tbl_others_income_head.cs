﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PMS_HT.Models
{
    public class tbl_others_income_head
    {
        [Key]
        public int? Others_Income_Head_ID { get; set; }
        [StringLength(50)]
        [DisplayName("Income Head")]
        public string Others_Income_Head_Name { get; set; }
        [StringLength(50)]

        [DisplayName("Description")]
        public string Others_Income_Description { get; set; }

    }
}