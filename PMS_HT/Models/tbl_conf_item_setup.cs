namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conf_item_setup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_conf_item_setup()
        {
            tbl_conference_utility_details = new HashSet<tbl_conference_utility_details>();
        }

        [Key]
        public int Conf_Item_ID { get; set; }

        [StringLength(50)]
        public string Conf_Item_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_utility_details> tbl_conference_utility_details { get; set; }
    }
}
