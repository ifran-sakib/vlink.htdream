namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_earning_head
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_earning_head()
        {
            tbl_salary_transactions = new HashSet<tbl_salary_transactions>();
        }

        [Key]
        public int Earning_Head_ID { get; set; }

        [StringLength(50)]
        public string Earning_Head_Name { get; set; }
        public string Type { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_salary_transactions> tbl_salary_transactions { get; set; }
    }
}
