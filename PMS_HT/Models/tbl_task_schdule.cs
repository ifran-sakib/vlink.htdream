namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_task_schdule
    {
        [Key]
        public int Task_Schdule_ID { get; set; }

        [Display(Name ="Maid Name")]
        public int? Maid_ID { get; set; }

        [Display(Name = "Task Name")]
        public int? Task_ID { get; set; }

        public int? Room_ID { get; set; }

        [StringLength(50)]
        public string Note { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Schedule Time")]
        public DateTime? Schdule_Date_Time { get; set; }

        [StringLength(50)]
        public string Task_Schdule_Status { get; set; }

        public DateTime? Task_Created_Date_Time { get; set; }

        public int? Created_By { get; set; }

        public int? Updated_By { get; set; }

        public DateTime? Updated_Date_Time { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

        public virtual tbl_maid_info tbl_maid_info { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }

        public virtual tbl_task_setup tbl_task_setup { get; set; }
    }
}
