namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_banquet_transactions
    {
        [Key]
        public int B_Tran_ID { get; set; }

        public int? Program_ID { get; set; }

        public int? Tran_Ref_ID { get; set; }

        public int? Banq_Booking_ID { get; set; }

        [StringLength(50)]
        public string Tran_Ref_Name { get; set; }

        [StringLength(200)]
        public string Particulars { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public DateTime? Tran_Date { get; set; }

        public virtual tbl_banquet_prog tbl_banquet_prog { get; set; }
        [ForeignKey("Tran_Ref_ID")]
        public virtual tbl_banquet_amenities tbl_banquet_amenities { get; set; }
        [ForeignKey("Banq_Booking_ID")]
        public virtual tbl_banquet_booking tbl_banquet_booking { get; set; }

    }
}
