namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_category()
        {
            tbl_item = new HashSet<tbl_item>();
            tbl_subcategory = new HashSet<tbl_subcategory>();
        }

        [Key]
        public int Cata_ID { get; set; }

        [StringLength(100)]
        public string Cata_Name { get; set; }

        public string Cata_Details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item> tbl_item { get; set; }
        public virtual ICollection<tbl_subcategory> tbl_subcategory { get; set; }
    }
}
