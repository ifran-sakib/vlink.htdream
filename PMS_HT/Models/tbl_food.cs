﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_food
    {
        [Key]
        public int Food_ID { get; set; }

        [StringLength(6)]
        [Display(Name = "Food No")]
        public string Food_No { get; set; }

        [StringLength(50)]
        [Display(Name = "Food Name")]
        public string Food_Name { get; set; }

        public int? Food_Cate_ID { get; set; }

        public int? Recipe_ID { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        public string Image { get; set; }

        public bool? Availability { get; set; }

        public string Quantity { get; set; }

        public decimal? Price { get; set; }

        public decimal? Discount { get; set; }

        public string Color { get; set; }

        [StringLength(300)]
        public string Note { get; set; }

        [ForeignKey("Food_Cate_ID")]
        public virtual tbl_food_category tbl_food_category { get; set; }

        [ForeignKey("Recipe_ID")]
        public virtual tbl_food_recipe tbl_food_recipe { get; set; }

    }
}