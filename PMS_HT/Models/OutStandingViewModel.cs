﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class OutStandingViewModel
    {
        public string Room_No { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public decimal Credit { get; set; }
        public DateTime Arrival_Date { get; set; }
        public DateTime Tran_Date { get; set; }
        public DateTime Checkout_Date { get; set; }
        public string Particulars { get; set; }
        public int Stay_Info_ID { get; set; }
    }
}