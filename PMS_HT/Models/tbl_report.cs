namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_report
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_report()
        {
            tbl_item_assignment = new HashSet<tbl_item_assignment>();
            tbl_report_history = new HashSet<tbl_report_history>();
        }

        [Key]
        public int Report_ID { get; set; }

        public int? Reported_By { get; set; }

        public int? Reported_To { get; set; }

        [StringLength(50)]
        public string Report_Subject { get; set; }

        [StringLength(200)]
        public string Report_Details { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        public DateTime? Created_Date { get; set; }

        public DateTime? Closed_Date { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item_assignment> tbl_item_assignment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_report_history> tbl_report_history { get; set; }
    }
}
