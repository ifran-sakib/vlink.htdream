using PMS_HT.Areas.Services.Models;
using System.Data.Entity;

namespace PMS_HT.Models
{
    public partial class PMS_HT_DB : DbContext
    {
        public PMS_HT_DB() : base("name=PMS_HT_DB")
        {

        }

        public virtual DbSet<Prorgram_Purpose> Prorgram_Purpose { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tbl_amenities_details> tbl_amenities_details { get; set; }
        public virtual DbSet<tbl_amenities_setup> tbl_amenities_setup { get; set; }
        public virtual DbSet<tbl_attendance> tbl_attendance { get; set; }
        public virtual DbSet<tbl_banquet_amenities> tbl_banquet_amenities { get; set; }
        public virtual DbSet<tbl_banquet_booking> tbl_banquet_booking { get; set; }
        public virtual DbSet<tbl_banquet_info> tbl_banquet_info { get; set; }
        public virtual DbSet<tbl_banquet_prog> tbl_banquet_prog { get; set; }
        public virtual DbSet<tbl_banquet_transactions> tbl_banquet_transactions { get; set; }
        public virtual DbSet<tbl_brand> tbl_brand { get; set; }
        public virtual DbSet<tbl_category> tbl_category { get; set; }
        public virtual DbSet<tbl_communication> tbl_communication { get; set; }
        public virtual DbSet<tbl_complain_heading> tbl_complain_heading { get; set; }
        public virtual DbSet<tbl_conf_item_setup> tbl_conf_item_setup { get; set; }
        public virtual DbSet<tbl_conference_program> tbl_conference_program { get; set; }
        public virtual DbSet<tbl_conference_purpose> tbl_conference_purpose { get; set; }
        public virtual DbSet<tbl_conference_room> tbl_conference_room { get; set; }
        public virtual DbSet<tbl_conference_transactions> tbl_conference_transactions { get; set; }
        public virtual DbSet<tbl_conference_utility_details> tbl_conference_utility_details { get; set; }
        public virtual DbSet<tbl_conference_utility_master> tbl_conference_utility_master { get; set; }
        public virtual DbSet<tbl_country> tbl_country { get; set; }
        public virtual DbSet<tbl_currency_convert> tbl_currency_convert { get; set; }
        public virtual DbSet<tbl_currency_setup> tbl_currency_setup { get; set; }
        public virtual DbSet<tbl_deduction_head> tbl_deduction_head { get; set; }
        public virtual DbSet<tbl_department> tbl_department { get; set; }
        public virtual DbSet<tbl_designation> tbl_designation { get; set; }
        public virtual DbSet<tbl_dining_location> tbl_dining_location { get; set; }
        public virtual DbSet<tbl_dining_set_up> tbl_dining_set_up { get; set; }
        public virtual DbSet<tbl_dining_waiter> tbl_dining_waiter { get; set; }
        public virtual DbSet<tbl_earning_head> tbl_earning_head { get; set; }
        public virtual DbSet<tbl_emp_field_permission> tbl_emp_field_permission { get; set; }
        public virtual DbSet<tbl_emp_info> tbl_emp_info { get; set; }
        public virtual DbSet<tbl_emp_mail> tbl_emp_mail { get; set; }
        public virtual DbSet<tbl_emp_module_permission> tbl_emp_module_permission { get; set; }
        public virtual DbSet<tbl_emp_sub_module_permission> tbl_emp_sub_module_permission { get; set; }
        public virtual DbSet<tbl_field_permission> tbl_field_permission { get; set; }
        public virtual DbSet<tbl_floor_setup> tbl_floor_setup { get; set; }
        public virtual DbSet<tbl_group_employee> tbl_group_employee { get; set; }
        public virtual DbSet<tbl_group_field_permission> tbl_group_field_permission { get; set; }
        public virtual DbSet<tbl_group_info> tbl_group_info { get; set; }
        public virtual DbSet<tbl_group_module_permission> tbl_group_module_permission { get; set; }
        public virtual DbSet<tbl_group_sub_module_permission> tbl_group_sub_module_permission { get; set; }
        public virtual DbSet<tbl_guest_complain> tbl_guest_complain { get; set; }
        public virtual DbSet<tbl_guest_info> tbl_guest_info { get; set; }
        public virtual DbSet<tbl_guest_queries> tbl_guest_queries { get; set; }
        public virtual DbSet<tbl_guest_stay_history> tbl_guest_stay_history { get; set; }
        public virtual DbSet<tbl_guest_stay_info> tbl_guest_stay_info { get; set; }
        public virtual DbSet<tbl_hotel_service> tbl_hotel_service { get; set; }
        public virtual DbSet<tbl_item> tbl_item { get; set; }
        public virtual DbSet<tbl_item_assignment> tbl_item_assignment { get; set; }
        public virtual DbSet<tbl_item_order_details> tbl_item_order_details { get; set; }
        public virtual DbSet<tbl_item_order_master> tbl_item_order_master { get; set; }
        public virtual DbSet<tbl_laundry_item> tbl_laundry_item { get; set; }
        public virtual DbSet<tbl_laundry_item_category> tbl_laundry_item_category { get; set; }
        public virtual DbSet<tbl_laundry_service> tbl_laundry_service { get; set; }
        public virtual DbSet<tbl_laundry_type> tbl_laundry_type { get; set; }
        public virtual DbSet<tbl_leave> tbl_leave { get; set; }
        public virtual DbSet<tbl_leave_cause> tbl_leave_cause { get; set; }
        public virtual DbSet<tbl_leave_status> tbl_leave_status { get; set; }
        public virtual DbSet<tbl_maid_info> tbl_maid_info { get; set; }
        public virtual DbSet<tbl_module_permissions> tbl_module_permissions { get; set; }
        public virtual DbSet<tbl_package> tbl_package { get; set; }
        public virtual DbSet<tbl_property_information> tbl_property_information { get; set; }
        public virtual DbSet<tbl_purchase_details> tbl_purchase_details { get; set; }
        public virtual DbSet<tbl_purchase_master> tbl_purchase_master { get; set; }
        public virtual DbSet<tbl_register_book> tbl_register_book { get; set; }
        public virtual DbSet<tbl_religion> tbl_religion { get; set; }
        public virtual DbSet<tbl_report> tbl_report { get; set; }
        public virtual DbSet<tbl_report_history> tbl_report_history { get; set; }
        public virtual DbSet<tbl_return_germent_in> tbl_return_germent_in { get; set; }
        public virtual DbSet<tbl_room_info> tbl_room_info { get; set; }
        public virtual DbSet<tbl_room_status> tbl_room_status { get; set; }
        public virtual DbSet<tbl_room_temp_monitor> tbl_room_temp_monitor { get; set; }
        public virtual DbSet<tbl_room_transactions> tbl_room_transactions { get; set; }
        public virtual DbSet<tbl_room_view_setup> tbl_room_view_setup { get; set; }
        public virtual DbSet<tbl_roomtype_setup> tbl_roomtype_setup { get; set; }
        public virtual DbSet<tbl_salary_transactions> tbl_salary_transactions { get; set; }
        public virtual DbSet<tbl_stock> tbl_stock { get; set; }
        public virtual DbSet<tbl_stock_location> tbl_stock_location { get; set; }
        public virtual DbSet<tbl_sub_module_permission> tbl_sub_module_permission { get; set; }
        public virtual DbSet<tbl_subcategory> tbl_subcategory { get; set; }
        public virtual DbSet<tbl_supplier> tbl_supplier { get; set; }
        public virtual DbSet<tbl_supplier_ledger> tbl_supplier_ledger { get; set; }
        public virtual DbSet<tbl_tariff> tbl_tariff { get; set; }
        public virtual DbSet<tbl_task_assignment> tbl_task_assignment { get; set; }
        public virtual DbSet<tbl_task_schdule> tbl_task_schdule { get; set; }
        public virtual DbSet<tbl_task_setup> tbl_task_setup { get; set; }
        public virtual DbSet<tbl_tax> tbl_tax { get; set; }
        public virtual DbSet<tbl_tax_amount> tbl_tax_amount { get; set; }
        public virtual DbSet<tbl_travel_agent_info> tbl_travel_agent_info { get; set; }
        public virtual DbSet<tbl_travel_info> tbl_travel_info { get; set; }
        public virtual DbSet<tbl_travel_transactions> tbl_travel_transactions { get; set; }
        public virtual DbSet<tbl_unit> tbl_unit { get; set; }
        public virtual DbSet<tbl_unit_setup> tbl_unit_setup { get; set; }
        public virtual DbSet<tbl_vehicle> tbl_vehicle { get; set; }
        public virtual DbSet<tbl_bank_info> tbl_bank_info { get; set; }
        public virtual DbSet<tbl_branch_info> tbl_branch_info { get; set; }
        public virtual DbSet<tbl_account_info> tbl_account_info { get; set; }
        public virtual DbSet<tbl_bank_transaction> tbl_bank_transaction { get; set; }
        public virtual DbSet<tbl_expense> tbl_expense { get; set; }
        public virtual DbSet<tbl_expense_head> tbl_expense_head { get; set; }
        public virtual DbSet<tbl_laundry_expense> tbl_laundry_expense { get; set; }
        public virtual DbSet<tbl_laundry_status> tbl_laundry_status { get; set; }
        public virtual DbSet<tbl_laundry_service_cost> tbl_laundry_service_cost { get; set; }
        public virtual DbSet<tbl_vacant_room> tbl_vacant_room { get; set; }
        public virtual DbSet<tbl_others_income_head> tbl_others_income_head { get; set; }
        public virtual DbSet<tbl_others_income> tbl_others_income { get; set; }

        public DbSet<tbl_food_category> tbl_food_category { get; set; }
        public DbSet<tbl_food> tbl_food { get; set; }
        public DbSet<tbl_food_side_extra> tbl_food_side_extra { get; set; }
        public DbSet<tbl_order_method> tbl_order_method { get; set; }
        public DbSet<tbl_payment_method> tbl_payment_method { get; set; }
        public DbSet<tbl_restaurant_company_info> tbl_restaurant_company_info { get; set; }
        public DbSet<tbl_restaurant_guest_info> tbl_restaurant_guest_info { get; set; }
        public DbSet<tbl_item_order_ledger> tbl_item_order_ledger { get; set; }
        public DbSet<tbl_guest_attendee> tbl_guest_attendee { get; set; }
        public DbSet<tbl_company_info> tbl_company_info { get; set; }
        public DbSet<tbl_guest_group_info> tbl_guest_group_info { get; set; }
        public DbSet<tbl_messages> tbl_messages { get; set; }
        public DbSet<tbl_preferences> tbl_preferences { get; set; }

        public DbSet<tbl_food_recipe> tbl_food_recipe { get; set; }
        public DbSet<tbl_food_recipe_items> tbl_food_recipe_items { get; set; }

        public DbSet<tbl_service> tbl_service { get; set; }
        public DbSet<tbl_service_company_info> tbl_service_company_info { get; set; }
        public DbSet<tbl_service_guest_info> tbl_service_guest_info { get; set; }
        public DbSet<tbl_service_item> tbl_service_item { get; set; }
        public DbSet<tbl_service_purchase_detail> tbl_service_purchase_detail { get; set; }
        public DbSet<tbl_service_purchase_ledger> tbl_service_purchase_ledger { get; set; }
        public DbSet<tbl_service_purchase_master> tbl_service_purchase_master { get; set; }

        public DbSet<tbl_guest_group_post_master> tbl_guest_group_post_master { get; set; }
        public DbSet<tbl_guest_group_post_master_task> tbl_guest_group_post_master_task { get; set; }
        public DbSet<tbl_guest_group_room_category> tbl_guest_group_room_category { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tbl_attendance>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_brand>()
                .Property(e => e.Brand_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_category>()
                .Property(e => e.Cata_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_conference_utility_details>()
                .Property(e => e.Total_day)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tbl_country>()
                .Property(e => e.Country_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_currency_setup>()
                .HasMany(e => e.tbl_currency_convert)
                .WithOptional(e => e.tbl_currency_setup)
                .HasForeignKey(e => e.From_Currency);

            modelBuilder.Entity<tbl_currency_setup>()
                .HasMany(e => e.tbl_currency_convert1)
                .WithOptional(e => e.tbl_currency_setup1)
                .HasForeignKey(e => e.To_Currency);

            modelBuilder.Entity<tbl_deduction_head>()
                .Property(e => e.Deduction_Head_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_department>()
                .Property(e => e.Department_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_designation>()
                .Property(e => e.Designation_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_earning_head>()
                .Property(e => e.Earning_Head_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_earning_head>()
                .HasMany(e => e.tbl_salary_transactions)
                .WithOptional(e => e.tbl_earning_head)
                .HasForeignKey(e => e.Tran_Ref_ID);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Gender)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Contact_No)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Age)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Address)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .Property(e => e.Emp_Status)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_emp_mail)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Sender_ID);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_emp_mail1)
                .WithOptional(e => e.tbl_emp_info1)
                .HasForeignKey(e => e.Receiver_ID);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_group_info)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Created_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_group_info1)
                .WithOptional(e => e.tbl_emp_info1)
                .HasForeignKey(e => e.Updated_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_guest_complain)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Assigned_Emp);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_guest_complain1)
                .WithOptional(e => e.tbl_emp_info1)
                .HasForeignKey(e => e.Created_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_register_book)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Created_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_report)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Reported_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_report1)
                .WithOptional(e => e.tbl_emp_info1)
                .HasForeignKey(e => e.Reported_To);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_task_assignment)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Created_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_task_schdule)
                .WithOptional(e => e.tbl_emp_info)
                .HasForeignKey(e => e.Updated_By);

            modelBuilder.Entity<tbl_emp_info>()
                .HasMany(e => e.tbl_task_schdule1)
                .WithOptional(e => e.tbl_emp_info1)
                .HasForeignKey(e => e.Created_By);

            modelBuilder.Entity<tbl_floor_setup>()
                .Property(e => e.Floor_No)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_floor_setup>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_group_module_permission>()
                .Property(e => e.Module_Name)
                .IsFixedLength();

            modelBuilder.Entity<tbl_guest_stay_info>()
                .Property(e => e.No_of_Nights)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_hotel_service>()
                .Property(e => e.Service_Heading)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_hotel_service>()
                .Property(e => e.Service_Description)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_item>()
                .Property(e => e.Item_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_laundry_item>()
                .Property(e => e.Laundry_Item_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_laundry_item_category>()
                .Property(e => e.Laundry_Item_Cata_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_laundry_service>()
                .Property(e => e.Service_Heading_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_laundry_service>()
                .Property(e => e.Service_Description)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_laundry_type>()
                .Property(e => e.Type_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_leave>()
                .Property(e => e.Month_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_leave>()
                .Property(e => e.Year_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_leave>()
                .Property(e => e.Leave_Period)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_leave_cause>()
                .Property(e => e.Cause_Name)
                .IsUnicode(false);
            

            modelBuilder.Entity<tbl_package>()
                .Property(e => e.Package_Details)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_room_info>()
                .Property(e => e.Room_No)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_room_status>()
                .Property(e => e.Status_Heading)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_room_status>()
                .Property(e => e.Status_Description)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_room_view_setup>()
                .Property(e => e.View_type)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_room_view_setup>()
                .Property(e => e.View_details)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_roomtype_setup>()
                .Property(e => e.Roomtype_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_salary_transactions>()
                .Property(e => e.Tran_Ref_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_salary_transactions>()
                .Property(e => e.Month_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_salary_transactions>()
                .Property(e => e.Year_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_stock_location>()
                .Property(e => e.Stock_Location_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_subcategory>()
                .Property(e => e.Subcata_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_tariff>()
                .Property(e => e.Tariff_Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_tariff>()
                .Property(e => e.Tariff_Details)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_task_assignment>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_task_schdule>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_task_schdule>()
                .Property(e => e.Task_Schdule_Status)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_task_setup>()
                .Property(e => e.Task_Heading)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_task_setup>()
                .Property(e => e.Task_Description)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_travel_agent_info>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_unit>()
                .Property(e => e.Unit_Name)
                .IsUnicode(false);
        }

        


    }
}
