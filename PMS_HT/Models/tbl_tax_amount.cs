namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_tax_amount
    {
        [Key]
        public int Tax_Amount_ID { get; set; }

        [StringLength(50)]
        public string Tax_Refer { get; set; }

        public int? Tax_ID { get; set; }

        public decimal? Tax_Amount { get; set; }

        public virtual tbl_tax tbl_tax { get; set; }
    }
}
