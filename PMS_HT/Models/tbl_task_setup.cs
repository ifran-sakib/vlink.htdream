namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_task_setup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_task_setup()
        {
            tbl_task_assignment = new HashSet<tbl_task_assignment>();
            tbl_task_schdule = new HashSet<tbl_task_schdule>();
        }

        [Key]
        public int Task_ID { get; set; }

        [StringLength(100)]
        public string Task_Heading { get; set; }

        [StringLength(200)]
        public string Task_Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_assignment> tbl_task_assignment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_schdule> tbl_task_schdule { get; set; }
    }
}
