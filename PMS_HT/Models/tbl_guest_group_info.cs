﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_guest_group_info
    {
        [Key]
        public int Guest_Group_ID { get; set; }

        [Display(Name = "Group Name")]
        public string Group_Name { get; set; }

        [StringLength(150)]
        public string Address { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [Display(Name = "Arrival Date")]
        public DateTime Arrival_Date { get; set; }

        [Display(Name = "Departure Date")]
        public DateTime Departure_Date { get; set; }

        [Range(2, 200)]
        [Display(Name = "No Of Rooms")]
        public int No_Of_Rooms { get; set; }

        [Display(Name = "Reserve Date")]
        public DateTime? Reserve_Date { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime? Booking_Date { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [Display(Name = "Order Method")]
        public string Order_Method { get; set; }

        [Display(Name = "Airport Pickup")]
        public bool? Airport_Pickup { get; set; }

        public string Transportation { get; set; }

        [Display(Name = "Requests")]
        public string Special_Request { get; set; }

        public int Created_By { get; set; }



        [ForeignKey("Created_By")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }



        public virtual ICollection<tbl_guest_stay_info> tbl_guest_stay_infos { get; set; }

        public virtual ICollection<tbl_guest_group_post_master> tbl_guest_group_post_masters { get; set; }

        public virtual ICollection<tbl_guest_group_post_master_task> tbl_guest_group_post_master_tasks { get; set; }

        public virtual ICollection<tbl_guest_group_room_category> tbl_guest_group_room_category { get; set; }


        public tbl_guest_group_info()
        {
            tbl_guest_stay_infos = new HashSet<tbl_guest_stay_info>();
            tbl_guest_group_post_masters = new HashSet<tbl_guest_group_post_master>();
            tbl_guest_group_post_master_tasks = new HashSet<tbl_guest_group_post_master_task>();
            tbl_guest_group_room_category = new HashSet<tbl_guest_group_room_category>();
        }
    }
}