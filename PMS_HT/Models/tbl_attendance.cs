namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_attendance
    {
        [Key]
        public int Attendance_ID { get; set; }

        public int? Emp_ID { get; set; }

        public TimeSpan? Punch_In_Time { get; set; }

        public TimeSpan? Punch_Out_Time { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(200)]
        public string Comments { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}
