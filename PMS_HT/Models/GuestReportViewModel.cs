﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class GuestReportViewModel
    {
        public string Room_No { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime Arrival_Date { get; set; }
        public DateTime Departure_Date { get; set; }
        public string Check_Out_Time { get; set; }
    }
}