namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_purchase_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_purchase_master()
        {
            tbl_purchase_details = new HashSet<tbl_purchase_details>();
            tbl_supplier_ledger = new HashSet<tbl_supplier_ledger>();
        }

        [Key]
        public int Purchase_Master_ID { get; set; }

        [StringLength(50)]
        public string Memo_No { get; set; }

        public decimal? Memo_Total { get; set; }

        public decimal? Advanced_Amount { get; set; }

        public decimal? Discount { get; set; }

        public int? Supplier_ID { get; set; }

        public DateTime? Purchase_Date { get; set; }

        public string status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchase_details> tbl_purchase_details { get; set; }

        public virtual ICollection<tbl_supplier_ledger> tbl_supplier_ledger { get; set; }

        [ForeignKey("Supplier_ID")]
        public virtual tbl_supplier tbl_supplier { get; set; }
    }
}
