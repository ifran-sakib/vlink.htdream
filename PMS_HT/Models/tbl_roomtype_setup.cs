namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_roomtype_setup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_roomtype_setup()
        {
            tbl_room_info = new HashSet<tbl_room_info>();
        }

        [Key]
        public int Roomtype_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Room Category")]
        public string Roomtype_Name { get; set; }

        public bool? YsnActive { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? Created_Date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_room_info> tbl_room_info { get; set; }
    }
}
