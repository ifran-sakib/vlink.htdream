namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_currency_convert
    {
        [Key]
        public int Convert_ID { get; set; }

        public int? From_Currency { get; set; }

        public int? To_Currency { get; set; }

        public decimal? Conversion_Rate { get; set; }

        public virtual tbl_currency_setup tbl_currency_setup { get; set; }

        public virtual tbl_currency_setup tbl_currency_setup1 { get; set; }
    }
}
