﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class tbl_laundry_status
    {
        [Key]
        public int Laundry_Status_ID { get; set; }
        public int? Guest_Tran_ID { get; set; }
        [StringLength(20)]
        public string Status { get; set; }
        [ForeignKey("Guest_Tran_ID")]
        public virtual tbl_room_transactions tbl_room_transactions { get; set; }
    }
}