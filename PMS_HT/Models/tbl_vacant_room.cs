﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_vacant_room
    {
        [Key]
        public int? Vacant_Room_Id { get; set; }
        public int Room_ID { get; set; }
        public decimal Rate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [ForeignKey("Room_ID")]
        public virtual tbl_room_info tbl_room_info { get; set; }
    }
}