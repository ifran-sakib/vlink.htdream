namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_item_order_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_item_order_master()
        {
            tbl_item_order_details = new HashSet<tbl_item_order_details>();
            tbl_item_order_ledger = new HashSet<tbl_item_order_ledger>();
            tbl_dining_set_up = new HashSet<tbl_dining_set_up>();
            tbl_dining_waiter = new HashSet<tbl_dining_waiter>();
        }

        [Key]
        public int Order_Master_ID { get; set; }

        [Display(Name = "Order Date")]
        public DateTime? Order_Date { get; set; }

        [Display(Name = "Serve Date")]
        public DateTime? Serve_Date { get; set; }

        [Display(Name = "Payment Date")]
        public DateTime? Payment_Date { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [Display(Name = "Total Person")]
        public int? Total_Person { get; set; }

        [Display(Name = "Order Methode")]
        public int? Order_Method_ID { get; set; }

        [Display(Name = "Payment Method")]
        public int? Pay_Method_ID { get; set; }

        [StringLength(50)]
        public string Banquet_Refer { get; set; }

        public int? Program_ID { get; set; }

        public int? Stay_Info_ID { get; set; }

        public string Note { get; set; }

        public int? Received_By_ID { get; set; }

        public int? Rest_Guest_ID { get; set; }

        public int? Rest_Company_ID { get; set; }



        [ForeignKey("Order_Method_ID")]
        public virtual tbl_order_method tbl_order_method { get; set; }

        [ForeignKey("Pay_Method_ID")]
        public virtual tbl_payment_method tbl_payment_method { get; set; }

        [ForeignKey("Stay_Info_ID")]
        public virtual tbl_guest_stay_info tbl_guest_stay_info { get; set; }

        [ForeignKey("Received_By_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        [ForeignKey("Program_ID")]
        public virtual tbl_banquet_prog tbl_banquet_prog { get; set; }

        [ForeignKey("Rest_Guest_ID")]
        public virtual tbl_restaurant_guest_info tbl_restaurant_guest_info { get; set; }

        [ForeignKey("Rest_Company_ID")]
        public virtual tbl_restaurant_company_info tbl_restaurant_company_info { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item_order_details> tbl_item_order_details { get; set; }

        public virtual ICollection<tbl_item_order_ledger> tbl_item_order_ledger { get; set; }

        public virtual ICollection<tbl_dining_set_up> tbl_dining_set_up { get; set; }

        public virtual ICollection<tbl_dining_waiter> tbl_dining_waiter { get; set; }
        
    }
}
