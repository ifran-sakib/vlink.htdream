namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_banquet_prog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_banquet_prog()
        {
            tbl_banquet_transactions = new HashSet<tbl_banquet_transactions>();
            tbl_item_order_master = new HashSet<tbl_item_order_master>();
        }

        [Key]
        public int Program_ID { get; set; }

        public int? Banquet_ID { get; set; }

        [StringLength(50)]
     
        public string Person_Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Person Address")]
        public string Person_Adress { get; set; }
       

        [StringLength(50)]
        [Display(Name = "Person Phone")]
        public string Person_Phone { get; set; }
 
        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? Country_ID { get; set; }
        [Display(Name ="Company Name")]
        [StringLength(50)]
        public string Company_Name { get; set; }
        
        [StringLength(100)]
        [Display(Name = "Company Address")]

        public string Company_Adress { get; set; }

        [StringLength(50)]
        [Display(Name = "Company Phone")]
        public string Company_Phone { get; set; }
     
        [StringLength(100)]
        [Display(Name = "Company Details")]
        public string Company_Details { get; set; }
    
        [StringLength(50)]
        [Display(Name = "Contact Person")]
        public string Contact_Person { get; set; }

        [StringLength(50)]
        [Display(Name = "Contact Person Phone")]
        public string Contact_Person_Phone { get; set; }

        [StringLength(100)]
        [Display(Name = "Contact Person Address")]
        public string Contact_Person_Address { get; set; }
        
        public virtual tbl_banquet_info tbl_banquet_info { get; set; }

        public virtual tbl_country tbl_country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_banquet_transactions> tbl_banquet_transactions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item_order_master> tbl_item_order_master { get; set; }
    }
}
