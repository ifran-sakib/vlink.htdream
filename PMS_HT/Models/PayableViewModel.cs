﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class PayableViewModel
    {
        public int? EmpId { get; set; }    
        public string EmpName { get; set; }
        public string MonthName { get; set; }
        public string YearName { get; set; }
        public decimal? Value { get; set; }
        public string Earning_Head { get; set; }
        public string Deduction_Head { get; set; }
        public string VParticular { get; set; }
        public decimal Payable { get; set; }
        public decimal Paid { get; set; }
        public decimal Due { get; set; }
        public DateTime? Date { get; set; }
    }
}