namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_purpose
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_conference_purpose()
        {
            tbl_conference_program = new HashSet<tbl_conference_program>();
        }

        [Key]
        public int Conf_Purpose_ID { get; set; }

        [StringLength(50)]
        public string Conf_Purpose_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_program> tbl_conference_program { get; set; }
    }
}
