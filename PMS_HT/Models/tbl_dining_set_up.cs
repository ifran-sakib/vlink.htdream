namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_dining_set_up
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_dining_set_up()
        {
            tbl_item_order_master = new HashSet<tbl_item_order_master>();
        }

        [Key]
        public int Dining_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Table No")]
        public string Dining_No { get; set; }

        public int? Capacity { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public int? Dining_Location_ID { get; set; }

        public int? Dining_Waiter_ID { get; set; }

        [ForeignKey("Dining_Location_ID")]
        public virtual tbl_dining_location tbl_dining_location { get; set; }

        [ForeignKey("Dining_Waiter_ID")]
        public virtual tbl_dining_waiter tbl_dining_waiter { get; set; }

        public virtual ICollection<tbl_item_order_master> tbl_item_order_master { get; set; }
    }
}
