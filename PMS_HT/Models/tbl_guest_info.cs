namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tbl_guest_info
    {      
        [Key]
        public int Guest_ID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(512)]
        [Display(Name = "Present Address")]
        public string Address { get; set; }

        [StringLength(512)]
        [Display(Name = "Permanent Address")]
        public string Permanent_Address { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        [Display(Name = "Phone 2")]
        public string AdditionalPhone { get; set; }

        [StringLength(50)]
        [Display(Name = "Father's Name")]
        public string Father_Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Mother's Name")]
        public string Mother_Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Marital Status")]
        public string Maritial_Status { get; set; }

        [StringLength(50)]
        [Display(Name = "Spouse Name")]
        public string Spouse_Name { get; set; }
        [Display(Name = "National ID")]
        public string National_ID { get; set; }

        [StringLength(500)]
        [Display(Name = "Contact Info for relative in Chittagong")]
        public string Nearest_Relative_Contact_Info { get; set; }

        public string Age { get; set; }

        [Display(Name = "Identification Mark")]
        public string Identification_Mark { get; set; }

        [Display(Name = "Skin Color")]
        public string Skin_Color { get; set; }

        public string Height { get; set; }

        [Display(Name = "Occupation  (with address)")]
        public string Occupation { get; set; }

        [StringLength(50)]
        [Display(Name = "Driving License")]
        public string Driving_License { get; set; }

        [StringLength(50)]
        [Display(Name = "Tin Certificate")]
        public string Tin_Certificate { get; set; }

        [StringLength(100)]
        public string Image { get; set; }

        public string Signature { get; set; }

        public string Fingerprint { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        public int? Religion_ID { get; set; }

        public int? Country_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Passport No")]
        public string Passport_No { get; set; }

        [StringLength(50)]
        [Display(Name = "Visa No")]
        public string Visa_No { get; set; }

        [Display(Name = "Visa Date")]
        public DateTime? Visa_Date { get; set; }

        [Display(Name = "Arrived From")]
        [StringLength(50)]
        public string Arrived_From { get; set; }

        [StringLength(50)]
        public string Arrival_Transportation { get; set; }

        public DateTime? Date_of_Arr_in_Country { get; set; }

        public TimeSpan? Time_of_Arr_in_Country { get; set; }

        [StringLength(50)]
        [Display(Name = "Purpose of visit")]
        public string Purpose_of_visit { get; set; }

        [StringLength(50)]
        [Display(Name = "Going To")]
        public string Going_To { get; set; }

        [StringLength(100)]
        public string Depart_Transportation { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Birth Date")]
        public DateTime? Birth_Date { get; set; }

        [Display(Name = "Special Preference")]
        public string Special_Preference { get; set; }

        public bool? DNR { get; set; }

        public bool? VIP { get; set; }



        [ForeignKey("Country_ID")]
        public virtual tbl_country tbl_country { get; set; }

        public virtual tbl_religion tbl_religion { get; set; }




        public virtual ICollection<tbl_communication> tbl_communication { get; set; }        

        public virtual ICollection<tbl_guest_stay_history> tbl_guest_stay_history { get; set; }

        public virtual ICollection<tbl_guest_stay_info> tbl_guest_stay_info { get; set; }

        public virtual ICollection<tbl_room_transactions> tbl_room_transactions { get; set; }

        public virtual ICollection<tbl_register_book> tbl_register_book { get; set; }


        public tbl_guest_info()
        {
            tbl_communication = new HashSet<tbl_communication>();
            tbl_guest_stay_history = new HashSet<tbl_guest_stay_history>();
            tbl_guest_stay_info = new HashSet<tbl_guest_stay_info>();
            tbl_room_transactions = new HashSet<tbl_room_transactions>();
            tbl_register_book = new HashSet<tbl_register_book>();
        }
    }
}

