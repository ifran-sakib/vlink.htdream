namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_group_employee
    {
        [Key]
        public int Group_Emp_ID { get; set; }

        public int? Group_ID { get; set; }

        public int? Emp_ID { get; set; }

        public bool? YsnActive { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_group_info tbl_group_info { get; set; }
    }
}
