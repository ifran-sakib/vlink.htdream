﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class tbl_branch_info
    {
        [Key]
        public int? Branch_ID { get; set; }
        [StringLength(50)]
        [Display(Name = "Branch Name")]
        public string Branch_Name { get; set; }
       
      
        [StringLength(100)]
      
        public string Address { get; set; }
    }
}