﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class ExpenseGroupWise
    {
        public int? Exp_ID { get; set; }
        public int? Exp_Head_ID { get; set; }
        public string Exp_Head_Name { get; set; }
        public decimal? Value { get; set; }
        public DateTime? date { get; set; }

    }
}