using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class DailyRoomLettingViewModel
    {
        public string Room_No { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public DateTime Arrival_Date { get; set; }
        public decimal Stay { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal CurrentBill { get; set; }
	public decimal OtherBill { get; set; }
        public decimal Total { get; set; }
        public decimal Payment { get; set; }
        public DateTime Tran_Date { get; set; }
        public decimal Balance { get; set; }
        public decimal Advanced_Amount { get; set; }
        public decimal Discount { get; set; }
        public DateTime CheckOut_Time { get; set; }
        public decimal Staff_Commission { get; set; }
        public decimal Baddebt { get; set; }
        public decimal Refund { get; set; }
        public decimal Surplus { get; set; }
        public string CheckOutString { get; set; }
        public string Status { get; set; }
    }

}