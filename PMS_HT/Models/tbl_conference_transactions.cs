namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_transactions
    {
        [Key]
        public int Conf_Tran_ID { get; set; }

        public int? Conf_Program_ID { get; set; }

        public int? Tran_Ref_ID { get; set; }

        [StringLength(50)]
        public string Tran_Ref_Name { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public DateTime? Tran_Date { get; set; }

        public virtual tbl_conference_program tbl_conference_program { get; set; }
    }
}
