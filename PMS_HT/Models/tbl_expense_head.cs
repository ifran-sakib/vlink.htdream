﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.ComponentModel;

namespace PMS_HT.Models
{
    public class tbl_expense_head
    {
        [Key]
        public int? Exp_Head_ID { get; set; }
        [StringLength(50)]
        [DisplayName("Expense Head")]
        public string Exp_Head_Name { get; set; }
        [StringLength(50)]
 
        [DisplayName("Description")]
        public string Exp_Description { get; set; }
         
    }
}