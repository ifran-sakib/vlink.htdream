﻿using PMS_HT.Areas.Services.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_guest_group_post_master_task
    {
        [Key]
        public int PM_Task_ID { get; set; }

        public int Guest_Group_ID { get; set; }

        public int Service_ID { get; set; }

        public int Post_Master_ID { get; set; }



        [ForeignKey("Guest_Group_ID")]
        public virtual tbl_guest_group_info tbl_guest_group_info { get; set; }

        [ForeignKey("Service_ID")]
        public virtual tbl_service tbl_service { get; set; }

        [ForeignKey("Post_Master_ID")]
        public virtual tbl_guest_group_post_master tbl_guest_group_post_master { get; set; }
    }
}