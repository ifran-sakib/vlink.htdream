﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_food_category
    {
        public tbl_food_category()
        {
            tbl_food = new HashSet<tbl_food>();
        }
        [Key]
        [Display(Name = "Category ID")]
        public int Food_Cate_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Category Name")]
        public string Food_Cate_Name { get; set; }

        public string Note { get; set; }

        [Display(Name = "Serial No")]
        public int? Serial_No { get; set; }

        public virtual ICollection<tbl_food> tbl_food { get; set; }
    }
}