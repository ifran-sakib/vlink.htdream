﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace PMS_HT.Models
{
    public class tbl_laundry_expense
    {
        [Key]
        public int Laundry_Expense_ID { get; set; }
        public int? Guest_Tran_ID { get; set; }

        public int? Laundry_Type_ID { get; set; }

        public decimal? Amount { get; set; }

        public string Particulars { get; set; }
        [ForeignKey("Guest_Tran_ID")]
        public virtual tbl_room_transactions tbl_room_transactions { get; set; }
        [ForeignKey("Laundry_Type_ID")]
        public virtual tbl_laundry_type tbl_laundry_type { get; set; }
    }
}