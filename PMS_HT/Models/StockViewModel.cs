﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class StockViewModel
    {
       
        public int Item_ID { get; set; }
        public int Stock_Location_ID { get; set; }
        public string Item_Name { get; set; }
        public string Stock_Location_Name { get; set; }
        public string Unit_Name { get; set; }
        public decimal Quantity { get; set; }
    }
}