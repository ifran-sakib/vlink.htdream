namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_hotel_service
    {
        [Key]
        public int Service_ID { get; set; }

        [StringLength(50)]
        public string Service_Heading { get; set; }

        [StringLength(100)]
        public string Service_Description { get; set; }

        public decimal? Cost { get; set; }
    }
}
