﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class PurchaseMDVM
    {
        public string Memo_No { get; set; }

        public decimal? Memo_Total { get; set; }

        public decimal? Advanced_Amount { get; set; }

        public decimal? Discount { get; set; }
        public int? Supplier_ID { get; set; }

        public DateTime? Purchase_Date { get; set; }
        public List<tbl_purchase_details> PurchaseDetails { get; set; }
    }
}