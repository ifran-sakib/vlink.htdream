namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_supplier_ledger
    {
        [Key]
        public int Supplier_Ledger_ID { get; set; }

        public int? Supplier_ID { get; set; }

        public int? Purchase_Master_ID { get; set; }

        public int? Tran_Ref_ID { get; set; }

        [StringLength(100)]
        public string Tran_Ref_Name { get; set; }
        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }
        
        public DateTime? Transaction_Date { get; set; }

        [StringLength(50)]
        public string Particulars { get; set; }

        public virtual tbl_supplier tbl_supplier { get; set; }

        [ForeignKey("Purchase_Master_ID")]
        public virtual tbl_purchase_master tbl_purchase_master { get; set; }
    }
}
