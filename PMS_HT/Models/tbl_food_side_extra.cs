﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_food_side_extra
    {
        [Key]
        public int Food_Side_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Food Side Name")]
        public string Food_Side_Name { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        public string Image { get; set; }

        public bool? Availability { get; set; }

        public decimal? Price { get; set; }

        public decimal? Discount { get; set; }
    }
}