namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_floor_setup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_floor_setup()
        {
            tbl_room_info = new HashSet<tbl_room_info>();
        }

        [Key]
        public int Floor_ID { get; set; }

        [StringLength(50)]
        [Display(Name = "Floor No")]
        public string Floor_No { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

        public bool? YsnActive { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? Created_Date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_room_info> tbl_room_info { get; set; }
    }
}
