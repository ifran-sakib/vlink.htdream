namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_emp_field_permission
    {
        [Key]
        public int Emp_Field_ID { get; set; }

        public int? Emp_ID { get; set; }

        [StringLength(50)]
        public string Field_Name { get; set; }

        public bool? Viewable { get; set; }

        public bool? Editable { get; set; }

        public int? Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        public int? Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}
