namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_stock
    {
        [Key]
        public int Stock_ID { get; set; }

        public int? Stock_Location_ID { get; set; }

        public int? Item_ID { get; set; }
        public int? Purchase_Details_ID { get; set; }

        public decimal? Stock_In { get; set; }

        public decimal? Stock_Out { get; set; }

        public DateTime? stock_change_date { get; set; }

        public string note { get; set; }

        [ForeignKey("Item_ID")]
        public virtual tbl_item tbl_item { get; set; }

        [ForeignKey("Stock_Location_ID")]
        public virtual tbl_stock_location tbl_stock_location { get; set; }
    }
}
