﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_food_recipe_items
    {
        [Key]
        public int Recipe_Item_ID { get; set; }

        public int? Recipe_ID { get; set; }

        public int? Item_ID { get; set; }

        public decimal? Quantity { get; set; }

        [ForeignKey("Item_ID")]
        public virtual tbl_item tbl_item { get; set; }
    }
}