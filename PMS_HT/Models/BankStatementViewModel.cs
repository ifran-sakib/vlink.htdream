﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class BankStatementViewModel
    {
        public DateTime date { get; set; }
        public string particular { get; set; }
        public decimal debit { get; set; }
        public decimal credit { get; set; }
        public decimal balance { get; set; }
    }
}