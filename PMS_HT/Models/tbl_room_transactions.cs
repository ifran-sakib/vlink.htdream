using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public partial class tbl_room_transactions
    {
        [Key]
        public int Guest_Tran_ID { get; set; }

        [Display(Name="Room No")]
        public int? Room_ID { get; set; }

        [Display(Name="Guest ID")]
        public int? Guest_ID { get; set; }

        [Display(Name="Employee")]
        public int? Emp_ID { get; set; }

        [Display(Name="Tran Ref ID")]
        public int? Tran_Ref_ID { get; set; }

        [StringLength(100)]
        [Display(Name="Tran Ref Name")]
        public string Tran_Ref_Name { get; set; }

        [StringLength(200)]
        public string Particulars { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        [Display(Name="Tran Date")]
        public DateTime? Tran_Date { get; set; }

        [Display(Name="Stay Info ID")]
        public int? Stay_Info_ID { get; set; }

        [Display(Name="laundry Item ID")]
        public int? Laundry_Item_ID { get; set; }

        [Display(Name="Laundry Service ID")]
        public int? Laundry_Service_ID { get; set; }

        [Display(Name="Garment ID")]
        public int? Garment_In_ID { get; set; }



        public virtual tbl_guest_info tbl_guest_info { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }

        [ForeignKey("Stay_Info_ID")]
        public virtual tbl_guest_stay_info tbl_guest_stay_info { get; set; }

        [ForeignKey("Laundry_Item_ID")]
        public virtual tbl_laundry_item tbl_laundry_item { get; set; }

        [ForeignKey("Laundry_Service_ID")]
        public virtual tbl_laundry_service tbl_laundry_service { get; set; }

        [ForeignKey("Garment_In_ID")]
        public virtual tbl_return_germent_in tbl_return_germent_in { get; set; }

        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}
