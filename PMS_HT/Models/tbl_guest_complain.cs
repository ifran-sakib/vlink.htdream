namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_guest_complain
    {
        [Key]
        public int Complain_ID { get; set; }

        public int? Heading_ID { get; set; }

        [StringLength(100)]
        public string Complain_Details { get; set; }

        public int? Assigned_Emp { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public int? Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        public virtual tbl_complain_heading tbl_complain_heading { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_emp_info tbl_emp_info1 { get; set; }
    }
}
