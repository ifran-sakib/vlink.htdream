﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class VacantRoomViewModel
    {
        public int Room_ID { get; set; }
        public string RoomNo { get; set; }
        public string Room_No { get; set; }
        public decimal Rate { get; set; }

        public DateTime Date { get; set; }
    }
}