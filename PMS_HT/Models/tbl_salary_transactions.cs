namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_salary_transactions
    {
        [Key]
      
        public int Salary_Tran_ID { get; set; }

        public int? Emp_ID { get; set; }

        public int? Tran_Ref_ID { get; set; }

        [StringLength(200)]
        public string Particular { get; set; }

        [StringLength(50)]
        public string Tran_Ref_Name { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public decimal? Balance { get; set; }

        [StringLength(50)]
        public string Month_Name { get; set; }

        [StringLength(50)]
        public string Year_Name { get; set; }

        public DateTime? Date { get; set; }

        public virtual tbl_earning_head tbl_earning_head { get; set; }
        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}
