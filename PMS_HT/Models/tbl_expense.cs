﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_expense
    {
        [Key]
        public int? Exp_ID { get; set; }

        public int? Exp_Head_ID { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        public int? Creted_By { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Created_Date { get; set; }

        public int? Modified_By { get; set; }

        public DateTime? Modified_Date { get; set; }




        [ForeignKey("Exp_Head_ID")]
        public virtual tbl_expense_head tbl_expense_head { get; set; }

        [ForeignKey("Creted_By")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        [ForeignKey("Modified_By")]
        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

    }
}