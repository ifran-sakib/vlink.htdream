using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public partial class tbl_guest_stay_info
    {
        
        [Key]
        public int Stay_Info_ID { get; set; }

        [Display(Name = "Arrival Date")]
        public DateTime Arrival_Date { get; set; }

        [Display(Name = "Departure Date")]
        public DateTime Departure_Date { get; set; }        

        [StringLength(10)]
        [Display(Name = "Nights")]
        public string No_of_Nights { get; set; }

        [Display(Name = "Total Adults")]
        public int? No_of_Adult { get; set; }

        [Display(Name = "Total Children")]
        public int? No_of_Child { get; set; }

        [Display(Name = "Extra Person")]
        public int? No_of_Extra_Person { get; set; }

        [Display(Name = "Extra Bed")]
        public int? No_of_Extra_Bed { get; set; }

        [StringLength(20)]
        public string Status { get; set; }
        
        [Display(Name = "Room No")]
        public int? Room_ID { get; set; }
        
        public int? Guest_ID { get; set; }

        public int? Company_ID { get; set; }

        public int? Guest_Group_ID { get; set; }

        public int? Travel_Agent_ID { get; set; }

        [Display(Name = "Reservation Date")]
        public DateTime? Reserved_Date { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime? Booking_Date { get; set; }

        [Display(Name = "Checkout Date")]
        public DateTime? Check_Out_Date { get; set; }

        [Display(Name = "Checkout Time")]
        public TimeSpan? Check_Out_Time { get; set; }

        public bool? IsConfirmed { get; set; }

        public bool? InOrOut { get; set; }

        [Display(Name = "Airport Pickup")]
        public bool? Airport_Pickup { get; set; }

        public string Transportation { get; set; }

        [Display(Name = "Requests")]
        public string Special_Request { get; set; }

        [StringLength(100)]
        [Display(Name = "CheckOut Note")]
        public string Check_Out_Note { get; set; }

        [StringLength(100)]
        public string Image { get; set; }

        public string Signature { get; set; }




        [ForeignKey("Guest_ID")]
        public virtual tbl_guest_info tbl_guest_info { get; set; }

        [ForeignKey("Room_ID")]
        public virtual tbl_room_info tbl_room_info { get; set; }

        [ForeignKey("Company_ID")]
        public virtual tbl_company_info tbl_company_info { get; set; }

        [ForeignKey("Guest_Group_ID")]
        public virtual tbl_guest_group_info tbl_guest_group_info { get; set; }

        [ForeignKey("Travel_Agent_ID")]
        public virtual tbl_travel_agent_info tbl_travel_agent_info { get; set; }



        public virtual ICollection<tbl_room_transactions> tbl_room_transactions { get; set; }

        public virtual ICollection<tbl_messages> tbl_messages { get; set; }

        public virtual ICollection<tbl_package> tbl_package { get; set; }



        public tbl_guest_stay_info()
        {
            tbl_room_transactions = new HashSet<tbl_room_transactions>();
            tbl_messages = new HashSet<tbl_messages>();
            tbl_package = new HashSet<tbl_package>();
        }
    }
}
