﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class BanquetCreditListViewModel
    {
        public string Banquet_Name { get; set; }
        public string Person_Name { get; set; }
        public string Person_Adress { get; set; }
		public string Company_Name { get; set; }
        public string Company_Adress { get; set; } 
		public DateTime? Start_Date { get; set; } 
        public DateTime? End_Date { get; set; }
		public string Purpose_Name { get; set; }  
		public decimal? Debit { get; set; } 
		public decimal? Credit { get; set; }
        public decimal? Due { get; set; }
    }
}