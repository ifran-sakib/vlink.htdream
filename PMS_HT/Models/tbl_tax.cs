namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_tax
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_tax()
        {
            tbl_conference_room = new HashSet<tbl_conference_room>();
            tbl_tax_amount = new HashSet<tbl_tax_amount>();
        }

        [Key]
        public int Tax_ID { get; set; }

        [StringLength(50)]
        public string Heading { get; set; }

        public decimal? Parcentage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_room> tbl_conference_room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_tax_amount> tbl_tax_amount { get; set; }
    }
}
