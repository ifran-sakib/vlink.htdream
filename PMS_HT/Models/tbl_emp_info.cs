namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;
    public partial class tbl_emp_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_emp_info()
        {
            tbl_attendance = new HashSet<tbl_attendance>();
            tbl_dining_waiter = new HashSet<tbl_dining_waiter>();
            tbl_emp_field_permission = new HashSet<tbl_emp_field_permission>();
            tbl_emp_mail = new HashSet<tbl_emp_mail>();
            tbl_emp_mail1 = new HashSet<tbl_emp_mail>();
            tbl_emp_module_permission = new HashSet<tbl_emp_module_permission>();
            tbl_emp_sub_module_permission = new HashSet<tbl_emp_sub_module_permission>();
            tbl_group_employee = new HashSet<tbl_group_employee>();
            tbl_group_info = new HashSet<tbl_group_info>();
            tbl_group_info1 = new HashSet<tbl_group_info>();
            tbl_guest_complain = new HashSet<tbl_guest_complain>();
            tbl_guest_complain1 = new HashSet<tbl_guest_complain>();
            tbl_item_order_master = new HashSet<tbl_item_order_master>();
            tbl_leave_status = new HashSet<tbl_leave_status>();
            tbl_leave = new HashSet<tbl_leave>();
            tbl_register_book = new HashSet<tbl_register_book>();
            tbl_report = new HashSet<tbl_report>();
            tbl_report1 = new HashSet<tbl_report>();
            tbl_task_assignment = new HashSet<tbl_task_assignment>();
            tbl_task_schdule = new HashSet<tbl_task_schdule>();
            tbl_task_schdule1 = new HashSet<tbl_task_schdule>();
        }

        [Key]
        public int Emp_ID { get; set; }

        [Display(Name = "Name")]
        [StringLength(50)]
        public string Emp_Name { get; set; }

        [Display(Name = "Gender")]
        [StringLength(50)]
        public string Emp_Gender { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime? Emp_DOB { get; set; }

        [Display(Name = "Contact No")]
        [StringLength(50)]
        public string Emp_Contact_No { get; set; }

        [Display(Name = "Age")]
        [StringLength(50)]
        public string Emp_Age { get; set; }

        [Display(Name = "Address")]
        [StringLength(50)]
        public string Emp_Address { get; set; }

        [DisplayName("Department")]
        public int? Department_ID { get; set; }

        [DisplayName("Designation")]
        public int? Designation_ID { get; set; }

        [Display(Name = "Status")]
        [StringLength(50)]
        public string Emp_Status { get; set; }

        [Display(Name = "Hired Date")]
        public DateTime? Date_Hired { get; set; }

        [StringLength(40)]
        public string user_name { get; set; }

        [MinLength(8)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        public bool? IsAuthenticated { get; set; }

        [StringLength(100)]
        public string Image { get; set; }

        [NotMapped]
        public HttpPostedFileBase File { get; set; }

        public Nullable<short> user_type { get; set; }

        [StringLength(50)]
        public string NID { get; set; }

        [Display(Name = "Discount Level")]
        [Range(0,100)]
        public decimal? Discount_Lavel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_attendance> tbl_attendance { get; set; }

        public virtual tbl_department tbl_department { get; set; }

        public virtual tbl_designation tbl_designation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_dining_waiter> tbl_dining_waiter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_field_permission> tbl_emp_field_permission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_mail> tbl_emp_mail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_mail> tbl_emp_mail1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_module_permission> tbl_emp_module_permission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_sub_module_permission> tbl_emp_sub_module_permission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_employee> tbl_group_employee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_info> tbl_group_info { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_info> tbl_group_info1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_guest_complain> tbl_guest_complain { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_guest_complain> tbl_guest_complain1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item_order_master> tbl_item_order_master { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_leave_status> tbl_leave_status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_leave> tbl_leave { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_register_book> tbl_register_book { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_report> tbl_report { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_report> tbl_report1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_assignment> tbl_task_assignment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_schdule> tbl_task_schdule { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_task_schdule> tbl_task_schdule1 { get; set; }
    }
}
