﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    [NotMapped]
    public class LaundryTransactionViewModel
    {
        public int? Room_ID { get; set; }
        public string Room_No { get; set; }
        public int? Guest_ID { get; set; }
        public string Name { get; set; }
        public decimal? Debit { get; set; }
    }
}