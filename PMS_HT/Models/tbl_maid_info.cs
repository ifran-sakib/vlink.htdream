namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    public partial class tbl_maid_info
    {
        public tbl_maid_info()
        {
            tbl_task_assignment = new HashSet<tbl_task_assignment>();
            tbl_task_schdule = new HashSet<tbl_task_schdule>();
        }

        [Key]
        public int Maid_ID { get; set; }

        public int? Emp_ID { get; set; }

        public string Status { get; set; }

        [StringLength(50)]
        public string Note { get; set; }

        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual ICollection<tbl_task_assignment> tbl_task_assignment { get; set; }

        public virtual ICollection<tbl_task_schdule> tbl_task_schdule { get; set; }
    }
}
