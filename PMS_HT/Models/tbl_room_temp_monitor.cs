namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_room_temp_monitor
    {
        [Key]
        public int Room_Temp_ID { get; set; }

        public int? Room_ID { get; set; }

        [StringLength(50)]
        public string Temperature { get; set; }

        [StringLength(50)]
        public string Note { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }
    }
}
