namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_utility_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_conference_utility_master()
        {
            tbl_conference_utility_details = new HashSet<tbl_conference_utility_details>();
        }

        [Key]
        public int Utility_Master_ID { get; set; }

        public decimal? Total_Amount { get; set; }

        public decimal? Discount { get; set; }

        public DateTime? Order_Date { get; set; }

        public int? Conf_Program_ID { get; set; }

        public virtual tbl_conference_program tbl_conference_program { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_utility_details> tbl_conference_utility_details { get; set; }
    }
}
