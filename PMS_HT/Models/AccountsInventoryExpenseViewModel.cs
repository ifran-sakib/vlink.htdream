using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class AccountsInventoryExpenseViewModel
    {
        public string Memo_No { get; set; }
        public decimal? PurchaseExpense { get; set; }
        public DateTime? PurchaseDate { get; set; }
    }
}