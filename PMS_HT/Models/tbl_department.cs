namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_department()
        {
            tbl_emp_info = new HashSet<tbl_emp_info>();
        }

        [Key]
        public int Department_ID { get; set; }
     
        [StringLength(50)]
        [DisplayName("Department")]
        public string Department_Name { get; set; }

        public bool? IsBuiltIn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_emp_info> tbl_emp_info { get; set; }
    }
}
