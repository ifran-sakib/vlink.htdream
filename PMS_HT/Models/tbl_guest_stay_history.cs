using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public partial class tbl_guest_stay_history
    {
        [Key]
        public int Stay_Info_History_ID { get; set; }

        [Display(Name = "Shift Time")]
        public DateTime? Shift_Date { get; set; }

        public int? Shift_From_ID { get; set; }

        public int? Shift_To_ID { get; set; }

        public int? Emp_ID { get; set; }

        [StringLength(80)]
        public string Reason { get; set; }

        [ForeignKey("Shift_From_ID")]
        public virtual tbl_guest_stay_info shift_from_stay_info { get; set; }

        [ForeignKey("Shift_To_ID")]
        public virtual tbl_guest_stay_info shift_to_stay_info { get; set; }

        [ForeignKey("Emp_ID")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }
    }
}
