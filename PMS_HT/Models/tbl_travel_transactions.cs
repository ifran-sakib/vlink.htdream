namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_travel_transactions
    {
        [Key]
        public int Travel_Tran_ID { get; set; }

        public int? Travel_Agent_ID { get; set; }

        [StringLength(100)]
        public string Particulars { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public DateTime? Tran_Date { get; set; }
    }
}
