namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_group_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_group_info()
        {
            tbl_group_employee = new HashSet<tbl_group_employee>();
            tbl_group_field_permission = new HashSet<tbl_group_field_permission>();
            tbl_group_module_permission = new HashSet<tbl_group_module_permission>();
            tbl_group_sub_module_permission = new HashSet<tbl_group_sub_module_permission>();
        }

        [Key]
        public int Group_ID { get; set; }

        [StringLength(50)]
        public string Group_Name { get; set; }

        [StringLength(50)]
        public string Group_Details { get; set; }

        public int? Created_By { get; set; }

        public int? Updated_By { get; set; }

        public DateTime? Created_Date { get; set; }

        public DateTime? Updated_Date { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_employee> tbl_group_employee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_field_permission> tbl_group_field_permission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_module_permission> tbl_group_module_permission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_group_sub_module_permission> tbl_group_sub_module_permission { get; set; }
    }
}
