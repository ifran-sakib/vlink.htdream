namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_guest_queries
    {
        [Key]
        public int Queries_ID { get; set; }

        [StringLength(100)]
        public string Queries_Details { get; set; }

        [StringLength(50)]
        public string Guest_Name { get; set; }

        [StringLength(50)]
        public string Queries_Status { get; set; }

        public DateTime? Queries_Date { get; set; }

        [StringLength(50)]
        public string Phone_No { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        public int? Country_ID { get; set; }

        [StringLength(50)]
        public string Comments { get; set; }

        [StringLength(50)]
        public string Complains { get; set; }

        public virtual tbl_country tbl_country { get; set; }
    }
}
