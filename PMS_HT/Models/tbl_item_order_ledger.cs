﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_item_order_ledger
    {
        [Key]
        public int Order_Ledger_ID { get; set; }

        public int? Order_Master_ID { get; set; }

        public int Tran_Ref_ID { get; set; }

        public string Tran_Ref_Name { get; set; }

        public string Particular { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public DateTime? Tran_Date { get; set; }

        [StringLength(100)]
        public string Note { get; set; }


        [ForeignKey("Order_Master_ID")]
        public virtual tbl_item_order_master tbl_item_order_master { get; set; }
    }
}