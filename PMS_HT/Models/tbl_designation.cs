namespace PMS_HT.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class tbl_designation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_designation()
        {
            tbl_emp_info = new HashSet<tbl_emp_info>();
        }

        [Key]

        public int Designation_ID { get; set; }
       
        [StringLength(50)]
        [DisplayName("Desingation")]
        public string Designation_Name { get; set; }

        public decimal? Basic { get; set; }

        public bool? IsBuiltIn { get; set; }


        public virtual ICollection<tbl_emp_info> tbl_emp_info { get; set; }
    }
}
