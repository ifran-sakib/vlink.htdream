namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_currency_setup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_currency_setup()
        {
            tbl_currency_convert = new HashSet<tbl_currency_convert>();
            tbl_currency_convert1 = new HashSet<tbl_currency_convert>();
        }

        [Key]
        public int Currency_ID { get; set; }

        [StringLength(50)]
        public string Currency_Name { get; set; }

        public int? Unit_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_currency_convert> tbl_currency_convert { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_currency_convert> tbl_currency_convert1 { get; set; }

        public virtual tbl_unit_setup tbl_unit_setup { get; set; }
    }
}
