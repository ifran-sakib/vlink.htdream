namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_travel_info
    {
        [Key]
        public int Travel_ID { get; set; }

        public int? Room_ID { get; set; }

        public int? Guest_ID { get; set; }

        public int? Travel_Agent_ID { get; set; }

        public int? Vehicle_ID { get; set; }

        [StringLength(50)]
        public string From_Location { get; set; }

        [StringLength(50)]
        public string To_Location { get; set; }

        [StringLength(50)]
        public string Route { get; set; }

        public DateTime? Journey_DateTime { get; set; }

        public DateTime? Return_DateTime { get; set; }
    }
}
