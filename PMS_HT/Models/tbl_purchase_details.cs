namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_purchase_details
    {
        [Key]
        public int Purchase_Details_ID { get; set; }

        [StringLength(50)]
        public string Memo_No { get; set; }

        public int? Item_ID { get; set; }

        public decimal? Quantity { get; set; }

        public int? Stock_Location_ID { get; set; }

        public decimal? Purchase_Price { get; set; }

        public int? Purchase_Master_ID { get; set; }

        public decimal? Item_Vat { get; set; }

        public bool? YsnActive { get; set; }

        [ForeignKey("Purchase_Master_ID")]
        public virtual tbl_purchase_master tbl_purchase_master { get; set; }

        public virtual tbl_stock_location tbl_stock_location { get; set; }
        [ForeignKey("Item_ID")]
        public virtual tbl_item tbl_item { get; set; }
    }
}
