namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_leave
    {
        [Key]
        public int Leave_ID { get; set; }

        public int? Emp_ID { get; set; }

        public int? Leave_Cause_ID { get; set; }

        public DateTime? Date_From { get; set; }

        public DateTime? Date_To { get; set; }

        public TimeSpan? Submitted_Time { get; set; }

        [StringLength(50)]
        public string Month_Name { get; set; }

        [StringLength(50)]
        public string Year_Name { get; set; }

        [StringLength(50)]
        public string Leave_Period { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_leave_cause tbl_leave_cause { get; set; }
    }
}
