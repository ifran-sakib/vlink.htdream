﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class tbl_payment_method
    {
        [Key]
        public int Pay_Method_ID { get; set;}

        [StringLength(20)]
        [Display(Name = "Payment Type")]
        public string Pay_Method_Name { get; set; }

        public bool? IsBuiltIn { get; set; }
    }
}