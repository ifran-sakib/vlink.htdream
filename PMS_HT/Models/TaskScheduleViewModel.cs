using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS_HT.Models
{
    public class TaskScheduleViewModel
    {
        public string TaskHeading { get; set; }
        public string RoomNo { get; set; }
        public DateTime? ScheduleDateTime { get; set; }
        public string TaskStatus { get; set; }
        public string Note { get; set; }
        public int Task_Schdule_ID { get; set; }
        
    }
}