namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_register_book
    {
        [Key]
        public int Visit_Person_ID { get; set; }

        [StringLength(50)]
        public string Visit_Person_Name { get; set; }

        [StringLength(50)]
        public string Visit_Purpose { get; set; }

        public DateTime? Visit_Date_Time { get; set; }

        public int? Guest_ID { get; set; }

        public int? Room_ID { get; set; }

        public int? Created_By { get; set; }

        [StringLength(50)]
        public string Visit_Person_Phone { get; set; }

        [StringLength(50)]
        public string Visit_Person_Address { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_guest_info tbl_guest_info { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }
    }
}
