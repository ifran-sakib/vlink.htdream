namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Prorgram_Purpose
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prorgram_Purpose()
        {
            tbl_banquet_booking = new HashSet<tbl_banquet_booking>();
        }

        [Key]
        public int P_Purpose_ID { get; set; }

        [StringLength(50)]
        public string Purpose_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_banquet_booking> tbl_banquet_booking { get; set; }
    }
}
