namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_report_history
    {
        [Key]
        public int Report_History_ID { get; set; }

        public int? Reported_By { get; set; }

        public int? Reported_To { get; set; }

        [StringLength(50)]
        public string Report_Subject { get; set; }

        [StringLength(200)]
        public string Report_Details { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        public DateTime? Created_Date { get; set; }

        public int? Report_ID { get; set; }

        public DateTime? Closed_Date { get; set; }

        public virtual tbl_report tbl_report { get; set; }
    }
}
