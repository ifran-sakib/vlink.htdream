namespace PMS_HT.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class tbl_unit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_unit()
        {
            tbl_item = new HashSet<tbl_item>();
        }

        [Key]
        public int Unit_ID { get; set; }

        [StringLength(100)]
        public string Unit_Name { get; set; }

        public bool? IsBuiltIn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_item> tbl_item { get; set; }
    }
}
