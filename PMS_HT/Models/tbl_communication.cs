namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_communication
    {
        [Key]
        public int Guest_Comm_ID { get; set; }

        public int? Guest_ID { get; set; }

        [StringLength(50)]
        public string Sent_To_Mail { get; set; }

        [StringLength(50)]
        public string Sent_From_Mail { get; set; }

        [StringLength(50)]
        public string Mail_Sub { get; set; }

        [StringLength(200)]
        public string Mail_Body { get; set; }

        public DateTime? Sent_Date { get; set; }

        public virtual tbl_guest_info tbl_guest_info { get; set; }
    }
}
