namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_task_assignment
    {
        [Key]
        public int Task_Assignment_ID { get; set; }

        public int? Maid_ID { get; set; }

        public int? Task_ID { get; set; }

        public int? Room_ID { get; set; }

        [StringLength(200)]
        public string Note { get; set; }

        [Display(Name ="Assign Time")]
        public DateTime? Task_Assignment_Date_Time { get; set; }

        public int? Created_By { get; set; }

        public bool? Task_Status { get; set; }

        public virtual tbl_emp_info tbl_emp_info { get; set; }

        public virtual tbl_maid_info tbl_maid_info { get; set; }

        public virtual tbl_room_info tbl_room_info { get; set; }

        public virtual tbl_task_setup tbl_task_setup { get; set; }
    }
}
