namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_conference_room
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_conference_room()
        {
            tbl_conference_program = new HashSet<tbl_conference_program>();
        }

        [Key]
        public int Conf_Room_ID { get; set; }

        [StringLength(50)]
        public string Room_Name { get; set; }

        [StringLength(50)]
        public string Room_Location { get; set; }

        public decimal? Rent { get; set; }

        public int? Tax_ID { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string Capacity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_conference_program> tbl_conference_program { get; set; }

        public virtual tbl_tax tbl_tax { get; set; }
    }
}
