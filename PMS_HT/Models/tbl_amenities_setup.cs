namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_amenities_setup
    {
        [Key]
        public int Amenities_ID { get; set; }

        [MaxLength(100)]
        public byte[] Amenities_type { get; set; }

        public bool? YsnActive { get; set; }

        public DateTime? Created_Date { get; set; }
    }
}
