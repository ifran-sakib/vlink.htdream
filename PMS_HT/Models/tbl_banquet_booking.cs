namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_banquet_booking
    {
        [Key]
        public int Banq_Booking_ID { get; set; }

        public int? Program_ID { get; set; }
        [Required(ErrorMessage ="Please enter a start date")]
        public DateTime Start_Date { get; set; }
        [Required(ErrorMessage = "Please enter a end date")]
        public DateTime End_Date { get; set; }
        [Required(ErrorMessage = "Please select a purpose")]
        public int P_Purpose_ID { get; set; }

        public virtual Prorgram_Purpose Prorgram_Purpose { get; set; }
        [ForeignKey("Program_ID")]
        public virtual tbl_banquet_prog tbl_banquet_prog { get; set; }
    }
}
