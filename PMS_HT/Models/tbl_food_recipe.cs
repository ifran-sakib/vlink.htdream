﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMS_HT.Models
{
    public class tbl_food_recipe
    {
        public tbl_food_recipe()
        {
            tbl_food_recipe_items = new HashSet<tbl_food_recipe_items>();
        }

        [Key]
        public int Recipe_ID { get; set; }

        public string Description { get; set; }

        public decimal? Total_Item_Cost { get; set; }

        public decimal? Ingredient_Cost { get; set; }

        public decimal? Profit_Margin { get; set; }

        public decimal? Total_Cost { get; set; }


        public virtual ICollection<tbl_food_recipe_items> tbl_food_recipe_items { get; set; }
    }
}