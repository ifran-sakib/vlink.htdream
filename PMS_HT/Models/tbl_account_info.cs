﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PMS_HT.Models
{
    public class tbl_account_info
    {
        [Key]
        public int? Acc_ID { get; set; }
        [StringLength(50)]
        [Display(Name ="Acc No")]
        public string Acc_No { get; set; }
        [StringLength(50)]
        [Display(Name = "Acc Name")]
        public string Acc_Name { get; set; }
     
        [StringLength(50)]
        public string Note { get; set; }

        public int? Bank_ID { get; set; }
        public int? Branch_ID { get; set; }
        [Display(Name ="Opening Balance")]
        public decimal? Opening_Balance { get; set; }

        public int? Creted_By { get; set; }
        [Display(Name ="Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Created_Date { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_Date { get; set; }
        [ForeignKey("Bank_ID")]

        public virtual tbl_bank_info tbl_bank_info { get; set; }
        [ForeignKey("Branch_ID")]
        public virtual tbl_branch_info tbl_branch_info { get; set; }
        [ForeignKey("Creted_By")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }
        [ForeignKey("Modified_By")]
        public virtual tbl_emp_info tbl_emp_info1 { get; set; }
    }
}



