namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_vehicle
    {
        [Key]
        public int Vehicle_ID { get; set; }

        [StringLength(50)]
        public string Vehicle_Name { get; set; }
    }
}
