﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Models
{
    public class tbl_guest_group_room_category
    {
        [Key]
        public int Group_Room_Category_ID { get; set; }

        public int Guest_Group_ID { get; set; }

        public int Roomtype_ID { get; set; }

        public int NoOfRooms { get; set; }



        [ForeignKey("Guest_Group_ID")]
        public virtual tbl_guest_group_info tbl_guest_group_info { get; set; }

        [ForeignKey("Roomtype_ID")]
        public virtual tbl_roomtype_setup tbl_roomtype_setup { get; set; }
    }
}