namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_laundry_item
    {
        [Key]
        public int Laundry_Item_ID { get; set; }

        [StringLength(100)]
        public string Laundry_Item_Name { get; set; }

        public int? Laundry_Item_Cata_ID { get; set; }

        public virtual tbl_laundry_item_category tbl_laundry_item_category { get; set; }

        public virtual ICollection<tbl_laundry_service_cost> tbl_laundry_service_cost { get; set; }
    }
}
