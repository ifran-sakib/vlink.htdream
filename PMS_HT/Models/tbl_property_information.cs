namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_information
    {
        public int id { get; set; }

        [StringLength(200)]
        public string Property_Name { get; set; }

        [StringLength(50)]
        public string Property_Type { get; set; }

        [StringLength(50)]
        public string Property_Grade { get; set; }

        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Postal_Code { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        public int? Country_ID { get; set; }

        [StringLength(200)]
        public string Phone { get; set; }

        [StringLength(200)]
        public string Fax { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        [StringLength(200)]
        public string Website { get; set; }

        [StringLength(50)]
        public string Registraion_VAT { get; set; }

        [StringLength(50)]
        public string Registraion_TIN { get; set; }

        [StringLength(50)]
        public string Registraion_CST { get; set; }
        public string Logo { get; set; }
        public string Logo_Location { get; set; }

        public bool ysndefault { get; set; }

        public virtual tbl_country tbl_country { get; set; }
    }
}
