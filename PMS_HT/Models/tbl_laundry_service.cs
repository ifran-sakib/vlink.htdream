namespace PMS_HT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_laundry_service
    {
        [Key]
        public int Laundry_Service_ID { get; set; }

        [StringLength(70)]
        public string Service_Heading_Name { get; set; }

        [StringLength(100)]
        public string Service_Description { get; set; }
        public virtual ICollection<tbl_laundry_service_cost> tbl_laundry_service_cost { get; set; }
    }
}
