namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPermanentAddressAndImageSignatureInTblStayInfoRemoveImageFromTblGuestInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_guest_info", "Permanent_Address", c => c.String(maxLength: 512));
            AddColumn("dbo.tbl_guest_stay_info", "Image", c => c.String(maxLength: 100));
            AddColumn("dbo.tbl_guest_stay_info", "Signature", c => c.String());
            AlterColumn("dbo.tbl_guest_info", "Address", c => c.String(maxLength: 512));

            //Sql("UPDATE tbl_guest_stay_info SET Image = (SELECT Image FROM tbl_guest_info WHERE Guest_ID = tbl_guest_stay_info.Guest_ID)");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_guest_info", "Address", c => c.String(maxLength: 200));
            DropColumn("dbo.tbl_guest_stay_info", "Signature");
            DropColumn("dbo.tbl_guest_stay_info", "Image");
            DropColumn("dbo.tbl_guest_info", "Permanent_Address");
        }
    }
}
