namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prorgram_Purpose",
                c => new
                    {
                        P_Purpose_ID = c.Int(nullable: false, identity: true),
                        Purpose_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.P_Purpose_ID);
            
            CreateTable(
                "dbo.tbl_banquet_booking",
                c => new
                    {
                        Banq_Booking_ID = c.Int(nullable: false, identity: true),
                        Program_ID = c.Int(),
                        Start_Date = c.DateTime(nullable: false),
                        End_Date = c.DateTime(nullable: false),
                        P_Purpose_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Banq_Booking_ID)
                .ForeignKey("dbo.Prorgram_Purpose", t => t.P_Purpose_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_banquet_prog", t => t.Program_ID)
                .Index(t => t.Program_ID)
                .Index(t => t.P_Purpose_ID);
            
            CreateTable(
                "dbo.tbl_banquet_prog",
                c => new
                    {
                        Program_ID = c.Int(nullable: false, identity: true),
                        Banquet_ID = c.Int(),
                        Person_Name = c.String(maxLength: 50),
                        Person_Adress = c.String(maxLength: 50),
                        Person_Phone = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        Country_ID = c.Int(),
                        Company_Name = c.String(maxLength: 50),
                        Company_Adress = c.String(maxLength: 100),
                        Company_Phone = c.String(maxLength: 50),
                        Company_Details = c.String(maxLength: 100),
                        Contact_Person = c.String(maxLength: 50),
                        Contact_Person_Phone = c.String(maxLength: 50),
                        Contact_Person_Address = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Program_ID)
                .ForeignKey("dbo.tbl_banquet_info", t => t.Banquet_ID)
                .ForeignKey("dbo.tbl_country", t => t.Country_ID)
                .Index(t => t.Banquet_ID)
                .Index(t => t.Country_ID);
            
            CreateTable(
                "dbo.tbl_banquet_info",
                c => new
                    {
                        Banquet_ID = c.Int(nullable: false, identity: true),
                        Banquet_Name = c.String(maxLength: 50),
                        Location = c.String(maxLength: 50),
                        Capacity = c.String(maxLength: 50),
                        Description = c.String(maxLength: 200),
                        Rent = c.Decimal(precision: 18, scale: 2),
                        Tax_ID = c.Int(),
                        Status = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Banquet_ID);
            
            CreateTable(
                "dbo.tbl_banquet_transactions",
                c => new
                    {
                        B_Tran_ID = c.Int(nullable: false, identity: true),
                        Program_ID = c.Int(),
                        Tran_Ref_ID = c.Int(),
                        Banq_Booking_ID = c.Int(),
                        Tran_Ref_Name = c.String(maxLength: 50),
                        Particulars = c.String(maxLength: 200),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Tran_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.B_Tran_ID)
                .ForeignKey("dbo.tbl_banquet_amenities", t => t.Tran_Ref_ID)
                .ForeignKey("dbo.tbl_banquet_booking", t => t.Banq_Booking_ID)
                .ForeignKey("dbo.tbl_banquet_prog", t => t.Program_ID)
                .Index(t => t.Program_ID)
                .Index(t => t.Tran_Ref_ID)
                .Index(t => t.Banq_Booking_ID);
            
            CreateTable(
                "dbo.tbl_banquet_amenities",
                c => new
                    {
                        Banquet_Amenities_ID = c.Int(nullable: false, identity: true),
                        Amenities_Heading = c.String(maxLength: 50),
                        Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Banquet_Amenities_ID);
            
            CreateTable(
                "dbo.tbl_country",
                c => new
                    {
                        Country_ID = c.Int(nullable: false, identity: true),
                        Country_Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Country_ID);
            
            CreateTable(
                "dbo.tbl_conference_program",
                c => new
                    {
                        Conf_Program_ID = c.Int(nullable: false, identity: true),
                        Conf_Room_ID = c.Int(),
                        Person_Name = c.String(maxLength: 50),
                        Person_Address = c.String(maxLength: 100),
                        Person_Phone = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        Country_ID = c.Int(),
                        Company_Name = c.String(maxLength: 50),
                        Company_Phone = c.String(maxLength: 50),
                        Company_Adress = c.String(maxLength: 100),
                        Company_Details = c.String(maxLength: 50),
                        Contact_Person_Phone = c.String(maxLength: 50),
                        Contact_Person_Address = c.String(maxLength: 100),
                        Conf_Purpose_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Conf_Program_ID)
                .ForeignKey("dbo.tbl_conference_purpose", t => t.Conf_Purpose_ID)
                .ForeignKey("dbo.tbl_conference_room", t => t.Conf_Room_ID)
                .ForeignKey("dbo.tbl_country", t => t.Country_ID)
                .Index(t => t.Conf_Room_ID)
                .Index(t => t.Country_ID)
                .Index(t => t.Conf_Purpose_ID);
            
            CreateTable(
                "dbo.tbl_conference_purpose",
                c => new
                    {
                        Conf_Purpose_ID = c.Int(nullable: false, identity: true),
                        Conf_Purpose_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Conf_Purpose_ID);
            
            CreateTable(
                "dbo.tbl_conference_room",
                c => new
                    {
                        Conf_Room_ID = c.Int(nullable: false, identity: true),
                        Room_Name = c.String(maxLength: 50),
                        Room_Location = c.String(maxLength: 50),
                        Rent = c.Decimal(precision: 18, scale: 2),
                        Tax_ID = c.Int(),
                        Description = c.String(maxLength: 255),
                        Status = c.String(maxLength: 50),
                        Capacity = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Conf_Room_ID)
                .ForeignKey("dbo.tbl_tax", t => t.Tax_ID)
                .Index(t => t.Tax_ID);
            
            CreateTable(
                "dbo.tbl_tax",
                c => new
                    {
                        Tax_ID = c.Int(nullable: false, identity: true),
                        Heading = c.String(maxLength: 50),
                        Parcentage = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Tax_ID);
            
            CreateTable(
                "dbo.tbl_tax_amount",
                c => new
                    {
                        Tax_Amount_ID = c.Int(nullable: false, identity: true),
                        Tax_Refer = c.String(maxLength: 50),
                        Tax_ID = c.Int(),
                        Tax_Amount = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Tax_Amount_ID)
                .ForeignKey("dbo.tbl_tax", t => t.Tax_ID)
                .Index(t => t.Tax_ID);
            
            CreateTable(
                "dbo.tbl_conference_transactions",
                c => new
                    {
                        Conf_Tran_ID = c.Int(nullable: false, identity: true),
                        Conf_Program_ID = c.Int(),
                        Tran_Ref_ID = c.Int(),
                        Tran_Ref_Name = c.String(maxLength: 50),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Tran_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Conf_Tran_ID)
                .ForeignKey("dbo.tbl_conference_program", t => t.Conf_Program_ID)
                .Index(t => t.Conf_Program_ID);
            
            CreateTable(
                "dbo.tbl_conference_utility_master",
                c => new
                    {
                        Utility_Master_ID = c.Int(nullable: false, identity: true),
                        Total_Amount = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Order_Date = c.DateTime(),
                        Conf_Program_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Utility_Master_ID)
                .ForeignKey("dbo.tbl_conference_program", t => t.Conf_Program_ID)
                .Index(t => t.Conf_Program_ID);
            
            CreateTable(
                "dbo.tbl_conference_utility_details",
                c => new
                    {
                        Utility_Details_ID = c.Int(nullable: false, identity: true),
                        Utility_Order_No = c.String(maxLength: 50),
                        Utility_Master_ID = c.Int(),
                        Conf_Item_ID = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        Start_Date = c.DateTime(),
                        End_Date = c.DateTime(),
                        Strat_Time = c.Time(precision: 7),
                        End_Time = c.Time(precision: 7),
                        Total_day = c.Decimal(precision: 18, scale: 0),
                        Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Utility_Details_ID)
                .ForeignKey("dbo.tbl_conf_item_setup", t => t.Conf_Item_ID)
                .ForeignKey("dbo.tbl_conference_utility_master", t => t.Utility_Master_ID)
                .Index(t => t.Utility_Master_ID)
                .Index(t => t.Conf_Item_ID);
            
            CreateTable(
                "dbo.tbl_conf_item_setup",
                c => new
                    {
                        Conf_Item_ID = c.Int(nullable: false, identity: true),
                        Conf_Item_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Conf_Item_ID);
            
            CreateTable(
                "dbo.tbl_guest_info",
                c => new
                    {
                        Guest_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Address = c.String(maxLength: 200),
                        Phone = c.String(maxLength: 50),
                        AdditionalPhone = c.String(maxLength: 50),
                        Father_Name = c.String(maxLength: 50),
                        Mother_Name = c.String(maxLength: 50),
                        Maritial_Status = c.String(maxLength: 50),
                        Spouse_Name = c.String(maxLength: 50),
                        National_ID = c.String(),
                        Nearest_Relative_Contact_Info = c.String(maxLength: 500),
                        Age = c.String(),
                        Identification_Mark = c.String(),
                        Skin_Color = c.String(),
                        Height = c.String(),
                        Occupation = c.String(),
                        Driving_License = c.String(maxLength: 50),
                        Tin_Certificate = c.String(maxLength: 50),
                        Image = c.String(maxLength: 100),
                        Signature = c.String(),
                        Fingerprint = c.String(),
                        Nationality = c.String(maxLength: 50),
                        Religion_ID = c.Int(),
                        Country_ID = c.Int(),
                        Passport_No = c.String(maxLength: 50),
                        Visa_No = c.String(maxLength: 50),
                        Visa_Date = c.DateTime(),
                        Arrived_From = c.String(maxLength: 50),
                        Arrival_Transportation = c.String(maxLength: 50),
                        Date_of_Arr_in_Country = c.DateTime(),
                        Time_of_Arr_in_Country = c.Time(precision: 7),
                        Purpose_of_visit = c.String(maxLength: 50),
                        Going_To = c.String(maxLength: 50),
                        Depart_Transportation = c.String(maxLength: 100),
                        Email = c.String(),
                        Birth_Date = c.DateTime(),
                        Special_Preference = c.String(),
                        DNR = c.Boolean(),
                        VIP = c.Boolean(),
                    })
                .PrimaryKey(t => t.Guest_ID)
                .ForeignKey("dbo.tbl_country", t => t.Country_ID)
                .ForeignKey("dbo.tbl_religion", t => t.Religion_ID)
                .Index(t => t.Religion_ID)
                .Index(t => t.Country_ID);
            
            CreateTable(
                "dbo.tbl_communication",
                c => new
                    {
                        Guest_Comm_ID = c.Int(nullable: false, identity: true),
                        Guest_ID = c.Int(),
                        Sent_To_Mail = c.String(maxLength: 50),
                        Sent_From_Mail = c.String(maxLength: 50),
                        Mail_Sub = c.String(maxLength: 50),
                        Mail_Body = c.String(maxLength: 200),
                        Sent_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Guest_Comm_ID)
                .ForeignKey("dbo.tbl_guest_info", t => t.Guest_ID)
                .Index(t => t.Guest_ID);
            
            CreateTable(
                "dbo.tbl_guest_stay_history",
                c => new
                    {
                        Stay_Info_History_ID = c.Int(nullable: false, identity: true),
                        Shift_Date = c.DateTime(),
                        Shift_From_ID = c.Int(),
                        Shift_To_ID = c.Int(),
                        Emp_ID = c.Int(),
                        Reason = c.String(maxLength: 80),
                        tbl_room_info_Room_ID = c.Int(),
                        tbl_guest_info_Guest_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Stay_Info_History_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.tbl_room_info_Room_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Shift_From_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Shift_To_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .ForeignKey("dbo.tbl_guest_info", t => t.tbl_guest_info_Guest_ID)
                .Index(t => t.Shift_From_ID)
                .Index(t => t.Shift_To_ID)
                .Index(t => t.Emp_ID)
                .Index(t => t.tbl_room_info_Room_ID)
                .Index(t => t.tbl_guest_info_Guest_ID);
            
            CreateTable(
                "dbo.tbl_guest_stay_info",
                c => new
                    {
                        Stay_Info_ID = c.Int(nullable: false, identity: true),
                        Arrival_Date = c.DateTime(nullable: false),
                        Departure_Date = c.DateTime(nullable: false),
                        No_of_Nights = c.String(maxLength: 10, unicode: false),
                        No_of_Adult = c.Int(),
                        No_of_Child = c.Int(),
                        No_of_Extra_Person = c.Int(),
                        No_of_Extra_Bed = c.Int(),
                        Status = c.String(maxLength: 20),
                        Room_ID = c.Int(),
                        Guest_ID = c.Int(),
                        Company_ID = c.Int(),
                        Guest_Group_ID = c.Int(),
                        Travel_Agent_ID = c.Int(),
                        Reserved_Date = c.DateTime(),
                        Booking_Date = c.DateTime(),
                        Check_Out_Date = c.DateTime(),
                        Check_Out_Time = c.Time(precision: 7),
                        IsConfirmed = c.Boolean(),
                        InOrOut = c.Boolean(),
                        Airport_Pickup = c.Boolean(),
                        Transportation = c.String(),
                        Special_Request = c.String(),
                        Check_Out_Note = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Stay_Info_ID)
                .ForeignKey("dbo.tbl_company_info", t => t.Company_ID)
                .ForeignKey("dbo.tbl_guest_group_info", t => t.Guest_Group_ID)
                .ForeignKey("dbo.tbl_guest_info", t => t.Guest_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .ForeignKey("dbo.tbl_travel_agent_info", t => t.Travel_Agent_ID)
                .Index(t => t.Room_ID)
                .Index(t => t.Guest_ID)
                .Index(t => t.Company_ID)
                .Index(t => t.Guest_Group_ID)
                .Index(t => t.Travel_Agent_ID);
            
            CreateTable(
                "dbo.tbl_company_info",
                c => new
                    {
                        Company_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Company_ID);
            
            CreateTable(
                "dbo.tbl_guest_group_info",
                c => new
                    {
                        Guest_Group_ID = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.Guest_Group_ID);
            
            CreateTable(
                "dbo.tbl_messages",
                c => new
                    {
                        Message_ID = c.Int(nullable: false, identity: true),
                        Stay_Info_ID = c.Int(),
                        Receiving_Emp_ID = c.Int(),
                        Delivering_Emp_ID = c.Int(),
                        Message = c.String(),
                        Senders_Details = c.String(maxLength: 255),
                        Receiving_Time = c.DateTime(),
                        Delivery_Time = c.DateTime(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Message_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Delivering_Emp_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Receiving_Emp_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .Index(t => t.Stay_Info_ID)
                .Index(t => t.Receiving_Emp_ID)
                .Index(t => t.Delivering_Emp_ID);
            
            CreateTable(
                "dbo.tbl_emp_info",
                c => new
                    {
                        Emp_ID = c.Int(nullable: false, identity: true),
                        Emp_Name = c.String(maxLength: 50, unicode: false),
                        Emp_Gender = c.String(maxLength: 50, unicode: false),
                        Emp_DOB = c.DateTime(),
                        Emp_Contact_No = c.String(maxLength: 50, unicode: false),
                        Emp_Age = c.String(maxLength: 50, unicode: false),
                        Emp_Address = c.String(maxLength: 50, unicode: false),
                        Department_ID = c.Int(),
                        Designation_ID = c.Int(),
                        Emp_Status = c.String(maxLength: 50, unicode: false),
                        Date_Hired = c.DateTime(),
                        user_name = c.String(maxLength: 40),
                        password = c.String(),
                        IsAuthenticated = c.Boolean(),
                        Image = c.String(maxLength: 100),
                        user_type = c.Short(),
                        NID = c.String(maxLength: 50),
                        Discount_Lavel = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Emp_ID)
                .ForeignKey("dbo.tbl_department", t => t.Department_ID)
                .ForeignKey("dbo.tbl_designation", t => t.Designation_ID)
                .Index(t => t.Department_ID)
                .Index(t => t.Designation_ID);
            
            CreateTable(
                "dbo.tbl_attendance",
                c => new
                    {
                        Attendance_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Punch_In_Time = c.Time(precision: 7),
                        Punch_Out_Time = c.Time(precision: 7),
                        Date = c.DateTime(),
                        Comments = c.String(maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.Attendance_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_department",
                c => new
                    {
                        Department_ID = c.Int(nullable: false, identity: true),
                        Department_Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Department_ID);
            
            CreateTable(
                "dbo.tbl_designation",
                c => new
                    {
                        Designation_ID = c.Int(nullable: false, identity: true),
                        Designation_Name = c.String(maxLength: 50, unicode: false),
                        Basic = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Designation_ID);
            
            CreateTable(
                "dbo.tbl_dining_waiter",
                c => new
                    {
                        Dining_Waiter_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Status = c.String(),
                        Note = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Dining_Waiter_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_dining_set_up",
                c => new
                    {
                        Dining_ID = c.Int(nullable: false, identity: true),
                        Dining_No = c.String(maxLength: 50),
                        Capacity = c.Int(),
                        Status = c.String(maxLength: 50),
                        Dining_Location_ID = c.Int(),
                        Dining_Waiter_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Dining_ID)
                .ForeignKey("dbo.tbl_dining_location", t => t.Dining_Location_ID)
                .ForeignKey("dbo.tbl_dining_waiter", t => t.Dining_Waiter_ID)
                .Index(t => t.Dining_Location_ID)
                .Index(t => t.Dining_Waiter_ID);
            
            CreateTable(
                "dbo.tbl_dining_location",
                c => new
                    {
                        Dining_Location_ID = c.Int(nullable: false, identity: true),
                        Location_Name = c.String(maxLength: 50),
                        Location_Type = c.String(maxLength: 50),
                        Point_A = c.Int(),
                        Point_B = c.Int(),
                        Point_C = c.Int(),
                        Radius = c.Int(),
                    })
                .PrimaryKey(t => t.Dining_Location_ID);
            
            CreateTable(
                "dbo.tbl_item_order_master",
                c => new
                    {
                        Order_Master_ID = c.Int(nullable: false, identity: true),
                        Order_Date = c.DateTime(),
                        Serve_Date = c.DateTime(),
                        Payment_Date = c.DateTime(),
                        Status = c.String(maxLength: 50),
                        Total_Person = c.Int(),
                        Order_Method_ID = c.Int(),
                        Pay_Method_ID = c.Int(),
                        Banquet_Refer = c.String(maxLength: 50),
                        Program_ID = c.Int(),
                        Stay_Info_ID = c.Int(),
                        Note = c.String(),
                        Received_By_ID = c.Int(),
                        Rest_Guest_ID = c.Int(),
                        Rest_Company_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Order_Master_ID)
                .ForeignKey("dbo.tbl_banquet_prog", t => t.Program_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Received_By_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .ForeignKey("dbo.tbl_order_method", t => t.Order_Method_ID)
                .ForeignKey("dbo.tbl_payment_method", t => t.Pay_Method_ID)
                .ForeignKey("dbo.tbl_restaurant_company_info", t => t.Rest_Company_ID)
                .ForeignKey("dbo.tbl_restaurant_guest_info", t => t.Rest_Guest_ID)
                .Index(t => t.Order_Method_ID)
                .Index(t => t.Pay_Method_ID)
                .Index(t => t.Program_ID)
                .Index(t => t.Stay_Info_ID)
                .Index(t => t.Received_By_ID)
                .Index(t => t.Rest_Guest_ID)
                .Index(t => t.Rest_Company_ID);
            
            CreateTable(
                "dbo.tbl_item_order_details",
                c => new
                    {
                        Order_Details_ID = c.Int(nullable: false, identity: true),
                        Food_ID = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Food_Side_ID = c.Int(),
                        Side_Quantity = c.Decimal(precision: 18, scale: 2),
                        Side_Price = c.Decimal(precision: 18, scale: 2),
                        VAT = c.Decimal(precision: 18, scale: 2),
                        SC = c.Decimal(precision: 18, scale: 2),
                        Total = c.Decimal(precision: 18, scale: 2),
                        Order_Master_ID = c.Int(),
                        Order_Date = c.DateTime(),
                        Note = c.String(),
                        YsnActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Order_Details_ID)
                .ForeignKey("dbo.tbl_food", t => t.Food_ID)
                .ForeignKey("dbo.tbl_food_side_extra", t => t.Food_Side_ID)
                .ForeignKey("dbo.tbl_item_order_master", t => t.Order_Master_ID)
                .Index(t => t.Food_ID)
                .Index(t => t.Food_Side_ID)
                .Index(t => t.Order_Master_ID);
            
            CreateTable(
                "dbo.tbl_food",
                c => new
                    {
                        Food_ID = c.Int(nullable: false, identity: true),
                        Food_No = c.String(maxLength: 6),
                        Food_Name = c.String(maxLength: 50),
                        Food_Cate_ID = c.Int(),
                        Description = c.String(maxLength: 300),
                        Image = c.String(),
                        Availability = c.Boolean(),
                        Quantity = c.String(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Color = c.String(),
                        Note = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Food_ID)
                .ForeignKey("dbo.tbl_food_category", t => t.Food_Cate_ID)
                .Index(t => t.Food_Cate_ID);
            
            CreateTable(
                "dbo.tbl_food_category",
                c => new
                    {
                        Food_Cate_ID = c.Int(nullable: false, identity: true),
                        Food_Cate_Name = c.String(maxLength: 50),
                        Note = c.String(),
                        Serial_No = c.Int(),
                    })
                .PrimaryKey(t => t.Food_Cate_ID);
            
            CreateTable(
                "dbo.tbl_food_side_extra",
                c => new
                    {
                        Food_Side_ID = c.Int(nullable: false, identity: true),
                        Food_Side_Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 300),
                        Image = c.String(),
                        Availability = c.Boolean(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Food_Side_ID);
            
            CreateTable(
                "dbo.tbl_item_order_ledger",
                c => new
                    {
                        Order_Ledger_ID = c.Int(nullable: false, identity: true),
                        Order_Master_ID = c.Int(),
                        Tran_Ref_ID = c.Int(nullable: false),
                        Tran_Ref_Name = c.String(),
                        Particular = c.String(),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Tran_Date = c.DateTime(),
                        Note = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Order_Ledger_ID)
                .ForeignKey("dbo.tbl_item_order_master", t => t.Order_Master_ID)
                .Index(t => t.Order_Master_ID);
            
            CreateTable(
                "dbo.tbl_order_method",
                c => new
                    {
                        Order_Method_ID = c.Int(nullable: false, identity: true),
                        Order_Method_Name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Order_Method_ID);
            
            CreateTable(
                "dbo.tbl_payment_method",
                c => new
                    {
                        Pay_Method_ID = c.Int(nullable: false, identity: true),
                        Pay_Method_Name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Pay_Method_ID);
            
            CreateTable(
                "dbo.tbl_restaurant_company_info",
                c => new
                    {
                        Rest_Company_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                        Address = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Rest_Company_ID);
            
            CreateTable(
                "dbo.tbl_restaurant_guest_info",
                c => new
                    {
                        Rest_Guest_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                        Address = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Rest_Guest_ID);
            
            CreateTable(
                "dbo.tbl_emp_field_permission",
                c => new
                    {
                        Emp_Field_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Field_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Emp_Field_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_emp_mail",
                c => new
                    {
                        Emp_Mail_ID = c.Int(nullable: false, identity: true),
                        Sent_To_Mail = c.String(maxLength: 50),
                        Sent_From_Mail = c.String(maxLength: 50),
                        Sender_ID = c.Int(),
                        Receiver_ID = c.Int(),
                        Mail_Sub = c.String(maxLength: 50),
                        Mail_Body = c.String(maxLength: 255),
                        Sent_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Emp_Mail_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Sender_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Receiver_ID)
                .Index(t => t.Sender_ID)
                .Index(t => t.Receiver_ID);
            
            CreateTable(
                "dbo.tbl_emp_module_permission",
                c => new
                    {
                        Emp_Module_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Module_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Emp_Module_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_emp_sub_module_permission",
                c => new
                    {
                        Emp_Sub_Module_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Sub_Module_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Emp_Sub_Module_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_group_employee",
                c => new
                    {
                        Group_Emp_ID = c.Int(nullable: false, identity: true),
                        Group_ID = c.Int(),
                        Emp_ID = c.Int(),
                        YsnActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Group_Emp_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .ForeignKey("dbo.tbl_group_info", t => t.Group_ID)
                .Index(t => t.Group_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_group_info",
                c => new
                    {
                        Group_ID = c.Int(nullable: false, identity: true),
                        Group_Name = c.String(maxLength: 50),
                        Group_Details = c.String(maxLength: 50),
                        Created_By = c.Int(),
                        Updated_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Group_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Updated_By)
                .Index(t => t.Created_By)
                .Index(t => t.Updated_By);
            
            CreateTable(
                "dbo.tbl_group_field_permission",
                c => new
                    {
                        Group_Field_ID = c.Int(nullable: false, identity: true),
                        Group_ID = c.Int(),
                        Field_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Group_Field_ID)
                .ForeignKey("dbo.tbl_group_info", t => t.Group_ID)
                .Index(t => t.Group_ID);
            
            CreateTable(
                "dbo.tbl_group_module_permission",
                c => new
                    {
                        Grp_Module_ID = c.Int(nullable: false, identity: true),
                        Group_ID = c.Int(),
                        Module_Name = c.String(maxLength: 10, fixedLength: true),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Grp_Module_ID)
                .ForeignKey("dbo.tbl_group_info", t => t.Group_ID)
                .Index(t => t.Group_ID);
            
            CreateTable(
                "dbo.tbl_group_sub_module_permission",
                c => new
                    {
                        Grp_Sub_Module_ID = c.Int(nullable: false, identity: true),
                        Group_ID = c.Int(),
                        Sub_Module_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Updated_By = c.Int(),
                        Updated_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Grp_Sub_Module_ID)
                .ForeignKey("dbo.tbl_group_info", t => t.Group_ID)
                .Index(t => t.Group_ID);
            
            CreateTable(
                "dbo.tbl_guest_complain",
                c => new
                    {
                        Complain_ID = c.Int(nullable: false, identity: true),
                        Heading_ID = c.Int(),
                        Complain_Details = c.String(maxLength: 100),
                        Assigned_Emp = c.Int(),
                        Status = c.String(maxLength: 50),
                        Created_By = c.Int(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Complain_ID)
                .ForeignKey("dbo.tbl_complain_heading", t => t.Heading_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Assigned_Emp)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .Index(t => t.Heading_ID)
                .Index(t => t.Assigned_Emp)
                .Index(t => t.Created_By);
            
            CreateTable(
                "dbo.tbl_complain_heading",
                c => new
                    {
                        Heading_ID = c.Int(nullable: false, identity: true),
                        Heading_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Heading_ID);
            
            CreateTable(
                "dbo.tbl_leave",
                c => new
                    {
                        Leave_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Leave_Cause_ID = c.Int(),
                        Date_From = c.DateTime(),
                        Date_To = c.DateTime(),
                        Submitted_Time = c.Time(precision: 7),
                        Month_Name = c.String(maxLength: 50, unicode: false),
                        Year_Name = c.String(maxLength: 50, unicode: false),
                        Leave_Period = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Leave_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .ForeignKey("dbo.tbl_leave_cause", t => t.Leave_Cause_ID)
                .Index(t => t.Emp_ID)
                .Index(t => t.Leave_Cause_ID);
            
            CreateTable(
                "dbo.tbl_leave_cause",
                c => new
                    {
                        Leave_Cause_ID = c.Int(nullable: false, identity: true),
                        Cause_Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Leave_Cause_ID);
            
            CreateTable(
                "dbo.tbl_leave_status",
                c => new
                    {
                        Leave_Status_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Reviewed_by = c.Int(),
                        Date_Time = c.DateTime(),
                        Is_Approved = c.Boolean(),
                    })
                .PrimaryKey(t => t.Leave_Status_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_register_book",
                c => new
                    {
                        Visit_Person_ID = c.Int(nullable: false, identity: true),
                        Visit_Person_Name = c.String(maxLength: 50),
                        Visit_Purpose = c.String(maxLength: 50),
                        Visit_Date_Time = c.DateTime(),
                        Guest_ID = c.Int(),
                        Room_ID = c.Int(),
                        Created_By = c.Int(),
                        Visit_Person_Phone = c.String(maxLength: 50),
                        Visit_Person_Address = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Visit_Person_ID)
                .ForeignKey("dbo.tbl_guest_info", t => t.Guest_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .Index(t => t.Guest_ID)
                .Index(t => t.Room_ID)
                .Index(t => t.Created_By);
            
            CreateTable(
                "dbo.tbl_room_info",
                c => new
                    {
                        Room_ID = c.Int(nullable: false, identity: true),
                        Room_No = c.String(maxLength: 50, unicode: false),
                        Roomtype_ID = c.Int(),
                        Floor_ID = c.Int(),
                        Room_view_ID = c.Int(),
                        Rate = c.Decimal(precision: 18, scale: 2),
                        Status = c.String(maxLength: 50),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Room_ID)
                .ForeignKey("dbo.tbl_floor_setup", t => t.Floor_ID)
                .ForeignKey("dbo.tbl_room_view_setup", t => t.Room_view_ID)
                .ForeignKey("dbo.tbl_roomtype_setup", t => t.Roomtype_ID)
                .Index(t => t.Roomtype_ID)
                .Index(t => t.Floor_ID)
                .Index(t => t.Room_view_ID);
            
            CreateTable(
                "dbo.tbl_floor_setup",
                c => new
                    {
                        Floor_ID = c.Int(nullable: false, identity: true),
                        Floor_No = c.String(maxLength: 50, unicode: false),
                        Description = c.String(maxLength: 150, unicode: false),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Floor_ID);
            
            CreateTable(
                "dbo.tbl_package",
                c => new
                    {
                        Package_ID = c.Int(nullable: false, identity: true),
                        Stay_Info_ID = c.Int(),
                        Receiving_Emp_ID = c.Int(),
                        Delivering_Emp_ID = c.Int(),
                        Package_Details = c.String(maxLength: 255, unicode: false),
                        Senders_Details = c.String(maxLength: 255),
                        Receiving_Time = c.DateTime(),
                        Delivery_Time = c.DateTime(),
                        Status = c.String(),
                        tbl_room_info_Room_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Package_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Delivering_Emp_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Receiving_Emp_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.tbl_room_info_Room_ID)
                .Index(t => t.Stay_Info_ID)
                .Index(t => t.Receiving_Emp_ID)
                .Index(t => t.Delivering_Emp_ID)
                .Index(t => t.tbl_room_info_Room_ID);
            
            CreateTable(
                "dbo.tbl_room_temp_monitor",
                c => new
                    {
                        Room_Temp_ID = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(),
                        Temperature = c.String(maxLength: 50),
                        Note = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Room_Temp_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .Index(t => t.Room_ID);
            
            CreateTable(
                "dbo.tbl_room_transactions",
                c => new
                    {
                        Guest_Tran_ID = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(),
                        Guest_ID = c.Int(),
                        Emp_ID = c.Int(),
                        Tran_Ref_ID = c.Int(),
                        Tran_Ref_Name = c.String(maxLength: 100),
                        Particulars = c.String(maxLength: 200),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Tran_Date = c.DateTime(),
                        Stay_Info_ID = c.Int(),
                        Laundry_Item_ID = c.Int(),
                        Laundry_Service_ID = c.Int(),
                        Garment_In_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Guest_Tran_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .ForeignKey("dbo.tbl_guest_info", t => t.Guest_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .ForeignKey("dbo.tbl_laundry_item", t => t.Laundry_Item_ID)
                .ForeignKey("dbo.tbl_laundry_service", t => t.Laundry_Service_ID)
                .ForeignKey("dbo.tbl_return_germent_in", t => t.Garment_In_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .Index(t => t.Room_ID)
                .Index(t => t.Guest_ID)
                .Index(t => t.Emp_ID)
                .Index(t => t.Stay_Info_ID)
                .Index(t => t.Laundry_Item_ID)
                .Index(t => t.Laundry_Service_ID)
                .Index(t => t.Garment_In_ID);
            
            CreateTable(
                "dbo.tbl_laundry_item",
                c => new
                    {
                        Laundry_Item_ID = c.Int(nullable: false, identity: true),
                        Laundry_Item_Name = c.String(maxLength: 100, unicode: false),
                        Laundry_Item_Cata_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Laundry_Item_ID)
                .ForeignKey("dbo.tbl_laundry_item_category", t => t.Laundry_Item_Cata_ID)
                .Index(t => t.Laundry_Item_Cata_ID);
            
            CreateTable(
                "dbo.tbl_laundry_item_category",
                c => new
                    {
                        Laundry_Item_Cata_ID = c.Int(nullable: false, identity: true),
                        Laundry_Item_Cata_Name = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Laundry_Item_Cata_ID);
            
            CreateTable(
                "dbo.tbl_laundry_service_cost",
                c => new
                    {
                        Laundry_Sevice_Cost_ID = c.Int(nullable: false, identity: true),
                        Laundry_Item_ID = c.Int(),
                        Laundry_Service_ID = c.Int(),
                        Cost = c.Decimal(precision: 18, scale: 2),
                        Note = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Laundry_Sevice_Cost_ID)
                .ForeignKey("dbo.tbl_laundry_item", t => t.Laundry_Item_ID)
                .ForeignKey("dbo.tbl_laundry_service", t => t.Laundry_Service_ID)
                .Index(t => t.Laundry_Item_ID)
                .Index(t => t.Laundry_Service_ID);
            
            CreateTable(
                "dbo.tbl_laundry_service",
                c => new
                    {
                        Laundry_Service_ID = c.Int(nullable: false, identity: true),
                        Service_Heading_Name = c.String(maxLength: 70, unicode: false),
                        Service_Description = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Laundry_Service_ID);
            
            CreateTable(
                "dbo.tbl_return_germent_in",
                c => new
                    {
                        Garment_In_ID = c.Int(nullable: false, identity: true),
                        Return_In_Name = c.String(maxLength: 100),
                        Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Garment_In_ID);
            
            CreateTable(
                "dbo.tbl_room_view_setup",
                c => new
                    {
                        Room_view_ID = c.Int(nullable: false, identity: true),
                        View_type = c.String(maxLength: 50, unicode: false),
                        View_details = c.String(maxLength: 100, unicode: false),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Room_view_ID);
            
            CreateTable(
                "dbo.tbl_roomtype_setup",
                c => new
                    {
                        Roomtype_ID = c.Int(nullable: false, identity: true),
                        Roomtype_Name = c.String(maxLength: 50, unicode: false),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Roomtype_ID);
            
            CreateTable(
                "dbo.tbl_tariff",
                c => new
                    {
                        Tarrif_ID = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(),
                        Tariff_Name = c.String(maxLength: 50, unicode: false),
                        Tariff_Details = c.String(maxLength: 155, unicode: false),
                        YsnActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Tarrif_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .Index(t => t.Room_ID);
            
            CreateTable(
                "dbo.tbl_task_assignment",
                c => new
                    {
                        Task_Assignment_ID = c.Int(nullable: false, identity: true),
                        Maid_ID = c.Int(),
                        Task_ID = c.Int(),
                        Room_ID = c.Int(),
                        Note = c.String(maxLength: 200, unicode: false),
                        Task_Assignment_Date_Time = c.DateTime(),
                        Created_By = c.Int(),
                        Task_Status = c.Boolean(),
                    })
                .PrimaryKey(t => t.Task_Assignment_ID)
                .ForeignKey("dbo.tbl_maid_info", t => t.Maid_ID)
                .ForeignKey("dbo.tbl_task_setup", t => t.Task_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .Index(t => t.Maid_ID)
                .Index(t => t.Task_ID)
                .Index(t => t.Room_ID)
                .Index(t => t.Created_By);
            
            CreateTable(
                "dbo.tbl_maid_info",
                c => new
                    {
                        Maid_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Status = c.String(),
                        Note = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Maid_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_task_schdule",
                c => new
                    {
                        Task_Schdule_ID = c.Int(nullable: false, identity: true),
                        Maid_ID = c.Int(),
                        Task_ID = c.Int(),
                        Room_ID = c.Int(),
                        Note = c.String(maxLength: 50, unicode: false),
                        Schdule_Date_Time = c.DateTime(),
                        Task_Schdule_Status = c.String(maxLength: 50, unicode: false),
                        Task_Created_Date_Time = c.DateTime(),
                        Created_By = c.Int(),
                        Updated_By = c.Int(),
                        Updated_Date_Time = c.DateTime(),
                    })
                .PrimaryKey(t => t.Task_Schdule_ID)
                .ForeignKey("dbo.tbl_maid_info", t => t.Maid_ID)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID)
                .ForeignKey("dbo.tbl_task_setup", t => t.Task_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Updated_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .Index(t => t.Maid_ID)
                .Index(t => t.Task_ID)
                .Index(t => t.Room_ID)
                .Index(t => t.Created_By)
                .Index(t => t.Updated_By);
            
            CreateTable(
                "dbo.tbl_task_setup",
                c => new
                    {
                        Task_ID = c.Int(nullable: false, identity: true),
                        Task_Heading = c.String(maxLength: 100, unicode: false),
                        Task_Description = c.String(maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.Task_ID);
            
            CreateTable(
                "dbo.tbl_report",
                c => new
                    {
                        Report_ID = c.Int(nullable: false, identity: true),
                        Reported_By = c.Int(),
                        Reported_To = c.Int(),
                        Report_Subject = c.String(maxLength: 50),
                        Report_Details = c.String(maxLength: 200),
                        Status = c.String(maxLength: 20),
                        Created_Date = c.DateTime(),
                        Closed_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Report_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Reported_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Reported_To)
                .Index(t => t.Reported_By)
                .Index(t => t.Reported_To);
            
            CreateTable(
                "dbo.tbl_item_assignment",
                c => new
                    {
                        Assignment_ID = c.Int(nullable: false, identity: true),
                        Report_ID = c.Int(),
                        Item_Name = c.String(maxLength: 50),
                        Service_Cost = c.Decimal(precision: 18, scale: 2),
                        Carrying_Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Assignment_ID)
                .ForeignKey("dbo.tbl_report", t => t.Report_ID)
                .Index(t => t.Report_ID);
            
            CreateTable(
                "dbo.tbl_report_history",
                c => new
                    {
                        Report_History_ID = c.Int(nullable: false, identity: true),
                        Reported_By = c.Int(),
                        Reported_To = c.Int(),
                        Report_Subject = c.String(maxLength: 50),
                        Report_Details = c.String(maxLength: 200),
                        Status = c.String(maxLength: 20),
                        Created_Date = c.DateTime(),
                        Report_ID = c.Int(),
                        Closed_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Report_History_ID)
                .ForeignKey("dbo.tbl_report", t => t.Report_ID)
                .Index(t => t.Report_ID);
            
            CreateTable(
                "dbo.tbl_travel_agent_info",
                c => new
                    {
                        Travel_Agent_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Address = c.String(),
                        Phone = c.String(),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Comission = c.Decimal(precision: 18, scale: 2),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Travel_Agent_ID);
            
            CreateTable(
                "dbo.tbl_religion",
                c => new
                    {
                        Religion_ID = c.Int(nullable: false, identity: true),
                        Religion_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Religion_ID);
            
            CreateTable(
                "dbo.tbl_guest_queries",
                c => new
                    {
                        Queries_ID = c.Int(nullable: false, identity: true),
                        Queries_Details = c.String(maxLength: 100),
                        Guest_Name = c.String(maxLength: 50),
                        Queries_Status = c.String(maxLength: 50),
                        Queries_Date = c.DateTime(),
                        Phone_No = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        Address = c.String(maxLength: 50),
                        Country_ID = c.Int(),
                        Comments = c.String(maxLength: 50),
                        Complains = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Queries_ID)
                .ForeignKey("dbo.tbl_country", t => t.Country_ID)
                .Index(t => t.Country_ID);
            
            CreateTable(
                "dbo.tbl_property_information",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Property_Name = c.String(maxLength: 200),
                        Property_Type = c.String(maxLength: 50),
                        Property_Grade = c.String(maxLength: 50),
                        Address = c.String(),
                        City = c.String(maxLength: 50),
                        Postal_Code = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Country_ID = c.Int(),
                        Phone = c.String(maxLength: 200),
                        Fax = c.String(maxLength: 200),
                        Email = c.String(maxLength: 200),
                        Website = c.String(maxLength: 200),
                        Registraion_VAT = c.String(maxLength: 50),
                        Registraion_TIN = c.String(maxLength: 50),
                        Registraion_CST = c.String(maxLength: 50),
                        Logo = c.String(),
                        Logo_Location = c.String(),
                        ysndefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.tbl_country", t => t.Country_ID)
                .Index(t => t.Country_ID);
            
            CreateTable(
                "dbo.sysdiagrams",
                c => new
                    {
                        diagram_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        principal_id = c.Int(nullable: false),
                        version = c.Int(),
                        definition = c.Binary(),
                    })
                .PrimaryKey(t => t.diagram_id);
            
            CreateTable(
                "dbo.tbl_account_info",
                c => new
                    {
                        Acc_ID = c.Int(nullable: false, identity: true),
                        Acc_No = c.String(maxLength: 50),
                        Acc_Name = c.String(maxLength: 50),
                        Note = c.String(maxLength: 50),
                        Bank_ID = c.Int(),
                        Branch_ID = c.Int(),
                        Opening_Balance = c.Decimal(precision: 18, scale: 2),
                        Creted_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Modified_By = c.Int(),
                        Modified_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Acc_ID)
                .ForeignKey("dbo.tbl_bank_info", t => t.Bank_ID)
                .ForeignKey("dbo.tbl_branch_info", t => t.Branch_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Creted_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Modified_By)
                .Index(t => t.Bank_ID)
                .Index(t => t.Branch_ID)
                .Index(t => t.Creted_By)
                .Index(t => t.Modified_By);
            
            CreateTable(
                "dbo.tbl_bank_info",
                c => new
                    {
                        Bank_ID = c.Int(nullable: false, identity: true),
                        Bank_Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Bank_ID);
            
            CreateTable(
                "dbo.tbl_branch_info",
                c => new
                    {
                        Branch_ID = c.Int(nullable: false, identity: true),
                        Branch_Name = c.String(maxLength: 50),
                        Address = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Branch_ID);
            
            CreateTable(
                "dbo.tbl_amenities_details",
                c => new
                    {
                        Amenities_Details_ID = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(),
                        Amenities_ID = c.Int(),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Amenities_Details_ID);
            
            CreateTable(
                "dbo.tbl_amenities_setup",
                c => new
                    {
                        Amenities_ID = c.Int(nullable: false, identity: true),
                        Amenities_type = c.Binary(maxLength: 100),
                        YsnActive = c.Boolean(),
                        Created_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Amenities_ID);
            
            CreateTable(
                "dbo.tbl_bank_transaction",
                c => new
                    {
                        Bnk_Tran_ID = c.Int(nullable: false, identity: true),
                        Tran_Ref_ID = c.Int(),
                        Tran_Type = c.String(maxLength: 50),
                        Particulars = c.String(maxLength: 200),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Creted_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Modified_By = c.Int(),
                        Modified_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Bnk_Tran_ID)
                .ForeignKey("dbo.tbl_account_info", t => t.Tran_Ref_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Creted_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Modified_By)
                .Index(t => t.Tran_Ref_ID)
                .Index(t => t.Creted_By)
                .Index(t => t.Modified_By);
            
            CreateTable(
                "dbo.tbl_brand",
                c => new
                    {
                        Brand_ID = c.Int(nullable: false, identity: true),
                        Brand_Name = c.String(maxLength: 100, unicode: false),
                        Brand_Details = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Brand_ID);
            
            CreateTable(
                "dbo.tbl_item",
                c => new
                    {
                        Item_ID = c.Int(nullable: false, identity: true),
                        Cata_ID = c.Int(),
                        Subcata_ID = c.Int(),
                        Brand_ID = c.Int(),
                        Unit_ID = c.Int(),
                        Item_Name = c.String(maxLength: 100, unicode: false),
                        Sales_Price = c.Decimal(precision: 18, scale: 2),
                        Purchase_Price = c.Decimal(precision: 18, scale: 2),
                        Reorder_Level = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Item_ID)
                .ForeignKey("dbo.tbl_brand", t => t.Brand_ID)
                .ForeignKey("dbo.tbl_category", t => t.Cata_ID)
                .ForeignKey("dbo.tbl_subcategory", t => t.Subcata_ID)
                .ForeignKey("dbo.tbl_unit", t => t.Unit_ID)
                .Index(t => t.Cata_ID)
                .Index(t => t.Subcata_ID)
                .Index(t => t.Brand_ID)
                .Index(t => t.Unit_ID);
            
            CreateTable(
                "dbo.tbl_category",
                c => new
                    {
                        Cata_ID = c.Int(nullable: false, identity: true),
                        Cata_Name = c.String(maxLength: 100, unicode: false),
                        Cata_Details = c.String(),
                    })
                .PrimaryKey(t => t.Cata_ID);
            
            CreateTable(
                "dbo.tbl_subcategory",
                c => new
                    {
                        Subcata_ID = c.Int(nullable: false, identity: true),
                        Subcata_Name = c.String(maxLength: 100, unicode: false),
                        Cata_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Subcata_ID)
                .ForeignKey("dbo.tbl_category", t => t.Cata_ID)
                .Index(t => t.Cata_ID);
            
            CreateTable(
                "dbo.tbl_stock",
                c => new
                    {
                        Stock_ID = c.Int(nullable: false, identity: true),
                        Stock_Location_ID = c.Int(),
                        Item_ID = c.Int(),
                        Purchase_Details_ID = c.Int(),
                        Stock_In = c.Decimal(precision: 18, scale: 2),
                        Stock_Out = c.Decimal(precision: 18, scale: 2),
                        stock_change_date = c.DateTime(),
                        note = c.String(),
                    })
                .PrimaryKey(t => t.Stock_ID)
                .ForeignKey("dbo.tbl_item", t => t.Item_ID)
                .ForeignKey("dbo.tbl_stock_location", t => t.Stock_Location_ID)
                .Index(t => t.Stock_Location_ID)
                .Index(t => t.Item_ID);
            
            CreateTable(
                "dbo.tbl_stock_location",
                c => new
                    {
                        Stock_Location_ID = c.Int(nullable: false, identity: true),
                        Stock_Location_Name = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Stock_Location_ID);
            
            CreateTable(
                "dbo.tbl_purchase_details",
                c => new
                    {
                        Purchase_Details_ID = c.Int(nullable: false, identity: true),
                        Memo_No = c.String(maxLength: 50),
                        Item_ID = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        Stock_Location_ID = c.Int(),
                        Purchase_Price = c.Decimal(precision: 18, scale: 2),
                        Purchase_Master_ID = c.Int(),
                        Item_Vat = c.Decimal(precision: 18, scale: 2),
                        YsnActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Purchase_Details_ID)
                .ForeignKey("dbo.tbl_item", t => t.Item_ID)
                .ForeignKey("dbo.tbl_purchase_master", t => t.Purchase_Master_ID)
                .ForeignKey("dbo.tbl_stock_location", t => t.Stock_Location_ID)
                .Index(t => t.Item_ID)
                .Index(t => t.Stock_Location_ID)
                .Index(t => t.Purchase_Master_ID);
            
            CreateTable(
                "dbo.tbl_purchase_master",
                c => new
                    {
                        Purchase_Master_ID = c.Int(nullable: false, identity: true),
                        Memo_No = c.String(maxLength: 50),
                        Memo_Total = c.Decimal(precision: 18, scale: 2),
                        Advanced_Amount = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Supplier_ID = c.Int(),
                        Purchase_Date = c.DateTime(),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.Purchase_Master_ID)
                .ForeignKey("dbo.tbl_supplier", t => t.Supplier_ID)
                .Index(t => t.Supplier_ID);
            
            CreateTable(
                "dbo.tbl_supplier",
                c => new
                    {
                        Supplier_ID = c.Int(nullable: false, identity: true),
                        Sup_Name = c.String(maxLength: 50),
                        Sup_Address = c.String(maxLength: 100),
                        Sup_Phone_No = c.String(maxLength: 50),
                        Opening_Balance = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Supplier_ID);
            
            CreateTable(
                "dbo.tbl_supplier_ledger",
                c => new
                    {
                        Supplier_Ledger_ID = c.Int(nullable: false, identity: true),
                        Supplier_ID = c.Int(),
                        Purchase_Master_ID = c.Int(),
                        Tran_Ref_ID = c.Int(),
                        Tran_Ref_Name = c.String(maxLength: 100),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Transaction_Date = c.DateTime(),
                        Particulars = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Supplier_Ledger_ID)
                .ForeignKey("dbo.tbl_purchase_master", t => t.Purchase_Master_ID)
                .ForeignKey("dbo.tbl_supplier", t => t.Supplier_ID)
                .Index(t => t.Supplier_ID)
                .Index(t => t.Purchase_Master_ID);
            
            CreateTable(
                "dbo.tbl_unit",
                c => new
                    {
                        Unit_ID = c.Int(nullable: false, identity: true),
                        Unit_Name = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Unit_ID);
            
            CreateTable(
                "dbo.tbl_currency_convert",
                c => new
                    {
                        Convert_ID = c.Int(nullable: false, identity: true),
                        From_Currency = c.Int(),
                        To_Currency = c.Int(),
                        Conversion_Rate = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Convert_ID)
                .ForeignKey("dbo.tbl_currency_setup", t => t.From_Currency)
                .ForeignKey("dbo.tbl_currency_setup", t => t.To_Currency)
                .Index(t => t.From_Currency)
                .Index(t => t.To_Currency);
            
            CreateTable(
                "dbo.tbl_currency_setup",
                c => new
                    {
                        Currency_ID = c.Int(nullable: false, identity: true),
                        Currency_Name = c.String(maxLength: 50),
                        Unit_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Currency_ID)
                .ForeignKey("dbo.tbl_unit_setup", t => t.Unit_ID)
                .Index(t => t.Unit_ID);
            
            CreateTable(
                "dbo.tbl_unit_setup",
                c => new
                    {
                        Unit_ID = c.Int(nullable: false, identity: true),
                        Unit_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Unit_ID);
            
            CreateTable(
                "dbo.tbl_deduction_head",
                c => new
                    {
                        Deduction_Head_ID = c.Int(nullable: false, identity: true),
                        Deduction_Head_Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Deduction_Head_ID);
            
            CreateTable(
                "dbo.tbl_earning_head",
                c => new
                    {
                        Earning_Head_ID = c.Int(nullable: false, identity: true),
                        Earning_Head_Name = c.String(maxLength: 50, unicode: false),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Earning_Head_ID);
            
            CreateTable(
                "dbo.tbl_salary_transactions",
                c => new
                    {
                        Salary_Tran_ID = c.Int(nullable: false, identity: true),
                        Emp_ID = c.Int(),
                        Tran_Ref_ID = c.Int(),
                        Particular = c.String(maxLength: 200),
                        Tran_Ref_Name = c.String(maxLength: 50, unicode: false),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Balance = c.Decimal(precision: 18, scale: 2),
                        Month_Name = c.String(maxLength: 50, unicode: false),
                        Year_Name = c.String(maxLength: 50, unicode: false),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Salary_Tran_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .ForeignKey("dbo.tbl_earning_head", t => t.Tran_Ref_ID)
                .Index(t => t.Emp_ID)
                .Index(t => t.Tran_Ref_ID);
            
            CreateTable(
                "dbo.tbl_expense",
                c => new
                    {
                        Exp_ID = c.Int(nullable: false, identity: true),
                        Exp_Head_ID = c.Int(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Note = c.String(maxLength: 100),
                        Creted_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Modified_By = c.Int(),
                        Modified_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Exp_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Creted_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Modified_By)
                .ForeignKey("dbo.tbl_expense_head", t => t.Exp_Head_ID)
                .Index(t => t.Exp_Head_ID)
                .Index(t => t.Creted_By)
                .Index(t => t.Modified_By);
            
            CreateTable(
                "dbo.tbl_expense_head",
                c => new
                    {
                        Exp_Head_ID = c.Int(nullable: false, identity: true),
                        Exp_Head_Name = c.String(maxLength: 50),
                        Exp_Description = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Exp_Head_ID);
            
            CreateTable(
                "dbo.tbl_field_permission",
                c => new
                    {
                        Field_Id = c.Int(nullable: false, identity: true),
                        Field_Name = c.String(maxLength: 50),
                        Sub_Module_ID = c.Int(),
                        Module_ID = c.Int(),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                    })
                .PrimaryKey(t => t.Field_Id);
            
            CreateTable(
                "dbo.tbl_guest_attendee",
                c => new
                    {
                        Attendee_ID = c.Int(nullable: false, identity: true),
                        Stay_Info_ID = c.Int(),
                        Emp_ID = c.Int(),
                        Purpose = c.String(),
                        Changed_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Attendee_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Emp_ID)
                .Index(t => t.Emp_ID);
            
            CreateTable(
                "dbo.tbl_hotel_service",
                c => new
                    {
                        Service_ID = c.Int(nullable: false, identity: true),
                        Service_Heading = c.String(maxLength: 50, unicode: false),
                        Service_Description = c.String(maxLength: 100, unicode: false),
                        Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Service_ID);
            
            CreateTable(
                "dbo.tbl_laundry_expense",
                c => new
                    {
                        Laundry_Expense_ID = c.Int(nullable: false, identity: true),
                        Guest_Tran_ID = c.Int(),
                        Laundry_Type_ID = c.Int(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Particulars = c.String(),
                    })
                .PrimaryKey(t => t.Laundry_Expense_ID)
                .ForeignKey("dbo.tbl_laundry_type", t => t.Laundry_Type_ID)
                .ForeignKey("dbo.tbl_room_transactions", t => t.Guest_Tran_ID)
                .Index(t => t.Guest_Tran_ID)
                .Index(t => t.Laundry_Type_ID);
            
            CreateTable(
                "dbo.tbl_laundry_type",
                c => new
                    {
                        Laundry_Type_ID = c.Int(nullable: false, identity: true),
                        Type_Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Laundry_Type_ID);
            
            CreateTable(
                "dbo.tbl_laundry_status",
                c => new
                    {
                        Laundry_Status_ID = c.Int(nullable: false, identity: true),
                        Guest_Tran_ID = c.Int(),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Laundry_Status_ID)
                .ForeignKey("dbo.tbl_room_transactions", t => t.Guest_Tran_ID)
                .Index(t => t.Guest_Tran_ID);
            
            CreateTable(
                "dbo.tbl_module_permissions",
                c => new
                    {
                        Module_ID = c.Int(nullable: false, identity: true),
                        Module_Name = c.String(maxLength: 50),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                    })
                .PrimaryKey(t => t.Module_ID);
            
            CreateTable(
                "dbo.tbl_others_income",
                c => new
                    {
                        Others_Income_ID = c.Int(nullable: false, identity: true),
                        Others_Income_Head_ID = c.Int(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Note = c.String(maxLength: 100),
                        Creted_By = c.Int(),
                        Created_Date = c.DateTime(),
                        Modified_By = c.Int(),
                        Modified_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Others_Income_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Creted_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Modified_By)
                .ForeignKey("dbo.tbl_others_income_head", t => t.Others_Income_Head_ID)
                .Index(t => t.Others_Income_Head_ID)
                .Index(t => t.Creted_By)
                .Index(t => t.Modified_By);
            
            CreateTable(
                "dbo.tbl_others_income_head",
                c => new
                    {
                        Others_Income_Head_ID = c.Int(nullable: false, identity: true),
                        Others_Income_Head_Name = c.String(maxLength: 50),
                        Others_Income_Description = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Others_Income_Head_ID);
            
            CreateTable(
                "dbo.tbl_preferences",
                c => new
                    {
                        Preference_ID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Preference_Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Preference_ID);
            
            CreateTable(
                "dbo.tbl_room_status",
                c => new
                    {
                        Room_Status_ID = c.Int(nullable: false, identity: true),
                        Status_Heading = c.String(maxLength: 50, unicode: false),
                        Status_Color = c.String(),
                        Status_Description = c.String(maxLength: 100, unicode: false),
                        YsnActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Room_Status_ID);
            
            CreateTable(
                "dbo.tbl_sub_module_permission",
                c => new
                    {
                        Sub_Module_ID = c.Int(nullable: false, identity: true),
                        Sub_Module_Name = c.String(maxLength: 50),
                        Module_ID = c.Int(),
                        Viewable = c.Boolean(),
                        Editable = c.Boolean(),
                    })
                .PrimaryKey(t => t.Sub_Module_ID);
            
            CreateTable(
                "dbo.tbl_travel_info",
                c => new
                    {
                        Travel_ID = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(),
                        Guest_ID = c.Int(),
                        Travel_Agent_ID = c.Int(),
                        Vehicle_ID = c.Int(),
                        From_Location = c.String(maxLength: 50),
                        To_Location = c.String(maxLength: 50),
                        Route = c.String(maxLength: 50),
                        Journey_DateTime = c.DateTime(),
                        Return_DateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Travel_ID);
            
            CreateTable(
                "dbo.tbl_travel_transactions",
                c => new
                    {
                        Travel_Tran_ID = c.Int(nullable: false, identity: true),
                        Travel_Agent_ID = c.Int(),
                        Particulars = c.String(maxLength: 100),
                        Debit = c.Decimal(precision: 18, scale: 2),
                        Credit = c.Decimal(precision: 18, scale: 2),
                        Tran_Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Travel_Tran_ID);
            
            CreateTable(
                "dbo.tbl_vacant_room",
                c => new
                    {
                        Vacant_Room_Id = c.Int(nullable: false, identity: true),
                        Room_ID = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Vacant_Room_Id)
                .ForeignKey("dbo.tbl_room_info", t => t.Room_ID, cascadeDelete: true)
                .Index(t => t.Room_ID);
            
            CreateTable(
                "dbo.tbl_vehicle",
                c => new
                    {
                        Vehicle_ID = c.Int(nullable: false, identity: true),
                        Vehicle_Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Vehicle_ID);
            
            CreateTable(
                "dbo.tbl_item_order_mastertbl_dining_set_up",
                c => new
                    {
                        tbl_item_order_master_Order_Master_ID = c.Int(nullable: false),
                        tbl_dining_set_up_Dining_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.tbl_item_order_master_Order_Master_ID, t.tbl_dining_set_up_Dining_ID })
                .ForeignKey("dbo.tbl_item_order_master", t => t.tbl_item_order_master_Order_Master_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_dining_set_up", t => t.tbl_dining_set_up_Dining_ID, cascadeDelete: true)
                .Index(t => t.tbl_item_order_master_Order_Master_ID)
                .Index(t => t.tbl_dining_set_up_Dining_ID);
            
            CreateTable(
                "dbo.tbl_item_order_mastertbl_dining_waiter",
                c => new
                    {
                        tbl_item_order_master_Order_Master_ID = c.Int(nullable: false),
                        tbl_dining_waiter_Dining_Waiter_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.tbl_item_order_master_Order_Master_ID, t.tbl_dining_waiter_Dining_Waiter_ID })
                .ForeignKey("dbo.tbl_item_order_master", t => t.tbl_item_order_master_Order_Master_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_dining_waiter", t => t.tbl_dining_waiter_Dining_Waiter_ID, cascadeDelete: true)
                .Index(t => t.tbl_item_order_master_Order_Master_ID)
                .Index(t => t.tbl_dining_waiter_Dining_Waiter_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_vacant_room", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_others_income", "Others_Income_Head_ID", "dbo.tbl_others_income_head");
            DropForeignKey("dbo.tbl_others_income", "Modified_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_others_income", "Creted_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_laundry_status", "Guest_Tran_ID", "dbo.tbl_room_transactions");
            DropForeignKey("dbo.tbl_laundry_expense", "Guest_Tran_ID", "dbo.tbl_room_transactions");
            DropForeignKey("dbo.tbl_laundry_expense", "Laundry_Type_ID", "dbo.tbl_laundry_type");
            DropForeignKey("dbo.tbl_guest_attendee", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_expense", "Exp_Head_ID", "dbo.tbl_expense_head");
            DropForeignKey("dbo.tbl_expense", "Modified_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_expense", "Creted_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_salary_transactions", "Tran_Ref_ID", "dbo.tbl_earning_head");
            DropForeignKey("dbo.tbl_salary_transactions", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_currency_setup", "Unit_ID", "dbo.tbl_unit_setup");
            DropForeignKey("dbo.tbl_currency_convert", "To_Currency", "dbo.tbl_currency_setup");
            DropForeignKey("dbo.tbl_currency_convert", "From_Currency", "dbo.tbl_currency_setup");
            DropForeignKey("dbo.tbl_item", "Unit_ID", "dbo.tbl_unit");
            DropForeignKey("dbo.tbl_stock", "Stock_Location_ID", "dbo.tbl_stock_location");
            DropForeignKey("dbo.tbl_purchase_details", "Stock_Location_ID", "dbo.tbl_stock_location");
            DropForeignKey("dbo.tbl_supplier_ledger", "Supplier_ID", "dbo.tbl_supplier");
            DropForeignKey("dbo.tbl_supplier_ledger", "Purchase_Master_ID", "dbo.tbl_purchase_master");
            DropForeignKey("dbo.tbl_purchase_master", "Supplier_ID", "dbo.tbl_supplier");
            DropForeignKey("dbo.tbl_purchase_details", "Purchase_Master_ID", "dbo.tbl_purchase_master");
            DropForeignKey("dbo.tbl_purchase_details", "Item_ID", "dbo.tbl_item");
            DropForeignKey("dbo.tbl_stock", "Item_ID", "dbo.tbl_item");
            DropForeignKey("dbo.tbl_item", "Subcata_ID", "dbo.tbl_subcategory");
            DropForeignKey("dbo.tbl_subcategory", "Cata_ID", "dbo.tbl_category");
            DropForeignKey("dbo.tbl_item", "Cata_ID", "dbo.tbl_category");
            DropForeignKey("dbo.tbl_item", "Brand_ID", "dbo.tbl_brand");
            DropForeignKey("dbo.tbl_bank_transaction", "Modified_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_bank_transaction", "Creted_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_bank_transaction", "Tran_Ref_ID", "dbo.tbl_account_info");
            DropForeignKey("dbo.tbl_account_info", "Modified_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_account_info", "Creted_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_account_info", "Branch_ID", "dbo.tbl_branch_info");
            DropForeignKey("dbo.tbl_account_info", "Bank_ID", "dbo.tbl_bank_info");
            DropForeignKey("dbo.tbl_banquet_booking", "Program_ID", "dbo.tbl_banquet_prog");
            DropForeignKey("dbo.tbl_property_information", "Country_ID", "dbo.tbl_country");
            DropForeignKey("dbo.tbl_guest_queries", "Country_ID", "dbo.tbl_country");
            DropForeignKey("dbo.tbl_guest_info", "Religion_ID", "dbo.tbl_religion");
            DropForeignKey("dbo.tbl_guest_stay_history", "tbl_guest_info_Guest_ID", "dbo.tbl_guest_info");
            DropForeignKey("dbo.tbl_guest_stay_history", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_stay_history", "Shift_To_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_guest_stay_history", "Shift_From_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_guest_stay_info", "Travel_Agent_ID", "dbo.tbl_travel_agent_info");
            DropForeignKey("dbo.tbl_messages", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_messages", "Receiving_Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_messages", "Delivering_Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_task_schdule", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_task_schdule", "Updated_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_task_assignment", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_report", "Reported_To", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_report", "Reported_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_report_history", "Report_ID", "dbo.tbl_report");
            DropForeignKey("dbo.tbl_item_assignment", "Report_ID", "dbo.tbl_report");
            DropForeignKey("dbo.tbl_register_book", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_task_assignment", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_task_schdule", "Task_ID", "dbo.tbl_task_setup");
            DropForeignKey("dbo.tbl_task_assignment", "Task_ID", "dbo.tbl_task_setup");
            DropForeignKey("dbo.tbl_task_schdule", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_task_schdule", "Maid_ID", "dbo.tbl_maid_info");
            DropForeignKey("dbo.tbl_task_assignment", "Maid_ID", "dbo.tbl_maid_info");
            DropForeignKey("dbo.tbl_maid_info", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_tariff", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_room_info", "Roomtype_ID", "dbo.tbl_roomtype_setup");
            DropForeignKey("dbo.tbl_room_info", "Room_view_ID", "dbo.tbl_room_view_setup");
            DropForeignKey("dbo.tbl_room_transactions", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_room_transactions", "Garment_In_ID", "dbo.tbl_return_germent_in");
            DropForeignKey("dbo.tbl_room_transactions", "Laundry_Service_ID", "dbo.tbl_laundry_service");
            DropForeignKey("dbo.tbl_room_transactions", "Laundry_Item_ID", "dbo.tbl_laundry_item");
            DropForeignKey("dbo.tbl_laundry_service_cost", "Laundry_Service_ID", "dbo.tbl_laundry_service");
            DropForeignKey("dbo.tbl_laundry_service_cost", "Laundry_Item_ID", "dbo.tbl_laundry_item");
            DropForeignKey("dbo.tbl_laundry_item", "Laundry_Item_Cata_ID", "dbo.tbl_laundry_item_category");
            DropForeignKey("dbo.tbl_room_transactions", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_room_transactions", "Guest_ID", "dbo.tbl_guest_info");
            DropForeignKey("dbo.tbl_room_transactions", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_room_temp_monitor", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_register_book", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_package", "tbl_room_info_Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_package", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_package", "Receiving_Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_package", "Delivering_Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_stay_info", "Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_guest_stay_history", "tbl_room_info_Room_ID", "dbo.tbl_room_info");
            DropForeignKey("dbo.tbl_room_info", "Floor_ID", "dbo.tbl_floor_setup");
            DropForeignKey("dbo.tbl_register_book", "Guest_ID", "dbo.tbl_guest_info");
            DropForeignKey("dbo.tbl_leave_status", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_leave", "Leave_Cause_ID", "dbo.tbl_leave_cause");
            DropForeignKey("dbo.tbl_leave", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_complain", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_complain", "Assigned_Emp", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_complain", "Heading_ID", "dbo.tbl_complain_heading");
            DropForeignKey("dbo.tbl_group_info", "Updated_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_group_info", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_group_sub_module_permission", "Group_ID", "dbo.tbl_group_info");
            DropForeignKey("dbo.tbl_group_module_permission", "Group_ID", "dbo.tbl_group_info");
            DropForeignKey("dbo.tbl_group_field_permission", "Group_ID", "dbo.tbl_group_info");
            DropForeignKey("dbo.tbl_group_employee", "Group_ID", "dbo.tbl_group_info");
            DropForeignKey("dbo.tbl_group_employee", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_emp_sub_module_permission", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_emp_module_permission", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_emp_mail", "Receiver_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_emp_mail", "Sender_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_emp_field_permission", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_dining_waiter", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_item_order_master", "Rest_Guest_ID", "dbo.tbl_restaurant_guest_info");
            DropForeignKey("dbo.tbl_item_order_master", "Rest_Company_ID", "dbo.tbl_restaurant_company_info");
            DropForeignKey("dbo.tbl_item_order_master", "Pay_Method_ID", "dbo.tbl_payment_method");
            DropForeignKey("dbo.tbl_item_order_master", "Order_Method_ID", "dbo.tbl_order_method");
            DropForeignKey("dbo.tbl_item_order_ledger", "Order_Master_ID", "dbo.tbl_item_order_master");
            DropForeignKey("dbo.tbl_item_order_details", "Order_Master_ID", "dbo.tbl_item_order_master");
            DropForeignKey("dbo.tbl_item_order_details", "Food_Side_ID", "dbo.tbl_food_side_extra");
            DropForeignKey("dbo.tbl_item_order_details", "Food_ID", "dbo.tbl_food");
            DropForeignKey("dbo.tbl_food", "Food_Cate_ID", "dbo.tbl_food_category");
            DropForeignKey("dbo.tbl_item_order_master", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_item_order_master", "Received_By_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_item_order_mastertbl_dining_waiter", "tbl_dining_waiter_Dining_Waiter_ID", "dbo.tbl_dining_waiter");
            DropForeignKey("dbo.tbl_item_order_mastertbl_dining_waiter", "tbl_item_order_master_Order_Master_ID", "dbo.tbl_item_order_master");
            DropForeignKey("dbo.tbl_item_order_mastertbl_dining_set_up", "tbl_dining_set_up_Dining_ID", "dbo.tbl_dining_set_up");
            DropForeignKey("dbo.tbl_item_order_mastertbl_dining_set_up", "tbl_item_order_master_Order_Master_ID", "dbo.tbl_item_order_master");
            DropForeignKey("dbo.tbl_item_order_master", "Program_ID", "dbo.tbl_banquet_prog");
            DropForeignKey("dbo.tbl_dining_set_up", "Dining_Waiter_ID", "dbo.tbl_dining_waiter");
            DropForeignKey("dbo.tbl_dining_set_up", "Dining_Location_ID", "dbo.tbl_dining_location");
            DropForeignKey("dbo.tbl_emp_info", "Designation_ID", "dbo.tbl_designation");
            DropForeignKey("dbo.tbl_emp_info", "Department_ID", "dbo.tbl_department");
            DropForeignKey("dbo.tbl_attendance", "Emp_ID", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_guest_stay_info", "Guest_ID", "dbo.tbl_guest_info");
            DropForeignKey("dbo.tbl_guest_stay_info", "Guest_Group_ID", "dbo.tbl_guest_group_info");
            DropForeignKey("dbo.tbl_guest_stay_info", "Company_ID", "dbo.tbl_company_info");
            DropForeignKey("dbo.tbl_guest_info", "Country_ID", "dbo.tbl_country");
            DropForeignKey("dbo.tbl_communication", "Guest_ID", "dbo.tbl_guest_info");
            DropForeignKey("dbo.tbl_conference_program", "Country_ID", "dbo.tbl_country");
            DropForeignKey("dbo.tbl_conference_utility_details", "Utility_Master_ID", "dbo.tbl_conference_utility_master");
            DropForeignKey("dbo.tbl_conference_utility_details", "Conf_Item_ID", "dbo.tbl_conf_item_setup");
            DropForeignKey("dbo.tbl_conference_utility_master", "Conf_Program_ID", "dbo.tbl_conference_program");
            DropForeignKey("dbo.tbl_conference_transactions", "Conf_Program_ID", "dbo.tbl_conference_program");
            DropForeignKey("dbo.tbl_tax_amount", "Tax_ID", "dbo.tbl_tax");
            DropForeignKey("dbo.tbl_conference_room", "Tax_ID", "dbo.tbl_tax");
            DropForeignKey("dbo.tbl_conference_program", "Conf_Room_ID", "dbo.tbl_conference_room");
            DropForeignKey("dbo.tbl_conference_program", "Conf_Purpose_ID", "dbo.tbl_conference_purpose");
            DropForeignKey("dbo.tbl_banquet_prog", "Country_ID", "dbo.tbl_country");
            DropForeignKey("dbo.tbl_banquet_transactions", "Program_ID", "dbo.tbl_banquet_prog");
            DropForeignKey("dbo.tbl_banquet_transactions", "Banq_Booking_ID", "dbo.tbl_banquet_booking");
            DropForeignKey("dbo.tbl_banquet_transactions", "Tran_Ref_ID", "dbo.tbl_banquet_amenities");
            DropForeignKey("dbo.tbl_banquet_prog", "Banquet_ID", "dbo.tbl_banquet_info");
            DropForeignKey("dbo.tbl_banquet_booking", "P_Purpose_ID", "dbo.Prorgram_Purpose");
            DropIndex("dbo.tbl_item_order_mastertbl_dining_waiter", new[] { "tbl_dining_waiter_Dining_Waiter_ID" });
            DropIndex("dbo.tbl_item_order_mastertbl_dining_waiter", new[] { "tbl_item_order_master_Order_Master_ID" });
            DropIndex("dbo.tbl_item_order_mastertbl_dining_set_up", new[] { "tbl_dining_set_up_Dining_ID" });
            DropIndex("dbo.tbl_item_order_mastertbl_dining_set_up", new[] { "tbl_item_order_master_Order_Master_ID" });
            DropIndex("dbo.tbl_vacant_room", new[] { "Room_ID" });
            DropIndex("dbo.tbl_others_income", new[] { "Modified_By" });
            DropIndex("dbo.tbl_others_income", new[] { "Creted_By" });
            DropIndex("dbo.tbl_others_income", new[] { "Others_Income_Head_ID" });
            DropIndex("dbo.tbl_laundry_status", new[] { "Guest_Tran_ID" });
            DropIndex("dbo.tbl_laundry_expense", new[] { "Laundry_Type_ID" });
            DropIndex("dbo.tbl_laundry_expense", new[] { "Guest_Tran_ID" });
            DropIndex("dbo.tbl_guest_attendee", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_expense", new[] { "Modified_By" });
            DropIndex("dbo.tbl_expense", new[] { "Creted_By" });
            DropIndex("dbo.tbl_expense", new[] { "Exp_Head_ID" });
            DropIndex("dbo.tbl_salary_transactions", new[] { "Tran_Ref_ID" });
            DropIndex("dbo.tbl_salary_transactions", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_currency_setup", new[] { "Unit_ID" });
            DropIndex("dbo.tbl_currency_convert", new[] { "To_Currency" });
            DropIndex("dbo.tbl_currency_convert", new[] { "From_Currency" });
            DropIndex("dbo.tbl_supplier_ledger", new[] { "Purchase_Master_ID" });
            DropIndex("dbo.tbl_supplier_ledger", new[] { "Supplier_ID" });
            DropIndex("dbo.tbl_purchase_master", new[] { "Supplier_ID" });
            DropIndex("dbo.tbl_purchase_details", new[] { "Purchase_Master_ID" });
            DropIndex("dbo.tbl_purchase_details", new[] { "Stock_Location_ID" });
            DropIndex("dbo.tbl_purchase_details", new[] { "Item_ID" });
            DropIndex("dbo.tbl_stock", new[] { "Item_ID" });
            DropIndex("dbo.tbl_stock", new[] { "Stock_Location_ID" });
            DropIndex("dbo.tbl_subcategory", new[] { "Cata_ID" });
            DropIndex("dbo.tbl_item", new[] { "Unit_ID" });
            DropIndex("dbo.tbl_item", new[] { "Brand_ID" });
            DropIndex("dbo.tbl_item", new[] { "Subcata_ID" });
            DropIndex("dbo.tbl_item", new[] { "Cata_ID" });
            DropIndex("dbo.tbl_bank_transaction", new[] { "Modified_By" });
            DropIndex("dbo.tbl_bank_transaction", new[] { "Creted_By" });
            DropIndex("dbo.tbl_bank_transaction", new[] { "Tran_Ref_ID" });
            DropIndex("dbo.tbl_account_info", new[] { "Modified_By" });
            DropIndex("dbo.tbl_account_info", new[] { "Creted_By" });
            DropIndex("dbo.tbl_account_info", new[] { "Branch_ID" });
            DropIndex("dbo.tbl_account_info", new[] { "Bank_ID" });
            DropIndex("dbo.tbl_property_information", new[] { "Country_ID" });
            DropIndex("dbo.tbl_guest_queries", new[] { "Country_ID" });
            DropIndex("dbo.tbl_report_history", new[] { "Report_ID" });
            DropIndex("dbo.tbl_item_assignment", new[] { "Report_ID" });
            DropIndex("dbo.tbl_report", new[] { "Reported_To" });
            DropIndex("dbo.tbl_report", new[] { "Reported_By" });
            DropIndex("dbo.tbl_task_schdule", new[] { "Updated_By" });
            DropIndex("dbo.tbl_task_schdule", new[] { "Created_By" });
            DropIndex("dbo.tbl_task_schdule", new[] { "Room_ID" });
            DropIndex("dbo.tbl_task_schdule", new[] { "Task_ID" });
            DropIndex("dbo.tbl_task_schdule", new[] { "Maid_ID" });
            DropIndex("dbo.tbl_maid_info", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_task_assignment", new[] { "Created_By" });
            DropIndex("dbo.tbl_task_assignment", new[] { "Room_ID" });
            DropIndex("dbo.tbl_task_assignment", new[] { "Task_ID" });
            DropIndex("dbo.tbl_task_assignment", new[] { "Maid_ID" });
            DropIndex("dbo.tbl_tariff", new[] { "Room_ID" });
            DropIndex("dbo.tbl_laundry_service_cost", new[] { "Laundry_Service_ID" });
            DropIndex("dbo.tbl_laundry_service_cost", new[] { "Laundry_Item_ID" });
            DropIndex("dbo.tbl_laundry_item", new[] { "Laundry_Item_Cata_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Garment_In_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Laundry_Service_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Laundry_Item_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Guest_ID" });
            DropIndex("dbo.tbl_room_transactions", new[] { "Room_ID" });
            DropIndex("dbo.tbl_room_temp_monitor", new[] { "Room_ID" });
            DropIndex("dbo.tbl_package", new[] { "tbl_room_info_Room_ID" });
            DropIndex("dbo.tbl_package", new[] { "Delivering_Emp_ID" });
            DropIndex("dbo.tbl_package", new[] { "Receiving_Emp_ID" });
            DropIndex("dbo.tbl_package", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_room_info", new[] { "Room_view_ID" });
            DropIndex("dbo.tbl_room_info", new[] { "Floor_ID" });
            DropIndex("dbo.tbl_room_info", new[] { "Roomtype_ID" });
            DropIndex("dbo.tbl_register_book", new[] { "Created_By" });
            DropIndex("dbo.tbl_register_book", new[] { "Room_ID" });
            DropIndex("dbo.tbl_register_book", new[] { "Guest_ID" });
            DropIndex("dbo.tbl_leave_status", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_leave", new[] { "Leave_Cause_ID" });
            DropIndex("dbo.tbl_leave", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_guest_complain", new[] { "Created_By" });
            DropIndex("dbo.tbl_guest_complain", new[] { "Assigned_Emp" });
            DropIndex("dbo.tbl_guest_complain", new[] { "Heading_ID" });
            DropIndex("dbo.tbl_group_sub_module_permission", new[] { "Group_ID" });
            DropIndex("dbo.tbl_group_module_permission", new[] { "Group_ID" });
            DropIndex("dbo.tbl_group_field_permission", new[] { "Group_ID" });
            DropIndex("dbo.tbl_group_info", new[] { "Updated_By" });
            DropIndex("dbo.tbl_group_info", new[] { "Created_By" });
            DropIndex("dbo.tbl_group_employee", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_group_employee", new[] { "Group_ID" });
            DropIndex("dbo.tbl_emp_sub_module_permission", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_emp_module_permission", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_emp_mail", new[] { "Receiver_ID" });
            DropIndex("dbo.tbl_emp_mail", new[] { "Sender_ID" });
            DropIndex("dbo.tbl_emp_field_permission", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_item_order_ledger", new[] { "Order_Master_ID" });
            DropIndex("dbo.tbl_food", new[] { "Food_Cate_ID" });
            DropIndex("dbo.tbl_item_order_details", new[] { "Order_Master_ID" });
            DropIndex("dbo.tbl_item_order_details", new[] { "Food_Side_ID" });
            DropIndex("dbo.tbl_item_order_details", new[] { "Food_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Rest_Company_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Rest_Guest_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Received_By_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Program_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Pay_Method_ID" });
            DropIndex("dbo.tbl_item_order_master", new[] { "Order_Method_ID" });
            DropIndex("dbo.tbl_dining_set_up", new[] { "Dining_Waiter_ID" });
            DropIndex("dbo.tbl_dining_set_up", new[] { "Dining_Location_ID" });
            DropIndex("dbo.tbl_dining_waiter", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_attendance", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_emp_info", new[] { "Designation_ID" });
            DropIndex("dbo.tbl_emp_info", new[] { "Department_ID" });
            DropIndex("dbo.tbl_messages", new[] { "Delivering_Emp_ID" });
            DropIndex("dbo.tbl_messages", new[] { "Receiving_Emp_ID" });
            DropIndex("dbo.tbl_messages", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_guest_stay_info", new[] { "Travel_Agent_ID" });
            DropIndex("dbo.tbl_guest_stay_info", new[] { "Guest_Group_ID" });
            DropIndex("dbo.tbl_guest_stay_info", new[] { "Company_ID" });
            DropIndex("dbo.tbl_guest_stay_info", new[] { "Guest_ID" });
            DropIndex("dbo.tbl_guest_stay_info", new[] { "Room_ID" });
            DropIndex("dbo.tbl_guest_stay_history", new[] { "tbl_guest_info_Guest_ID" });
            DropIndex("dbo.tbl_guest_stay_history", new[] { "tbl_room_info_Room_ID" });
            DropIndex("dbo.tbl_guest_stay_history", new[] { "Emp_ID" });
            DropIndex("dbo.tbl_guest_stay_history", new[] { "Shift_To_ID" });
            DropIndex("dbo.tbl_guest_stay_history", new[] { "Shift_From_ID" });
            DropIndex("dbo.tbl_communication", new[] { "Guest_ID" });
            DropIndex("dbo.tbl_guest_info", new[] { "Country_ID" });
            DropIndex("dbo.tbl_guest_info", new[] { "Religion_ID" });
            DropIndex("dbo.tbl_conference_utility_details", new[] { "Conf_Item_ID" });
            DropIndex("dbo.tbl_conference_utility_details", new[] { "Utility_Master_ID" });
            DropIndex("dbo.tbl_conference_utility_master", new[] { "Conf_Program_ID" });
            DropIndex("dbo.tbl_conference_transactions", new[] { "Conf_Program_ID" });
            DropIndex("dbo.tbl_tax_amount", new[] { "Tax_ID" });
            DropIndex("dbo.tbl_conference_room", new[] { "Tax_ID" });
            DropIndex("dbo.tbl_conference_program", new[] { "Conf_Purpose_ID" });
            DropIndex("dbo.tbl_conference_program", new[] { "Country_ID" });
            DropIndex("dbo.tbl_conference_program", new[] { "Conf_Room_ID" });
            DropIndex("dbo.tbl_banquet_transactions", new[] { "Banq_Booking_ID" });
            DropIndex("dbo.tbl_banquet_transactions", new[] { "Tran_Ref_ID" });
            DropIndex("dbo.tbl_banquet_transactions", new[] { "Program_ID" });
            DropIndex("dbo.tbl_banquet_prog", new[] { "Country_ID" });
            DropIndex("dbo.tbl_banquet_prog", new[] { "Banquet_ID" });
            DropIndex("dbo.tbl_banquet_booking", new[] { "P_Purpose_ID" });
            DropIndex("dbo.tbl_banquet_booking", new[] { "Program_ID" });
            DropTable("dbo.tbl_item_order_mastertbl_dining_waiter");
            DropTable("dbo.tbl_item_order_mastertbl_dining_set_up");
            DropTable("dbo.tbl_vehicle");
            DropTable("dbo.tbl_vacant_room");
            DropTable("dbo.tbl_travel_transactions");
            DropTable("dbo.tbl_travel_info");
            DropTable("dbo.tbl_sub_module_permission");
            DropTable("dbo.tbl_room_status");
            DropTable("dbo.tbl_preferences");
            DropTable("dbo.tbl_others_income_head");
            DropTable("dbo.tbl_others_income");
            DropTable("dbo.tbl_module_permissions");
            DropTable("dbo.tbl_laundry_status");
            DropTable("dbo.tbl_laundry_type");
            DropTable("dbo.tbl_laundry_expense");
            DropTable("dbo.tbl_hotel_service");
            DropTable("dbo.tbl_guest_attendee");
            DropTable("dbo.tbl_field_permission");
            DropTable("dbo.tbl_expense_head");
            DropTable("dbo.tbl_expense");
            DropTable("dbo.tbl_salary_transactions");
            DropTable("dbo.tbl_earning_head");
            DropTable("dbo.tbl_deduction_head");
            DropTable("dbo.tbl_unit_setup");
            DropTable("dbo.tbl_currency_setup");
            DropTable("dbo.tbl_currency_convert");
            DropTable("dbo.tbl_unit");
            DropTable("dbo.tbl_supplier_ledger");
            DropTable("dbo.tbl_supplier");
            DropTable("dbo.tbl_purchase_master");
            DropTable("dbo.tbl_purchase_details");
            DropTable("dbo.tbl_stock_location");
            DropTable("dbo.tbl_stock");
            DropTable("dbo.tbl_subcategory");
            DropTable("dbo.tbl_category");
            DropTable("dbo.tbl_item");
            DropTable("dbo.tbl_brand");
            DropTable("dbo.tbl_bank_transaction");
            DropTable("dbo.tbl_amenities_setup");
            DropTable("dbo.tbl_amenities_details");
            DropTable("dbo.tbl_branch_info");
            DropTable("dbo.tbl_bank_info");
            DropTable("dbo.tbl_account_info");
            DropTable("dbo.sysdiagrams");
            DropTable("dbo.tbl_property_information");
            DropTable("dbo.tbl_guest_queries");
            DropTable("dbo.tbl_religion");
            DropTable("dbo.tbl_travel_agent_info");
            DropTable("dbo.tbl_report_history");
            DropTable("dbo.tbl_item_assignment");
            DropTable("dbo.tbl_report");
            DropTable("dbo.tbl_task_setup");
            DropTable("dbo.tbl_task_schdule");
            DropTable("dbo.tbl_maid_info");
            DropTable("dbo.tbl_task_assignment");
            DropTable("dbo.tbl_tariff");
            DropTable("dbo.tbl_roomtype_setup");
            DropTable("dbo.tbl_room_view_setup");
            DropTable("dbo.tbl_return_germent_in");
            DropTable("dbo.tbl_laundry_service");
            DropTable("dbo.tbl_laundry_service_cost");
            DropTable("dbo.tbl_laundry_item_category");
            DropTable("dbo.tbl_laundry_item");
            DropTable("dbo.tbl_room_transactions");
            DropTable("dbo.tbl_room_temp_monitor");
            DropTable("dbo.tbl_package");
            DropTable("dbo.tbl_floor_setup");
            DropTable("dbo.tbl_room_info");
            DropTable("dbo.tbl_register_book");
            DropTable("dbo.tbl_leave_status");
            DropTable("dbo.tbl_leave_cause");
            DropTable("dbo.tbl_leave");
            DropTable("dbo.tbl_complain_heading");
            DropTable("dbo.tbl_guest_complain");
            DropTable("dbo.tbl_group_sub_module_permission");
            DropTable("dbo.tbl_group_module_permission");
            DropTable("dbo.tbl_group_field_permission");
            DropTable("dbo.tbl_group_info");
            DropTable("dbo.tbl_group_employee");
            DropTable("dbo.tbl_emp_sub_module_permission");
            DropTable("dbo.tbl_emp_module_permission");
            DropTable("dbo.tbl_emp_mail");
            DropTable("dbo.tbl_emp_field_permission");
            DropTable("dbo.tbl_restaurant_guest_info");
            DropTable("dbo.tbl_restaurant_company_info");
            DropTable("dbo.tbl_payment_method");
            DropTable("dbo.tbl_order_method");
            DropTable("dbo.tbl_item_order_ledger");
            DropTable("dbo.tbl_food_side_extra");
            DropTable("dbo.tbl_food_category");
            DropTable("dbo.tbl_food");
            DropTable("dbo.tbl_item_order_details");
            DropTable("dbo.tbl_item_order_master");
            DropTable("dbo.tbl_dining_location");
            DropTable("dbo.tbl_dining_set_up");
            DropTable("dbo.tbl_dining_waiter");
            DropTable("dbo.tbl_designation");
            DropTable("dbo.tbl_department");
            DropTable("dbo.tbl_attendance");
            DropTable("dbo.tbl_emp_info");
            DropTable("dbo.tbl_messages");
            DropTable("dbo.tbl_guest_group_info");
            DropTable("dbo.tbl_company_info");
            DropTable("dbo.tbl_guest_stay_info");
            DropTable("dbo.tbl_guest_stay_history");
            DropTable("dbo.tbl_communication");
            DropTable("dbo.tbl_guest_info");
            DropTable("dbo.tbl_conf_item_setup");
            DropTable("dbo.tbl_conference_utility_details");
            DropTable("dbo.tbl_conference_utility_master");
            DropTable("dbo.tbl_conference_transactions");
            DropTable("dbo.tbl_tax_amount");
            DropTable("dbo.tbl_tax");
            DropTable("dbo.tbl_conference_room");
            DropTable("dbo.tbl_conference_purpose");
            DropTable("dbo.tbl_conference_program");
            DropTable("dbo.tbl_country");
            DropTable("dbo.tbl_banquet_amenities");
            DropTable("dbo.tbl_banquet_transactions");
            DropTable("dbo.tbl_banquet_info");
            DropTable("dbo.tbl_banquet_prog");
            DropTable("dbo.tbl_banquet_booking");
            DropTable("dbo.Prorgram_Purpose");
        }
    }
}
