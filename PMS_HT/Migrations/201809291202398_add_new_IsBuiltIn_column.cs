namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_new_IsBuiltIn_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_department", "IsBuiltIn", c => c.Boolean());
            AddColumn("dbo.tbl_designation", "IsBuiltIn", c => c.Boolean());
            AddColumn("dbo.tbl_order_method", "IsBuiltIn", c => c.Boolean());
            AddColumn("dbo.tbl_payment_method", "IsBuiltIn", c => c.Boolean());
            AddColumn("dbo.tbl_unit", "IsBuiltIn", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.tbl_unit", "IsBuiltIn");
            DropColumn("dbo.tbl_payment_method", "IsBuiltIn");
            DropColumn("dbo.tbl_order_method", "IsBuiltIn");
            DropColumn("dbo.tbl_designation", "IsBuiltIn");
            DropColumn("dbo.tbl_department", "IsBuiltIn");
        }
    }
}
