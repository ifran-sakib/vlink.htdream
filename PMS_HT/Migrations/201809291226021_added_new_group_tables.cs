namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_new_group_tables : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.tbl_servicetbl_service_company_info", newName: "tbl_service_company_infotbl_service");
            DropPrimaryKey("dbo.tbl_service_company_infotbl_service");
            CreateTable(
                "dbo.tbl_guest_group_post_master_task",
                c => new
                {
                    PM_Task_ID = c.Int(nullable: false, identity: true),
                    Guest_Group_ID = c.Int(nullable: false),
                    Service_ID = c.Int(nullable: false),
                    Post_Master_ID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.PM_Task_ID)
                .ForeignKey("dbo.tbl_group_info", t => t.Guest_Group_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_guest_group_post_master", t => t.Post_Master_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_service", t => t.Service_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_guest_group_info", t => t.Guest_Group_ID, cascadeDelete: true)
                .Index(t => t.Guest_Group_ID)
                .Index(t => t.Service_ID)
                .Index(t => t.Post_Master_ID);

            CreateTable(
                "dbo.tbl_guest_group_post_master",
                c => new
                {
                    Post_Master_ID = c.Int(nullable: false, identity: true),
                    Guest_Group_ID = c.Int(),
                    Stay_Info_ID = c.Int(),
                })
                .PrimaryKey(t => t.Post_Master_ID)
                .ForeignKey("dbo.tbl_guest_group_info", t => t.Guest_Group_ID)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .Index(t => t.Guest_Group_ID)
                .Index(t => t.Stay_Info_ID);

            AddColumn("dbo.tbl_guest_group_info", "Group_Name", c => c.String());
            AddColumn("dbo.tbl_guest_group_info", "Arrival_Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.tbl_guest_group_info", "Departure_Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.tbl_guest_group_info", "No_Of_Rooms", c => c.Int(nullable: false));
            AddColumn("dbo.tbl_guest_group_info", "Reserve_Date", c => c.DateTime());
            AddColumn("dbo.tbl_guest_group_info", "Booking_Date", c => c.DateTime());
            AddColumn("dbo.tbl_guest_group_info", "Status", c => c.String(maxLength: 50));
            AddColumn("dbo.tbl_guest_group_info", "Order_Method", c => c.String());
            AddColumn("dbo.tbl_guest_group_info", "Airport_Pickup", c => c.Boolean());
            AddColumn("dbo.tbl_guest_group_info", "Transportation", c => c.String());
            AddColumn("dbo.tbl_guest_group_info", "Special_Request", c => c.String());
            AddColumn("dbo.tbl_guest_group_info", "Created_By", c => c.Int(nullable: false));
            AddColumn("dbo.tbl_room_transactions", "Routed_To_ID", c => c.Int());
            AlterColumn("dbo.tbl_guest_group_info", "Address", c => c.String(maxLength: 150));
            AlterColumn("dbo.tbl_guest_group_info", "Phone", c => c.String(maxLength: 15));
            AddPrimaryKey("dbo.tbl_service_company_infotbl_service", new[] { "tbl_service_company_info_Service_Company_ID", "tbl_service_Service_ID" });
            CreateIndex("dbo.tbl_guest_group_info", "Created_By");
            AddForeignKey("dbo.tbl_guest_group_info", "Created_By", "dbo.tbl_emp_info", "Emp_ID", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.tbl_guest_group_post_master_task", "Guest_Group_ID", "dbo.tbl_guest_group_info");
            DropForeignKey("dbo.tbl_guest_group_post_master_task", "Service_ID", "dbo.tbl_service");
            DropForeignKey("dbo.tbl_guest_group_post_master", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_guest_group_post_master_task", "Post_Master_ID", "dbo.tbl_guest_group_post_master");
            DropForeignKey("dbo.tbl_guest_group_post_master", "Guest_Group_ID", "dbo.tbl_guest_group_info");
            DropForeignKey("dbo.tbl_guest_group_post_master_task", "Guest_Group_ID", "dbo.tbl_group_info");
            DropForeignKey("dbo.tbl_guest_group_info", "Created_By", "dbo.tbl_emp_info");
            DropIndex("dbo.tbl_guest_group_post_master", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_guest_group_post_master", new[] { "Guest_Group_ID" });
            DropIndex("dbo.tbl_guest_group_post_master_task", new[] { "Post_Master_ID" });
            DropIndex("dbo.tbl_guest_group_post_master_task", new[] { "Service_ID" });
            DropIndex("dbo.tbl_guest_group_post_master_task", new[] { "Guest_Group_ID" });
            DropIndex("dbo.tbl_guest_group_info", new[] { "Created_By" });
            DropPrimaryKey("dbo.tbl_service_company_infotbl_service");
            AlterColumn("dbo.tbl_guest_group_info", "Phone", c => c.String());
            AlterColumn("dbo.tbl_guest_group_info", "Address", c => c.String());
            DropColumn("dbo.tbl_room_transactions", "Routed_To_ID");
            DropColumn("dbo.tbl_guest_group_info", "Created_By");
            DropColumn("dbo.tbl_guest_group_info", "Special_Request");
            DropColumn("dbo.tbl_guest_group_info", "Transportation");
            DropColumn("dbo.tbl_guest_group_info", "Airport_Pickup");
            DropColumn("dbo.tbl_guest_group_info", "Order_Method");
            DropColumn("dbo.tbl_guest_group_info", "Status");
            DropColumn("dbo.tbl_guest_group_info", "Booking_Date");
            DropColumn("dbo.tbl_guest_group_info", "Reserve_Date");
            DropColumn("dbo.tbl_guest_group_info", "No_Of_Rooms");
            DropColumn("dbo.tbl_guest_group_info", "Departure_Date");
            DropColumn("dbo.tbl_guest_group_info", "Arrival_Date");
            DropColumn("dbo.tbl_guest_group_info", "Group_Name");
            DropTable("dbo.tbl_guest_group_post_master");
            DropTable("dbo.tbl_guest_group_post_master_task");
            AddPrimaryKey("dbo.tbl_service_company_infotbl_service", new[] { "tbl_service_Service_ID", "tbl_service_company_info_Service_Company_ID" });
            RenameTable(name: "dbo.tbl_service_company_infotbl_service", newName: "tbl_servicetbl_service_company_info");
        }

    }
}
