namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_stringlength_note_of_tbl_expense_others_income_bank_transactions : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tbl_bank_transaction", "Particulars", c => c.String(maxLength: 500));
            AlterColumn("dbo.tbl_expense", "Note", c => c.String(maxLength: 500));
            AlterColumn("dbo.tbl_others_income", "Note", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_others_income", "Note", c => c.String(maxLength: 100));
            AlterColumn("dbo.tbl_expense", "Note", c => c.String(maxLength: 100));
            AlterColumn("dbo.tbl_bank_transaction", "Particulars", c => c.String(maxLength: 200));
        }
    }
}
