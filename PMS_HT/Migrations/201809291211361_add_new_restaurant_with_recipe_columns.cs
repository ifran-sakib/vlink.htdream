namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_new_restaurant_with_recipe_columns : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_food_recipe",
                c => new
                    {
                        Recipe_ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Total_Item_Cost = c.Decimal(precision: 18, scale: 2),
                        Ingredient_Cost = c.Decimal(precision: 18, scale: 2),
                        Profit_Margin = c.Decimal(precision: 18, scale: 2),
                        Total_Cost = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Recipe_ID);
            
            CreateTable(
                "dbo.tbl_food_recipe_items",
                c => new
                    {
                        Recipe_Item_ID = c.Int(nullable: false, identity: true),
                        Recipe_ID = c.Int(),
                        Item_ID = c.Int(),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Recipe_Item_ID)
                .ForeignKey("dbo.tbl_item", t => t.Item_ID)
                .ForeignKey("dbo.tbl_food_recipe", t => t.Recipe_ID)
                .Index(t => t.Recipe_ID)
                .Index(t => t.Item_ID);
            
            AddColumn("dbo.tbl_food", "Recipe_ID", c => c.Int());
            CreateIndex("dbo.tbl_food", "Recipe_ID");
            AddForeignKey("dbo.tbl_food", "Recipe_ID", "dbo.tbl_food_recipe", "Recipe_ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_food", "Recipe_ID", "dbo.tbl_food_recipe");
            DropForeignKey("dbo.tbl_food_recipe_items", "Recipe_ID", "dbo.tbl_food_recipe");
            DropForeignKey("dbo.tbl_food_recipe_items", "Item_ID", "dbo.tbl_item");
            DropIndex("dbo.tbl_food_recipe_items", new[] { "Item_ID" });
            DropIndex("dbo.tbl_food_recipe_items", new[] { "Recipe_ID" });
            DropIndex("dbo.tbl_food", new[] { "Recipe_ID" });
            DropColumn("dbo.tbl_food", "Recipe_ID");
            DropTable("dbo.tbl_food_recipe_items");
            DropTable("dbo.tbl_food_recipe");
        }
    }
}
