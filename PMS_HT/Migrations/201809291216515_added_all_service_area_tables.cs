namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_all_service_area_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_service_company_info",
                c => new
                {
                    Service_Company_ID = c.Int(nullable: false, identity: true),
                    Name = c.String(maxLength: 100),
                    Phone = c.String(maxLength: 15),
                    Address = c.String(maxLength: 200),
                    Email = c.String(maxLength: 60),
                })
                .PrimaryKey(t => t.Service_Company_ID);

            CreateTable(
                "dbo.tbl_service",
                c => new
                {
                    Service_ID = c.Int(nullable: false, identity: true),
                    Name = c.String(maxLength: 100),
                    VAT = c.Decimal(precision: 18, scale: 2),
                    SC = c.Decimal(precision: 18, scale: 2),
                    CC = c.Decimal(precision: 18, scale: 2),
                    Created_By = c.Int(),
                    Updated_By = c.Int(),
                    IsBuiltIn = c.Boolean(),
                    IsAvailable = c.Boolean(nullable: false, defaultValue: true),
                })
                .PrimaryKey(t => t.Service_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Updated_By)
                .Index(t => t.Created_By)
                .Index(t => t.Updated_By);

            CreateTable(
                "dbo.tbl_service_guest_info",
                c => new
                {
                    Service_Guest_ID = c.Int(nullable: false, identity: true),
                    Name = c.String(maxLength: 100),
                    Phone = c.String(maxLength: 15),
                    Address = c.String(maxLength: 200),
                    Email = c.String(maxLength: 60),
                })
                .PrimaryKey(t => t.Service_Guest_ID);

            CreateTable(
                "dbo.tbl_service_item",
                c => new
                {
                    Service_Item_ID = c.Int(nullable: false, identity: true),
                    Name = c.String(maxLength: 100),
                    Image = c.String(maxLength: 300),
                    Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    Service_ID = c.Int(nullable: false),
                    Created_By = c.Int(),
                    Updated_By = c.Int(),
                    IsBuiltIn = c.Boolean(),
                    IsAvailable = c.Boolean(nullable: false, defaultValue: true),
                })
                .PrimaryKey(t => t.Service_Item_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Created_By)
                .ForeignKey("dbo.tbl_emp_info", t => t.Updated_By)
                .ForeignKey("dbo.tbl_service", t => t.Service_ID, cascadeDelete: true)
                .Index(t => t.Service_ID)
                .Index(t => t.Created_By)
                .Index(t => t.Updated_By);

            CreateTable(
                "dbo.tbl_service_purchase_master",
                c => new
                {
                    Service_Master_ID = c.Int(nullable: false, identity: true),
                    Purchase_Date = c.DateTime(),
                    Booking_Date = c.DateTime(),
                    Payment_Method_ID = c.Int(),
                    Service_ID = c.Int(),
                    Stay_Info_ID = c.Int(),
                    Note = c.String(maxLength: 400),
                    Status = c.String(maxLength: 50),
                    Served_By = c.Int(),
                    Service_Guest_ID = c.Int(),
                    Service_Company_ID = c.Int(),
                })
                .PrimaryKey(t => t.Service_Master_ID)
                .ForeignKey("dbo.tbl_emp_info", t => t.Served_By)
                .ForeignKey("dbo.tbl_guest_stay_info", t => t.Stay_Info_ID)
                .ForeignKey("dbo.tbl_payment_method", t => t.Payment_Method_ID)
                .ForeignKey("dbo.tbl_service", t => t.Service_ID)
                .ForeignKey("dbo.tbl_service_company_info", t => t.Service_Company_ID)
                .ForeignKey("dbo.tbl_service_guest_info", t => t.Service_Guest_ID)
                .Index(t => t.Payment_Method_ID)
                .Index(t => t.Service_ID)
                .Index(t => t.Stay_Info_ID)
                .Index(t => t.Served_By)
                .Index(t => t.Service_Guest_ID)
                .Index(t => t.Service_Company_ID);

            CreateTable(
                "dbo.tbl_service_purchase_detail",
                c => new
                {
                    Service_Details_ID = c.Int(nullable: false, identity: true),
                    Service_Item_ID = c.Int(),
                    Quantity = c.Decimal(precision: 18, scale: 2),
                    Price = c.Decimal(precision: 18, scale: 2),
                    VAT = c.Decimal(precision: 18, scale: 2),
                    SC = c.Decimal(precision: 18, scale: 2),
                    Total = c.Decimal(precision: 18, scale: 2),
                    Service_Master_ID = c.Int(),
                    Note = c.String(maxLength: 400),
                })
                .PrimaryKey(t => t.Service_Details_ID)
                .ForeignKey("dbo.tbl_service_item", t => t.Service_Item_ID)
                .ForeignKey("dbo.tbl_service_purchase_master", t => t.Service_Master_ID)
                .Index(t => t.Service_Item_ID)
                .Index(t => t.Service_Master_ID);

            CreateTable(
                "dbo.tbl_service_purchase_ledger",
                c => new
                {
                    Service_Ledger_ID = c.Int(nullable: false, identity: true),
                    Service_Master_ID = c.Int(),
                    Tran_Ref_ID = c.Int(nullable: false),
                    Tran_Ref_Name = c.String(maxLength: 50),
                    Particular = c.String(maxLength: 150),
                    Debit = c.Decimal(precision: 18, scale: 2),
                    Credit = c.Decimal(precision: 18, scale: 2),
                    Tran_Date = c.DateTime(),
                    Note = c.String(maxLength: 400),
                })
                .PrimaryKey(t => t.Service_Ledger_ID)
                .ForeignKey("dbo.tbl_service_purchase_master", t => t.Service_Master_ID)
                .Index(t => t.Service_Master_ID);

            CreateTable(
                "dbo.tbl_servicetbl_service_company_info",
                c => new
                {
                    tbl_service_Service_ID = c.Int(nullable: false),
                    tbl_service_company_info_Service_Company_ID = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.tbl_service_Service_ID, t.tbl_service_company_info_Service_Company_ID })
                .ForeignKey("dbo.tbl_service", t => t.tbl_service_Service_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_service_company_info", t => t.tbl_service_company_info_Service_Company_ID, cascadeDelete: true)
                .Index(t => t.tbl_service_Service_ID)
                .Index(t => t.tbl_service_company_info_Service_Company_ID);

            CreateTable(
                "dbo.tbl_service_guest_infotbl_service",
                c => new
                {
                    tbl_service_guest_info_Service_Guest_ID = c.Int(nullable: false),
                    tbl_service_Service_ID = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.tbl_service_guest_info_Service_Guest_ID, t.tbl_service_Service_ID })
                .ForeignKey("dbo.tbl_service_guest_info", t => t.tbl_service_guest_info_Service_Guest_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_service", t => t.tbl_service_Service_ID, cascadeDelete: true)
                .Index(t => t.tbl_service_guest_info_Service_Guest_ID)
                .Index(t => t.tbl_service_Service_ID);


            Sql("INSERT INTO tbl_service (Name, VAT, SC, Created_By, Updated_By, IsBuiltIn) VALUES ('Room Rent', 15, 0, 2, 2, 1)");
            Sql("INSERT INTO tbl_service (Name, VAT, SC, Created_By, Updated_By, IsBuiltIn) VALUES ('Room Service', 15, 0, 2, 2, 1)");

        }

        public override void Down()
        {
            DropForeignKey("dbo.tbl_service_purchase_ledger", "Service_Master_ID", "dbo.tbl_service_purchase_master");
            DropForeignKey("dbo.tbl_service_purchase_detail", "Service_Master_ID", "dbo.tbl_service_purchase_master");
            DropForeignKey("dbo.tbl_service_purchase_detail", "Service_Item_ID", "dbo.tbl_service_item");
            DropForeignKey("dbo.tbl_service_purchase_master", "Service_Guest_ID", "dbo.tbl_service_guest_info");
            DropForeignKey("dbo.tbl_service_purchase_master", "Service_Company_ID", "dbo.tbl_service_company_info");
            DropForeignKey("dbo.tbl_service_purchase_master", "Service_ID", "dbo.tbl_service");
            DropForeignKey("dbo.tbl_service_purchase_master", "Payment_Method_ID", "dbo.tbl_payment_method");
            DropForeignKey("dbo.tbl_service_purchase_master", "Stay_Info_ID", "dbo.tbl_guest_stay_info");
            DropForeignKey("dbo.tbl_service_purchase_master", "Served_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_service_item", "Service_ID", "dbo.tbl_service");
            DropForeignKey("dbo.tbl_service_item", "Updated_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_service_item", "Created_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_service_guest_infotbl_service", "tbl_service_Service_ID", "dbo.tbl_service");
            DropForeignKey("dbo.tbl_service_guest_infotbl_service", "tbl_service_guest_info_Service_Guest_ID", "dbo.tbl_service_guest_info");
            DropForeignKey("dbo.tbl_servicetbl_service_company_info", "tbl_service_company_info_Service_Company_ID", "dbo.tbl_service_company_info");
            DropForeignKey("dbo.tbl_servicetbl_service_company_info", "tbl_service_Service_ID", "dbo.tbl_service");
            DropForeignKey("dbo.tbl_service", "Updated_By", "dbo.tbl_emp_info");
            DropForeignKey("dbo.tbl_service", "Created_By", "dbo.tbl_emp_info");
            DropIndex("dbo.tbl_service_guest_infotbl_service", new[] { "tbl_service_Service_ID" });
            DropIndex("dbo.tbl_service_guest_infotbl_service", new[] { "tbl_service_guest_info_Service_Guest_ID" });
            DropIndex("dbo.tbl_servicetbl_service_company_info", new[] { "tbl_service_company_info_Service_Company_ID" });
            DropIndex("dbo.tbl_servicetbl_service_company_info", new[] { "tbl_service_Service_ID" });
            DropIndex("dbo.tbl_service_purchase_ledger", new[] { "Service_Master_ID" });
            DropIndex("dbo.tbl_service_purchase_detail", new[] { "Service_Master_ID" });
            DropIndex("dbo.tbl_service_purchase_detail", new[] { "Service_Item_ID" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Service_Company_ID" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Service_Guest_ID" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Served_By" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Stay_Info_ID" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Service_ID" });
            DropIndex("dbo.tbl_service_purchase_master", new[] { "Payment_Method_ID" });
            DropIndex("dbo.tbl_service_item", new[] { "Updated_By" });
            DropIndex("dbo.tbl_service_item", new[] { "Created_By" });
            DropIndex("dbo.tbl_service_item", new[] { "Service_ID" });
            DropIndex("dbo.tbl_service", new[] { "Updated_By" });
            DropIndex("dbo.tbl_service", new[] { "Created_By" });
            DropTable("dbo.tbl_service_guest_infotbl_service");
            DropTable("dbo.tbl_servicetbl_service_company_info");
            DropTable("dbo.tbl_service_purchase_ledger");
            DropTable("dbo.tbl_service_purchase_detail");
            DropTable("dbo.tbl_service_purchase_master");
            DropTable("dbo.tbl_service_item");
            DropTable("dbo.tbl_service_guest_info");
            DropTable("dbo.tbl_service");
            DropTable("dbo.tbl_service_company_info");
        }
    }
}
