namespace PMS_HT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_tbl_guest_group_room_category : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_guest_group_room_category",
                c => new
                    {
                        Group_Room_Category_ID = c.Int(nullable: false, identity: true),
                        Guest_Group_ID = c.Int(nullable: false),
                        Roomtype_ID = c.Int(nullable: false),
                        NoOfRooms = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Group_Room_Category_ID)
                .ForeignKey("dbo.tbl_guest_group_info", t => t.Guest_Group_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_roomtype_setup", t => t.Roomtype_ID, cascadeDelete: true)
                .Index(t => t.Guest_Group_ID)
                .Index(t => t.Roomtype_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_guest_group_room_category", "Roomtype_ID", "dbo.tbl_roomtype_setup");
            DropForeignKey("dbo.tbl_guest_group_room_category", "Guest_Group_ID", "dbo.tbl_guest_group_info");
            DropIndex("dbo.tbl_guest_group_room_category", new[] { "Roomtype_ID" });
            DropIndex("dbo.tbl_guest_group_room_category", new[] { "Guest_Group_ID" });
            DropTable("dbo.tbl_guest_group_room_category");
        }
    }
}
