﻿using PMS_HT.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using PMS_HT.Models;
using System.Threading.Tasks;

namespace PMS_HT.CustomClass
{
    public class SendEmail
    {
        private PMS_HT_DB db = new PMS_HT_DB();
        private readonly CommonRepoprtData _rpd = new CommonRepoprtData();

        private readonly string From = "hongkongqqmusic7@gmail.com";
        private readonly string Password = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"].ToString();
        private const string SmtpHostName = "smtp.gmail.com";
        private const int SmtpPort = 587;
        private MailAddress To;
        private string Subject;
        private string Body;
        private int RoomId;
        private DateTime CheckIn;
        private DateTime CheckOut;
        private decimal Stay;
        private decimal Paid;
        private int TotalRooms;
        private int TotalGuests;
        private string Name;
        private string Address;
        private string Phone;
        private int ConfirmationNumber;

        public SendEmail(string to, string subject, int roomId, DateTime checkIn, DateTime checkOut, double totalStay,
            decimal paid, int totalRooms, int totalGuests, string guestName, string guestAddress, string guestPhone, int confirmationNumber)
        {
            To = new MailAddress(to);
            Subject = subject;
            RoomId = roomId;
            CheckIn = checkIn;
            CheckOut = checkOut;
            Stay = (decimal)totalStay;
            Paid = paid;
            TotalRooms = totalRooms;
            TotalGuests = totalGuests;
            Name = guestName;
            Address = guestAddress;
            Phone = guestPhone;
            ConfirmationNumber = confirmationNumber;

            Task.Run(() => EmailSender());
        }

        private void EmailBodyBuilder()
        {
            tbl_room_info tbl_room = db.tbl_room_info.Find(RoomId);
            decimal vat = 0;
            if (tbl_room != null)
            {
                decimal rate = (decimal)(tbl_room.Rate + (tbl_room.Rate * vat / 100));
                decimal payable = rate * Stay;
                decimal due = payable - Paid;

                var body = @"<div style=""background-color:#eeeeee;padding:3% 5% 3% 10%;border:2px;"">
		                    <h4 style=""display:inline;"">RESERVATION DETAILS </h4><h5 style=""display:inline;"">(CONFIRMATION NUMBER : {0}) <strong>CONFIRMED</strong></h5><br />
		                    <div>
			                    <div style=""text-align:center;""> 
				                    <img src=""https://s-ec.bstatic.com/images/hotel/max1024x768/537/53766954.jpg"" border=""3"" style=""width:200px;""> 
			                    </div>
			                    <br />
			                    <div> 
				                    <h4> {1} </h4>
				                    <span>{2}</span><br />
				                    <span>{3}</span><br />
				                    <span>hotelsuitesadafcox@gmail.com</span><br /><br />
				
				                    <strong>Check-In</strong> {4} <br />
				                    <strong>Check-Out</strong> {5} <br />
				                    <strong>Rooms {6} | Guests-{7} | Nights-{8}</strong> <br />
				                    <table style=""width:100%;border:2px solid black;"">
					                    <tr>
						                    <td style=""background-color:#80acf2;width:50%"">Room Rate</td><td style=""text-align:right; padding-right:15%;"">BDT {9}</td>
					                    </tr>
					                    <tr>
						                    <td style=""background-color:#98bcf5;"">Total Charge</td><td style=""text-align:right; padding-right:15%;"">BDT {10}</td>
					                    </tr>					
					                    <tr>
						                    <td style=""background-color:#80acf2;"">Paid</td><td style=""text-align:right; padding-right:15%;"">BDT {11}</td>
					                    </tr>
					                    <tr>
						                    <td style=""background-color:#98bcf5;"">Due</td><td style=""text-align:right; padding-right:15%;"">BDT {12}</td>
					                    </tr>
				                    </table>
			                    </div>
			
			                    <div> 
				                    <h4>GUEST DETAILS</h4>
				                    <strong>{13}</strong> <br />
				                    {14} <br />
				                    {15} <br />
				                    {16}<br />
				                    <h4>ROOM DETAILS</h4>
				                    <table style=""width:100%;border:2px solid black;"">
					                    <tr>
						                    <td style=""background-color:#80acf2;width:50%;"">Room No.</td><td style=""text-align:center"">{17}</td>
					                    </tr>
					                    <tr>
						                    <td style=""background-color:#98bcf5;"">Room Type</td><td style=""text-align:center"">{18}</td>
					                    </tr>					
					                    <tr>
						                    <td style=""background-color:#80acf2;"">Floor No.</td><td style=""text-align:center"">{19}</td>
					                    </tr>
				                    </table>
			                    </div>
			                    <div style=""flex:3; text-align:left;"">
			                    </div>			                    
                            </div>
                            <br />
		                    <strong>This is an auto generated email. For any query contact {20}. Powered by <a href=""http://www.v-linknetwork.com""> V-Link Network</a></strong>
	                    </div>";

                Body = string.Format(body, ConfirmationNumber, _rpd.PropertyName, _rpd.PropertyAddress,
                    _rpd.PropertyPhone, CheckIn.ToString("dddd, dd MMMM yyyy"), CheckOut.ToString("dddd, dd MMMM yyyy"), TotalRooms,
                    TotalGuests, Stay, rate.ToString("N"), payable.ToString("N"), Paid.ToString("N"), due.ToString("N"), Name, Address, Phone, To, tbl_room.Room_No,
                    tbl_room.tbl_roomtype_setup.Roomtype_Name, tbl_room.tbl_floor_setup.Floor_No, _rpd.PropertyName);

            }
        }

        protected async Task EmailSender()
        {
            EmailBodyBuilder();
            NetworkCredential credentials = new NetworkCredential(From, Password);
            SmtpClient client = new SmtpClient(SmtpHostName)
            {
                Port = SmtpPort,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = credentials
            };
            using (var mail = new MailMessage())
            {
                mail.From = new MailAddress(From);
                mail.To.Add(To);
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                await client.SendMailAsync(mail);
            }
        }
    }
}

//FileStream fileStream = new FileStream(attachmentName, FileMode.Open, FileAccess.Read);
//var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
//var message = new MailMessage();
////message.To.Add(new MailAddress("hongkongqqmusic7@gmail.com"));
//message.To.Add(new MailAddress("arnabjitu@gmail.com"));  // replace with valid value 
////message.To.Add(new MailAddress("rubayet.atc@gmail.com"));  // replace with valid value 
//message.From = new MailAddress("hongkongqqmusic7@gmail.com");  // replace with valid value
//message.Subject = "Hotel Safina DB backup";
//message.Body = string.Format(body, "Auto Backup", "hongkongqqmusic7@gmail.com", "Test Message");
//message.IsBodyHtml = true;
//Attachment attachment;
//attachment = new Attachment(fileStream, fileName);
//message.Attachments.Add(attachment);
//using (var smtp = new SmtpClient())
//{
//    var credential = new NetworkCredential
//    {
//        UserName = "hongkongqqmusic7@gmail.com",  // replace with valid value
//        Password = "vlink1Network"  // replace with valid value
//    };
//    smtp.Credentials = credential;
//    smtp.Host = "smtp.gmail.com";
//    smtp.Port = 587;
//    smtp.EnableSsl = true;
//    await smtp.SendMailAsync(message);
//}