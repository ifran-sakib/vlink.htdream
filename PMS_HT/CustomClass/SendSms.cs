﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace PMS_HT.CustomClass
{
    public class SendSms
    {
        public string from = "InfoSMS";
        public string to;
        public string text;

        public SendSms(string to, string message)
        {
            this.to = to;
            this.text = message;
            PhoneNumberFixer();
        }

        protected void PhoneNumberFixer()
        {
            string number = this.to;
            string newno = "+";
            if (number[0] != '+')
            {
                if (number.Length == 11)
                {
                    newno = newno + "88" + number;                    
                }
                else if (number.Length == 13)
                {
                    newno = newno + number;
                }
                if (newno.Length == 14)
                {
                    this.to = newno;
                    Task.Run(() => SMSSender());
                }
            }
            else
            {
                Task.Run(() => SMSSender());
            }
        }

        protected async Task SMSSender()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://107.20.199.106/");
            string jsonStringifiedMessage = new JavaScriptSerializer().Serialize(this);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "restapi/sms/1/text/single");
            request.Content = new StringContent(jsonStringifiedMessage, Encoding.UTF8, "application/json");
            request.Headers.Add("accept", "application/json");
            request.Headers.Add("authorization", "Basic di1saW5rbmV0d29yay5jb206UGFzc3dvcmQxIyM=");

            var result = await client.SendAsync(request);
        }
    }
}