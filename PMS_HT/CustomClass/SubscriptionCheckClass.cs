﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web.Mvc;

namespace PMS_HT.CustomClass
{
    public static class SubscriptionCheckClass
    {
        public static DateTime SubscriptionCheck(this ControllerBase controller)
        {
            SimpleSecurity ss = new SimpleSecurity();
            string subscriptionExpireDate = ss.Decrypt(ConfigurationManager.AppSettings["SubscriptionExpireDate"].ToString());
            DateTime Subscription_End = DateTime.ParseExact(subscriptionExpireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            int totalDays = (int)Math.Ceiling(((Subscription_End - DateTime.Now).TotalMinutes) / 1440);
            if (totalDays <= 5 && Subscription_End > DateTime.Now)
            {
                string str = "Subscription Ends In " + totalDays;
                if (totalDays == 1)
                {
                    str += " Day";
                }
                else
                {
                    str += " Days";
                }
                controller.ViewBag.Subscription = str;
            }
            return Subscription_End;
        }
    }
}