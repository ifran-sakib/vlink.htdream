﻿using System;

namespace PMS_HT.CustomClass
{
    public static class Rounder
    {
        public static decimal Round(decimal val)
        {
            val /= 10;
            decimal mul = Math.Ceiling(val) * 10;
            return mul;
        }

        public static decimal Round(double val)
        {
            val /= 10;
            decimal mul = Convert.ToDecimal(Math.Ceiling(val) * 10);
            return mul;
        }

        public static decimal Round(int a)
        {
            decimal val = a / 10;
            int lastDigit = a % 10;
            val = lastDigit == 0 ? val : (val + 1);
            decimal mul = val * 10;
            return mul;
        }

        public static decimal Round5(decimal val)
        {
            int a = Convert.ToInt32(Math.Floor(val));
            decimal mul = Convert.ToDecimal((a / 10) * 10);
            int lastDigit = a % 10;

            if (lastDigit >= 3 && lastDigit < 7)
            {
                mul += 5;
            }
            else if (lastDigit == 7 && IsFraction(val))
            {
                mul += 10;
            }
            else if (lastDigit == 7 && !IsFraction(val))
            {
                mul += 5;
            }
            else if (lastDigit > 7 && lastDigit <= 9)
            {
                mul += 10;
            }

            return mul;
        }

        public static decimal Round5(double val)
        {
            int a = Convert.ToInt32(Math.Floor(val));
            int lastDigit = a % 10;
            decimal mul = Convert.ToDecimal((a / 10) * 10);
            if (lastDigit >= 3 && lastDigit < 7)
            {
                mul += 5;
            }
            else if (lastDigit == 7 && IsFraction(val))
            {
                mul += 10;
            }
            else if (lastDigit == 7 && !IsFraction(val))
            {
                mul += 5;
            }
            else if (lastDigit > 7 && lastDigit <= 9)
            {
                mul += 10;
            }

            return mul;
        }

        public static decimal Round5(int a)
        {
            decimal mul = Convert.ToDecimal((a / 10) * 10);
            int lastDigit = a % 10;
            if (lastDigit >= 3 && lastDigit <= 7)
            {
                mul += 5;
            }
            else if (lastDigit > 7 && lastDigit <= 9)
            {
                mul += 10;
            }

            return mul;
        }



        public static bool IsFraction(decimal val)
        {
            return ((val * 2) % 2) == 0 ? false : true;
        }

        public static bool IsFraction(double val)
        {
            return ((val * 2) % 2) == 0 ? false : true;
        }

        public static bool IsFraction(int val)
        {
            return false;
        }
    }
}