﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Controllers;
using PMS_HT.Models;

namespace PMS_HT.Areas.Repair.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class StayInfoRepairController : CustomControllerBase
    {
        public static Dictionary<string, string> StayStatus = new Dictionary<string, string>()
                                                                    {
                                                                        {"CheckIn", "CheckIn" },
                                                                        {"Reserved", "Reserved" },
                                                                        {"Booked", "Booked" },
                                                                        {"Clear", "Clear" }
                                                                    };

        

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);
            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }
            return View(tbl_guest_stay_info);
        }        

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);
            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }

            ViewBag.Status = new SelectList(StayStatus, "Key", "Value", tbl_guest_stay_info.Status);
            return PartialView(tbl_guest_stay_info);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_guest_stay_info tbl_guest_stay_info)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_guest_stay_info).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details",new { id = tbl_guest_stay_info.Stay_Info_ID});
            }

            ViewBag.Status = new SelectList(StayStatus, "Key", "Value", tbl_guest_stay_info.Status);
            return PartialView(tbl_guest_stay_info);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
