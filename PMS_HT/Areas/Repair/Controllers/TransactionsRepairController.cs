﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PMS_HT.Controllers;
using PMS_HT.Models;

namespace PMS_HT.Areas.Repair.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class TransactionsRepairController : CustomControllerBase
    {
        public static Dictionary<int, string> TranRefName = new Dictionary<int, string>()
                                                                    {
                                                                        {1, "Discount" },
                                                                        {2, "Vat" },
                                                                        {3, "paid" },
                                                                        {4, "Regular Charge" },
                                                                        {5, "otherBill" },
                                                                        {6, "extraCharge" },
                                                                        {7, "refund" },
                                                                        {8, "badDebt" },
                                                                        {9, "surPlus" },
                                                                        {11, "OutStanding" },
                                                                        {12, "OutStanding Paid" },
                                                                        {13, "cancellation_refund" },
                                                                        {14, "booking_paid" },
                                                                        {15, "Half Vat" },
                                                                        {16, "Half Regular Charge" },
                                                                        {17, "SC" },
                                                                        {18, "Half SC" }
                                                                    };
        private string TranRefMapper(int? id)
        {
            var tranRefName = TranRefName.FirstOrDefault(a => a.Key == id).Value;
            return tranRefName;
        }
        public ActionResult AllTransactions(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbl_room_transactions = db.tbl_room_transactions
                .Include(t => t.tbl_emp_info)
                .Include(t => t.tbl_guest_info)
                .Include(t => t.tbl_guest_stay_info)
                .Include(t => t.tbl_laundry_item)
                .Include(t => t.tbl_laundry_service)
                .Include(t => t.tbl_return_germent_in)
                .Include(t => t.tbl_room_info)
                .Where(a => a.Stay_Info_ID == id)
                .OrderBy(b => b.Tran_Date);
            ViewBag.Id = id;
            return View(tbl_room_transactions.ToList());
        }

        public ActionResult Create(int id)
        {
            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(id);
            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }

            ViewBag.Guest_ID = tbl_guest_stay_info.Guest_ID;
            ViewBag.Stay_Info_ID = tbl_guest_stay_info.Stay_Info_ID;

            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_guest_stay_info.Room_ID);
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name");
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name");
            ViewBag.Garment_In_ID = new SelectList(db.tbl_return_germent_in, "Garment_In_ID", "Return_In_Name");
            ViewBag.Tran_Ref_Name = new SelectList(TranRefName, "Key", "Value");

            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_room_transactions tbl_room_transactions)
        {
            if (ModelState.IsValid)
            {
                string tranRefName = TranRefMapper(tbl_room_transactions.Tran_Ref_ID);
                tbl_room_transactions.Tran_Ref_Name = tranRefName;
                db.tbl_room_transactions.Add(tbl_room_transactions);
                db.SaveChanges();
                return RedirectToAction("AllTransactions", new { id = tbl_room_transactions.Stay_Info_ID });
            }

            tbl_guest_stay_info tbl_guest_stay_info = db.tbl_guest_stay_info.Find(tbl_room_transactions.Stay_Info_ID);
            if (tbl_guest_stay_info == null)
            {
                return HttpNotFound();
            }

            ViewBag.Guest_ID = tbl_guest_stay_info.Guest_ID;
            ViewBag.Stay_Info_ID = tbl_guest_stay_info.Stay_Info_ID;

            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_guest_stay_info.Room_ID);
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name");
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name");
            ViewBag.Garment_In_ID = new SelectList(db.tbl_return_germent_in, "Garment_In_ID", "Return_In_Name");
            ViewBag.Tran_Ref_Name = new SelectList(TranRefName, "Key", "Value");

            return PartialView(tbl_room_transactions);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_transactions tbl_room_transactions = db.tbl_room_transactions.Find(id);
            if (tbl_room_transactions == null)
            {
                return HttpNotFound();
            }

            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_room_transactions.Room_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_room_transactions.Emp_ID);
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", tbl_room_transactions.Laundry_Item_ID);
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", tbl_room_transactions.Laundry_Service_ID);
            ViewBag.Garment_In_ID = new SelectList(db.tbl_return_germent_in, "Garment_In_ID", "Return_In_Name", tbl_room_transactions.Garment_In_ID);
            ViewBag.Tran_Ref_Name = new SelectList(TranRefName, "Key", "Value", tbl_room_transactions.Tran_Ref_ID);

            return PartialView(tbl_room_transactions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_room_transactions tbl_room_transactions)
        {
            if (ModelState.IsValid)
            {
                string tranRefName = TranRefMapper(tbl_room_transactions.Tran_Ref_ID);
                tbl_room_transactions.Tran_Ref_Name = tranRefName;
                db.Entry(tbl_room_transactions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AllTransactions", new { id = tbl_room_transactions.Stay_Info_ID });
            }

            ViewBag.Room_ID = new SelectList(db.tbl_room_info, "Room_ID", "Room_No", tbl_room_transactions.Room_ID);
            ViewBag.Emp_ID = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_room_transactions.Emp_ID);
            ViewBag.Laundry_Item_ID = new SelectList(db.tbl_laundry_item, "Laundry_Item_ID", "Laundry_Item_Name", tbl_room_transactions.Laundry_Item_ID);
            ViewBag.Laundry_Service_ID = new SelectList(db.tbl_laundry_service, "Laundry_Service_ID", "Service_Heading_Name", tbl_room_transactions.Laundry_Service_ID);
            ViewBag.Garment_In_ID = new SelectList(db.tbl_return_germent_in, "Garment_In_ID", "Return_In_Name", tbl_room_transactions.Garment_In_ID);
            ViewBag.Tran_Ref_Name = new SelectList(TranRefName, "Key", "Value", tbl_room_transactions.Tran_Ref_ID);

            return PartialView(tbl_room_transactions);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_room_transactions tbl_room_transactions = db.tbl_room_transactions.Find(id);
            if (tbl_room_transactions == null)
            {
                return HttpNotFound();
            }
            return PartialView(tbl_room_transactions);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_room_transactions tbl_room_transactions = db.tbl_room_transactions.Find(id);
            int stayInfoId = (int)tbl_room_transactions.Stay_Info_ID;
            db.tbl_room_transactions.Remove(tbl_room_transactions);
            db.SaveChanges();
            return RedirectToAction("AllTransactions", new { id = stayInfoId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
