﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PMS_HT.Areas.Services.Models;
using PMS_HT.Models;

namespace PMS_HT.Areas.Services.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class ServiceController : Controller
    {
        private PMS_HT_DB db = new PMS_HT_DB();

        // GET: Services/Service
        public ActionResult Index()
        {
            var tbl_service = db.tbl_service.Include(t => t.tbl_emp_info1).Include(t => t.tbl_emp_info2);
            return View(tbl_service.ToList());
        }

        // GET: Services/Service/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_service tbl_service = db.tbl_service.Find(id);
            if (tbl_service == null)
            {
                return HttpNotFound();
            }
            return View(tbl_service);
        }

        // GET: Services/Service/Create
        public ActionResult Create()
        {
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name");
            return View();
        }

        // POST: Services/Service/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Service_ID,Name,VAT,SC,CC,Created_By,Updated_By,IsBuiltIn,IsAvailable")] tbl_service tbl_service)
        {
            if (ModelState.IsValid)
            {
                db.tbl_service.Add(tbl_service);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Created_By);
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Updated_By);
            return View(tbl_service);
        }

        // GET: Services/Service/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_service tbl_service = db.tbl_service.Find(id);
            if (tbl_service == null)
            {
                return HttpNotFound();
            }
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Created_By);
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Updated_By);
            return View(tbl_service);
        }

        // POST: Services/Service/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Service_ID,Name,VAT,SC,CC,Created_By,Updated_By,IsBuiltIn,IsAvailable")] tbl_service tbl_service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_service).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Created_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Created_By);
            ViewBag.Updated_By = new SelectList(db.tbl_emp_info, "Emp_ID", "Emp_Name", tbl_service.Updated_By);
            return View(tbl_service);
        }

        // GET: Services/Service/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_service tbl_service = db.tbl_service.Find(id);
            if (tbl_service == null)
            {
                return HttpNotFound();
            }
            return View(tbl_service);
        }

        // POST: Services/Service/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_service tbl_service = db.tbl_service.Find(id);
            db.tbl_service.Remove(tbl_service);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult ServiceChargeFix()
        {
            tbl_service roomData = db.tbl_service.Find(1);
            decimal scPercent = Convert.ToDecimal(roomData.SC);
            decimal sc = 0;

            //Remove previous service charge rows for checkedin or booked guest begin
            var allScRows = db.tbl_room_transactions
                .Where(a => (a.tbl_guest_stay_info.Status == "CheckIn" || a.tbl_guest_stay_info.Status == "Booked") && (a.Tran_Ref_ID == 17 || a.Tran_Ref_ID == 18))
                .ToList();
            db.tbl_room_transactions.RemoveRange(allScRows);
            db.SaveChanges();
            //Remove previous service charge rows for checkedin or booked guest end

            if (scPercent > 0)
            {
                var checkedInChargeRows = db.tbl_room_transactions
                    .Where(a => (a.tbl_guest_stay_info.Status == "CheckIn" || a.tbl_guest_stay_info.Status == "Booked") && (a.Tran_Ref_ID == 4 || a.Tran_Ref_ID == 16))
                    .ToList();
                tbl_room_transactions[] newScRow = new tbl_room_transactions[checkedInChargeRows.Count];
                int i = 0;
                foreach (var item in checkedInChargeRows)
                {
                    sc = Convert.ToDecimal(item.Debit) * scPercent / 100;
                    newScRow[i] = new tbl_room_transactions();
                    newScRow[i].Guest_ID = item.Guest_ID;
                    newScRow[i].Room_ID = item.Room_ID;
                    newScRow[i].Stay_Info_ID = item.Stay_Info_ID;
                    newScRow[i].Debit = sc;
                    newScRow[i].Credit = 0;
                    newScRow[i].Tran_Date = item.Tran_Date;
                    newScRow[i].Particulars = item.Particulars;
                    newScRow[i].Emp_ID = item.Emp_ID;
                    if (item.Tran_Ref_ID == 4)
                    {
                        newScRow[i].Tran_Ref_ID = 17;
                        newScRow[i].Tran_Ref_Name = "SC";
                    }
                    else
                    {
                        newScRow[i].Tran_Ref_ID = 18;
                        newScRow[i].Tran_Ref_Name = "Half SC";
                    }
                    i++;
                }
                db.tbl_room_transactions.AddRange(newScRow);
                db.SaveChanges();
                return Content("Changed");
            }
            return Content("Not Changed");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
