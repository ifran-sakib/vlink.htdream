﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service_company_info
    {
        [Key]
        public int Service_Company_ID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(60)]
        public string Email { get; set; }


        public ICollection<tbl_service> tbl_services { get; set; }


        public tbl_service_company_info()
        {
            tbl_services = new HashSet<tbl_service>();
        }
    }
}