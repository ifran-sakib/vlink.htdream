﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service_purchase_detail
    {
        [Key]
        public int Service_Details_ID { get; set; }

        public int? Service_Item_ID { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Price { get; set; }
        
        public decimal? VAT { get; set; }

        public decimal? SC { get; set; }

        public decimal? Total { get; set; }

        public int? Service_Master_ID { get; set; }

        [StringLength(400)]
        public string Note { get; set; }


        [ForeignKey("Service_Item_ID")]
        public virtual tbl_service_item tbl_service_item { get; set; }

        [ForeignKey("Service_Master_ID")]
        public virtual tbl_service_purchase_master tbl_service_purchase_master { get; set; }


    }
}