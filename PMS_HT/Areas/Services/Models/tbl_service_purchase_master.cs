﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service_purchase_master
    {
        [Key]
        public int Service_Master_ID { get; set; }

        [Display(Name = "Purchase Date")]
        public DateTime? Purchase_Date { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime? Booking_Date { get; set; }

        public int? Payment_Method_ID { get; set; }

        public int? Service_ID { get; set; }

        public int? Stay_Info_ID { get; set; }

        [StringLength(400)]
        public string Note { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public int? Served_By { get; set; }

        public int? Service_Guest_ID { get; set; }

        public int? Service_Company_ID { get; set; }


        [ForeignKey("Payment_Method_ID")]
        public virtual tbl_payment_method tbl_payment_method { get; set; }

        [ForeignKey("Service_ID")]
        public virtual tbl_service tbl_service { get; set; }

        [ForeignKey("Stay_Info_ID")]
        public virtual tbl_guest_stay_info tbl_guest_stay_info { get; set; }

        [ForeignKey("Served_By")]
        public virtual tbl_emp_info tbl_emp_info { get; set; }

        [ForeignKey("Service_Guest_ID")]
        public virtual tbl_service_guest_info tbl_service_guest_info { get; set; }

        [ForeignKey("Service_Company_ID")]
        public virtual tbl_service_company_info tbl_service_company_info { get; set; }

        public virtual ICollection<tbl_service_purchase_detail> tbl_service_purchase_details { get; set; }

        public virtual ICollection<tbl_service_purchase_ledger> tbl_service_purchase_ledgers { get; set; }



        public tbl_service_purchase_master()
        {
            tbl_service_purchase_details = new HashSet<tbl_service_purchase_detail>();
            tbl_service_purchase_ledgers = new HashSet<tbl_service_purchase_ledger>();
        }
    }
}