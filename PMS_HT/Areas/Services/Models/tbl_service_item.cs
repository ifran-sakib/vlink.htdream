﻿using PMS_HT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service_item
    {
        [Key]
        public int Service_Item_ID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Image { get; set; }

        public decimal Price { get; set; }
        
        public int Service_ID { get; set; }
        
        public int? Created_By { get; set; }

        public int? Updated_By { get; set; }

        public bool? IsBuiltIn { get; set; }

        public bool IsAvailable { get; set; }



        [ForeignKey("Service_ID")]
        public virtual tbl_service tbl_service { get; set; }

        [ForeignKey("Created_By")]
        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

        [ForeignKey("Updated_By")]
        public virtual tbl_emp_info tbl_emp_info2 { get; set; }
                
    }
}