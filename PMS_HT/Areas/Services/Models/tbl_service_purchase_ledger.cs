﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service_purchase_ledger
    {
        [Key]
        public int Service_Ledger_ID { get; set; }

        public int? Service_Master_ID { get; set; }

        public int Tran_Ref_ID { get; set; }

        [StringLength(50)]
        public string Tran_Ref_Name { get; set; }

        [StringLength(150)]
        public string Particular { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        [Display(Name ="Date")]
        public DateTime? Tran_Date { get; set; }

        [StringLength(400)]
        public string Note { get; set; }


        [ForeignKey("Service_Master_ID")]
        public virtual tbl_service_purchase_master tbl_service_purchase_master { get; set; }

    }
}