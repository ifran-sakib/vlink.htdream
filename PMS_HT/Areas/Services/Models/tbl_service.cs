﻿using PMS_HT.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS_HT.Areas.Services.Models
{
    public class tbl_service
    {
        [Key]
        public int Service_ID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public decimal? VAT { get; set; }

        public decimal? SC { get; set; }

        public decimal? CC { get; set; }

        public int? Created_By { get; set; }

        public int? Updated_By { get; set; }

        public bool? IsBuiltIn { get; set; }

        public bool IsAvailable { get; set; }

        [ForeignKey("Created_By")]
        public virtual tbl_emp_info tbl_emp_info1 { get; set; }

        [ForeignKey("Updated_By")]
        public virtual tbl_emp_info tbl_emp_info2 { get; set; }

        public ICollection<tbl_service_item> tbl_service_items { get; set; }

        public ICollection<tbl_service_purchase_master> tbl_service_purchase_masters { get; set; }

        public ICollection<tbl_service_guest_info> tbl_service_guest_infos { get; set; }

        public ICollection<tbl_service_company_info> tbl_service_company_infos { get; set; }



        public tbl_service()
        {
            tbl_service_items = new HashSet<tbl_service_item>();
            tbl_service_purchase_masters = new HashSet<tbl_service_purchase_master>();
            tbl_service_guest_infos = new HashSet<tbl_service_guest_info>();
            tbl_service_company_infos = new HashSet<tbl_service_company_info>();
        }
    }
}